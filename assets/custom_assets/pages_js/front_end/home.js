var AnswerStatus = 1;
var QuestionStatus = 1;
var SameAnswerStatus = 1;
var AnswerArray = [];
var current_GroupMembersList = [];
var praise_AddedMembersList = [];
var SubmitUpdate = 1;
var SubmitPoll = 1;
var SubmitPraise = 1;
var SubmitAnnounce = 1;
var membersForReplyCC = [];
var TimerLimit = 60;
var group = "main_home";

$(document).ready(function () {
    function CheckIfAnswersEmpty() {
        AnswerStatus = 1;
        $("body .poll_answer").each(function () {
            if ($(this).val() == "") {
                AnswerStatus = 0;
                return;
            }
        });
    }

    $('body').on("click", ".expand_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.truncated_body').css("display", "none");
        temp_this.parent('.truncated_body').siblings('.complete_body').css("display", "block");
    });
    $('body').on("click", ".collapse_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.complete_body').css("display", "none");
        temp_this.parent('.complete_body').siblings('.truncated_body').css("display", "block");
    });

//  Clear all cc users when tab is changed
    $('body').on('click', '.post_type', function (e) {
        $('.AddedMembers .added_user').remove();
        current_GroupMembersList = [];
    });
    $('body').on('click', '.share_box', function (e) {
        $('.share_box').hide();
        $('.show_share').show();
    });

// Autoincease the height of textarea
    $("body .share_text").keyup(function (e) {
        $(this).height(20);
        $(this).height(this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth")));
    });

    /******************************* Code for getting list of members in cc *******************************/
    /*Gives the list of all memebers of scholarspace*/
    $('body').on('focus', '.addMembersHere_home', function () {
        var category = $(this).parents('.category').attr('id');
        var ListItem = "";
        if ($(this).val() == "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    group: group,
                    current_GroupMembersList: current_GroupMembersList
                },
                success: function (data) {
//                    console.log(data);
                    $('#' + category + ' .people_list .ListOfMembers_home').html("");
                    $.each(data, function (key, value) {
                        if (current_GroupMembersList.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + category + ' .notify_pop').show();
                    $('#' + category + ' .people_list .ListOfMembers_home').append(ListItem);
                }
            });
        }
    });
    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '.home .addMembersHere_home', function () {
        var category = $(this).parents('.category').attr('id');
        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    group: group,
                    Item: Item
                },
                success: function (data) {
                    $('#' + category + ' .people_list .ListOfMembers_home').html("");
                    $.each(data, function (key, value) {
                        if (current_GroupMembersList.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('.home #' + category + ' .notify_pop').show();
                    $('.home #' + category + ' .people_list .ListOfMembers_home').append(ListItem);
                }
            });
        }
    });

    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '.ListOfMembers_home li', function () {
        var category = $(this).parents('.category').attr('id');
        var name = $(this).children('h5').html();
        var college_users_intranet_id = $(this).attr('intranet_id');
        var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company added_user'>\
                            <h6> " + name + " </h6>\
                            <span class='pop_close'></span>\
                        </div>";
        $('.home #' + category + ' .AddedMembers').append(AddedItem);
        $('.home #' + category + ' .addMembersHere_home').val('');
        $('.home #' + category + ' .notify_pop').hide();
        current_GroupMembersList.push($(this).attr('intranet_id'));
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '.pop_close', function () {
        var intranet_id = $(this).parents('div.company').attr('intranet_id');
        var index = current_GroupMembersList.indexOf(intranet_id);
        if (index > -1) {
            current_GroupMembersList.splice(index, 1);
        }
        $(this).parents('div.company').remove();
    });
    $('body').click(function () {
        var category = $(this).parents('.category').attr('id');
        $('#' + category + ' .notify_pop').hide();
    });


    /******************************************UPDATE***********************************************/
    $('body').on('submit', '#update_form', function (e) {
        e.preventDefault();
        if (SubmitUpdate == 1) {
            var update_content = $('#update_content').val();
            var images_array = [];
            var addAllGroup;
            $('#update_form input[name=uploaded_file\\[\\]]').each(function () {
                if ($(this).val() != "") {
                    images_array.push($(this).val());
                }
            });
            if ($(".addAllGroup").length == 0) {
                addAllGroup = 0;
            } else {
                addAllGroup = 1;
            }

            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/PostUpdate_Method",
                data: {
                    group: group,
                    update_content: update_content,
                    current_GroupMembersList: current_GroupMembersList,
                    addAllGroup: addAllGroup,
                    images_array: images_array
                },
                success: function (data) {
                    if (data != '0') {
                        $('#all_post_content').prepend(data);
                    }
                    $('#update_content').val('');
                    $('.AddedMembers .added_user').remove();
                    $('#update_form .file_upload_content_ul').html('');
                    $('#update_form')[0].reset();
                    current_GroupMembersList = [];
                }
            });
        }
    });
    $('#update').on('keyup', '#update_content', function () {
        if ($('#update_content').val() == "") {
            SubmitUpdate = 0;
            $('.post_update').prop('disabled', true);
        } else {
            SubmitUpdate = 1;
            $('.post_update').prop('disabled', false);
        }
    });

    /******************************************POLL***********************************************/
    $('#poll').on('keyup', '.poll_question', function () {
        if ($('.poll_question').val() == "") {
            QuestionStatus = 0;
        } else {
            QuestionStatus = 1;
        }
        CheckIfAnswersEmpty();
        if (QuestionStatus == 1 && AnswerStatus == 1)
            $('.post_poll').removeAttr('disabled');
        else {
            $('.post_poll').prop('disabled', true);
        }
    });
    $('#poll').on('keyup', '.poll_answer', function () {
        if ($('.poll_question').val() == "") {
            QuestionStatus = 0;
        } else {
            QuestionStatus = 1;
        }
        CheckIfAnswersEmpty();
        if (QuestionStatus == 1 && AnswerStatus == 1) {
            $('.post_poll').prop('disabled', false);
        } else {
            $('.post_poll').prop('disabled', true);
        }
    });
    $('#poll').on('click', '.add_more_answer', function () {
        CheckIfAnswersEmpty();
        var CurrentAlpha = $('.box_a:last-child .answer_box').html();
        var NextAlpha = CurrentAlpha.substring(0, CurrentAlpha.length - 1) + String.fromCharCode(CurrentAlpha.charCodeAt(CurrentAlpha.length - 1) + 1);
        if (AnswerStatus == 1) {
            var append_option = "<div class='box_a'>\
                                <span class ='answer_box'>" + NextAlpha + "</span>\
                                <input type ='text' placeholder='Answer' class ='poll_text poll_answer' name ='answer'>\
                                <span class='delete_answer'><i class='fa fa-minus'></i></span>\
                                </div>";
            $('.poll_post_answer_div').append(append_option);
        }
    });
    $('#poll').on('click', '.delete_answer', function () {
        $(this).parents('.box_a').remove();
        CheckIfAnswersEmpty();
        if (AnswerStatus == 1 && QuestionStatus == 1) {
            $('.post_poll').prop('disabled', false);
        }
    });
    $('body').on('click', '.add_company', function () {
        $(this).children('input.poll_text').focus();
    });

    $('body').on('submit', '#poll_form', function (e) {
        e.preventDefault();
        if ($('#poll_question').val() == "") {
            $('.post_poll').prop('disabled', false);
            SubmitPoll = 0;
        }

        $(".poll_answer").each(function () {
            if ($(this).val() == "") {
                SubmitPoll = 0;
                return false;
            }
        });
        if (SubmitPoll == 1) {
            var question = $('#poll_question').val();
            var answer_array = [];
            var images_array = [];
            var addAllGroup;
            $(".poll_answer").each(function () {
                if ($(this).val() != "") {
                    answer_array.push($(this).val());
                }
            });
            $('#poll_form input[name=uploaded_file\\[\\]]').each(function () {
                if ($(this).val() != "") {
                    images_array.push($(this).val());
                }
            });
            if ($(".addAllGroup").length == 0) {
                addAllGroup = 0;
            } else {
                addAllGroup = 1;
            }

            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/PostPoll_Method",
                data: {
                    group: group,
                    question: question,
                    answer_array: answer_array,
                    current_GroupMembersList: current_GroupMembersList,
                    addAllGroup: addAllGroup,
                    images_array: images_array
                },
                success: function (data) {
//                    console.log(data);
                    if (data != '0') {
                        $('#all_post_content').prepend(data);
                    }
                    $('#poll_question').val('');
                    $(".poll_answer").each(function () {
                        $(this).val('');
                    });
                    $('.AddedMembers .added_user').remove();
                    $('#poll_form .file_upload_content_ul').html('');
                    current_GroupMembersList = [];
                }
            });
        }
    });

    /*********************************************************PRAISE****************************************************/
    $('body').on('click', '.praise_prev', function () {
        var ImageID = $('.current_praise_badge').attr('badge_id');
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/intranet_posts/GetPrevNextBadge_Method",
            data: {
                Trigger: "prev",
                ImageID: ImageID
            },
            success: function (data) {
                if (data[0] != 'null') {
                    $('.current_praise_badge').attr('badge_id', data[0]);
                    $('.current_praise_badge').attr('src', base_url + "" + data[1]);
                }
            }
        });
    });
    $('body').on('click', '.praise_next', function () {
        var ImageID = $('.current_praise_badge').attr('badge_id');
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/intranet_posts/GetPrevNextBadge_Method",
            data: {
                Trigger: "next",
                ImageID: ImageID
            },
            success: function (data) {
                if (data[0] != 'null') {
                    $('.current_praise_badge').attr('badge_id', data[0]);
                    $('.current_praise_badge').attr('src', base_url + "" + data[1]);
                }
            }
        });
    });

    /*Gives the list of all memebers of scholarspace*/
    $('body').on('focus', '.PraiseMembers', function () {
        var category = $(this).parents('.category').attr('id');
        var ListItem = "";
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
            data: {
                group: group
            },
            success: function (data) {
//                    console.log(data);
                $('#' + category + ' .Praising_div .praise_people_list .Praise_ListOfMembers').html("");
                $.each(data, function (key, value) {
                    if (praise_AddedMembersList.indexOf(value.college_users_intranet_id) == -1) {
                        ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                    }
                });
                $('#' + category + ' .Praising_div .praise_notify_pop').show();
                $('#' + category + ' .Praising_div .praise_people_list .Praise_ListOfMembers').append(ListItem);
            }
        });
    });

    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '.PraiseMembers', function () {
        var category = $(this).parents('.category').attr('id');
        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    Item: Item
                },
                success: function (data) {
                    $('#' + category + ' .praise_people_list .Praise_ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (praise_AddedMembersList.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + category + ' .Praising_div .praise_notify_pop').show();
                    $('#' + category + ' .Praising_div .praise_people_list .Praise_ListOfMembers').append(ListItem);
                }
            });
        }
    });
    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '.Praise_ListOfMembers li', function () {
        var category = $(this).parents('.category').attr('id');
        var name = $(this).children('h5').html();
        var college_users_intranet_id = $(this).attr('intranet_id');
        var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company added_user'>\
                            <h6> " + name + " </h6>\
                            <span class='pop_close'></span>\
                        </div>";
        $('.Praise_AddedMembers').append(AddedItem);
        $('.PraiseMembers').val('');
        $('.praise_notify_pop').hide();
        praise_AddedMembersList.push($(this).attr('intranet_id'));
    });
    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '.pop_close', function () {
        var intranet_id = $(this).parents('div.company').attr('intranet_id');
        var index = praise_AddedMembersList.indexOf(intranet_id);
        if (index > -1) {
            praise_AddedMembersList.splice(index, 1);
        }
        $(this).parents('div.company').remove();
    });
    $('body').click(function () {
        var category = $(this).parents('.category').attr('id');
        $('#' + category + ' .Praising_div .praise_notify_pop').hide();
    });
    $('body').on('keyup', '#praise_content', function (e) {
        if ($('#praise_content').val() == "") {
            $('.post_praise').prop('disabled', true);
            SubmitPraise = 0;
        } else {
            if (praise_AddedMembersList.length <= 0) {
                $('.post_praise').prop('disabled', true);
                SubmitPraise = 0;
            } else {
                $('.post_praise').prop('disabled', false);
                SubmitPraise = 1;
            }
        }
    });
    /* When you press post button for praise */
    $('body').on('submit', '#praise_form', function (e) {

        e.preventDefault();
        if ($('#praise_content').val() == "") {
            $('.post_praise').prop('disabled', false);
            SubmitPraise = 0;
        }

        if (praise_AddedMembersList.length <= 0) {
            $('.post_praise').prop('disabled', false);
            SubmitPraise = 0;
        }

        if (SubmitPraise == 1) {
            var addAllGroup;
            var images_array = [];
            if ($("#praise_form .addAllGroup").length == 0) {
                addAllGroup = 0;
            } else {
                addAllGroup = 1;
            }

            $('#praise_form input[name=uploaded_file\\[\\]]').each(function () {
                if ($(this).val() != "") {
                    images_array.push($(this).val());
                }
            });
            var PostPraiseContent = $('#praise_content').val();
            var ImageID = $('#praise_form .current_praise_badge').attr('badge_id');
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/PostPraise_Method",
                data: {
                    group: group,
                    PostPraiseContent: PostPraiseContent,
                    praise_AddedMembersList: praise_AddedMembersList,
                    current_GroupMembersList: current_GroupMembersList,
                    images_array: images_array,
                    addAllGroup: addAllGroup,
                    ImageID: ImageID
                },
                success: function (data) {
//                    console.log(data);
                    if (data != '0') {
                        $('#all_post_content').prepend(data);
                    }
                    $('#praise_content').val('');
                    $('#PraiseMembers').val('');
                    $('#praise_form .AddedMembers .added_user').remove();
                    $('#praise_form .Praising_div .added_user').remove();
                    $('#praise_form .file_upload_content_ul').html('');
                    current_GroupMembersList = [];
                    praise_AddedMembersList = [];
                }
            });
        }
    });

    /*********************************************ANNOUNCE********************************************************/
    tinymce.init({
        selector: '.announce_textpad',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                var tinymce_announce_content = tinyMCE.get('announce_textpad').getContent();
                if (tinymce_announce_content == "") {
                    $('.post_announce').prop('disabled', true);
                    SubmitAnnounce = 0;
                } else {
                    if ($('.announce_heading').val() == "") {
                        $('.post_announce').prop('disabled', true);
                        SubmitAnnounce = 0;
                    } else {
                        $('.post_announce').prop('disabled', false);
                        SubmitAnnounce = 1;
                    }
                }
            });
        }
    });

    var announce_content;
    $('body').on('keyup', '.announce_heading', function () {
        announce_content = tinyMCE.get('announce_textpad').getContent();
        if ($('.announce_heading').val() == "") {
            $('.post_announce').prop('disabled', true);
            SubmitAnnounce = 0;
        } else {
            $('.post_announce').prop('disabled', false);
            SubmitAnnounce = 1;
        }

        if (announce_content == "") {
            $('.post_announce').prop('disabled', true);
            SubmitAnnounce = 0;
        }
    });

    $('body').on('submit', '#announce_form', function (e) {
        e.preventDefault();
        var addAllGroup;
        if ($("#announce_form .addAllGroup").length == 0) {
            addAllGroup = 0;
        } else {
            addAllGroup = 1;
        }

        if ($('#announce_form .announce_heading').val() == "") {
            $('.post_announce').prop('disabled', false);
            SubmitAnnounce = 0;
        }

        if (announce_content == "") {
            $('.post_announce').prop('disabled', false);
            SubmitAnnounce = 0;
        }

        if (SubmitAnnounce == 1) {
            var images_array = [];
            var announce_heading = $('.announce_heading').val();
            var announce_content = tinyMCE.get('announce_textpad').getContent();
            $('#announce_form input[name=uploaded_file\\[\\]]').each(function () {
                if ($(this).val() != "") {
                    images_array.push($(this).val());
                }
            });
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/PostAnnounce_Method",
                data: {
                    group: group,
                    current_GroupMembersList: current_GroupMembersList,
                    announce_heading: announce_heading,
                    announce_content: announce_content,
                    images_array: images_array,
                    addAllGroup: addAllGroup,
                },
                success: function (data) {
//                    console.log(data);
                    if (data != '0') {
                        $('#all_post_content').prepend(data);
                    }
                    $('#announce_form .announce_heading').val('');
                    tinyMCE.get('announce_textpad').setContent('');
                    $('#announce_form .AddedMembers .added_user').remove();
                    $('#announce_form .file_upload_content_ul').html('');
                    current_GroupMembersList = [];
                }
            });
        }
    });


    /*************************************** file upload code start here ********************************************/
    var category;
    $('.pin2').click(function () {
        var temp_this = $(this);
        if (temp_this.hasClass('showing')) {
            temp_this.removeClass("showing");
            $('.attach_list').hide();
            $('.pin2').css('box-shadow', 'none');
        } else {
            temp_this.addClass("showing");
            $('.attach_list').show();
            $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
        }
    });
    $("body").on("click", ".upload_file_from_computer", function () {
        category = $(this).parents('.category').attr('id');
        $('#' + category + ' .upload_post_file').trigger('click');
    });
    $("body").on("change", ".upload_post_file", function () {
        category = $(this).parents('.category').attr('id');
        $('#' + category + ' .pin2').css('box-shadow', 'none');
        $("#" + category + " .add_files").css("display", "block");
        $("#" + category + " .pin2").removeClass("showing");
        $("#" + category + " .image_upload_form").trigger("submit");
    });
    $(".image_upload_form").submit(function (e) {
        e.preventDefault();
        category = $(this).parents('.category').attr('id');
        $("#" + category + " .upload_processing_li").css("display", "block");
        process_li_append();
        var data = new FormData($("#" + category + " .image_upload_form")[0]);
        var file_name = ($("#" + category + " .upload_post_file").val()).split('\\');
        var file_extention = file_name[2].split('.');
        var file_extention = file_extention[1];
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/image_upload_form",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.trim() != 0) {
                    var temp_html = '<li class="uploaded_file_li_post">\
                                        <div class="company image_upload_item" id="remove_com">\
                                            <div class="row">\
                                                <div class="col-md-2">\
                                                    <div class="top_user_img">';
                    if (file_extention == 'jpg') {
                        temp_html += '<img src="' + base_url + data + '">';
                    } else {
                        temp_html += '<img src="' + base_url + 'assets_front/image/other_file.png">';
                    }
                    temp_html += '<input type="hidden" name="uploaded_file[]" value="' + data + '"/>\
                                                    </div>\
                                                </div>\
                                                <div class="col-md-8">\
                                                    <div class="pull-left uploaded_img_message_div front-end-font">\
                                                        <a>' + file_name[2] + '</a>\
                                                        <h5 class="file_msg">File attached successfully.</h5>\
                                                    </div>\
                                                </div>\
                                                <div class="col-md-2">\
                                                    <span file_path="' + data + '" class="file_upload_cancel pop_close pull-right"></span>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </li>';
                    process_li_remove();
                    $("#" + category + " .file_upload_content_ul").append(temp_html);
                    $("#" + category + " .image_upload_form")[0].reset();
                }
            }
        });
    });

    $('body').on("click", ".file_upload_cancel", function () {
        category = $(this).parents('.category').attr('id');
        var temp_this = $(this);
        var file_path = temp_this.attr("file_path");
        if (file_path != '') {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/remove_file_upload",
                data: {
                    file_path: file_path
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        var count = 0;
                        temp_this.parents().eq(3).remove();
                        $("#" + category + " .uploaded_file_li_post").each(function () {
                            count++;
                        });
                        if (count == 0) {
                            $("#" + category + " .add_files").css("display", "none");
                        }
                    }
                }
            });
        }
    });

    //append progres bar list
    function process_li_append() {
        var temp_proccess_li = '<li class="temp_upload_processing_li">\
                                    <div class="company image_upload_item" id="remove_com">\
                                        <div class="row">\
                                            <div class="col-md-2">\
                                                <div class="top_user_img">\
                                                    <img src="' + base_url + 'assets_front/image/upload_file1.png">\
                                                </div>\
                                            </div>\
                                            <div class="col-md-10">\
                                                <div style="width: 100%" class="pull-left uploaded_img_message_div front-end-font">\
                                                    <h5>\
                                                        <div class="progress progress-sm active" style="margin-top: 13px; width: 100%; height: 15px !important;">\
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%;border-radius: 5px;">\
                                                                <span class="sr-only">20% Complete</span>\
                                                            </div>\
                                                        </div>\
                                                    </h5>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </li>';
        $("#" + category + " .file_upload_content_ul").append(temp_proccess_li);
    }

    //remove progres bar list
    function process_li_remove() {
        $("#" + category + " .temp_upload_processing_li").remove();
    }
    /*************************************** file upload code Ends here ********************************************/

    /* Group function */
    $("body").on("click", ".comment_in1", function () {
        $(this).siblings(".more_share").css('display', 'block');
        $(this).siblings(".more_share").children(".reply_form").children("div").children(".reply_textarea").focus();
        $(this).css('display', 'none');
    });
    $("body").on("click", ".click_more", function () {
        var temp_this = $(this);
        if (temp_this.hasClass('active_more')) {
            $(this).siblings('.more_list').hide();
            $(this).removeClass("active_more");
        } else {
            $('.more_list').hide();
            $(this).siblings('.more_list').show();
            $(".click_more").removeClass("active_more");
            temp_this.addClass("active_more");
        }
    });

    //when clicked on like for a post
    $("body").on("click", ".post_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/like_post",
            data: {
                post_id: post_id,
                group: group,
                like_action: like_action
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_like_body-" + post_id).children('.current_user_like').text("You ");
                            $("#post_like_div-" + post_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_like_div-" + post_id).css("display", "none");
                            $("#post_like_body-" + post_id).children('.current_user_like').text("");
                        }
                    }
                }
            }
        });
    });

    var submitReply = 1;
    $("body").on("keyup", ".reply_textarea", function () {
        var temp_this = $(this);
        if ($(this).val() == "") {
            $(temp_this).parents('div.text_poll2').siblings('button.reply_post_button').prop('disabled', true);
            submitReply = 0;
        } else {
            $(temp_this).parents('div.text_poll2').siblings('button.reply_post_button').prop('disabled', false);
            submitReply = 1;
        }
    });

    /************ When the link reply is pressed **************/
    $("body").on("click", ".reply_link", function (e) {
        $(this).parents('div.post_action_div').siblings('div.commenting_box').children('div.reply_box').children('div.comment_in1').click();
    });

    // reply for a post
    $("body").on("submit", ".reply_form", function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var post_id = $(this).parents('.reply_box').attr('id');
        var reply_message = temp_this.children('.text_poll2').children('.reply_textarea').val();
        if (reply_message != '' && submitReply == 1) {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/save_post_reply",
                data: {
                    post_id: post_id,
                    group: group,
                    reply_message: reply_message,
                    membersForReplyCC: membersForReplyCC,
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() != '0') {
                        temp_this.parents().eq(1).siblings('.all_reply').append(data);
                        temp_this[0].reset();
                        temp_this.children('.reply_post_button').removeClass('active_post_btn');
                        temp_this.parent().siblings('.comment_in1').css("display", "block");
                        temp_this.parent().css("display", "none");
                        membersForReplyCC = [];
                    }
                }
            });
        }
    });

    /*********Get the userlist for tagging in reply for post box**********/

    /*Gives the list of all memebers of scholarspace*/
    $('body').on('click', '.addCCMembersToReply', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var ListItem = "";
        if ($(this).val() == "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    group: group,
                },
                success: function (data) {
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (membersForReplyCC.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + post_id + ' .notify_pop').show();
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').append(ListItem);
                }
            });
        }
    });
    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '.addCCMembersToReply', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    group: group,
                    Item: Item
                },
                success: function (data) {
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (membersForReplyCC.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + post_id + ' .notify_pop').show();
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '.Reply_ListOfMembers li', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var name = $(this).children('h5').html();
        var college_users_intranet_id = $(this).attr('intranet_id');
        var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company added_user'>\
                            <h6> " + name + " </h6>\
                            <span class='pop_close'></span>\
                        </div>";
        $('#' + post_id + ' .AddedCCMembersOfReply').append(AddedItem);
        $('#' + post_id + ' .addCCMembersToReply').val('');
        $('#' + post_id + ' .notify_pop').hide();
        membersForReplyCC.push($(this).attr('intranet_id'));
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '.pop_close', function () {
        var intranet_id = $(this).parents('div.company').attr('intranet_id');
        var index = membersForReplyCC.indexOf(intranet_id);
        if (index > -1) {
            membersForReplyCC.splice(index, 1);
        }
        $(this).parents('div.company').remove();
    });

    // Submit vote for a answer
    $('body').on('submit', '.poll_answer_form', function (e) {
        var ele = $(this).parents('.post_type_div').attr('id');
        e.preventDefault();
        var post_id = $(this).parents('div.post_content_div').attr('id');
        var poll_id = $(this).parents('div.post_type_div').attr('id');
        var formData = $(this).serialize() + "&group=" + group + "&post_id=" + post_id + "&poll_id=" + poll_id;

        if ($(this).serialize() != "" && group != "") {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/VoteForPoll_Method",
                data: formData,
                success: function (data) {
//                    console.log(data);
                    if (data != 0) {
                        $('div.poll_answer_div').html('');
                        $('div.poll_answer_div').append(data);
                        var counter = setInterval(timer, 1000); //1000 will  run it every 1 second
                        function timer()
                        {
                            TimerLimit = TimerLimit - 1;
                            if (TimerLimit <= 0)
                            {
                                clearInterval(counter);
                                $('#' + ele + ' .change_vote_button').remove();
                                return;
                            }
                        }
                    }
                }
            });
        }
    });


    $('body').on('click', '.change_vote_button', function (e) {
        e.preventDefault();
        var post_id = $(this).parents('div.post_content_div').attr('id');
        var poll_id = $(this).parents('div.post_type_div').attr('id');

        if (poll_id != "" && group != "" && post_id != "") {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/ChangeVote_Method",
                data: {
                    group: group,
                    post_id: post_id,
                    poll_id: poll_id,
                },
                success: function (data) {
                    if (data != 0) {
                        $('div.poll_answer_div').html('');
                        $('div.poll_answer_div').append(data);
                    }
                }
            });
        }
    });

    //post reply like
    $("body").on("click", ".post_reply_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var post_reply_id = temp_this.parent().attr("post_reply_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/like_post_reply",
            data: {
                post_id: post_id,
                post_reply_id: post_reply_id,
                like_action: like_action
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("You ");
                            $("#post_reply_like_div-" + post_reply_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_reply_like_div-" + post_reply_id).css("display", "none");
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("");
                        }
                    }
                }
            }
        });
    });

    //delete_post
    $("body").on("click", ".delete_post", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_id = temp_this.parents().eq(2).attr("post_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/delete_post",
                data: {
                    group: group,
                    post_id: post_id
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The post has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });

    //delete post reply
    $("body").on("click", ".delete_post_reply", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_reply_id = temp_this.parents().eq(2).attr("post_reply_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/delete_post_reply",
                data: {
                    group: group,
                    post_reply_id: post_reply_id
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The message has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });

    // When clicked on follow or unfollow for a post
    $("body").on("click", ".post_follow", function () {
        var ele = $(this);
        var post_id = ele.parents('div.post_content_div').attr('id');
        var follow_action = ele.attr('follow_action');

        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/FollowPost_Method",
            data: {
                group: group,
                post_id: post_id,
                follow_action: follow_action
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() == 1) {
                    if (follow_action == "follow") {
                        ele.attr('follow_action', 'unfollow');
                        ele.html('Unfollow');
                    } else if (follow_action == "unfollow") {
                        ele.attr('follow_action', 'follow');
                        ele.html('Follow');
                    }
                }
            }
        });
    });


    /**
     * Code for filtering the posts
     * having 3 filters
     * All, Top, Following
     **/

    var All_flag = 0;
    var Top_flag = 0;
    var Following_flag = 0;
    $('body').on('click', '.PostFilter', function (e) {
        e.preventDefault();
        var ele = $(this);
        var filter = ele.attr("id");

        if (filter == "allPosts") {
            FilterPosts_Method(filter);
            All_flag++;
        } else if (filter == "topPosts") {
            FilterPosts_Method(filter);
            Top_flag++;
        } else if (filter == "followingPosts") {
            FilterPosts_Method(filter);
            Following_flag++;
        }
    });

    function FilterPosts_Method(filter) {
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/FilterPosts_Method",
            data: {
                group: group,
                filter: filter
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() != 0) {
                    if (filter == "allPosts") {
                        if (data != "") {
                            $('#all_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#all_tab #all_post_content').html(data);
                        } else {
                            $('#all_tab #all_post_content').html("");
                        }
                    } else if (filter == "topPosts") {
                        if (data != "") {
                            $('#top_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#top_tab #all_post_content').html(data);
                        } else {
                            $('#top_tab #all_post_content').html("");
                        }
                    } else if (filter == "followingPosts") {
                        if (data != "") {
                            $('#follow_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#follow_tab #all_post_content').html(data);
                        } else {
                            $('#follow_tab #all_post_content').html("");
                        }
                    }
                }
            }
        });
    }
    /*** Filter posts section ends ***/

    //when clicked on like for a announce in top box
    $('body').on('click', '.like_announce', function (e) {
        var temp_this = $(this);
        var post_id = temp_this.parents('div.user_announce').attr("id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/like_post",
            data: {
                post_id: post_id,
                group: group,
                like_action: like_action
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.children('i').removeClass('fa fa-thumbs-up');
                        temp_this.children('i').addClass('fa fa-thumbs-down');
                        temp_this.attr("like_action", "unlike");
                        temp_this.attr("title", "Unlike");
                        like_count++;
                        temp_this.children('span.like_num').text(like_count);
                        temp_this.attr("like_count", like_count);
                    } else {
                        temp_this.children('i').removeClass('fa fa-thumbs-down');
                        temp_this.children('i').addClass('fa fa-thumbs-up');
                        temp_this.attr("like_action", "like");
                        temp_this.attr("title", "Like");
                        like_count--;
                        temp_this.children('span.like_num').text(like_count);
                        temp_this.attr("like_count", like_count);
                    }
                }
            }
        });
    });

    $('body .fancybox-thumbs').fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: false,
        arrows: false,
        nextClick: true,
        helpers: {
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });

});