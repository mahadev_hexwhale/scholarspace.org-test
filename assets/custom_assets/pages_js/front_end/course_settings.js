course_id = $('#course_id').val();

$(document).ready(function () {

    $('body').on('submit', '#MemberAttendance_Form', function (e) {
        e.preventDefault();
        var attendance_array = {};
        $(".attendance_td").each(function () {
            var attendance = $(this).find('input[type=radio]:checked').val();
            var index = $(this).find('input[type=radio]:checked').attr('name');
            attendance_array[index] = attendance;
        });
        $.ajax({
            type: "POST",
            url: site_url + "user/courses/UpdateMemberAttendance_Method",
            data: {
                course_id: course_id,
                attendance_array: attendance_array
            },
            success: function (data) {
                if (data.trim() != "0") {
                    alert("Attendance is updated successfully.");
                }
            }
        });
    });

    $('body').on('submit', '#MemberIAMarks_Form', function (e) {
        e.preventDefault();
        var iamarks_array = {};
        $(".iamarks_td").each(function () {
            var marks = $(this).find('input[type=text]').val();
            var index = $(this).find('input[type=text]').attr('name');
            iamarks_array[index] = marks;
        });
        $.ajax({
            type: "POST",
            url: site_url + "user/courses/UpdateMemberIAMarks_Method",
            data: {
                course_id: course_id,
                iamarks_array: iamarks_array
            },
            success: function (data) {
                if (data.trim() != "0") {
                    alert("IA Marks updated successfully.");
                }
            }
        });
    });

    $('body').on('submit', '.Marks_Form', function (e) {
        e.preventDefault();
        var exam = $(this).attr('fid');
        var marks_array = {};
        var ele = $(this).attr('id');
        $("#" + ele + " .marks_td").each(function () {
            var marks = $(this).find('input[type=text]').val();
            var index = $(this).find('input[type=text]').attr('name');
            marks_array[index] = marks;
        });

        $.ajax({
            type: "POST",
            url: site_url + "user/courses/UpdateMemberMarks_Method",
            data: {
                course_id: course_id,
                exam: exam,
                marks_array: marks_array
            },
            success: function (data) {
                if (data.trim() != "0") {
                    alert("Marks updated successfully.");
                }
            }
        });
    });

    $('body').on('submit', '.AddMore_Form', function (e) {
        e.preventDefault();
        var user_type = $(this).attr('id');
        var email = $(this).find('input[type="text"]').val();

        $.ajax({
            type: 'POST',
            url: site_url + "user/courses/AddMoreToCourse_Method",
            data: {
                course_id: course_id,
                email: email,
                user_type: user_type
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() != "0") {
                    $("#" + user_type + " .required_error_label").html(data);
                }
            }
        });
    });

    /* Validation check for text box */
    function textValidation(form_id) {
        var status = 1;
        $(form_id + ' .required_field').each(function () {
            var temp_this = $(this);
            var title = temp_this.attr("title");
            if (temp_this.val().trim() == '') {
                status = 0;
                $(temp_this).siblings('.required_error_label').text("* " + title + " is Required");
            }
        });
        return status;
    }

    /* remove the required error msg */
    $("input, textarea").focus(function () {
        var temp_this = $(this);
        $(temp_this).siblings('.required_error_label').text("");
    });

    $('body').on('submit', '#EditCourseDetails_Form', function (e) {
        e.preventDefault();
        var course_name = $(this).find('input[name="course_name"]').val();
        var course_brief = $(this).find('textarea[name="course_brief"]').val();

        var valid_count = 0;
        if (!textValidation("#EditCourseDetails_Form")) {
            valid_count++;
        }

        if (valid_count == "0") {
            $.ajax({
                type: 'POST',
                url: site_url + "user/courses/EditCourseDetails_Method",
                data: {
                    course_id: course_id,
                    course_name: course_name,
                    course_brief: course_brief
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() != "0") {
                        $('#EditCourseDetails_Modal').modal('hide');
                        window.location.reload();
                    }
                }
            });
        }
    });

    $('body').on('click', '.publish_course', function (e) {
        var result = confirm("Do you want to publish this course ?");
        if (result) {
            $.ajax({
                type: 'POST',
                url: site_url + "user/courses/PublishCourse_Method",
                data: {
                    course_id: course_id
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() != "0") {
                        location.reload();
                    }
                }
            });
        }
    });

});