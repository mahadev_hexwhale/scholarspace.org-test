
/*** Initializations ***/
var GroupID = $("#grpID").val();
var members_json = jQuery.parseJSON($("#members_json").val());
var NewMembers = [];        //Array of all memeber's intranet_id who are getting added from add_more button
var AllNewMembers = [];     //Array of all member's intranet_id who are not members of this group but they are members of the college
var SubmitForm = 1;
var group_members_group = {};
var All_flag = 0;
var Top_flag = 0;
var Following_flag = 0;
var AddedMembersList_more = [];     //Array of all member's intranet_id who are already members of this group
$.each(members_json, function (index, value) {
    AddedMembersList_more.push(value.user_intranet_id);
});

/*** Initializations ends ***/


$(document).ready(function () {

    tinymce.init({
        selector: '.group_info_textpad',
        menubar: false,
        statusbar: false,
        height: 350,
        theme: 'modern',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste autoresize"
        ],
        style_formats: [
            {title: 'Heading 1', block: 'h1'},
            {title: 'Paragraph', inline: 'p'},
        ],
        autoresize_bottom_margin: 25,
        advlist_bullet_styles: "default",
        advlist_number_styles: "default",
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "styleselect bold italic bullist numlist link unlink",
    });

    /** Function to initialize datatable and get all the entries of datatable **/
    $(function () {
        $('#group_files_table').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
//            "bAutoWidth": true,
//            "bDeferRender": true,
//            "bProcessing": true,
//            "bServerSide": true,
//            "sServerMethod": "GET",
//            "sAjaxSource": site_url + 'user/groups/GroupFilesDatatable_FetchMethod',
//            "iDisplayLength": 10,
//            "aLengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
//            "aaSorting": [[0, 'asc']],
        });
    });

    /*To reset all contents of modal on opening*/
    $('body').on('click', '#AddMembersToGroup_Modal', function () {
        $('.notify_pop').fadeOut();
    });

    $('body').on('click', '#AddMembersToGroup_Button', function () {
        $('#AddMembersToGroup_Modal input[type=text]').val('');
        $('#AddMembersToGroup_Modal select').val('');
        $('#AddMembersToGroup_Modal input[type=radio]').removeAttr('checked');
        $('.AddedMembers').html('');
        $('.group_error').fadeOut();
        $('#AddMembersToGroup_Modal').modal('show');
        AddedMembersList_more = [];
        $.each(members_json, function (index, value) {
            AddedMembersList_more.push(value.user_intranet_id);
        });
        NewMembers = [];
        AllNewMembers = [];
    });

    /*Gives the list of all memebers of scholarspace*/
    $('body').on('click', '#AddMembersToGroup_Modal #addMoreGroupMembers', function () {
        var ListItem = "";
        if ($(this).val() == "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/groups/GetRemainingMembers_Method",
                data: {
                    GroupID: GroupID,
                    AddedMembersList_more: AddedMembersList_more
                },
                success: function (data) {
//                    console.log(data);
                    $('#AddMembersToGroup_Modal .people_list .ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (key == "student_list") {
                            $.each(value, function (inner_index, inner_value) {
                                if (AddedMembersList_more.indexOf(inner_value.college_users_intranet_id) == -1) {
                                    if ($.inArray(value.college_users_intranet_id, AllNewMembers) == -1)
                                        AllNewMembers.push(inner_value.college_users_intranet_id);
                                    ListItem += "<li list_item_type = 'student_list' intranet_id='" + inner_value.college_users_intranet_id + "'>\
                                                    <div class='notify_img'>\
                                                        <img src='" + base_url + "" + inner_value.profile_picture + "' alt=''>\
                                                    </div>\
                                                    <h5>" + inner_value.fname + " " + inner_value.lname + "</h5>\
                                                    <br>\
                                                    <h6>" + inner_value.stream_name + "</h6>\
                                                </li>";
                                }
                            });
                        } else if (key == "student_batch_list") {
                            $.each(value, function (inner_index, inner_value) {
                                var test_array = inner_index in group_members_group;
                                if (test_array != true) {
                                    ListItem += '<li list_item_type = "student_batch_list" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                    <div style="width: 10%; float: left;" class="notify_img">\
                                                        <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                    </div>\
                                                    <div style="width: 90%; float: left;" >\
                                                    ' + inner_index + '\
                                                    </div>\
                                                </li>';
                                }
                            });
                        }
                    });
                    $('#AddMembersToGroup_Modal .notify_pop').show();
                    $('#AddMembersToGroup_Modal .people_list .ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '#AddMembersToGroup_Modal #addMoreGroupMembers', function () {
        var ListItem = "";
        var Item = $(this).val();

        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/groups/FilterAddMoreCollegeMembers_Method",
                data: {
                    AddedMembersList_more: AddedMembersList_more,
                    Item: Item
                },
                success: function (data) {
//                    console.log(data);
                    $('#AddMembersToGroup_Modal .people_list .ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (key == "student_list") {
                            $.each(value, function (inner_index, inner_value) {
                                if (AddedMembersList_more.indexOf(inner_value.college_users_intranet_id) == -1) {
                                    ListItem += "<li list_item_type = 'student_list' intranet_id='" + inner_value.college_users_intranet_id + "'>\
                                                    <div class='notify_img'>\
                                                        <img src='" + base_url + "" + inner_value.profile_picture + "' alt=''>\
                                                    </div>\
                                                    <h5>" + inner_value.fname + " " + inner_value.lname + "</h5>\
                                                    <h6>" + inner_value.stream_name + "</h6>\
                                                </li>";
                                }
                            });
                        } else if (key == "student_batch_list") {
                            $.each(value, function (inner_index, inner_value) {
                                var test_array = inner_index in group_members_group;
                                if (test_array != true) {
                                    ListItem += '<li list_item_type = "student_batch_list" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                    <div style="width: 10%; float: left;" class="notify_img">\
                                                        <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                    </div>\
                                                    <div style="width: 90%; float: left;" >\
                                                    ' + inner_index + '\
                                                    </div>\
                                                </li>';
                                }
                            });
                        }
                    });
                    $('#AddMembersToGroup_Modal .notify_pop').show();
                    $('#AddMembersToGroup_Modal .people_list .ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '#AddMembersToGroup_Modal .ListOfMembers li', function () {

        var temp_this = $(this);
        var list_item_type = temp_this.attr("list_item_type");
        if (list_item_type == "student_list") {
            var name = $(this).children('h5').html();
            var college_users_intranet_id = $(this).attr('intranet_id');
            var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company'>\
                            <h6> " + name + " </h6>\
                            <span item_type = 'student_list' class='pop_close'></span>\
                        </div>";
            $('#AddMembersToGroup_Modal .AddedMembers').append(AddedItem);
            $('#AddMembersToGroup_Modal #addMoreGroupMembers').val('');
            $('#AddMembersToGroup_Modal .notify_pop').hide();

            AddedMembersList_more.push($(this).attr('intranet_id'));
            NewMembers.push($(this).attr('intranet_id'));
        } else if (list_item_type == "student_batch_list") {
            var users_id = temp_this.attr("users_id");
            var group_id = temp_this.attr("group_id");
            var group_name = group_id.replace('</h5><h6>', ' - ');
            group_name = group_name.replace('<h5>', '<h6>');
            var AddedItem = '<div class="company" group_id = "' + group_id + '" users_id ="' + users_id + '">\
                                <span class="color_grp"></span>\
                                ' + group_name + '\
                                <span item_type = "student_batch_list" class="pop_close remove_selected_user_group"></span>\
                            </div>';
            $('#AddMembersToGroup_Modal .AddedMembers').append(AddedItem);
            $('#AddMembersToGroup_Modal #addMoreGroupMembers').val('');
            $('#AddMembersToGroup_Modal .notify_pop').hide();

            group_members_group[group_id] = users_id;
        }
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '#AddMembersToGroup_Modal .pop_close', function () {
        var temp_this = $(this);
        var item_type = temp_this.attr('item_type');
        if (item_type == "student_list") {
            var intranet_id = $(this).parents('div.company').attr('intranet_id');
            var index = AddedMembersList_more.indexOf(intranet_id);
            if (index > -1) {
                AddedMembersList_more.splice(index, 1);
            }
            var index1 = NewMembers.indexOf(intranet_id);
            if (index1 > -1) {
                NewMembers.splice(index1, 1);
            }
            $(this).parents('div.company').remove();
        } else if (item_type == "student_batch_list") {
            var group_id = temp_this.parent("div").attr("group_id");
            delete group_members_group[group_id];
            $(this).parents('div.company').remove();
        }
    });

    /* When you press the add button after selecting members in add more members modal */
    $('body').on('click', '#AddMembersToGroup_Modal .addMoreGroupMembers_Button', function () {
        group_members_group = JSON.stringify(group_members_group);
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/groups/addMoreGroupMembers_Method",
            data: {
                AddedMembersList_more: AddedMembersList_more,
                group_members_group: group_members_group,
                NewMembers: NewMembers,
                AllNewMembers: AllNewMembers,
                GroupID: GroupID
            },
            success: function (data) {
//                console.log(data);
                var AppendUser = "";
                if (data != '0') {
                    $.each(data, function (key, value) {
                        AppendUser += "<li user_intranet_id='" + value.college_users_intranet_id + "'>\
                                            <div class='existing_user col-md-12'>\
                                                <div class='user_pic'>\
                                                    <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                                </div>\
                                                <div class='pp_details'>\
                                                    <h5>\
                                                        <span class='on_dot' title='offline'></span>\
                                                         " + value.fname + " " + value.lname + "\
                                                    </h5>\
                                                    <h6>\
                                                        <strong>" + value.stream_name + "</strong>\
                                                    </h6>\
                                                </div>\
                                            <span class='btn btn-default btn-xs user_delete'><i class='fa fa-trash'></i> Delete</span>\
                                            </div>\
                                        </li>";
                    });
                    $('#AddMembersToGroup_Modal .ExistingMembers ul').prepend(AppendUser);
                    $('#AddMembersToGroup_Modal .AddedMembers').html('');
                    alert('User added Successfully');
                } else {
                    alert('Error adding user.. Try again.');
                }
            }
        });
    });

    /* When you want to delete a member from a group */
    $('body').on('click', '#AddMembersToGroup_Modal .user_delete', function () {
        var ele = $(this);
        var user_intranet_id = ele.parents('li').attr('user_intranet_id');
        bootbox.confirm('Are you sure you want to delete this user?', function (ok) {
            if (ok) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/groups/DeleteGroupMember_Method",
                    data: {
                        user_intranet_id: user_intranet_id,
                        GroupID: GroupID
                    },
                    success: function (data) {
                        if (data == "1") {
                            var intranet_id = ele.parents('li').attr('user_intranet_id');
                            var index = AddedMembersList_more.indexOf(intranet_id);
                            if (index > -1) {
                                AddedMembersList_more.splice(index, 1);
                            }
                            var index1 = NewMembers.indexOf(intranet_id);
                            if (index1 > -1) {
                                NewMembers.splice(index, 1);
                            }
                            ele.parents('li').remove();
                            alert('User Deleted Successfully');
                        } else {
                            alert('Error deleting user.. Try again.');
                        }
                    }
                });
            }
        });
    });

    /* This will refresh the page after AddMembersToGroup_Modal is closed */
    $('#AddMembersToGroup_Modal').on('hidden.bs.modal', function () {
        location.reload();
    });

    /* When you are updating the group image change displayed image after selecting new image */
    $('body').on('change', '#group_image', function () {
        var data = new FormData($("#EditGroup_Form")[0]);
        $('.grp_img_loading').fadeIn();
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/UploadGroupImage_Method",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data != 0) {
                    $('.grp_img_loading').fadeOut();
                    $('#EditGroup_Form #group_icon_image').attr('src', base_url + "" + data);
                    $('#hidden_image_path').val(data);
                }
            }
        });
    });

    /* After submitting the form for updating the group details */
    $('body').on('submit', '#EditGroup_Form', function (e) {
        e.preventDefault();
        if ($('#group_name').val() == "") {
            $('#group_name').parent('div').siblings('div.error_msg').css('color', 'red');
            $('#group_name').parent('div').siblings('div.error_msg').html('This field is required');
            $('#group_name').parent('div').siblings('div.error_msg').fadeIn();
            SubmitForm = 0;
        }

        if (SubmitForm == 1) {
            GroupName = $('#group_name').val();
            VisibleTo = $('.VisibleTo:checked').val();
            GroupAccess = $('.GroupAccess:checked').val();
            GroupIconImage = $('#hidden_image_path').val();
            $.ajax({
                type: "POST",
                url: site_url + "user/groups/UpdateGroupSettings_Method",
                data: {
                    GroupID: GroupID,
                    GroupName: GroupName,
                    VisibleTo: VisibleTo,
                    GroupAccess: GroupAccess,
                    GroupIconImage: GroupIconImage
                },
                success: function (data) {
                    window.location.href = window.location.href;
                }
            });
        }
    });

    /*To check whether the entered name is already present in database. This runs on keyup*/
    $('body').on('keyup', '#EditGroup_Form #group_name', function (e) {
        var hidden_name = $('#hidden_name').val();
        if ($('#group_name').val() != "" && $('#group_name').val() != hidden_name) {
            var GroupName = $('#group_name').val();
            $.ajax({
                type: "POST",
                url: site_url + "user/groups/CheckGroupName_Method",
                data: {
                    GroupName: GroupName,
                },
                success: function (data) {
                    if (data == 1) {
                        $('#group_name').parent('div').siblings('div.error_msg').html('');
                        $('#group_name').parent('div').siblings('div.error_msg').css('color', 'red');
                        $('#group_name').parent('div').siblings('div.error_msg').html('This Name is not available.');
                        $('#group_name').parent('div').siblings('div.error_msg').fadeIn();
                        SubmitForm = 0;
                    } else {
                        $('#group_name').parent('div').siblings('div.error_msg').html('');
                        $('#group_name').parent('div').siblings('div.error_msg').css('color', 'green');
                        $('#group_name').parent('div').siblings('div.error_msg').html('This Name available.');
                        $('#group_name').parent('div').siblings('div.error_msg').fadeIn();
                        SubmitForm = 1;
                    }
                }
            });
        } else {
            $('#group_name').parent('div').siblings('div.error_msg').html('');
        }
    });

    /* To get the on hover effect of "joined" text */
    $(".join_leave").hover(
            function () {
                $('.joined_group').hide();
                $('.leave_group').show();
            },
            function () {
                $('.joined_group').show();
                $('.leave_group').hide();
            }
    );

    /* When user presses the leave button get user deleted from group */
    $('body').on('click', '.join_leave', function () {
        bootbox.confirm('Are you sure you want to leave?', function (ok) {
            if (ok) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/groups/LeaveFromGroup_Method",
                    data: {
                        GroupID: GroupID,
                    },
                    success: function (data) {
                        if (data == 1) {
                            window.location.href = window.location.href;
                        }
                    }
                });
            }
        });
    });

    /* After pressing the delelte group button */
    $('body').on('click', '.DeleteGroup', function () {
        bootbox.confirm('Are you sure you want to delete this group?<br><br><font size="2">Note:Deleting this group will delete all members from group and delete the group.</font>', function (ok) {
            if (ok) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/groups/DeleteGroup_Method",
                    data: {
                        GroupID: GroupID,
                    },
                    success: function (data) {
                        if (data == 1) {
                            window.location.href = base_url + "user/home";
                        }
                    }
                });
            }
        });
    });

    /* When user presses join button so that he can join the group */
    $('body').on('click', '.JoinGroupNow_Button', function () {
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/JoinGroupNow_Method",
            data: {
                GroupID: GroupID,
            },
            success: function (data) {
                if (data != "0") {
                    window.location.href = window.location.href;
                }
            }
        });
    });

    /* When you approve a request to join a group */
    $('body').on('click', '#AddMembersToGroup_Modal .ApproveRequest', function () {
        var ele = $(this);
        var user_intranet_id = ele.parents('li').attr('user_intranet_id');
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/ApproveGroupJoinRequest_Method",
            data: {
                GroupID: GroupID,
                user_intranet_id: user_intranet_id
            },
            success: function (data) {
                if (data == "1") {
                    var User = ele.parents('li').wrap('<p/>').parent().html();
                    ele.parents('li').remove();
                    $('#AddMembersToGroup_Modal .ExistingMembers ul').prepend(User);
                    $('.ApproveRequest').remove();
                    $('.DenyRequest').removeAttr("style");
                    $('.DenyRequest').html("<i class='fa fa-trash'></i> Delete");
                    $('.DenyRequest').attr('class', 'btn btn-default btn-xs user_delete');
                    alert("User Added Successfully.");
                } else {
                    alert("Error Approving the request. Please try again.");
                }
            }
        });
    });

    $('body').on('click', '#AddMembersToGroup_Modal .DenyRequest', function () {
        var ele = $(this);
        var user_intranet_id = ele.parents('li').attr('user_intranet_id');
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/DenyGroupJoinRequest_Method",
            data: {
                GroupID: GroupID,
                user_intranet_id: user_intranet_id
            },
            success: function (data) {
                if (data == '1') {
                    ele.parents('li').remove();
                    alert("User deleted Successfully.");
                } else {
                    alert("There is an error deleting the user. Please try again.");
                }
            }
        });
    });

    $('body').on('click', '.group_info_edit_button', function () {
        $('.group_info_edit_button_div').hide();
        $('.group_info_div').hide();

        var info_content = $('.group_info_div').html();
        if ($('.group_info_div').children('div').hasClass('temp_p')) {
            tinyMCE.get('group_info_textpad').setContent('');
        } else {
            tinyMCE.get('group_info_textpad').setContent(info_content);
        }

        $('.group_info_save_button_div').show();
        $('.group_info_textpad_div').show();
    });

    $('body').on('submit', '#EditGroupInfo_Form', function (e) {
        e.preventDefault();
        var info_content = tinyMCE.get('group_info_textpad').getContent();
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/UpdateGroupInfo_Method",
            data: {
                GroupID: GroupID,
                info_content: info_content
            },
            success: function (data) {
                if (data != 0) {
                    $('.group_info_div').html(data);
                    tinyMCE.get('group_info_textpad').setContent(data);
                    $('.group_info_div').html(data);

                    $('.group_info_textpad_div').hide();
                    $('.group_info_save_button_div').hide();
                    $('.group_info_div').show();
                    $('.group_info_edit_button_div').show();
                }
            }
        });
    });

    $('body').on('click', '.cancel_edit', function (e) {
        $('.group_info_textpad_div').hide();
        $('.group_info_save_button_div').hide();
        $('.group_info_div').show();
        $('.group_info_edit_button_div').show();
    });

    $('body .fancybox-thumbs').fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        arrows: false,
        nextClick: false,
        helpers: {
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });

    $('body').on('submit', '#SearchThisGroup_Form', function (e) {
        e.preventDefault();
        var formdata = $(this).serialize() + "&GroupID=" + GroupID;
        var string = $('#SearchString').val();
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/SearchThisGroup_Method",
            data: formdata,
            success: function (data) {
//                console.log(data);
                if (data.trim() != 0) {
                    $('#all_tab').html("<div class='all_post_content' class='global_details'>");
                    $('#all_tab .all_post_content').html(data);
                    $('#all_tab .all_post_content').highlight(string);
                    $('#all_tab').addClass('active');
                    All_flag = Top_flag = Following_flag = 0;
                } else {
                    alert("No results found..!!");
                }
            }
        });
    });

    /**
     * Code for filtering the posts
     * having 3 filters
     * All, Top, Following
     **/

    $('body').on('click', '.PostFilter', function (e) {
        e.preventDefault();
        var ele = $(this);
        var filter = ele.attr("id");

        if (filter == "allPosts") {
            FilterPosts_Method(filter);
            All_flag++;
        } else if (filter == "topPosts") {
            FilterPosts_Method(filter);
            Top_flag++;
        } else if (filter == "followingPosts") {
            FilterPosts_Method(filter);
            Following_flag++;
        }
    });

    function FilterPosts_Method(filter) {
        $.ajax({
            type: "POST",
            url: site_url + "user/groups/FilterPosts_Method",
            data: {
                GroupID: GroupID,
                filter: filter
            },
            success: function (data) {
                if (data.trim() != 0) {
                    if (filter == "allPosts") {
                        if (data != "") {
                            $('#all_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#all_tab #all_post_content').html(data);
                        } else {
                            $('#all_tab #all_post_content').html("");
                        }
                    } else if (filter == "topPosts") {
                        if (data != "") {
                            $('#top_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#top_tab #all_post_content').html(data);
                        } else {
                            $('#top_tab #all_post_content').html("");
                        }
                    } else if (filter == "followingPosts") {
                        if (data != "") {
                            $('#follow_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#follow_tab #all_post_content').html(data);
                        } else {
                            $('#follow_tab #all_post_content').html("");
                        }
                    }
                }
            }
        });
    }
    /*** Filter posts section ends ***/

});