var global = 0;
var num = 0;
var res = 0;
$(document).ready(function () {
    $('.people_list_input').focus(function () {
        var temp_this = $(this);
        if (!temp_this.siblings('.people_list_drop_down').hasClass('active')) {
            temp_this.siblings('.people_list_drop_down').show();
            temp_this.siblings('.people_list_drop_down').addClass('active');
        } else {
            temp_this.siblings('.people_list_drop_down').hide();
            temp_this.siblings('.people_list_drop_down').removeClass('active');
        }
    });

    $('#topic1').click(function () {
        $('#open_topic').css('display', 'block');
        $('#topic1').css('display', 'none');

    });

    $('#topic2').click(function () {
        $('#open_topic2').css('display', 'block');
        $('#topic2').css('display', 'none');

    });

    $('#comment_in1').click(function () {
        $('#more_in1').css('display', 'block');
        $('#comment_in1').css('display', 'none');
    });

    $('#set_box').click(function () {
        $(".setting_list").slideToggle();
        if (global == 0) {
            $('.setting_box1').css('box-shadow', ' 0 1px 2px 1px rgba(0,0,0,.2)');
            $('.setting_box1').css('background-color', 'white');
            global++;
        } else {
            $('.setting_box1').css('box-shadow', 'none');
            $('.setting_box1').css('background-color', 'transparent');
            global--;
        }
    });

    $('#notify_id').click(function () {
        if (global == 0) {
            $('.notify_pop').show();
            global++;
        } else {
            $('.notify_pop').hide();
            global--;
        }
    });
    $('.pop_close').click(function () {
        $('#second_notify').show();
        $('.add_text ').css('display', 'block');
        $('#remove_com').css('display', 'none');
        $('#notify_id').css('display', 'none');

    });

    $('#notify_id2').click(function () {
        $('#second_notify').hide();

    });

    /* code is for name popup */

    $(".res_top_name").mouseleave(function () {
        $(this).fadeOut();
//            global--;
    });

    $('.edit').click(function () {
        $('.edit_more').css('display', 'block');
    });

    $('.edit2').click(function () {
        $('.edit_more2').css('display', 'block');
    });
    $('#open_chat').click(function () {
        if (num == 0) {
            $('.online_chat ').css('display', 'block');
            $('.online_box ').css('bottom', '364px');
            num++;
        } else {
            $('.online_chat ').css('display', 'none');
            $('.online_box ').css('bottom', '0px');
            $('.set_pop ').hide();
            num--;
        }
    });

    $('.online_box div .on_set').click(function () {
        if (res == 0) {
            $('.set_pop ').show();
            res++;
        } else {
            $('.set_pop ').hide();
            res--;
        }

    });

    $('.on_dash').click(function () {
        $('.online_chat ').css('display', 'none');
        $('.online_box ').css('bottom', '0px');
        $('.set_pop ').hide();
    });

    $("#chat_pop_name").hover(function () {
        if (global == 0) {
            $('#chat_name_res').show();
            global++;
        }
        else {
            $('#chat_name_res').hide();
            global--;
        }

    });

    $('.praise_icons').click(function () {
        var i = 0;
        if (i == 0)
        {
            $('.praise_icons').css('background-position', '0px -46px');
            i++;
            $('.praise_icons').click(function () {
                $('.praise_icons').css('background-position', '0px -92px');
            });
        }

    });

    $('.share_box').click(function () {
        $(this).css('display', 'none');
        $('.show_share').show();
    });

    $('.pin2').click(function () {
        if (num == 0) {
            $('.attach_list').show();
            $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
            num++;
        }
        else {
            $('.attach_list').hide();
            $('.pin2').css('box-shadow', 'none');
            num--;
        }
    });

    $('.pin').click(function () {
        if (num == 0) {
            $('.attach_list').show();
            $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
            num++;
        }
        else {
            $('.attach_list').hide();
            $('.pin2').css('box-shadow', 'none');
            num--;
        }
    });
});
function myFunction(name) {
    if (global == 0) {
        document.getElementById("text_change").innerHTML = " " + name;
        global++;
    }
    else {
        document.getElementById("text_change").innerHTML = "Edit topics ";
        global--;
    }
}
function myFunction2(name) {
    if (global == 0) {
        document.getElementById("text_change2").innerHTML = " " + name;
        global++;
    }
    else {
        document.getElementById("text_change2").innerHTML = "Edit topics ";
        global--;
    }
}


/************ Code by Mahadev ************/

$(document).ready(function () {
    // Autocomplete for search in top right i.e, search for people and groups
    $("#SearchString_Autocomplete").autocomplete({
        source: function (request, response)
        {
            $.ajax({
                type: "POST",
                url: site_url + "user/groups/GetListOfPeopleAndGroups_Method",
                dataType: "json",
                data: {
                    search_string: request.term,
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        focus: function (event, ui) {
            $("#SearchString_Autocomplete").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $("#SearchString_Autocomplete").val(ui.item.label);
            var selected_id = ui.item.value;
            var temp = selected_id.split('>-<');
            if (temp[0] == 'g') {
                window.location = site_url + "user/groups?grp_id=" + temp[1];
            } else {
                window.location = site_url + "user/user_profile/view/" + temp[0];
            }
            return false;
        }
    });

    // When you hover on the name of the person who posted
    $(".top_name").hover(function () {
        var temp_this = $(this);
        var user_id = temp_this.attr('uid');
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/dashboard/GetUserDetails",
            data: {
                user_id: user_id
            },
            success: function (data) {
                if (data != "") {
                    var division = "<span class='point_top'></span>\
                                        <div class='row'>\
                                            <div class='col-md-3'>\
                                                <div class='intra_user_img' style='padding: 4px 9px;'>\
                                                    <img src='" + base_url + data[0].profile_picture + "' alt=''>\
                                                </div>\
                                            </div>\
                                            <div class='col-md-9'>\
                                                <div class='pop_user_info'>\
                                                    <h5>" + data[0].fname + " " + data[0].lname + "</h5>\
                                                    <h6>" + data[0].stream_name + "</h6>\
                                                    <h6>" + data[0].stream_course_name + "</h6>\
                                                    <h6>Email : <a href='mailto:" + data[0].email + "'>" + data[0].email + "</a></h6>\
                                                </div>\
                                            </div>\
                                            <div class='col-md-12'>\
                                                <div class='pop_send'>\
                                                    <a href='#' data-toggle='modal' data-target='#myModal2'>Send Message</a>";
                    if (data[1] == 1) {
                        division += "<a href='javascript:void(0)' class='FollowThisUser' follow_action='unfollow'><i class = 'fa fa-times'></i> Unfollow</a>";
                    } else {
                        division += "<a href='javascript:void(0)' class='FollowThisUser' follow_action='follow'><i class = 'fa fa-check'></i> Follow</a>";
                    }
                    division += "</div>\
                                            </div>\
                                        </div>";
                    temp_this.parent('h5').siblings('div.res_top_name').html('');
                    temp_this.parent('h5').siblings('div.res_top_name').append(division).fadeIn();
                }
            }
        });
    });

    // When clicked on follow button inside the user details popup
    $('body').on('click', '.FollowThisUser', function () {
        var ele = $(this);
        var user_id = ele.parents('.res_top_name').siblings('h5').children('a.top_name').attr('uid');
        var follow_action = ele.attr('follow_action');
        $.ajax({
            type: "POST",
            url: site_url + "user/members_list/FollowUser_Method",
            data: {
                user_id: user_id,
                follow_action: follow_action,
            },
            success: function (data) {
                if (data.trim() == 1) {
                    if (follow_action == 'follow') {
                        ele.attr('follow_action', 'unfollow');
                        var temp = '<i class="fa fa-times"></i>';
                        ele.html(temp + ' Unfollow');
                    }
                    else if (follow_action == 'unfollow') {
                        ele.attr('follow_action', 'follow');
                        var temp = '<i class="fa fa-check"></i>';
                        ele.html(temp + ' Follow');
                    }
                }
            }
        });
    });

    $('body').on('click', '.alerts_li', function () {
        $('.alerts_popup').slideToggle();
    });

    $('body').on('click', '.activities a', function () {
        var ele = $(this);
        var post_id = $(this).parents('div.activities').attr('id');
        var group_type = $(this).parents('div.activities').attr('group');
        var type = $(this).parents('div.activities').attr('type');

        $.ajax({
            type: "POST",
            url: site_url + "user/notification/MarkAsRead_Method",
            data: {
                post_id: post_id,
                group_type: group_type,
                type: type
            },
            success: function (data) {
//                    console.log(data);
                if (data.trim() == '1') {

                }
            }
        });
    });

    /********* End of Mahadev code ********/

    /*.......................beginssssss anjali code................. */
    $('html').on('dblclick', 'body:not(.new_notify_box,.poll_text)', function () {
        $('.notify_pop,.praise_notify_pop').css('display', 'none');
    });
    /*.......................endddddd anjali code................. */
});