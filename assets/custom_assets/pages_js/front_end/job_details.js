$("body").on("keyup", ".comment_reply_textarea", function () {
    var temp_this = $(this);
    var this_value = temp_this.val();
    if (this_value != "") {
        temp_this.parents().eq(0).siblings(".commet_reply_submit_button_div").find(".comment_reply_submit_btn").addClass("active_post_btn");
    } else {
        temp_this.parents().eq(0).siblings(".commet_reply_submit_button_div").find(".comment_reply_submit_btn").removeClass("active_post_btn");
    }
});

$("body").on("submit", ".comment_reply_form", function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var reply_text = temp_this.find(".comment_reply_textarea").val();
    var reply_count = temp_this.parents().eq(2).find(".comment_count_span").text();

    if (reply_text != '') {
        var formData = temp_this.serialize();
        $.ajax({
            type: "POST",
            url: site_url + "user/jobopening/save_reply_comment",
            data: formData,
            success: function (data) {
                if (data.trim() != "0") {
                    reply_count++;
                    if (reply_count == 1) {
                        temp_this.parents().eq(2).find(".comments_reply_container").css("display", "block");
                    }
                    temp_this.parents().eq(2).find(".comment_reply_list").prepend(data);
                    temp_this[0].reset();
                    temp_this.parent().css("display", "none");
                    temp_this.find(".comment_reply_submit_btn").removeClass('active_post_btn');
                    temp_this.parents().eq(2).find(".comment_count_span").text(reply_count);
                }
            }
        });
    }
});

$("body").on("click", ".comment_reply_delete_btn", function () {
    var temp_this = $(this);
    var job_id = temp_this.attr("job_id");
    var comment_id = temp_this.attr("comment_id");
    var comment_reply_id = temp_this.attr("comment_reply_id");
    var comment_reply_count = temp_this.parents().eq(9).find(".comment_count_span").text();

    var confirm_check = confirm("Are you sure ?");
    if (confirm_check) {
        $.ajax({
            type: "POST",
            url: site_url + "user/jobopening/comment_reply_delete",
            data: {
                job_id: job_id,
                comment_id: comment_id,
                comment_reply_id: comment_reply_id
            },
            success: function (data) {
                if (data.trim() == "1") {
                    comment_reply_count--;
                    if (comment_reply_count) {
                        temp_this.parents().eq(9).find(".comment_count_span").text(comment_reply_count);
                    } else {
                        temp_this.parents().eq(9).find(".comment_count_span").text(comment_reply_count);
                        temp_this.parents().eq(10).css("display", "none");
                    }
                    temp_this.parents().eq(7).remove();
                }
            }
        });
    }
});

/* Reply comment button */
$("body").on("click", ".reply_comment_btn", function () {
    var temp_this = $(this);
    temp_this.parents().eq(3).siblings(".comment_reply_box").css("display", "block");
});

/* cancel reply div */
$("body").on("click", ".cancel_reply_to_comment", function () {
    var temp_this = $(this);
    temp_this.parents().eq(3)[0].reset();
    temp_this.parents().eq(4).css("display", "none");
});

$("#write_comment_form").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var formData = temp_this.serialize();
    if (post_count) {
        $.ajax({
            type: "POST",
            url: site_url + "user/jobopening/submit_comment",
            data: formData,
            success: function (data) {
                if (data.trim() != "0") {
                    var comment_count = $("#comment_count_span").text();
                    $("#comment_count_span").text(parseInt(comment_count) + 1);
                    $("#comments_div").prepend(data);
                    temp_this[0].reset();
                    $("#comment_box").css("display", "none");
                }
            }
        });
    }
});
$("body").on("click", ".delete_post_comment", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr('comment_id');
    var confirmation_result = confirm("Are you sure?");
    if (confirmation_result) {
        $.ajax({
            type: "POST",
            url: site_url + "user/jobopening/delete_job_comment",
            data: {
                comment_id: comment_id
            },
            success: function (data) {
                if (data.trim() == '1') {
                    var comment_count = $("#comment_count_span").text();
                    $("#comment_count_span").text(parseInt(comment_count) - 1);
                    temp_this.parents().eq(4).remove();
                }
            }
        });
    }
});
$("body").on("click", ".comment_like_btn", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr("comment_id");
    var job_id = temp_this.attr("job_id");
    var user_like_status = temp_this.attr("like_status");
    var like_count = temp_this.siblings('span').children('.comment_count_like_span').text(); //comment_count_like_span
    $.ajax({
        type: "post",
        url: site_url + "user/jobopening/comment_like",
        data: {
            comment_id: comment_id,
            job_id: job_id,
            user_like_status: user_like_status
        },
        success: function (data) {
            if (data.trim() == "1") {
                if (user_like_status == "0") {
                    var temp_html = '<i class="fa fa-thumbs-down"></i> Unlike';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "1");
                    like_count++;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count > 0) {
                        temp_this.siblings('span').css("display", "inline");
                    }
                } else {
                    var temp_html = '<i class="fa fa-thumbs-up"></i> Like';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "0");
                    like_count--;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count < 1) {
                        temp_this.siblings('span').css("display", "none");
                    }
                }
            }
        }
    });
});
$(".like_post_btn").click(function () {
    var temp_this = $(this);
    var job_id = temp_this.attr("job_id");
    var user_like_status = temp_this.attr("like_status");
    var like_count = temp_this.siblings('span').children('.comment_count_like_span').text(); //comment_count_like_span

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/like_job_commet",
        data: {
            job_id: job_id,
            user_like_status: user_like_status
        },
        success: function (data) {
            if (data.trim() == "1") {
                if (user_like_status == "0") {
                    var temp_html = '<i class="fa fa-thumbs-down"></i> Unlike';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "1");
                    like_count++;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count > 0) {
                        temp_this.siblings('span').css("display", "inline");
                    }
                } else {
                    var temp_html = '<i class="fa fa-thumbs-up"></i> Like';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "0");
                    like_count--;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count < 1) {
                        temp_this.siblings('span').css("display", "none");
                    }
                }
            }
        }
    });
});
$("#write_comment_btn").click(function () {
    $("#comment_box").css("display", "block");
});
$("#cancel_comment").click(function () {
    $("#write_comment_form")[0].reset();
    $("#comment_box").css("display", "none");
});

$("body").on("click", ".edit_post_comment", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr("comment_id");

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/get_job_comment_details",
        dataType: 'JSON',
        data: {
            comment_id: comment_id
        },
        success: function (data) {
            if (data != "0") {
                tinyMCE.get('jobCommentEditModalTextarea').setContent(data.comment);
                $("#jobCommentEditModalText").val(data.comment);
                $("#jobCommentIdEditModal").val(comment_id);
                $("#editJobCommentModal").modal("show");
            }
        }
    });
});

$("#editJobCommentModalForm").submit(function (e) {
    e.preventDefault();

    var temp_this = $(this);
    var formData = temp_this.serialize();
    var job_comment = $("#jobCommentEditModalText").val();
    var job_comment_id = $("#jobCommentIdEditModal").val();

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/update_job_comment_form",
        data: formData,
        success: function (data) {
            if (data.trim() == '1') {
                $("#job_comment-" + job_comment_id).html(job_comment);
                $("#editJobCommentModal").modal("hide");
            }
        }
    });
});

$("body").on("click", ".comment_reply_edit_btn", function () {
    var temp_this = $(this);
    var comment_reply_id = temp_this.attr("comment_reply_id");

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/get_job_comment_reply_details",
        dataType: 'JSON',
        data: {
            comment_reply_id: comment_reply_id
        },
        success: function (data) {
            if (data != "0") {
                tinyMCE.get('jobCommentReplyEditModalTextarea').setContent(data.comment_reply);
                $("#jobCommentReplyEditModalText").val(data.comment_reply);
                $("#jobCommentReplyIdEditModal").val(comment_reply_id);
                $("#editJobCommentReplyModal").modal("show");
            }
        }
    });
});

$("#editJobCommentReplyModalForm").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var formData = temp_this.serialize();

    var job_comment_reply = $("#jobCommentReplyEditModalText").val();
    var job_comment_reply_id = $("#jobCommentReplyIdEditModal").val();

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/update_job_comment_reply_form",
        data: formData,
        success: function (data) {
            if (data.trim() == '1') {
                $("#job_comment_reply-" + job_comment_reply_id).html(job_comment_reply);
                $("#editJobCommentReplyModal").modal("hide");
            }
        }
    });
});

$("body").on("click", ".edit_post", function () {
    var temp_this = $(this);
    var job_id = temp_this.attr("post_id");
    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/get_job_content",
        dataType: 'JSON',
        data: {
            job_id: job_id
        },
        success: function (data) {
            $('#jobTitleEditModalText').val(data.title);
            tinyMCE.get('jobDescriptionEditModalTextarea').setContent(data.description);
            $('#jobDescriptionEditModalText').val(data.description);
            $('#jobIdEditModalText').val(job_id);
            $("#editJobModal").modal("show");
        }
    });
});

$("#editJobModalForm").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var job_title = $('#jobTitleEditModalText').val();
    var job_description = $('#jobDescriptionEditModalText').val();
    var formData = temp_this.serialize();

    $.ajax({
        type: "POST",
        url: site_url + "user/jobopening/update_job_form",
        data: formData,
        success: function (data) {
            if (data.trim() == "1") {
                $("#job_title").html(job_title);
                $("#job_description").html(job_description);
                temp_this[0].reset();
                $("#editJobModal").modal("hide");
            }
        }
    });
});