
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var SubmitEmail = 1;
var email_unique_status = 1;
$(document).ready(function () {
    $('body').on('submit', '#ChangeEmail_Form', function (e) {
        e.preventDefault();
        var formdata = $(this).serialize();
        var email = $('input[name=email]').val();
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            SubmitEmail = 1;
        } else {
            SubmitEmail = 0;
            $('.email_error_msg').text("Incorrect email.").show();
        }

        if (!email_unique_status) {
            e.preventDefault();
            $(".email_error_msg").text("Email alreay exist, try other.");
            SubmitEmail = 0;
        }

        if (SubmitEmail == 1) {
            $.ajax({
                type: "POST",
                url: site_url + "user/settings/ChangeEmail_Method",
                data: formdata,
                success: function (data) {
                    console.log(data);
                    if (data.trim() == "1") {
                        $('#ChangeEmail_Modal').modal('hide');
                    }
                }
            });
        }
    });

    $('body').on('focus', '#ChangeEmail_Form input', function (e) {
        if ($(this).val() == "")
            $('.email_error_msg').hide();
    });

    $('body').on("keyup", "#ChangeEmail_Form #email", function () {
        var emailAddress = $(this).val();
        $.ajax({
            type: "POST",
            url: site_url + "user/settings/check_unique_email",
            data: {
                email: emailAddress
            },
            success: function (data) {
                if (data.trim() == "0")     {
                    email_unique_status = 0;
                    $("#ChangeEmail_Modal .email_error_msg").text("Email alreay exist, Try other.").show();
                } else {
                    $("#ChangeEmail_Modal .email_error_msg").text("");
                    email_unique_status = 1;
                }
            }
        });
    });

    $(".date_text").datepicker({
        changeYear: true,
        maxDate: 0,
        minDate: "-50y",
        dateFormat: 'dd-mm-yy',
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });

    /***** Change profile picture on selecting ****/
    $('body').on("click", ".profile_class", function () {
        $(this).parent("div").children("#profile_upload").trigger("click");
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#default_image').attr('src', e.target.result);
                $('#default_image').css('width', '120px');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile_upload").change(function () {
        readURL(this);
    });
    /**** End of profile picture change ****/


    var phone_unique_status = 1;
    $('body').on("submit", "#ProfileSettings_Form", function (e) {
        var Upadate_flag = 1;
        e.preventDefault();

        var f_name = $("#ProfileSettings_Form .fname").val();
        var l_name = $("#ProfileSettings_Form .lname").val();
        var location = $("#ProfileSettings_Form .location").val();
        var phone = $("#ProfileSettings_Form .phone").val();
        var dob = $("#ProfileSettings_Form .dob").val();

        var re = /^[a-z ,.'-]+$/i;
        if (!re.test(f_name)) {
            Upadate_flag = 0;
            $("#ProfileSettings_Form .fname").siblings('span.profile_error_msg').text('Invalid name.').show();
        }

        var re = /^[a-z ,.'-]+$/i;
        if (!re.test(l_name)) {
            Upadate_flag = 0;
            $("#ProfileSettings_Form .lname").siblings('span.profile_error_msg').text('Invalid name.').show();
        }

        var re = /^[a-z ,.'-]+$/i;
        if (location != "") {
            if (!re.test(location)) {
                Upadate_flag = 0;
                $("#ProfileSettings_Form .location").siblings('span.profile_error_msg').text('Invalid location.').show();
            }
        }

        var re = /^[0-9]{10}$/;
        if (phone != "") {
            if (!re.test(phone)) {
                Upadate_flag = 0;
                $("#ProfileSettings_Form .phone").siblings('span.profile_error_msg').text('Invalid mobile.').show();
            }
        }

        if (!phone_unique_status) {
            e.preventDefault();
            $("#ProfileSettings_Form .phone").siblings('span.profile_error_msg').text('Phone alreay exist, try other.').show();
            Upadate_flag = 0;
        }

        if (Upadate_flag == 1) {
            $.ajax({
                type: "POST",
                url: site_url + "user/settings/UpadateSettings_Method",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    if (data.trim() == "1") {
                        alert("Settings updated successfully.");
                    }
                }
            });
        }
    });

    $(".phone").keyup(function () {
        var mobile = $(this).val();
        $.ajax({
            type: "POST",
            url: site_url + "user/settings/check_unique_phone",
            data: {
                mobile: mobile
            },
            success: function (data) {
                if (data.trim() == "0") {
                    phone_unique_status = 0;
                    $("#ProfileSettings_Form .phone").siblings('span.profile_error_msg').text('Phone alreay exist, try other.').show();
                } else {
                    phone_unique_status = 1;
                    $("#ProfileSettings_Form .phone").siblings('span.profile_error_msg').text('').show();
                }
            }
        });
    });

    $('body').on("focus", "#ProfileSettings_Form input", function () {
        $(this).siblings('span.profile_error_msg').text('').hide();
    });

    var Change_flag = 1;
    $('body').on("submit", "#ChangePassword_Form", function (e) {
        e.preventDefault();
        var old_password = $("#ChangePassword_Form .old_password").val();
        var new_password = $("#ChangePassword_Form .new_password").val();
        var confirm_password = $("#ChangePassword_Form .confirm_password").val();

        var re = /^[0-9]{6,}$/;
        if (old_password != "") {
            Change_flag = 1;
        } else {
            Change_flag = 0;
            $("#ChangePassword_Form .old_password").siblings('span.profile_error_msg').text('Invalid password').show();
        }
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (new_password != "" && re.test(new_password)) {
            Change_flag = 1;
        } else {
            Change_flag = 0;
            $("#ChangePassword_Form .new_password").siblings('span.profile_error_msg').text('Invalid password. (min 8 characters,1 Alphabet and 1 Number needed.)').show();
        }
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (confirm_password != "" && re.test(confirm_password)) {
            Change_flag = 1;
        } else {
            Change_flag = 0;
            $("#ChangePassword_Form .confirm_password").siblings('span.profile_error_msg').text('Invalid password. (min 8 characters,1 Alphabet and 1 Number needed.)').show();
        }

        if (new_password == confirm_password) {
            if (Change_flag == 1) {
                var FormData = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: site_url + "user/settings/UpadatePassword_Method",
                    data: FormData,
                    success: function (data) {
                        alert(data);
                    }
                });
            }
        } else {
            $("#ChangePassword_Form .confirm_password").siblings('span.profile_error_msg').text('Passwords do not match.').show();
        }
    });

    $('body').on("focus", "#ChangePassword_Form input", function () {
        $(this).siblings('span.profile_error_msg').text('').hide();
    });

});

