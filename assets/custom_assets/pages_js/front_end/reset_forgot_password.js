$(document).ready(function () {
    var Change_flag = 1;
    $('body').on("submit", "#reset_password_form", function (e) {
        e.preventDefault();
        var password = $("#reset_password_form #password").val();
        var confirm_password = $("#reset_password_form #confirm_password").val();
        var uid = $("#reset_password_form #uid").val();
        var code = $("#reset_password_form #cid").val();

        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (password != "" && re.test(password)) {
            Change_flag = 1;
        } else {
            Change_flag = 0;
            $("#reset_password_form .password").parent('div').siblings('.error_msg').text('Invalid password. (min 8 characters,1 Alphabet and 1 Number needed.)').show();
        }
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (confirm_password != "" && re.test(confirm_password)) {
            Change_flag = 1;
        } else {
            Change_flag = 0;
            $("#reset_password_form .confirm_password").parent('div').siblings('.error_msg').text('Invalid password. (min 8 characters,1 Alphabet and 1 Number needed.)').show();
        }

        if (password == confirm_password) {
            if (Change_flag == 1) {
                $.ajax({
                    type: "POST",
                    url: site_url + "forgot/UpdatePassword_Method",
                    data: {
                        uid: uid,
                        code: code,
                        password: password,
                        confirm_password: confirm_password
                    },
                    success: function (data) {
//                        console.log(data);
                        if (data.trim() == 1) {
                            $('.success_alert').fadeIn();
                            setTimeout(function () {
                                window.location = site_url;
                            }, 4000);
                        } else {
                            $('.danger_alert').fadeIn();
                            setTimeout(function () {
                                $('.danger_alert').fadeOut();
                            }, 3000);
                        }
                    }
                });
            }
        } else {
            $("#reset_password_form .confirm_password").parent('div').siblings('.error_msg').text('Passwords do not match.').show();
        }
    });

    $('body').on("focus", "#reset_password_form input", function () {
        $(this).parent('div').siblings('.error_msg').text('').hide();
    });

});