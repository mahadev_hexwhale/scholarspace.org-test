var group = $("#group").val();
var current_GroupMembersList = [];
var praise_AddedMembersList = [];
var membersForReplyCC = [];
var TimerLimit = 60;

$("#group").remove();

$(document).ready(function () {
    $('body').on("click", ".expand_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.truncated_body').css("display", "none");
        temp_this.parent('.truncated_body').siblings('.complete_body').css("display", "block");
    });
    $('body').on("click", ".collapse_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.complete_body').css("display", "none");
        temp_this.parent('.complete_body').siblings('.truncated_body').css("display", "block");
    });
    
    $('body').click(function () {
        var category = $(this).parents('.category').attr('id');
        $('#' + category + ' .notify_pop').hide();
    });


    /* Group function */
    $("body").on("click", ".comment_in1", function () {
        $(this).siblings(".more_share").css('display', 'block');
        $(this).siblings(".more_share").children(".reply_form").children("div").children(".reply_textarea").focus();
        $(this).css('display', 'none');
    });
    $("body").on("click", ".click_more", function () {
        var temp_this = $(this);
        if (temp_this.hasClass('active_more')) {
            $(this).siblings('.more_list').hide();
            $(this).removeClass("active_more");
        } else {
            $('.more_list').hide();
            $(this).siblings('.more_list').show();
            $(".click_more").removeClass("active_more");
            temp_this.addClass("active_more");
        }
    });

    //when clicked on like for a post
    $("body").on("click", ".post_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/like_post",
            data: {
                post_id: post_id,
                group: group,
                like_action: like_action
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_like_body-" + post_id).children('.current_user_like').text("You ");
                            $("#post_like_div-" + post_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_like_div-" + post_id).css("display", "none");
                            $("#post_like_body-" + post_id).children('.current_user_like').text("");
                        }
                    }
                }
            }
        });
    });

    var submitReply = 1;
    $("body").on("keyup", ".reply_textarea", function () {
        var temp_this = $(this);
        if ($(this).val() == "") {
            $(temp_this).parents('div.text_poll2').siblings('button.reply_post_button').prop('disabled', true);
            submitReply = 0;
        } else {
            $(temp_this).parents('div.text_poll2').siblings('button.reply_post_button').prop('disabled', false);
            submitReply = 1;
        }
    });

    /************ When the link reply is pressed **************/
    $("body").on("click", ".reply_link", function (e) {
        $(this).parents('div.post_action_div').siblings('div.commenting_box').children('div.reply_box').children('div.comment_in1').click();
    });

    // reply for a post
    $("body").on("submit", ".reply_form", function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var post_id = $(this).parents('.reply_box').attr('id');
        var reply_message = temp_this.children('.text_poll2').children('.reply_textarea').val();
        if (reply_message != '' && submitReply == 1) {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/save_post_reply",
                data: {
                    post_id: post_id,
                    group: group,
                    reply_message: reply_message,
                    membersForReplyCC: membersForReplyCC,
                },
                success: function (data) {
                    if (data.trim() != '0') {
                        temp_this.parents().eq(1).siblings('.all_reply').append(data);
                        temp_this[0].reset();
                        temp_this.children('.reply_post_button').removeClass('active_post_btn');
                        temp_this.parent().siblings('.comment_in1').css("display", "block");
                        temp_this.parent().css("display", "none");
                        membersForReplyCC = [];
                    }
                }
            });
        }
    });

    /*********Get the userlist for tagging in reply for post box**********/

    /*Gives the list of all memebers of scholarspace*/
    $('body').on('click', '.addCCMembersToReply', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var ListItem = "";
        if ($(this).val() == "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/GetCollegeMembers_Method",
                data: {
                    group: group,
                    current_GroupMembersList: membersForReplyCC
                },
                success: function (data) {
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (membersForReplyCC.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + post_id + ' .notify_pop').show();
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').append(ListItem);
                }
            });
        }
    });
    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '.addCCMembersToReply', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/intranet_posts/FilterCollegeMembers_Method",
                data: {
                    group: group,
                    Item: Item
                },
                success: function (data) {
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (membersForReplyCC.indexOf(value.college_users_intranet_id) == -1) {
                            ListItem += "<li intranet_id='" + value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + value.fname + " " + value.lname + "</h5><br>\
                                        <h6>" + value.stream_name + "</h6>\
                                    </li>";
                        }
                    });
                    $('#' + post_id + ' .notify_pop').show();
                    $('#' + post_id + ' .people_list .Reply_ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '.Reply_ListOfMembers li', function () {
        var post_id = $(this).parents('.reply_box').attr('id');
        var name = $(this).children('h5').html();
        var college_users_intranet_id = $(this).attr('intranet_id');
        var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company added_user'>\
                            <h6> " + name + " </h6>\
                            <span class='pop_close'></span>\
                        </div>";
        $('#' + post_id + ' .AddedCCMembersOfReply').append(AddedItem);
        $('#' + post_id + ' .addCCMembersToReply').val('');
        $('#' + post_id + ' .notify_pop').hide();
        membersForReplyCC.push($(this).attr('intranet_id'));
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '.pop_close', function () {
        var intranet_id = $(this).parents('div.company').attr('intranet_id');
        var index = membersForReplyCC.indexOf(intranet_id);
        if (index > -1) {
            membersForReplyCC.splice(index, 1);
        }
        $(this).parents('div.company').remove();
    });

    // Submit vote for a answer
    $('body').on('submit', '.poll_answer_form', function (e) {
        var ele = $(this).parents('.post_type_div').attr('id');
        e.preventDefault();
        var post_id = $(this).parents('div.post_content_div').attr('id');
        var poll_id = $(this).parents('div.post_type_div').attr('id');
        var formData = $(this).serialize() + "&group=" + group + "&post_id=" + post_id + "&poll_id=" + poll_id;

        if ($(this).serialize() != "" && group != "") {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/VoteForPoll_Method",
                data: formData,
                success: function (data) {
//                    console.log(data);
                    if (data != 0) {
                        $('div.poll_answer_div').html('');
                        $('div.poll_answer_div').append(data);
                        var counter = setInterval(timer, 1000); //1000 will  run it every 1 second
                        function timer()
                        {
                            TimerLimit = TimerLimit - 1;
                            if (TimerLimit <= 0)
                            {
                                clearInterval(counter);
                                $('#' + ele + ' .change_vote_button').remove();
                                return;
                            }
                        }
                    }
                }
            });
        }
    });


    $('body').on('click', '.change_vote_button', function (e) {
        e.preventDefault();
        var post_id = $(this).parents('div.post_content_div').attr('id');
        var poll_id = $(this).parents('div.post_type_div').attr('id');

        if (poll_id != "" && group != "" && post_id != "") {
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/ChangeVote_Method",
                data: {
                    group: group,
                    post_id: post_id,
                    poll_id: poll_id,
                },
                success: function (data) {
                    if (data != 0) {
                        $('div.poll_answer_div').html('');
                        $('div.poll_answer_div').append(data);
                    }
                }
            });
        }
    });

    //post reply like
    $("body").on("click", ".post_reply_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var post_reply_id = temp_this.parent().attr("post_reply_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/like_post_reply",
            data: {
                post_id: post_id,
                post_reply_id: post_reply_id,
                like_action: like_action
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("You ");
                            $("#post_reply_like_div-" + post_reply_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_reply_like_div-" + post_reply_id).css("display", "none");
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("");
                        }
                    }
                }
            }
        });
    });

    //delete_post
    $("body").on("click", ".delete_post", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_id = temp_this.parents().eq(2).attr("post_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/delete_post",
                data: {
                    group: group,
                    post_id: post_id
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The post has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });

    //delete post reply
    $("body").on("click", ".delete_post_reply", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_reply_id = temp_this.parents().eq(2).attr("post_reply_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/intranet_posts/delete_post_reply",
                data: {
                    group: group,
                    post_reply_id: post_reply_id
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The message has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });

    // When clicked on follow or unfollow for a post
    $("body").on("click", ".post_follow", function () {
        var ele = $(this);
        var post_id = ele.parents('div.post_content_div').attr('id');
        var follow_action = ele.attr('follow_action');

        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/FollowPost_Method",
            data: {
                group: group,
                post_id: post_id,
                follow_action: follow_action
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() == 1) {
                    if (follow_action == "follow") {
                        ele.attr('follow_action', 'unfollow');
                        ele.html('Unfollow');
                    } else if (follow_action == "unfollow") {
                        ele.attr('follow_action', 'follow');
                        ele.html('Follow');
                    }
                }
            }
        });
    });


    /**
     * Code for filtering the posts
     * having 3 filters
     * All, Top, Following
     **/

    var All_flag = 0;
    var Top_flag = 0;
    var Following_flag = 0;
    $('body').on('click', '.PostFilter', function (e) {
        e.preventDefault();
        var ele = $(this);
        var filter = ele.attr("id");

        if (filter == "allPosts") {
            FilterPosts_Method(filter);
            All_flag++;
        } else if (filter == "topPosts") {
            FilterPosts_Method(filter);
            Top_flag++;
        } else if (filter == "followingPosts") {
            FilterPosts_Method(filter);
            Following_flag++;
        }
    });

    function FilterPosts_Method(filter) {
        $.ajax({
            type: "POST",
            url: site_url + "user/intranet_posts/FilterPosts_Method",
            data: {
                group: group,
                filter: filter
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() != 0) {
                    if (filter == "allPosts") {
                        if (data != "") {
                            $('#all_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#all_tab #all_post_content').html(data);
                        } else {
                            $('#all_tab #all_post_content').html("");
                        }
                    } else if (filter == "topPosts") {
                        if (data != "") {
                            $('#top_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#top_tab #all_post_content').html(data);
                        } else {
                            $('#top_tab #all_post_content').html("");
                        }
                    } else if (filter == "followingPosts") {
                        if (data != "") {
                            $('#follow_tab').html("<div id='all_post_content' class='global_details'>");
                            $('#follow_tab #all_post_content').html(data);
                        } else {
                            $('#follow_tab #all_post_content').html("");
                        }
                    }
                }
            }
        });
    }
    /*** Filter posts section ends ***/

});