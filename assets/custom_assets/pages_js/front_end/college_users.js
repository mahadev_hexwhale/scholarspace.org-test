
$(document).ready(function () {
    $('.DeleteUserFromCollege_Button').click(function () {
        var ele = $(this);
        var user_id = ele.attr('user_id');
        var intranet_id = ele.attr('intranet_id');
        bootbox.confirm('Are you sure you want to Delete this?', function (ok) {
            if (ok) {
                if (user_id != "" && intranet_id != "") {
                    $.ajax({
                        type: "POST",
                        url: site_url + "user/members_list/DeleteMemberFromCollege_Method",
                        data: {
                            user_id: user_id,
                            intranet_id: intranet_id
                        },
                        success: function (data) {
                            if (data == '1') {
                                ele.parents('div.people_page_info').fadeOut();
                                $('body').animate({scrollTop: 0}, 'slow');
                                $('.success_alert').fadeIn();
                                setTimeout(function () {
                                    $('.success_alert').fadeOut();
                                }, 2500);
                            } else {
                                $('.danger_alert').fadeIn();
                                setTimeout(function () {
                                    $('.danger_alert').fadeOut();
                                }, 2500);
                            }

                        }
                    });
                }
            }
        });
    });

    $('body').on('click', '.FollowUser_Button', function () {
        var ele = $(this);
        var user_id = ele.attr('user_id');
        var follow_action = ele.attr('follow_action');

        $.ajax({
            type: "POST",
            url: site_url + "user/members_list/FollowUser_Method",
            data: {
                user_id: user_id,
                follow_action: follow_action,
            },
            success: function (data) {
                if (data.trim() == 1) {
                    if (follow_action == 'follow') {
                        ele.attr('follow_action', 'unfollow');
                        var temp = '<i class="fa fa-times"></i>';
                        ele.html(temp + ' Unfollow');
                    }
                    else if (follow_action == 'unfollow') {
                        ele.attr('follow_action', 'follow');
                        var temp = '<i class="fa fa-check"></i>';
                        ele.html(temp + ' Follow');
                    }
                }
            }
        });
    });


});