$(document).ready(function () {
//    $('body').on("click", ".fees_year_head", function () {
//        var temp_this = $(this);
//        if (temp_this.siblings('.fees_year_body').hasClass('fees_body_active')) {
//            temp_this.siblings('.fees_year_body').slideUp('slow');
//            temp_this.siblings('.fees_year_body').removeClass('fees_body_active');
//        } else {
//            $('.fees_year_body').slideUp();
//            $('.fees_year_body').removeClass('fees_body_active');
//            temp_this.siblings('.fees_year_body').addClass('fees_body_active');
//            temp_this.siblings('.fees_year_body').slideDown('slow');
//        }
//    });
    //addNewFeesModal
//    $("#addNewFeesBtn").click(function () {
//        $("#addNewFeesModal").modal("show");
//    });
    $(".edit_fees").click(function () {
        var temp_this = $(this);

        var stream_id = temp_this.parents().eq(1).attr('stream');
        var stream_course_id = temp_this.parents().eq(1).attr('stream_course');

        var course_name = temp_this.parent('td').siblings('.course_name').text();
        var first_year_fee = temp_this.parent('td').siblings('.first_year').attr('fee');
        var second_year_fee = temp_this.parent('td').siblings('.second_year').attr('fee');
        var third_year_fee = temp_this.parent('td').siblings('.third_year').attr('fee');
        var forth_year_fee = temp_this.parent('td').siblings('.forth_year').attr('fee');

        $("#streamHidden").val(stream_id);
        $("#streamCourseHidden").val(stream_course_id);
        $("#courseNameModal").text(course_name);
        $("#firstYearFeeText").val(first_year_fee);
        $("#secondYearFeeText").val(second_year_fee);
        $("#thirdYearFeeText").val(third_year_fee);
        $("#forthYearFeeText").val(forth_year_fee);

        $("#editFeesModal").modal("show");
    });

    $("#editFeesModalForm").submit(function (e) {
        e.preventDefault();
        var formData = $(this).serialize();

        $.ajax({
            type: "POST",
            url: site_url + "user/fees/edit_fees_submit",
            data: formData,
            success: function (data) {
                alert(data);
            }
        });
    });
});