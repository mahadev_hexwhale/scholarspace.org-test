
var chat_check_counter = 0;
var stop_scroll = 1;
var content = document.querySelector('body');
var controller = $('#controller_name').val();
var site_url = $('#site_url').val();
var group_members = new Array();

//$('#controller_name').remove();

var lastMessageTimestamp;

var AnswerStatus = 1;
var QuestionStatus = 1;
var SameAnswerStatus = 1;
var AnswerArray = [];
var current_GroupMembersList = [];
var praise_AddedMembersList = [];
var SubmitUpdate = 1;
var SubmitPoll = 1;
var SubmitPraise = 1;
var SubmitAnnounce = 1;
var membersForReplyCC = [];
var TimerLimit = 60;

$('#controller').remove();


$(document).ready(function () {

    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '.create_chat_group .addGroupMemebr', function() {

        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/"+ controller+"/displayChatUserList",
                data: {
                    Item: Item
                },
                success: function(data) {
                    $('.people_list .ListOfMembers_home').html("");
                    $.each(data, function(key, value) {
                        if (current_GroupMembersList.indexOf(value.id) == -1) {
                            ListItem += "<li user_id='" + value.id + "'>\
                                         <h5>" + value.first_name + " " + value.last_name + "</h5><br>\
                                        </li>";
                        }
                    });
                    $('.create_chat_group .ListOfMembers_home').append(ListItem);
                    $('.create_chat_group .notify_pop').show();
                }
            });
        }
    });


    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '.create_chat_group .ListOfMembers_home li', function() {
        var user_id = $(this).attr('user_id');
        var name = $(this).children('h5').html();
        var AddedItem = "<div user_id='" + user_id + "' class = 'company added_user'>\
                            <h6> " + name + " </h6>\
                            <span class='pop_close'></span>\
                        </div>";
        $('.create_chat_group .AddedMembers').append(AddedItem);
        $('.create_chat_group .ListOfMembers_home').val("");
        $('.create_chat_group .notify_pop').hide();
        current_GroupMembersList.push(user_id);
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '.create_chat_group .pop_close', function() {
        var user_id = $(this).parents('.added_user').attr('user_id');
        var index = current_GroupMembersList.indexOf(user_id);
        if (index > -1) {
            current_GroupMembersList.splice(index, 1);
        }
    });
    $('body').click(function() {
        var category = $(this).parents('.category').attr('id');
        $('#' + category + ' .notify_pop').hide();
    });






//Code to exectue When user click create group button
    $('body').on('click', '.pp_submit', function() {
        var group_name = $('.modal-body li #group_name').val();
        var chatGroupMember = new Array();
        for (var i = 0; i < current_GroupMembersList.length; i++) {
            if (current_GroupMembersList[i]) {
                chatGroupMember.push(current_GroupMembersList[i]);
            }
        }
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/"+ controller+"/createChatGroup",
            data: {
                groupName: group_name,
                chatGroupMember: chatGroupMember,
            },

            success: function(data) {
                console.log(data);
                $('#collegeChatgroupModel').modal('hide');
                bootbox.alert("Group successfully sent");                

            }
        });

    });


    // When user mouse over thre group name
    $('body').on('mousein', '.group_chat_id', function() {
        var group_chat_id = $(this).attr('id');
        var group_name = "";
        //  var group_members = new Array();
        var groupMember = "";

        //code to get the group details.
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller + "/getChatGroupDetails",
            data: {
                groupChatId: group_chat_id
            },
            success: function(data) {
                $('.groupDetails .pop_user_info ul').html("");
                $('.groupDetails h4').html("");
                group_name = data.group_name;
                //To change the first letter  in capital.
                group_name = group_name.charAt(0).toUpperCase() + group_name.slice(1);
                group_members = data.group_members;


                $('.groupDetails h4').append(group_name);
                $.each(group_members, function(key, value) {
                    groupMember += "<li>\
                                    <p>" + value + "</p>\
                                 </li>";
                });
                $('.groupDetails .pop_user_info ul').append(groupMember);

            }
        });

    });
    
  
    //Code to show all chat group.
     $('.chat-groups ').on('click', '#GroupList #ShowAll', function() {
        var groupMember="" ;
        
        //code to get the all group.
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller + "/showAllGroup",
     
            success: function(data) {
               $('.chat-groups #GroupList ul').html("");
               $.each(data.chat_group, function(key, value) {
                    groupMember += "<input type=\"hidden\" name=\"list_of_chat_member \" value=\"\">\
                                    <li class=\"group_chat_id\" id="+value.group_chat_id+ ">"+value.group_name+ "</li>";
                });
                $.each(data.college_group, function(key, value) {
                    groupMember += "<input type=\"hidden\" name=\"list_of_chat_member \" value=\"\">\
                                    <li class=\"group_chat_id\" id="+value.id+ ">"+value.name+ "</li>";
                });
                $('.chat-groups #GroupList ul').append(groupMember);
                $('.chat-groups #GroupList ul').append('<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#GroupList">'+ "Hide All" +'</button>');

            }
        });

    });
   
});