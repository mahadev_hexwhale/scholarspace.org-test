$("#write_comment_form").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var formData = temp_this.serialize();
    if (post_count) {
        $.ajax({
            type: "POST",
            url: site_url + "user/internship/submit_comment",
            data: formData,
            success: function (data) {
                if (data.trim() != "0") {
                    var comment_count = $("#comment_count_span").text();
                    $("#comment_count_span").text(parseInt(comment_count) + 1);
                    $("#comments_div").prepend(data);
                    temp_this[0].reset();
                    $("#comment_box").css("display", "none");
                }
            }
        });
    }
});
$("body").on("click", ".delete_post_comment", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr('comment_id');
    var confirmation_result = confirm("Are you sure?");
    if (confirmation_result) {
        $.ajax({
            type: "POST",
            url: site_url + "user/internship/delete_internship_comment",
            data: {
                comment_id: comment_id
            },
            success: function (data) {
                if (data.trim() == '1') {
                    var comment_count = $("#comment_count_span").text();
                    $("#comment_count_span").text(parseInt(comment_count) - 1);
                    temp_this.parents().eq(4).remove();
                }
            }
        });
    }
});

$("body").on("click", ".comment_like_btn", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr("comment_id");
    var internship_id = temp_this.attr("internship_id");
    var user_like_status = temp_this.attr("like_status");
    var like_count = temp_this.siblings('span').children('.comment_count_like_span').text(); //comment_count_like_span
    $.ajax({
        type: "post",
        url: site_url + "user/internship/comment_like",
        data: {
            comment_id: comment_id,
            internship_id: internship_id,
            user_like_status: user_like_status
        },
        success: function (data) {
            if (data.trim() == "1") {
                if (user_like_status == "0") {
                    var temp_html = '<i class="fa fa-thumbs-down"></i> Unlike';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "1");
                    like_count++;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count > 0) {
                        temp_this.siblings('span').css("display", "inline");
                    }
                } else {
                    var temp_html = '<i class="fa fa-thumbs-up"></i> Like';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "0");
                    like_count--;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count < 1) {
                        temp_this.siblings('span').css("display", "none");
                    }
                }
            }
        }
    });
});

$(".like_post_btn").click(function () {
    var temp_this = $(this);
    var internship_id = temp_this.attr("internship_id");
    var user_like_status = temp_this.attr("like_status");
    var like_count = temp_this.siblings('span').children('.comment_count_like_span').text(); //comment_count_like_span

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/like_internship_commet",
        data: {
            internship_id: internship_id,
            user_like_status: user_like_status
        },
        success: function (data) {
            if (data.trim() == "1") {
                if (user_like_status == "0") {
                    var temp_html = '<i class="fa fa-thumbs-down"></i> Unlike';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "1");
                    like_count++;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count > 0) {
                        temp_this.siblings('span').css("display", "inline");
                    }
                } else {
                    var temp_html = '<i class="fa fa-thumbs-up"></i> Like';
                    temp_this.html(temp_html);
                    temp_this.attr("like_status", "0");
                    like_count--;
                    temp_this.siblings('span').children('.comment_count_like_span').text(like_count);
                    if (like_count < 1) {
                        temp_this.siblings('span').css("display", "none");
                    }
                }
            }
        }
    });
});

$("body").on("click", ".comment_reply_delete_btn", function () {
    var temp_this = $(this);
    var internship_id = temp_this.attr("internship_id");
    var comment_id = temp_this.attr("comment_id");
    var comment_reply_id = temp_this.attr("comment_reply_id");
    var comment_reply_count = temp_this.parents().eq(9).find(".comment_count_span").text();

    var confirm_check = confirm("Are you sure ?");
    if (confirm_check) {
        $.ajax({
            type: "POST",
            url: site_url + "user/internship/comment_reply_delete",
            data: {
                internship_id: internship_id,
                comment_id: comment_id,
                comment_reply_id: comment_reply_id
            },
            success: function (data) {
                if (data.trim() == "1") {
                    comment_reply_count--;
                    if (comment_reply_count) {
                        temp_this.parents().eq(9).find(".comment_count_span").text(comment_reply_count);
                    } else {
                        temp_this.parents().eq(9).find(".comment_count_span").text(comment_reply_count);
                        temp_this.parents().eq(10).css("display", "none");
                    }
                    temp_this.parents().eq(7).remove();
                }
            }
        });
    }
});

$("body").on("keyup", ".comment_reply_textarea", function () {
    var temp_this = $(this);
    var this_value = temp_this.val();
    if (this_value != "") {
        temp_this.parents().eq(0).siblings(".commet_reply_submit_button_div").find(".comment_reply_submit_btn").addClass("active_post_btn");
    } else {
        temp_this.parents().eq(0).siblings(".commet_reply_submit_button_div").find(".comment_reply_submit_btn").removeClass("active_post_btn");
    }
});

$("body").on("submit", ".comment_reply_form", function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var reply_text = temp_this.find(".comment_reply_textarea").val();
    var reply_count = temp_this.parents().eq(2).find(".comment_count_span").text();

    if (reply_text != '') {
        var formData = temp_this.serialize();
        $.ajax({
            type: "POST",
            url: site_url + "user/internship/save_reply_comment",
            data: formData,
            success: function (data) {
                if (data.trim() != "0") {
                    reply_count++;
                    if (reply_count == 1) {
                        temp_this.parents().eq(2).find(".comments_reply_container").css("display", "block");
                    }
                    temp_this.parents().eq(2).find(".comment_reply_list").prepend(data);
                    temp_this[0].reset();
                    temp_this.parent().css("display", "none");
                    temp_this.find(".comment_reply_submit_btn").removeClass('active_post_btn');
                    temp_this.parents().eq(2).find(".comment_count_span").text(reply_count);
                }
            }
        });
    }
});

/* Reply comment button */
$("body").on("click", ".reply_comment_btn", function () {
    var temp_this = $(this);
    temp_this.parents().eq(3).siblings(".comment_reply_box").css("display", "block");
});

/* cancel reply div */
$("body").on("click", ".cancel_reply_to_comment", function () {
    var temp_this = $(this);
    temp_this.parents().eq(3)[0].reset();
    temp_this.parents().eq(4).css("display", "none");
});

$("#write_comment_btn").click(function () {
    $("#comment_box").css("display", "block");
});

$("#cancel_comment").click(function () {
    $("#write_comment_form")[0].reset();
    $("#comment_box").css("display", "none");
});

$("body").on("click", ".edit_post_comment", function () {
    var temp_this = $(this);
    var comment_id = temp_this.attr("comment_id");
    $.ajax({
        type: "POST",
        url: site_url + "user/internship/get_internship_comment_details",
        dataType: 'JSON',
        data: {
            comment_id: comment_id
        },
        success: function (data) {
            if (data != "0") {
                tinyMCE.get('internshipCommentEditModalTextarea').setContent(data.comment);
                $("#internshipCommentEditModalText").val(data.comment);
                $("#internshipCommentIdEditModal").val(comment_id);
                $("#editIntershipCommentModal").modal("show");
            }
        }
    });
});

$("#editInternshipCommentModalForm").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var formData = temp_this.serialize();

    var internship_comment = $("#internshipCommentEditModalText").val();
    var internship_comment_id = $("#internshipCommentIdEditModal").val();

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/update_internship_comment_form",
        data: formData,
        success: function (data) {
            if (data.trim() == '1') {
                $("#internship_comment-" + internship_comment_id).html(internship_comment);
                $("#editIntershipCommentModal").modal("hide");
            }
        }
    });
});

$("body").on("click", ".comment_reply_edit_btn", function () {
    var temp_this = $(this);
    var comment_reply_id = temp_this.attr("comment_reply_id");

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/get_internship_comment_reply_details",
        dataType: 'JSON',
        data: {
            comment_reply_id: comment_reply_id
        },
        success: function (data) {
            if (data != "0") {
                tinyMCE.get('internshipCommentReplyEditModalTextarea').setContent(data.comment_reply);
                $("#internshipCommentReplyEditModalText").val(data.comment_reply);
                $("#internshipCommentReplyIdEditModal").val(comment_reply_id);
                $("#editIntershipCommentReplyModal").modal("show");
            }
        }
    });
});

$("#editInternshipCommentReplyModalForm").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var formData = temp_this.serialize();

    var internship_comment_reply = $("#internshipCommentReplyEditModalText").val();
    var internship_comment_reply_id = $("#internshipCommentReplyIdEditModal").val();

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/update_internship_comment_reply_form",
        data: formData,
        success: function (data) {
            if (data.trim() == '1') {
                $("#internship_comment_reply-" + internship_comment_reply_id).html(internship_comment_reply);
                $("#editIntershipCommentReplyModal").modal("hide");
            }
        }
    });
});
$("body").on("click", ".edit_post", function () {
    var temp_this = $(this);
    var internship_id = temp_this.attr("post_id");

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/get_internship_content",
        dataType: 'JSON',
        data: {
            internship_id: internship_id
        },
        success: function (data) {
            $('#internshipTitleEditModalText').val(data.title);
            tinyMCE.get('internshipDescriptionEditModalTextarea').setContent(data.description);
            $('#internshipDescriptionEditModalText').val(data.description);
            $('#internshipStartDateEditModalText').datepicker("setDate", new Date(data.start_date));
            $('#internshipEndDateEditModalText').datepicker("setDate", new Date(data.end_date));
            $('#internshipIdEditModalText').val(internship_id);
            $("#editIntershipModal").modal("show");
        }
    });
});

$("#editInternshipModalForm").submit(function (e) {
    e.preventDefault();
    var temp_this = $(this);
    var internship_id = $("#internshipIdEditModalText").val();
    var internship_title = $('#internshipTitleEditModalText').val();
    var internship_description = $('#internshipDescriptionEditModalText').val();
    var internship_start_date = $('#internshipStartDateEditModalText').val();
    var internship_end_date = $('#internshipEndDateEditModalText').val();

    var formData = temp_this.serialize();

    $.ajax({
        type: "POST",
        url: site_url + "user/internship/update_internship_form",
        data: formData,
        success: function (data) {
            if (data.trim() == "1") {
                var start_date_temp = internship_start_date.split("/");
                var end_date_temp = internship_end_date.split("/");

                $("#internship_title").html(internship_title);
                $("#internship_description").html(internship_description);

                $("#internship_start_date").html($.datepicker.formatDate('dd-M-yy', new Date(start_date_temp[2] + "/" + start_date_temp[1] + "/" + start_date_temp[0])));
                $("#internship_end_date").html($.datepicker.formatDate('dd-M-yy', new Date(end_date_temp[2] + "/" + end_date_temp[1] + "/" + end_date_temp[0])));

                temp_this[0].reset();
                $("#editIntershipModal").modal("hide");
            }
        }
    });
});