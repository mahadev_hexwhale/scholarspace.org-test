$(document).ready(function () {
    $('body').on('click', '.markAsRead_button', function () {
        var ele = $(this);
        var post_id = $(this).parents('div.alert_content_div').attr('id');
        var group_type = $(this).parents('div.alert_content_div').attr('group');
        var type = $(this).parents('div.alert_content_div').attr('type');
        
        $.ajax({
            type: "POST",
            url: site_url + "user/notification/MarkAsRead_Method",
            data: {
                post_id: post_id,
                group_type: group_type,
                type: type
            },
            success: function (data) {
//                console.log(data);
                if (data.trim() == '1') {
                    ele.parents('div.alert_content_div').removeClass('unread_alert');
                    ele.remove();
                }
            }
        });
    });

    $('body').on('click', '.alert_details h6 a', function () {
        var ele = $(this);
        ele.parents('h6').siblings('button.markAsRead_button').click();
    });

    $('body').on('click', '.markAllAsRead_button', function () {
        var fixed_array = [];
        var unfixed_array = [];
        $('.unread_alert').each(function () {
            var post_id = $(this).attr('id');
            var group = $(this).attr('group');
            if (group === 'fixed') {
                fixed_array.push(post_id);
            } else if (group === 'unfixed') {
                unfixed_array.push(post_id);
            }
        });

        $.ajax({
            type: "POST",
            url: site_url + "user/notification/MarkAllAsRead_Method",
            data: {
                fixed_array: fixed_array,
                unfixed_array: unfixed_array
            },
            success: function (data) {
                if (data.trim() != '0') {
                    $('.unread_alert').each(function () {
                        $(this).removeClass('unread_alert');
                        $(this).find('button.markAsRead_button').remove();
                    });
                }
            }
        });
    });
});