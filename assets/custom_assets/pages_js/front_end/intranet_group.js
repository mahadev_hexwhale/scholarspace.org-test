var AddedMembersList = [];
var group_members_group = {};
var SubmitNow = 1;
var RadioChecked = 1;

$(document).ready(function () {
    /*To reset all contents of modal on opening*/
    $('body').on('click', '#CreatNewGroup_Button', function () {
        $('#CreatNewGroup_Modal input[type=text]').val('');
        $('#CreatNewGroup_Modal select').val('');
        $('#CreatNewGroup_Modal input[type=radio]').removeAttr('checked');
        $('#CreatNewGroup_Modal .AddedMembers').html('');
        $('#CreatNewGroup_Modal .group_error').fadeOut();
    });

    /*Gives the list of all memebers of scholarspace*/
    $('body').on('click', '#CreatNewGroup_Modal #addGroupMembers', function () {
        var ListItem = "";
        if ($(this).val() == "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/groups/GetCollegeMembers_Method",
                data: {},
                success: function (data) {
                    $('#CreatNewGroup_Modal .people_list .ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (key == "student_list") {
                            $.each(value, function (inner_index, inner_value) {
                                if (AddedMembersList.indexOf(inner_value.college_users_intranet_id) == -1) {
                                    ListItem += "<li list_item_type = 'student_list' intranet_id='" + inner_value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + inner_value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + inner_value.fname + " " + inner_value.lname + "</h5><br>\
                                        <h6>" + inner_value.stream_name + "</h6>\
                                    </li>";
                                }
                            });
                        } else if (key == "student_batch_list") {
                            $.each(value, function (inner_index, inner_value) {
                                var test_array = inner_index in group_members_group;
                                if (test_array != true) {
                                    ListItem += '<li list_item_type = "student_batch_list" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                    <div style="width: 10%; float: left;" class="notify_img">\
                                                        <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                    </div>\
                                                    <div style="width: 90%; float: left;" >\
                                                    ' + inner_index + '\
                                                    </div>\
                                                </li>';
                                }
                            });
                        }
                    });
                    $('#CreatNewGroup_Modal .notify_pop').show();
                    $('#CreatNewGroup_Modal .people_list .ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Filters the members according to fname or lname or email and makes a list*/
    $('body').on('keyup', '#CreatNewGroup_Modal  #addGroupMembers', function () {
        var ListItem = "";
        var Item = $(this).val();
        if (Item != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: site_url + "user/groups/FilterCollegeMembers_Method",
                data: {
                    Item: Item
                },
                success: function (data) {
//                    console.log(data);
                    $('#CreatNewGroup_Modal .people_list .ListOfMembers').html("");
                    $.each(data, function (key, value) {
                        if (key == "student_list") {
                            $.each(value, function (inner_index, inner_value) {
                                if (AddedMembersList.indexOf(inner_value.college_users_intranet_id) == -1) {
                                    ListItem += "<li list_item_type = 'student_list' intranet_id='" + inner_value.college_users_intranet_id + "'>\
                                        <div class='notify_img'>\
                                            <img src='" + base_url + inner_value.profile_picture + "' alt=''>\
                                        </div>\
                                        <h5>" + inner_value.fname + " " + inner_value.lname + "</h5><br>\
                                        <h6>" + inner_value.stream_name + "</h6>\
                                    </li>";
                                }
                            });
                        } else if (key == "student_batch_list") {
                            $.each(value, function (inner_index, inner_value) {
                                var test_array = inner_index in group_members_group;
                                if (test_array != true) {
                                    ListItem += '<li list_item_type = "student_batch_list" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                    <div style="width: 10%; float: left;" class="notify_img">\
                                                        <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                    </div>\
                                                    <div style="width: 90%; float: left;" >\
                                                    ' + inner_index + '\
                                                    </div>\
                                                </li>';
                                }
                            });
                        }
                    });
                    $('#CreatNewGroup_Modal .notify_pop').show();
                    $('#CreatNewGroup_Modal .people_list .ListOfMembers').append(ListItem);
                }
            });
        }
    });

    /*Handles the things that should happen when you select a member from a list*/
    $('body').on('click', '#CreatNewGroup_Modal .ListOfMembers li', function () {
        var temp_this = $(this);
        var list_item_type = temp_this.attr("list_item_type");
        if (list_item_type == "student_list") {
            var name = $(this).children('h5').html();
            var college_users_intranet_id = $(this).attr('intranet_id');
            var AddedItem = "<div intranet_id='" + college_users_intranet_id + "' class = 'company'>\
                            <h6> " + name + " </h6>\
                            <span item_type = 'student_list' class='pop_close'></span>\
                        </div>";
            $('#CreatNewGroup_Modal .AddedMembers').append(AddedItem);
            $('#CreatNewGroup_Modal #addGroupMembers').val('');
            $('#CreatNewGroup_Modal .notify_pop').hide();
            AddedMembersList.push($(this).attr('intranet_id'));
        } else if (list_item_type == "student_batch_list") {
            var users_id = temp_this.attr("users_id");
            var group_id = temp_this.attr("group_id");
            var group_name = group_id.replace('</h5><h6>', ' - ');
            group_name = group_name.replace('<h5>', '<h6>');
            var AddedItem = '<div class="company" group_id = "' + group_id + '" users_id ="' + users_id + '">\
                                <span class="color_grp"></span>\
                                ' + group_name + '\
                                <span item_type = "student_batch_list" class="pop_close remove_selected_user_group"></span>\
                            </div>';
            $('#CreatNewGroup_Modal .AddedMembers').append(AddedItem);
            $('#CreatNewGroup_Modal #addGroupMembers').val('');
            $('#CreatNewGroup_Modal .notify_pop').hide();
            group_members_group[group_id] = users_id;
        }
    });

    /*When you want to delete a member from list which is already added*/
    $('body').on('click', '#CreatNewGroup_Modal .pop_close', function () {
        var temp_this = $(this);
        var item_type = temp_this.attr('item_type');
        if (item_type == "student_list") {
            var intranet_id = $(this).parents('div.company').attr('intranet_id');
            var index = AddedMembersList.indexOf(intranet_id);
            if (index > -1) {
                AddedMembersList.splice(index, 1);
            }
            $(this).parents('div.company').remove();
        } else if (item_type == "student_batch_list") {
            var group_id = temp_this.parent("div").attr("group_id");
            delete group_members_group[group_id];
        }
    });

    $('body').on('click', '#CreatNewGroup_Modal #CreatNewGroup_Form', function () {
        $('#CreatNewGroup_Modal .notify_pop').fadeOut();
    });

    /*When you submit the form for new group creation*/
    $('body').on('submit', '#CreatNewGroup_Modal #CreatNewGroup_Form', function (e) {
        e.preventDefault();
        if ($('.GroupName').val() == "") {
            $('.GroupName').siblings('.group_error').fadeIn();
            SubmitNow = 0;
        }
        if (AddedMembersList.length <= 0 && Object.keys(group_members_group).length <= 0) {
            $('.grp_members_error').fadeIn();
            SubmitNow = 0;
        }
        if ($('.VisibleTo').val() == "") {
            $('.VisibleTo').parents('div.visible_select').siblings('.group_error').fadeIn();
            SubmitNow = 0;
        }
        if ($('.GroupAccess:checked').val() == undefined) {
            $('.GroupAccess').parents('div').siblings('.group_error').fadeIn();
            RadioChecked = 0;
        } else if (SubmitNow == 1) {
            RadioChecked = 1;
        }

        if (SubmitNow == 1 && RadioChecked == 1) {
            GroupName = $('.GroupName').val();
            VisibleTo = $('.VisibleTo').val();
            GroupAccess = $('input[name=GroupAccess]:checked').val();
            group_members_group = JSON.stringify(group_members_group);

            $.ajax({
                type: "POST",
                url: site_url + "user/groups/CreatNewGroup_Method",
                data: {
                    AddedMembersList: AddedMembersList,
                    group_members_group: group_members_group,
                    GroupName: GroupName,
                    VisibleTo: VisibleTo,
                    GroupAccess: GroupAccess
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() != 0) {
                        bootbox.alert("Group created successfully.", function () {
                            window.location = base_url + "user/groups/?grp_id=" + data;
                        });
                        $('#CreatNewGroup_Modal').modal('hide');
                    } else {
                        bootbox.alert("Error in creating group. Please try again.");
                        $('#CreatNewGroup_Modal').modal('hide');
                    }
                }
            });
        }
    });

    /*To check whether the entered name is already present in database. This runs on keyup*/
    $('body').on('keyup', '#CreatNewGroup_Modal #GroupName', function (e) {
        if ($('#GroupName').val() != "") {
            var GroupName = $('#GroupName').val();
            $.ajax({
                type: "POST",
                url: site_url + "user/groups/CheckGroupName_Method",
                data: {
                    GroupName: GroupName,
                },
                success: function (data) {
                    if (data == 1) {
                        $('.GroupName').siblings('.group_error').html('');
                        $('.GroupName').siblings('.group_error').css('color', 'red');
                        $('.GroupName').siblings('.group_error').html('This Group name is not available.');
                        $('.GroupName').siblings('.group_error').fadeIn();
                        SubmitNow = 0;
                    } else {
                        $('.GroupName').siblings('.group_error').html('');
                        $('.GroupName').siblings('.group_error').css('color', 'green');
                        $('.GroupName').siblings('.group_error').html('This Group name is available.');
                        $('.GroupName').siblings('.group_error').fadeIn();
                        SubmitNow = 1;
                    }
                }
            });
        }
    });

    /*To hide all error messages on focusing of perticular element*/
    $('body').on('focus', '#CreatNewGroup_Form input.GroupAccess, input.addGroupMembers, #CreatNewGroup_Form select', function (e) {
        $(this).siblings('.group_error').fadeOut();
        $(this).parents('div').siblings('.group_error').fadeOut();
    });

});