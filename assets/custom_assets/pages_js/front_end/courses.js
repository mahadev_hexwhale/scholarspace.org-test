var course_id = "";     // required for course settings page

$(document).ready(function () {

    /* teacher added member array */
    var group_member_ids = {};
    var group_members_group = {};

    $("#start_datepicker").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#end_datepicker").datepicker("option", "minDate", selected)
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
    $("#end_datepicker").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#start_datepicker").datepicker("option", "maxDate", selected);
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });

    $("#addNewCourseBtn").click(function () {
        $("#addTeacherCoursesModal").modal("show");
    });

    var submit_button_value;
    $(".submit_btn").click(function () {
        var temp_this = $(this);
        submit_button_value = temp_this.val();
    });

    $("#addTeacherCoursesModalForm").submit(function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var formData = temp_this.serialize() + "&selected_users=" + JSON.stringify(group_member_ids) + "&selected_users_group=" + JSON.stringify(group_members_group) + "&submit=" + submit_button_value;
        var valid_count = 0;
        var group_count = Object.keys(group_members_group).length;
        var member_count = Object.keys(group_members_group).length;
        if (!textValidation("#addTeacherCoursesModalForm")) {
            valid_count++;
        }
        if (!textAreaValidation("#addTeacherCoursesModalForm")) {
            valid_count++;
        }
        if (group_count == 0 && member_count == 0) {
            $(".eligible_students_required_error_label").text("* Eligible Students is Required");
        }
        if (valid_count == 0 && (group_count != 0 || member_count != 0)) {
            $.ajax({
                type: "POST",
                url: site_url + "user/courses/save_course",
                data: formData,
                success: function (data) {
//                    console.log(data);
                    if (data.trim() == "1") {
                        $("#addTeacherCoursesModal").modal("hide");
                        location.reload();
                    }
                }
            });
        }
    });

    /* remove the required error msg */
    $("input, textarea, select").focus(function () {
        var temp_this = $(this);
        $(temp_this).siblings('.required_error_label').text("");
        $('.eligible_students_required_error_label').text("");
    });

    $("#eligible_students_input").keyup(function () {
        var filter = $(this).val();
        $.ajax({
            type: 'POST',
            url: base_url + "user/courses/get_eligible_student",
            dataType: 'JSON',
            data: {
                flag: "success",
                filter: filter,
                course_id: course_id
            },
            success: function (data) {
//                console.log(data);
                $("#courses_people_list_id").html("");
                $.each(data, function (index, value) {
                    if (index == "student_list") {
                        $.each(value, function (inner_index, inner_value) {
                            var test_array = inner_index in group_member_ids;
                            // above line returns true if inner_index is found in group_member_ids array else returns false
                            if (test_array != true) {
                                var temp_html = '<li user_id="' + inner_index + '" course_title="' + inner_value['course_title'] + '" stream_title = "' + inner_value['stream_title'] + '" profile_picture = "' + inner_value['profile_picture'] + '" list_item_type = "student" class="people_list_li" user_name = "' + inner_value['first_name'] + " " + inner_value['last_name'] + '">\
                                                <div class="notify_img">\
                                                    <img src="' + base_url + inner_value['profile_picture'] + '" alt="">\
                                                </div>\
                                                <h5>' + inner_value['first_name'] + " " + inner_value['last_name'] + ' (' + inner_value['stream_title'] + ')</h5>\
                                                <h6>' + inner_value['course_title'] + '</h6>\
                                            </li>';
                                $("#courses_people_list_id").append(temp_html);
                            }
                        });
                    } else if (index == "student_batch_list") {
                        $.each(value, function (inner_index, inner_value) {
                            var test_array = inner_index in group_members_group;
                            if (test_array != true) {
                                var temp_html = '<li list_item_type = "student_group" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                <div style="width: 10%; float: left;" class="notify_img">\
                                                    <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                </div>\
                                                <div style="width: 90%; float: left;" >\
                                                ' + inner_index + '\
                                                </div>\
                                            </li>';
                                $("#courses_people_list_id").append(temp_html);
                            }
                        });
                    }
                });
            }
        });
    });

    $("#eligible_students_input").focus(function () {
        $.ajax({
            type: 'POST',
            url: base_url + "user/courses/get_eligible_student",
            dataType: 'JSON',
            data: {
                flag: "success",
                course_id: course_id
            },
            success: function (data) {
//                console.log(data);
                $("#courses_people_list_id").html("");
                $.each(data, function (index, value) {
                    if (index == "student_list") {
                        $.each(value, function (inner_index, inner_value) {
                            var test_array = inner_index in group_member_ids;
                            // above line returns true if inner_index is found in group_member_ids array else returns false
                            if (test_array != true) {
                                var temp_html = '<li user_id="' + inner_index + '" course_title="' + inner_value['course_title'] + '" stream_title = "' + inner_value['stream_title'] + '" profile_picture = "' + inner_value['profile_picture'] + '" list_item_type = "student" class="people_list_li" user_name = "' + inner_value['first_name'] + " " + inner_value['last_name'] + '">\
                                                <div class="notify_img">\
                                                    <img src="' + base_url + inner_value['profile_picture'] + '" alt="">\
                                                </div>\
                                                <h5>' + inner_value['first_name'] + " " + inner_value['last_name'] + ' (' + inner_value['stream_title'] + ')</h5>\
                                                <h6>' + inner_value['course_title'] + '</h6>\
                                            </li>';
                                $("#courses_people_list_id").append(temp_html);
                            }
                        });
                    } else if (index == "student_batch_list") {
                        $.each(value, function (inner_index, inner_value) {
                            var test_array = inner_index in group_members_group;
                            if (test_array != true) {
                                var temp_html = '<li list_item_type = "student_group" group_id = "' + inner_index + '" users_id = "' + inner_value + '" class="people_list_li">\
                                                <div style="width: 10%; float: left;" class="notify_img">\
                                                    <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                                </div>\
                                                <div style="width: 90%; float: left;" >\
                                                ' + inner_index + '\
                                                </div>\
                                            </li>';
                                $("#courses_people_list_id").append(temp_html);
                            }
                        });
                    }
                });
            }
        });
    });

    $("body").on("click", ".people_list_li", function () {
        var temp_this = $(this);
        var list_item_type = temp_this.attr("list_item_type");
        if (list_item_type == "student_group") {
            var users_id = temp_this.attr("users_id");
            var group_id = temp_this.attr("group_id");
            var group_name = group_id.replace('</h5><h6>', ' - ');
            group_name = group_name.replace('<h5>', '<h6>');
            group_members_group[group_id] = users_id;
            var temp_html = '<div class="company" group_id = "' + group_id + '" users_id ="' + users_id + '">\
                                <span class="color_grp"></span>\
                                ' + group_name + '\
                                <span item_type = "group" class="pop_close remove_selected_user_group"></span>\
                            </div>';
            $("#selected_users_container_div").append(temp_html);
            temp_this.remove();
        } else if (list_item_type == "student") {
            var user_id = temp_this.attr("user_id");
            var user_name = temp_this.attr("user_name");
            var profile_picture = temp_this.attr('profile_picture');
            var stream_title = temp_this.attr('stream_title');
            var course_title = temp_this.attr('course_title');
            group_member_ids[user_id] = user_name;
            var temp_html = '<div class="company" stream_title="' + stream_title + '" course_title = "' + course_title + '" user_id = "' + user_id + '" profile_picture="' + profile_picture + '" user_name ="' + user_name + '">\
                                <span class="color_user"></span>\
                                <h6>' + user_name + '</h6>\
                                <span item_type = "student" class="pop_close remove_selected_user_group"></span>\
                            </div>';
            $("#selected_users_container_div").append(temp_html);
            temp_this.remove();
        }
        $("body .people_list_drop_down").css("display", "none");
        $('body .people_list_drop_down').hide();
        $('body .people_list_drop_down').removeClass('active');
        $('#eligible_students_input').val('');
    });

    /* remove selected user group */
    $("body").on("click", ".remove_selected_user_group", function () {
        var temp_this = $(this);
        var item_type = temp_this.attr('item_type');
        if (item_type == "group") {
            var group_id = temp_this.parent("div").attr("group_id");
            var users_id = temp_this.parent("div").attr("users_id");
            var temp_html = '<li list_item_type = "student_group" group_id = "' + group_id + '" users_id = "' + users_id + '" class="people_list_li">\
                                <div style="width: 10%; float: left;" class="notify_img">\
                                    <img src="http://localhost/scholarspace/assets_front/image/profile_pic.png" alt="">\
                                </div>\
                                <div style="width: 90%; float: left;" >\
                                ' + group_id + '\
                                </div>\
                            </li>';
            $("#courses_people_list_id").append(temp_html);
            temp_this.parent("div").remove();
            delete group_members_group[group_id];
        }
        else if (item_type == "student") {
            var user_id = temp_this.parent("div").attr("user_id");
            var user_name = temp_this.parent("div").attr("user_name");
            var profile_picture = temp_this.parent("div").attr("profile_picture");
            var stream_title = temp_this.parent("div").attr('stream_title');
            var course_title = temp_this.parent("div").attr('course_title');
            var temp_html = '<li user_id="' + user_id + '" stream_title="' + stream_title + '" course_title = "' + course_title + '" list_item_type = "student" class="people_list_li" profile_picture="' + profile_picture + '" user_name = "' + user_name + '">\
                                <div class="notify_img">\
                                    <img src="' + base_url + profile_picture + '" alt="">\
                                </div>\
                                <h5>' + user_name + ' (' + stream_title + ')</h5>\
                                <h6>' + course_title + '</h6>\
                            </li>';
            $("#courses_people_list_id").append(temp_html);
            temp_this.parent("div").remove();
            delete group_member_ids[user_id];
        }
    });

    /* Validation check for text box */
    function textValidation(form_id) {
        var status = 1;
        $(form_id + ' .required_field').each(function () {
            var temp_this = $(this);
            var title = temp_this.attr("title");
            if (temp_this.val().trim() == '' && !temp_this.hasClass("people_list_input")) {
                status = 0;
                $(temp_this).siblings('.required_error_label').text("* " + title + " is Required");
            }
        });
        return status;
    }

    /* Validation check for textarea */
    function textAreaValidation(form_id) {
        var status = 1;
        $(form_id + ' textarea').each(function () {
            var temp_this = $(this);
            var title = temp_this.attr("title");
            if (temp_this.val().trim() == '') {
                status = 0;
                $(temp_this).siblings('.required_error_label').text("* " + title + " is Required");
            }
        });
        return status;
    }

    add_teacher();
    function add_teacher(destroy) {
        $(".auto_fill_email, .associates_email").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/courses/GetTeacherEmail",
                    dataType: "json",
                    data: {
                        email: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            focus: function (event, ui) {
                $(this).val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $(this).val(ui.item.label);
                return false;
            }
        });
    }

    add_assistant();
    function add_assistant(destroy) {
        $(".auto_fill_assistant, .assistant_email").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/courses/GetStudentEmail",
                    dataType: "json",
                    data: {
                        email: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            focus: function (event, ui) {
                $(this).val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $(this).val(ui.item.label);
                return false;
            }
        });
    }

    $("body").on("click", ".add_more_teacher", function () {
        var element = '<input type="text" title="Add Additional Teacher" name ="addtional_teacher[]" placeholder="Enter email" style="width: 40%" class="pop_text pop_up_taxt auto_fill_email">';
        $('.add_more_teacher_div').append(element);
        add_teacher(true);
    });

    $("body").on("click", ".add_more_assistant", function () {
        var element = '<input type="text" title="Add Assistance" name ="assistants[]" placeholder="Enter email" style="width: 40%" class="pop_text pop_up_taxt auto_fill_assistant">';
        $('.add_more_assistant_div').append(element);
        add_assistant(true);
    });

    // This is the functionality for course settings page
    $('body').on('submit', '#AddMoremembers_Modal', function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var selected_users = JSON.stringify(group_member_ids);
        var selected_users_group = JSON.stringify(group_members_group);
        var group_count = Object.keys(group_members_group).length;
        var member_count = Object.keys(group_member_ids).length;

        if (group_count == 0 && member_count == 0) {
            $(".eligible_students_required_error_label").text("* Eligible Students Required");
        }
        if (group_count != 0 || member_count != 0) {
            $.ajax({
                type: "POST",
                url: site_url + "user/courses/AddMoremembers_Method",
                data: {
                    course_id: course_id,
                    selected_users: selected_users,
                    selected_users_group: selected_users_group
                },
                success: function (data) {
//                    console.log(data);
                    if (data.trim() == "1") {
                        $('#AddMoremembers_Modal').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    });

});
//$(document).mouseup(function (e) {
//    var popup = $("body .people_list_drop_down");
//    if (!$('body .people_list_drop_down').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0 && !$('#eligible_students_input').is(e.target)) {
//        popup.hide();
//        $('body .people_list_drop_down').removeClass('active');
//    }
//});