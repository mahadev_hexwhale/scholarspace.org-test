﻿/*!
 * Tri-State checkbox
 *
 * Copyright 2011, http://webworld-develop.blogspot.com/
 * Artistic License 2.0
 * http://www.opensource.org/licenses/artistic-license-2.0
 *
 * Date: Fr Apr 22 08:00:00 2011 -0200
**/

(function($){var j={classes:{checkbox:"customcheck",checked:"customcheckfull",partial:"customcheckpartial",unchecked:"customchecknone"},children:null};var k={init:function(i){return this.each(function(){if(i){$.extend(j,i)}var a=$(this).hide();var b=j.classes.checked;var c=j.classes.partial;var d=j.classes.unchecked;var e=d;var f=j.children.filter(":disabled");var g=j.children.filter(":not(:disabled)");if(a.is(":checked")){e=(f.length>0)?c:b;g.attr("checked",true)}else if(g.filter(":checked").length>0){e=(g.filter(":checked").length==g.length&&f.length==0)?b:c}var h=$("<span class='"+j.classes.checkbox+" "+e+"'></span>").insertBefore(a);h.click(function(){if(h.hasClass(b)||(h.hasClass(c)&&f.length>0)){h.removeClass(b).removeClass(c).addClass(d);g.attr("checked",false);a.attr("checked",false)}else{if(f.length>0){h.removeClass(d).removeClass(b).addClass(c)}else{h.removeClass(d).removeClass(c).addClass(b)}g.attr("checked",true);a.attr("checked",true)}});g.click(function(){if($(this).is(":checked")){if(g.filter(":checked").length==g.length&&f.length==0){h.removeClass(c).removeClass(d).addClass(b);a.attr("checked",true)}else{h.removeClass(d).addClass(c);a.attr("checked",true)}}else{if(g.filter(":not(:checked)").length==g.length){h.removeClass(b).removeClass(c).addClass(d);a.attr("checked",false)}else{h.removeClass(b).removeClass(d).addClass(c)}}})})}};$.fn.tristate=function(a){if(k[a]){return k[a].apply(this,Array.prototype.slice.call(arguments,1))}else if(typeof a==='object'||!a){return k.init.apply(this,arguments)}}})(jQuery);