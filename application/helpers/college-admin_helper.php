<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    function college_membership_request() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        
        $sql_query = "SELECT `id` FROM `college_user_request_to_admin`";
        $query_result = $ci->data_fetch->data_query($sql_query);

        return count($query_result);
    }

    function intranet_default_group() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');

        $sql_query = "SELECT * FROM `intranet_default_groups`";
        $query_result = $ci->data_fetch->data_query($sql_query);

        return $query_result;
    }

    function get_college_admin_list($college_id) {
        $ci = & get_instance();
        $ci->load->model('data_fetch');

        $sql_query = "SELECT * FROM `college_admin` WHERE `college_id` = '$college_id'";
        $query_result = $ci->data_fetch->data_query($sql_query);
        return $query_result;
    }

    /*     * **********Mahadev************ */

    function GroupList_Method() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $college_id = $ci->session->userdata('college_id');

        $SelectGroups = "SELECT * FROM college_groups WHERE college_id = '$college_id'";
        $SelectGroups_Result = $ci->data_fetch->data_query($SelectGroups);

        return $SelectGroups_Result;
    }

    // Check if the user is already a member of group
    function CheckIfMemberOfGroup_Method($GroupID) {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $user = $ci->ion_auth->user()->row();
        $college_id = $ci->session->userdata('college_id');
        $GroupID = $ci->encryption_decryption_object->is_valid_input($GroupID);

        $SelectGroupsUsers = "SELECT user_id FROM college_group_users "
                . "WHERE college_id = '$college_id' AND group_id = '$GroupID' AND user_id = '$user->id'";
        $SelectGroupsUsers_Result = $ci->data_fetch->data_query($SelectGroupsUsers);
        if (count($SelectGroupsUsers_Result))
            return 1;
        else
            return 0;
    }

    // Check if the user has already requested to join group
    function CheckIfRequestedForGroup_Method($GroupID) {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $GroupID = $ci->encryption_decryption_object->is_valid_input($GroupID);
        $user = $ci->ion_auth->user()->row();
        $college_id = $ci->session->userdata('college_id');

        $SelectGroupsUsersRequest = "SELECT user_id FROM college_user_group_request "
                . "WHERE college_id = '$college_id' AND group_id = '$GroupID' AND user_id = '$user->id'";
        $SelectGroupsUsersRequest_Result = $ci->data_fetch->data_query($SelectGroupsUsersRequest);

        if (count($SelectGroupsUsersRequest_Result))
            return 1;
        else
            return 0;
    }

    function GetCollegeDetails() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $college_id = $ci->session->userdata('college_id');
        $query = "SELECT t1.college_name,t1.city,t1.state,t1.zipcode, "
                . "t2.user_id as admin_user_id, t3.image as college_image "
                . "FROM college t1 "
                . "INNER JOIN college_admin t2 ON t1.id = t2.college_id "
                . "LEFT JOIN college_image t3 ON t3.college_id = t1.id "
                . "WHERE t1.id = '$college_id' LIMIT 0,1";
        $college_details = $ci->data_fetch->data_query($query);
        return $college_details[0];
    }

    function GetUserDetails() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $user_id = $ci->ion_auth->user()->row()->id;
        $college_id = $ci->session->userdata('college_id');
        $query_user = "SELECT t1.intranet_user_type, t1.joining_year, t1.id AS user_intranet_id,"
                . "t2.email, t2.first_name, t2.last_name, t2.phone,t2.profile_picture "
                . "FROM college_users_intranet t1 "
                . "INNER JOIN users t2 ON t2.id = t1.user_id "
                . "WHERE t2.id = '$user_id' "
                . "LIMIT 0,1";
        $user_details = $ci->data_fetch->data_query($query_user);
        return $user_details[0];
    }

    //Get the all unread message.
    function getUnreadMessage() {
        $ci = & get_instance();

        $ci->load->model('usersearch');
        $user_id = $ci->ion_auth->user()->row()->id;
        $result = $ci->usersearch->getAllUnreadMessage($user_id);
        return $result;
    }

    //Get the all unread message  from user.
    function getUnreadMsgCount($chatwith) {
        $ci = & get_instance();
        $ci->load->model('usersearch');
        $chatwith = $ci->encryption_decryption_object->is_valid_input($chatwith);

        $user_id = $ci->ion_auth->user()->row()->id;
        $unread_message = "";
        $check_chat_exists = $ci->usersearch->CheckChatExists($user_id, $chatwith);
        if ($check_chat_exists != 0) {
            // $chat_status = $ci->usersearch->getUserChatStatus($check_chat_exists);
            // $chat_status = $chat_status[0];
            //Get the count of  all  the unread message.
            $unread_message = $ci->usersearch->GetChatAllUnreadMessages($check_chat_exists, $user_id);
            return $unread_message;
        } else {
            return FALSE;
        }
    }

    function send_email($email, $subject, $body) {
        $ci = & get_instance();
        $ci->load->library('my_phpmailer');
        
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "jogendra@hexwhale.com";
        $mail->Password = "getinside";
        $mail->setFrom('jogendra@hexwhale.com', 'ScholarSpace');
        $mail->addReplyTo('jogendra@hexwhale.com', 'ScholarSpace');
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->addAddress($email);
        $mail->send();
    }

}