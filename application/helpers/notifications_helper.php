<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    /* .................... Notification List for logged in user .................. */
    function GetAllNotifications()
    {
        $ci = &get_instance();
        $ci->load->model('data_fetch');
        $logged_user_id = $ci->ion_auth->user()->row()->id;
        $college_id = $ci->session->userdata('college_id');

        $sql_query = "SELECT post_tb.post_id, post_tb.type, post_tb.user_id AS posted_user_id, like_tb.timestamp,like_tb.read_status, 'like' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_like_details like_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = like_tb.post_id "
            . "WHERE like_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.post_id,post_tb.type,post_tb.user_id AS posted_user_id, post_tb.timestamp, notify_tb.read_status, 'notify' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_notify_user_details notify_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = notify_tb.post_id "
            . "WHERE notify_tb.user_id = '$logged_user_id' AND post_tb.user_id != '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.post_id, post_tb.type, post_tb.user_id AS posted_user_id, reply_tb.timestamp, reply_tb.read_status, 'reply' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_reply reply_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = reply_tb.post_id "
            . "WHERE reply_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, like_tb.timestamp, like_tb.read_status, 'like' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_post_like_details like_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = like_tb.post_id "
            . "WHERE like_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, notify_tb.timestamp, notify_tb.read_status, 'notify' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_posts_notify_user_details notify_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = notify_tb.post_id "
            . "WHERE notify_tb.user_id = '$logged_user_id' AND post_tb.user_id != '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, reply_tb.timestamp, reply_tb.read_status, 'reply' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_post_like_details reply_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = reply_tb.post_id "
            . "WHERE reply_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' "
            . "GROUP BY post_tb.id "
            . "ORDER BY timestamp DESC "
            . "LIMIT 0,30";
        return $all_posts = $ci->data_fetch->data_query($sql_query);
    }

    function GetUnareadNotifications()
    {
        $ci = &get_instance();
        $ci->load->model('data_fetch');
        $logged_user_id = $ci->ion_auth->user()->row()->id;
        $college_id = $ci->session->userdata('college_id');

        $sql_query = "SELECT post_tb.post_id, post_tb.type, post_tb.user_id AS posted_user_id, like_tb.timestamp,like_tb.read_status, 'like' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_like_details like_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = like_tb.post_id "
            . "WHERE like_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND like_tb.read_status = '0'"
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.post_id,post_tb.type,post_tb.user_id AS posted_user_id, post_tb.timestamp, notify_tb.read_status, 'notify' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_notify_user_details notify_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = notify_tb.post_id "
            . "WHERE notify_tb.user_id = '$logged_user_id' AND post_tb.user_id != '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND notify_tb.read_status = '0'"
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.post_id, post_tb.type, post_tb.user_id AS posted_user_id, reply_tb.timestamp, reply_tb.read_status, 'reply' AS type, 'fixed' AS group_type "
            . "FROM college_fixed_group_posts_reply reply_tb "
            . "INNER JOIN college_fixed_group_posts post_tb ON post_tb.post_id = reply_tb.post_id "
            . "WHERE reply_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND reply_tb.read_status = '0'"
            . "GROUP BY post_tb.post_id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, like_tb.timestamp, like_tb.read_status, 'like' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_post_like_details like_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = like_tb.post_id "
            . "WHERE like_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND like_tb.read_status = '0'"
            . "GROUP BY post_tb.id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, notify_tb.timestamp, notify_tb.read_status, 'notify' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_posts_notify_user_details notify_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = notify_tb.post_id "
            . "WHERE notify_tb.user_id = '$logged_user_id' AND post_tb.user_id != '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND notify_tb.read_status = '0'"
            . "GROUP BY post_tb.id "
            . "UNION "
            . "SELECT post_tb.id AS post_id, post_tb.type, post_tb.user_id AS posted_user_id, reply_tb.timestamp, reply_tb.read_status, 'reply' AS type, 'unfixed' AS group_type "
            . "FROM college_unfixed_group_post_like_details reply_tb "
            . "INNER JOIN college_unfixed_group_posts post_tb ON post_tb.id = reply_tb.post_id "
            . "WHERE reply_tb.user_id != '$logged_user_id' AND post_tb.user_id = '$logged_user_id' "
            . "AND post_tb.college_id = '$college_id' AND reply_tb.read_status = '0'"
            . "GROUP BY post_tb.id "
            . "ORDER BY timestamp DESC "
            . "LIMIT 0,30";
        return $all_posts = $ci->data_fetch->data_query($sql_query);
    }


}