<?php

class Encryption
{

    var $skey = "ZBb!?+jCzu8wY^amGhV!9BJxb<fWCE9g"; // you can change it

    public function safe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value)
    {
        if (!$value) {
            return false;
        }
        $text = "wrap%||%" . $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value)
    {
        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function is_valid_input($value)
    {
        if (!$value) {
            return false;
        }
        $decoded_string = $this->decode($value);

        $decoded_string = explode('%||%', $decoded_string);

        if ($decoded_string[0] == "wrap") {
            return $decoded_string[1];
        } else {
            return false;
        }
    }

    public function EncryptThis($value)
    {
        foreach ($value as $k => $v) {
            switch ($k) {
                case 'id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'user_intranet_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'college_users_intranet_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'user_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'stream_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'stream_course_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'group_admin_user_id':
                    $newVal = $this->encode($v);
                    $value->$k = $newVal;
                    break;
            }
        }
        return $value;
    }

    public function GetOriginalIDs($Array)
    {
        // Get original ids
        foreach ($Array as $key => $value) {
            $NewVal = $this->is_valid_input($value);
            if ($NewVal) {
                $Array[$key] = $NewVal;
            } else {
                return 0;
            }
        }
        return $Array;
    }

}

/** use
 * $converter = new Encryption;
 * $encoded = $converter->encode($str);
 * $decoded = $converter->decode($encoded);
 *
 * echo "$encoded<p>$decoded";
 * die();
 */
?>