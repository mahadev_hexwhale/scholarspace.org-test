<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_insert extends CI_Model {

    //normal query function 
    public function data_query($sql_query) {
        return $this->db->query($sql_query);
    }

    public function add_fields($data_array, $table_name) {
        if ($this->db->insert($table_name, $data_array)) {
            return $this->db->insert_id();
        } else {
            return "error";
        }
    }

}
