<?php

class Courses_model extends CI_Model {

    public function GetTeacherCourses($college_id, $user_id, $tense, $course_status) {
        $today_date = date('Y-m-d');
        $sql_query = "SELECT `id` AS course_id,`course_name` FROM `college_course` "
                . "WHERE `college_id` = '$college_id' AND `created_teacher_id` = '$user_id' ";
        switch ($tense) {
            case 'past':
                $sql_query .= "AND `start_date` < '$today_date' AND `end_date` < '$today_date' ";
                break;
            case 'present':
                $sql_query .= "AND `start_date` <= '$today_date' AND `end_date` >= '$today_date' ";
                break;
            case 'future':
                $sql_query .= "AND `start_date` > '$today_date' AND `end_date` > '$today_date' ";
                break;
        }
        if ($course_status == "published") {
            $sql_query .= "AND `save_and_draft` = '0'";
        } else if ($course_status == "drafted") {
            $sql_query .= "AND `save_and_draft` = '1'";
        }
        return $query_result = $this->data_fetch->data_query($sql_query);
    }

    public function GetMemberCourses($college_id, $user_id, $tense, $course_status) {
        $today_date = date('Y-m-d');
        $sql_query = "SELECT t1.`id` AS course_id,`course_name` FROM `college_course` t1 "
                . "INNER JOIN college_course_members t2 ON t2.course_id = t1.id "
                . "WHERE t1.`college_id` = '$college_id' AND t2.`user_id` = '$user_id' ";
        switch ($tense) {
            case 'past':
                $sql_query .= "AND t1.`start_date` < '$today_date' AND t1.`end_date` < '$today_date' ";
                break;
            case 'present':
                $sql_query .= "AND t1.`start_date` <= '$today_date' AND t1.`end_date` >= '$today_date' ";
                break;
            case 'future':
                $sql_query .= "AND t1.`start_date` > '$today_date' AND t1.`end_date` > '$today_date' ";
                break;
        }
        if ($course_status == "published") {
            $sql_query .= "AND t1.`save_and_draft` = '0'";
        } else if ($course_status == "drafted") {
            $sql_query .= "AND t1.`save_and_draft` = '1'";
        }
        return $query_result = $this->data_fetch->data_query($sql_query);
    }

    public function GetMarksofMember($course_id, $user_id, $exam_type) {
        $query = "SELECT * FROM ";
        switch ($exam_type) {
            case 'midterm':
                $query .= "college_course_members_midterm_marks ";
                break;
            case 'external':
                $query .= "college_course_members_finalmarks ";
                break;
            case 'final':
                $query .= "	college_course_members_total_final_marks ";
                break;
        }
        $query .= "WHERE course_id = '$course_id' AND user_id = '$user_id' ";
        $query_result = $this->data_fetch->data_query($query);
        if ($query_result)
            return $query_result[0];
    }

}
