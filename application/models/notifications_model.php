<?php

class Notifications_model extends CI_Model {

    public function GetFixedGroupPosts($logged_user_id) {
        $FixedPostsArray = array();

        /* .................... Notification List for logged in user from FIXED group .................. */
        // Get all the notify user alerts
        $sql_query = "SELECT * FROM `college_fixed_group_posts_notify_user_details` "
                . "WHERE user_id = '$logged_user_id'";
        $query_result1 = $this->data_fetch->data_query($sql_query);
        $data['fixed_posts']['notify'] = $query_result1;
        foreach ($query_result1 as $value) {
            $FixedPostsArray[$value->post_id] = $value->post_id;
        }

        // Get all the liked alerts
        $sql_query = "SELECT t1.* FROM `college_fixed_group_posts_like_details` t1 "
                . "INNER JOIN college_fixed_group_posts t2 ON t2.post_id = t1.post_id "
                . "WHERE t1.user_id != '$logged_user_id' AND t2.user_id = '$logged_user_id'";
        $query_result2 = $this->data_fetch->data_query($sql_query);
        $data['fixed_posts']['like'] = $query_result2;
        foreach ($query_result2 as $value) {
            $FixedPostsArray[$value->post_id] = $value->post_id;
        }

        // Get all the commented alerts
        $sql_query = "SELECT t1.* FROM `college_fixed_group_posts_reply` t1 "
                . "INNER JOIN college_fixed_group_posts t2 ON t2.post_id = t1.post_id "
                . "WHERE t2.user_id = '$logged_user_id' AND t1.user_id != '$logged_user_id'";
        $query_result3 = $this->data_fetch->data_query($sql_query);
        $data['fixed_posts']['reply'] = $query_result3;
        foreach ($query_result3 as $value) {
            $FixedPostsArray[$value->post_id] = $value->post_id;
        }
        return $FixedPostsArray;
    }

    public function GetUnfixedGroupPosts($logged_user_id) {
        $UnfixedPostsArray = array();
        /* .................... Notification List for logged in user from UNFIXED group .................. */
        // Get all the notify user alerts
        $sql_query = "SELECT t1.* FROM `college_unfixed_group_posts_notify_user_details` t1 "
                . "WHERE t1.user_id = '$logged_user_id'";
        $query_result1 = $this->data_fetch->data_query($sql_query);
        $data['unfixed_posts']['notify'] = $query_result1;
        foreach ($query_result1 as $value) {
            $UnfixedPostsArray[] = $value->post_id;
        }

        // Get all the liked alerts
        $sql_query = "SELECT t1.* FROM `college_unfixed_group_post_like_details` t1 "
                . "INNER JOIN college_unfixed_group_posts t2 ON t2.id = t1.post_id "
                . "WHERE t1.user_id != '$logged_user_id' AND t2.user_id = '$logged_user_id'";
        $query_result2 = $this->data_fetch->data_query($sql_query);
        $data['unfixed_posts']['like'] = $query_result2;
        foreach ($query_result2 as $value) {
            $UnfixedPostsArray[] = $value->post_id;
        }

        // Get all the commented alerts
        $sql_query = "SELECT t1.* FROM `college_unfixed_group_post_reply` t1 "
                . "INNER JOIN college_unfixed_group_posts t2 ON t2.id = t1.post_id "
                . "WHERE t1.user_id != '$logged_user_id' AND t2.user_id = '$logged_user_id'";
        $query_result3 = $this->data_fetch->data_query($sql_query);
        $data['unfixed_posts']['reply'] = $query_result3;
        foreach ($query_result3 as $value) {
            $UnfixedPostsArray[] = $value->post_id;
        }
        
        return $UnfixedPostsArray;
    }

}
