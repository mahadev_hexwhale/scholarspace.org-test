<?php

class Android_model extends CI_Model {

    //normal query function 
    public function insert_token($sql_query) {
        return $this->db->query($sql_query);
    }

    public function get_college($user_id) {
        $res = $this->db
                        ->select('c.college_name,c.id')
                        ->join('college as c', 'c.id=cui.college_id')
                        ->where('cui.user_id', $user_id)
                        ->get('college_users_intranet as cui')->result();
        if ($res) {
            return $res;
        } else {
            return "";
        }
    }

    public function get_chat_names($college_id, $key_word) {
        $res = $this->db
                        ->select('u.first_name,u.last_name')
                        ->join('users as u', 'u.id=cui.college_id')
                        ->where('cui.user_id', $user_id)
                        ->get('college_users_intranet as cui')->result();
        if ($res) {
            return $res;
        } else {
            return "";
        }
    }

    public function get_recent_chat_users($user_id) {
        
//        $result = $this->db
//                        ->select('u1.first_name,u1.last_name,u1.id')
//                        ->join('users as u1', 'u1.id = ch.user_id1')
//                        ->join('users as u2', 'u2.id = ch.user_id2')
////                        ->join('users as u3', 'u3.id != '.$user_id)
//                        ->where('ch.user_id2', $user_id)
//                        ->or_where('ch.user_id1', $user_id)
//                        ->group_by('u1.id')
//                        ->order_by('ch.last_timestamp', 'desc')
//                        ->limit(10)
//                        ->get('chats as ch')->result();

        $query = "SELECT U.id,U.first_name,U.last_name FROM users U,chats C "
                ."WHERE CASE WHEN C.user_id1 = '$user_id' THEN C.user_id2 = U.id "
                ."WHEN C.user_id2 = '$user_id' THEN C.user_id1= U.id END ORDER BY C.last_timestamp DESC LIMIT 0,10";
        
        $query_result = $this->data_fetch->data_query($query);
        
        if(!empty($query_result)){
            return $query_result;
        } else {
            return null;
        }
    }

    public function get_chat_history($user_id, $person_id) {

        $res2 = $this->db
                        ->select('u.first_name,u.last_name,u.id')
                        ->join('users as u', 'u.id=ch.user_id1')
                        ->where('ch.user_id2', $user_id)
                        ->get('chats as ch')->result();

        $res = $this->db
                        ->select('u.first_name,u.last_name,u.id')
                        ->join('users as u', 'u.id=ch.user_id2')
                        ->where('ch.user_id1', $user_id)
                        ->get('chats as ch')->result();

        $result = array_merge($res, $res2);

        return $result;
    }

    public function getUserList($College_id, $type, $user_id) {
        $logged_user_id = $user_id;
        $SelectCollegeUsers = "SELECT t1.* "
                . "FROM `college_users_intranet` t1 "
                . "INNER JOIN users t2 ON t1.user_id = t2.id "
                . "WHERE `college_id` = '$College_id' AND user_id != '$logged_user_id' "
                . "ORDER BY t2.first_name ASC";
        $SelectCollegeUsers_Result = $this->db->query($SelectCollegeUsers)->result();
        foreach ($SelectCollegeUsers_Result as $value) {
            $intranet_user_type = $value->intranet_user_type;
            switch ($intranet_user_type) {
                case 'student':
                    $SelectDetailsOfStudent = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_student_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfStudent_Result = $this->db->query($SelectDetailsOfStudent)->result();

                    foreach ($SelectDetailsOfStudent_Result as $value1) {
                        $StudentsList[$value1->college_users_intranet_id] = $value1;
                    }
                    break;
                case 'alumni':
                    $SelectDetailsOfAlumni = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_alumni_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfAlumni_Result = $this->db->query($SelectDetailsOfAlumni)->result();
                    foreach ($SelectDetailsOfAlumni_Result as $value2) {
                        $AlumniList[$value2->college_users_intranet_id] = $value2;
                    }
                    break;
                case 'teacher':
                    $SelectDetailsOfTeacher = "SELECT t1.*, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_teacher_users_intranet` AS t1 "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfTeacher_Result = $this->db->query($SelectDetailsOfTeacher)->result();
                    foreach ($SelectDetailsOfTeacher_Result as $value3) {
                        $TeacherList[$value3->college_users_intranet_id] = $value3;
                    }
                    break;
            }
        }
        if ($type == 'student')
            if (!empty($StudentsList))
                return $StudentsList;
        if ($type == 'alumni')
            if (!empty($AlumniList))
                return $AlumniList;
        if ($type == 'teacher')
            if (!empty($TeacherList))
                return $TeacherList;
    }

    public function FilterCollegeMembers_Method($College_id, $Item, $logged_user_id) {
        if ($Item != "" && $College_id) {

            $SelectCollegeUsers = "SELECT t1.* "
                    . "FROM `college_users_intranet` t1 "
                    . "INNER JOIN users t2 ON t1.user_id = t2.id "
                    . "WHERE `college_id` = '$College_id' AND user_id != '$logged_user_id' "
                    . "ORDER BY t2.first_name ASC";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            $List = array();
            foreach ($SelectCollegeUsers_Result as $key => $value) {
                $intranet_user_type = $value->intranet_user_type;

                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id ";
                        if ($Item != "") {
                            $SelectDetailsOfStudent .= "AND (t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id ";
                        if ($Item != "") {
                            $SelectDetailsOfAlumni .= "AND (t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                }
            }
            foreach ($List as $key => $value) {
                $List[$key] = $this->encryption_decryption_object->EncryptThis($value);
            }
            return $List;
        }
    }

    public function CheckChatExists($user_id, $chatwith) {
        $query = "SELECT * FROM `chats` WHERE (`user_id1` = '$user_id' AND `user_id2` = '$chatwith') OR (`user_id1` = '$chatwith' AND `user_id2` = '$user_id') LIMIT 0 , 30";

        $query_result = $this->data_fetch->data_query($query);

        if (count($query_result) > 0) {
            foreach ($query_result as $value) {
                return $value->chat_id;
            }
        } else {
            return 0;
        }
    }

    public function GetChatLast30Messages($check_chat_exists) {
        $query = "SELECT * FROM ( SELECT *  FROM `chat_user` WHERE `chat_id` = $check_chat_exists ORDER BY  `chat_user`.`timestamp` DESC LIMIT 0 , 30) sub ORDER BY id ASC";
        $query_result = $this->data_fetch->data_query($query);
        return $query_result;
    }

    public function CreateChat($user_id, $chatwith) {

        $data_array = array(
            "user_id1" => $user_id,
            "user_id2" => $chatwith
        );

//        $query = "INSERT INTO `test_scholarspace_db`.`chats` (`chat_id`, `user_id1`, `user_id2`) VALUES (NULL, '$user_id', '$chatwith')";
//        $query_result = $this->data_insert->data_query($query);
        $res = $this->add_fields($data_array, "chats");
        if ($res != "error") {
            return true;
        } else {
            return false;
        }
    }

    public function insertChat($chat_id, $user_id, $message) {

        $data_array = array(
            "chat_id" => $chat_id,
            "user_id" => $user_id,
            "message" => $message
        );
        $res = $this->add_fields($data_array, "chat_user");
        if ($res != "error") {
            return true;
        } else {
            return false;
        }
    }

    public function getLatestMessage($user_id, $chat_id, $timestamp) {

        $query = "SELECT *  FROM `chat_user` WHERE `chat_id` = $chat_id AND `timestamp` > '$timestamp' AND user_id !=  '$user_id'";
        $query_result = $this->data_fetch->data_query($query);
        return $query_result;
    }

    public function add_fields($data_array, $table_name) {
        if ($this->db->insert($table_name, $data_array)) {
            return $this->db->insert_id();
        } else {
            return "error";
        }
    }

    public function allUsers($key_word, $uid) {
        $query = "SELECT `id`,`first_name`,`last_name` FROM `users` WHERE id!='$uid' AND id != 1 AND (`email` LIKE '$key_word%' OR `phone` LIKE '$key_word%' OR `first_name` LIKE '$key_word%') LIMIT 0,10";
        $query_result = $this->data_fetch->data_query($query);
        return $query_result;
    }

    public function updateRecentTime($timestamp,$chat_id) {
        $data = array(
            'last_timestamp' => $timestamp
        );
        $this->db->where('chat_id', $chat_id);
        $this->db->update('chats', $data);
    }

}
