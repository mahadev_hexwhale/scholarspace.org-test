<?php

class Posts_model extends CI_Model
{

    // param 1 : Type of the group (fixed of unfixed).
    // param 2 : Array of post ids .
    // return : array containing all the details of posts

    public function get_all_posts($group_type, $PostsArray)
    {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');

        $data = array();
        if (!empty($PostsArray)) {
            $data['posts_list'] = $PostsArray;
            $data['posts_file_list'] = array();
            $data['posts_reply_list'] = array();
            $data['posts_like_current_user'] = array();
            $data['posts_follow_current_user'] = array();
            $data['posts_like_all_user'] = array();
            $data['posts_reply_like_all_user'] = array();
            $data['posts_reply_like_current_user'] = array();
            $data['posts_notified_users_list'] = array();
            $data['posts_reply_notified_users_list'] = array();

            $data['posts_update_details'] = array();
            $data['posts_poll_details'] = array();
            $data['posts_poll_answer_details'] = array();
            $data['posts_poll_answer_vote_details'] = array();
            $data['posts_praise_details'] = array();
            $data['posts_praise_member_details'] = array();
            $data['posts_announce_details'] = array();
            $data['group_type'] = array();

            foreach ($PostsArray as $post_id) {
                if ($group_type == "fixed") {
                    $data['group_type'][$post_id] = "fixed";
                    $GetAllPosts = "SELECT t1.*,t1.post_id, "
                        . "t2.first_name, t2.last_name, t2.profile_picture "
                        . "FROM college_fixed_group_posts t1 "
                        . "INNER JOIN users t2 ON t1.user_id = t2.id "
                        . "WHERE t1.college_id = '$college_id' AND t1.post_id = '$post_id'"
                        . "group by t1.post_id "
                        . "ORDER BY t1.timestamp DESC ";
                    $GetAllPosts_Result[$post_id] = $this->data_fetch->data_query($GetAllPosts);
                } else if ($group_type == "unfixed") {
                    $data['group_type'][$post_id] = "unfixed";
                    $GetAllPosts = "SELECT t1.*,t1.id as post_id, "
                        . "t2.first_name, t2.last_name, t2.profile_picture "
                        . "FROM college_unfixed_group_posts t1 "
                        . "INNER JOIN users t2 ON t1.user_id = t2.id "
                        . "WHERE college_id = '$college_id' AND t1.id = '$post_id' "
                        . "GROUP BY t1.id "
                        . "ORDER BY t1.timestamp DESC ";
                    $GetAllPosts_Result[$post_id] = $this->data_fetch->data_query($GetAllPosts);
                }
            }
            $data['posts_list'] = $GetAllPosts_Result;

            foreach ($data['posts_list'] as $value) {
                if (!empty($value)) {
                    $value = $value[0];
                    $post_id = $value->post_id;

                    //post like for the current user
                    if ($group_type == "fixed") {
                        $sql_query = "SELECT post_id as id FROM `college_fixed_group_posts` "
                            . "WHERE `post_id` = '$post_id' AND `user_id` = '$user_id' "
                            . "AND `college_id` = '$college_id' LIMIT 1";
                    } else {
                        $sql_query = "SELECT `id` FROM `college_unfixed_group_posts` "
                            . "WHERE `id` = '$post_id' AND `user_id` = '$user_id' "
                            . "AND `college_id` = '$college_id' LIMIT 1";
                    }
                    $query_result = $this->data_fetch->data_query($sql_query);
                    if (count($query_result)) {
                        $data['posts_current_user'][$post_id] = 1;
                    } else {
                        $data['posts_current_user'][$post_id] = 0;
                    }
                    
                    switch ($value->type) {
                        case 'update':
                            $sql_query = "SELECT id AS update_id, update_content FROM ";

                            if ($group_type == "fixed") {
                                $sql_query .= "college_fixed_group_update_post_details "
                                    . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                            } else {
                                $sql_query .= "college_unfixed_group_update_post_details "
                                    . "WHERE post_id = '$post_id'";
                            }
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_update_details'][$post_id] = $query_result;
                            
                            break;
                        case 'poll':
                            $sql_query = "SELECT id AS poll_id, question FROM ";
                            if ($group_type == "fixed") {
                                $sql_query .= "college_fixed_group_poll_post_details "
                                    . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                            } else {
                                $sql_query .= "college_unfixed_group_poll_post_details "
                                    . "WHERE post_id = '$post_id'";
                            }
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_poll_details'][$post_id] = $query_result;

                            foreach ($data['posts_poll_details'][$post_id] as $poll_value) {
                                $poll_id = $poll_value->poll_id;
                                $sql_query = "SELECT id as ans_id, answer FROM ";
                                if ($group_type == "fixed")
                                    $sql_query .= "college_fixed_group_poll_post_answer_details ";
                                else
                                    $sql_query .= "college_unfixed_group_poll_post_answer_details ";
                                $sql_query .= "WHERE post_id = '$post_id' AND poll_id = '$poll_id'";
                                $query_result = $this->data_fetch->data_query($sql_query);
                                $data['posts_poll_answer_details'][$post_id][$poll_id] = $query_result;

                                foreach ($data['posts_poll_answer_details'][$post_id][$poll_id] as $poll_ans_value) {
                                    $poll_ans_id = $poll_ans_value->ans_id;
                                    $sql_query = "SELECT count(id) as vote_count, voted_user_id FROM ";
                                    if ($group_type == "fixed")
                                        $sql_query .= "college_fixed_group_poll_post_answer_vote_details ";
                                    else
                                        $sql_query .= "college_unfixed_group_poll_post_answer_vote_details ";
                                    $sql_query .= "WHERE post_id = '$post_id' AND poll_id = '$poll_id' AND ans_id = '$poll_ans_id'";
                                    $query_result = $this->data_fetch->data_query($sql_query);
                                    $data['posts_poll_answer_vote_details'][$post_id][$poll_id][$poll_ans_id] = $query_result;
                                }
                                if ($group_type == "fixed") {
                                    $sql_query = "SELECT `id` FROM `college_fixed_group_poll_post_answer_vote_details` ";
                                } else {
                                    $sql_query = "SELECT `id` FROM `college_unfixed_group_poll_post_answer_vote_details` ";
                                }
                                $sql_query .= "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
                                $query_result = $this->data_fetch->data_query($sql_query);
                                if (count($query_result)) {
                                    $data['posts_poll_answer_vote_current_user'][$post_id] = 1;
                                } else {
                                    $data['posts_poll_answer_vote_current_user'][$post_id] = 0;
                                }
                            }
                            break;
                        case 'praise':
                            $sql_query = "SELECT t1.id AS praise_id,t1.praise_content,t1.badge_id, t2.image as badge_image ";
                            if ($group_type == "fixed") {
                                $sql_query .= "FROM college_fixed_group_praise_post_details t1 "
                                    . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                                    . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                            } else {
                                $sql_query .= "FROM college_unfixed_group_praise_post_details t1 "
                                    . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                                    . "WHERE post_id = '$post_id'";
                            }
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_praise_details'][$post_id] = $query_result;

                            foreach ($data['posts_praise_details'][$post_id] as $praise_value) {
                                $praise_id = $praise_value->praise_id;
                                $sql_query = "SELECT member_user_id as praised_user_id FROM ";
                                if ($group_type == "fixed") {
                                    $sql_query .= "college_fixed_group_praise_post_member_details ";
                                } else {
                                    $sql_query .= "college_unfixed_group_praise_post_member_details ";
                                }
                                $sql_query .= "WHERE post_id = '$post_id' AND praise_id = '$praise_id'";

                                $query_result = $this->data_fetch->data_query($sql_query);
                                $data['posts_praise_member_details'][$post_id] = $query_result;
                            }
                            break;
                        case 'announce':
                            $sql_query = "SELECT id AS announce_id, heading,content FROM ";
                            if ($group_type == "fixed") {
                                $sql_query .= "college_fixed_group_announce_post_details "
                                    . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                            } else {
                                $sql_query .= "college_unfixed_group_announce_post_details "
                                    . "WHERE post_id = '$post_id'";
                            }
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_announce_details'][$post_id] = $query_result;
                            break;
                    }

                    //post files list
                    if ($group_type == "fixed") {
                        $sql_query = "SELECT `id`,`file_type`,`file_path` "
                            . "FROM `college_fixed_group_posts_files` WHERE `post_id` = '$post_id'";
                    } else {
                        $sql_query = "SELECT `id`,`file_type`,`file_path` "
                            . "FROM `college_unfixed_group_posts_files` WHERE `post_id` = '$post_id'";
                    }
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_file_list'][$post_id] = $query_result;

                    //post reply msg
                    if ($group_type == "fixed") {
                        $sql_query = "SELECT post_reply_id AS post_reply_id,`user_id`,`reply_message`,`reply_like`,`timestamp` ";
                        $sql_query .= "FROM `college_fixed_group_posts_reply` ";
                    } else {
                        $sql_query = "SELECT id AS post_reply_id,`user_id`,`reply_content`,`timestamp` ";
                        $sql_query .= "FROM `college_unfixed_group_post_reply` ";
                    }
                    $sql_query .= "WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_reply_list'][$post_id] = $query_result;

                    //post reply like
                    foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                        $post_reply_id = $inner_value->post_reply_id;

                        $sql_query = "SELECT `user_id` FROM ";
                        if ($group_type == "fixed")
                            $sql_query .= "`college_fixed_group_posts_reply_like_details` ";
                        else
                            $sql_query .= "`college_unfixed_group_post_reply_like_details` ";

                        $sql_query .= "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";

                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                        //post like for the current user
                        $sql_query = "SELECT `id` FROM ";
                        if ($group_type == "fixed")
                            $sql_query .= "`college_fixed_group_posts_reply_like_details` ";
                        else
                            $sql_query .= "`college_unfixed_group_post_reply_like_details` ";

                        $sql_query .= "WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' "
                            . "AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";

                        $query_result = $this->data_fetch->data_query($sql_query);

                        if (count($query_result)) {
                            $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                        } else {
                            $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                        }

                        //post reply notified user list
                        $sql_query = "SELECT `user_id` FROM ";
                        if ($group_type == "fixed")
                            $sql_query .= "`college_fixed_group_posts_reply_notify_user_details` ";
                        else
                            $sql_query .= "`college_unfixed_group_post_reply_notify_user_details` ";
                        $sql_query .= "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";

                        $query_result = $this->data_fetch->data_query($sql_query);
                        if (count($query_result)) {
                            $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                        } else {
                            $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                        }
                    }

                    //post reply like
                    if ($group_type == "fixed") {
                        $sql_query = "SELECT post_reply_id AS post_reply_id,`user_id` "
                            . "FROM `college_fixed_group_posts_reply_like_details` ";
                    } else {
                        $sql_query = "SELECT id AS post_reply_id,`user_id` "
                            . "FROM `college_unfixed_group_post_reply_like_details` ";
                    }
                    $sql_query .= "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";

                    $query_result = $this->data_fetch->data_query($sql_query);
                    foreach ($query_result as $value) {
                        $data['posts_reply_like'][$post_id][$value->post_reply_id] = $value->user_id;
                    }

                    //post like for all the user
                    $sql_query = "SELECT `id`,`user_id` FROM ";
                    if ($group_type == "fixed")
                        $sql_query .= "`college_fixed_group_posts_like_details` ";
                    else
                        $sql_query .= "`college_unfixed_group_post_like_details` ";
                    $sql_query .= "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_like_all_user'][$post_id] = $query_result;

                    //post like for the current user
                    $sql_query = "SELECT `id` FROM ";
                    if ($group_type == "fixed")
                        $sql_query .= "`college_fixed_group_posts_like_details` ";
                    else
                        $sql_query .= "`college_unfixed_group_post_like_details` ";
                    $sql_query .= "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";

                    $query_result = $this->data_fetch->data_query($sql_query);

                    if (count($query_result)) {
                        $data['posts_like_current_user'][$post_id] = 1;
                    } else {
                        $data['posts_like_current_user'][$post_id] = 0;
                    }

                    //post follow for the current user
                    $sql_query_follow = "SELECT `id` FROM ";
                    if ($group_type == "fixed")
                        $sql_query_follow .= "`college_fixed_group_posts_follow` ";
                    else
                        $sql_query_follow .= "`college_unfixed_group_posts_follow` ";
                    $sql_query_follow .= "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                    $sql_query_follow_result = $this->data_fetch->data_query($sql_query_follow);

                    if (count($sql_query_follow_result)) {
                        $data['posts_follow_current_user'][$post_id] = 1;
                    } else {
                        $data['posts_follow_current_user'][$post_id] = 0;
                    }

                    //Post notified user list
                    $sql_query = "SELECT `user_id` FROM ";
                    if ($group_type == "fixed")
                        $sql_query .= "`college_fixed_group_posts_notify_user_details` ";
                    else
                        $sql_query .= "`college_unfixed_group_posts_notify_user_details` ";
                    $sql_query .= "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";


                    $query_result = $this->data_fetch->data_query($sql_query);
                    if (count($query_result)) {
                        foreach ($query_result as $value) {
                            $data['posts_notified_users_list'][$post_id][] = $value->user_id;
                        }
                    } else {
                        $data['posts_notified_users_list'][$post_id] = array();
                    }
                }
            }
        }
        return $data;
    }

    public function GetFixedPostDetails($all_posts)
    {
        $FixedPostsArray = array();
        foreach ($all_posts as $value) {
            if ($value->group_type == "fixed") {
                $FixedPostsArray[] = $value->post_id;
            }
        }

        if (count($FixedPostsArray)) {
            $FixedPostsArray = array_unique($FixedPostsArray);
            return $FixedPostDetails = $this->posts_model->get_all_posts("fixed", $FixedPostsArray);
        }
    }

    public function GetUnfixedPostDetails($all_posts)
    {
        $UnfixedPostsArray = array();
        foreach ($all_posts as $value) {
            if ($value->group_type == "unfixed") {
                $UnfixedPostsArray[] = $value->post_id;
            }
        }

        if (count($UnfixedPostsArray)) {
            $UnfixedPostsArray = array_unique($UnfixedPostsArray);
            return $UnfixedPostDetails = $this->get_all_posts("unfixed", $UnfixedPostsArray);
        }
    }
}
