<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_update extends CI_Model {

//normal query function 
    public function data_query($sql_query) {
        return $this->db->query($sql_query);
    }

    public function update_fields($table_name, $data_array, $where_array) {
        $this->db->where($where_array);
        if ($this->db->update($table_name, $data_array)) {
            return TRUE;
        } else {
            return "error";
        }
        return $id;
    }

}
