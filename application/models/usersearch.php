<?php

class UserSearch extends CI_Model {

    /**
     * @param $College_id
     * @param $type
     * @return
     * @internal param $UserID
     */
    //function to short the reslt by timestamp.


    public function getUserList($College_id, $type) {
        $logged_user_id = $this->ion_auth->user()->row()->id;

        $StudentsList = array();
        $AlumniList = array();
        $TeacherList = array();
        $SelectCollegeUsers = "SELECT t1.*,t3.* "
                . "FROM `college_users_intranet` t1 "
                . " INNER JOIN users t2 ON t1.user_id = t2.id "
                . " INNER JOIN chats as t3 ON (t1.user_id = t3.user_id1 OR t1.user_id = t3.user_id2)"
                . " WHERE (`college_id` = '$College_id' AND user_id != '$logged_user_id') "
                . " AND (t3.user_id1='$logged_user_id' OR t3.user_id2='$logged_user_id')"
                . " ORDER BY t3.last_timestamp DESC LIMIT 10";

        $SelectCollegeUsers_Result = $this->db->query($SelectCollegeUsers)->result();


        if (count($SelectCollegeUsers_Result)) {
            foreach ($SelectCollegeUsers_Result as $value) {
                $intranet_user_type = $value->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->db->query($SelectDetailsOfStudent);
                        if ($SelectDetailsOfStudent_Result->num_rows() >= 1) {
                            $SelectDetailsOfStudent_Result = $SelectDetailsOfStudent_Result->result();
                            foreach ($SelectDetailsOfStudent_Result as $value1) {
                                $StudentsList[$value1->college_users_intranet_id] = $value1;
                            }
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->db->query($SelectDetailsOfAlumni);
                        if ($SelectDetailsOfAlumni_Result->num_rows() >= 1) {
                            $SelectDetailsOfAlumni_Result = $SelectDetailsOfAlumni_Result->result();
                            foreach ($SelectDetailsOfAlumni_Result as $value2) {
                                $AlumniList[$value2->college_users_intranet_id] = $value2;
                            }
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->db->query($SelectDetailsOfTeacher);
                        if ($SelectDetailsOfTeacher_Result->num_rows() >= 1) {
                            $SelectDetailsOfTeacher_Result = $SelectDetailsOfTeacher_Result->result();
                            foreach ($SelectDetailsOfTeacher_Result as $value3) {
                                $TeacherList[$value3->college_users_intranet_id] = $value3;
                            }
                        }
                        break;
                }
            }

            if ($type == 'student')
                if (!empty($StudentsList)) {
                    return $StudentsList;
                }

            if ($type == 'alumni')
                if (!empty($AlumniList)) {
                    return $AlumniList;
                }

            if ($type == 'teacher')
                if (!empty($TeacherList)) {
                    return $TeacherList;
                }
        }
    }

    public function FilterCollegeMembers_Method($College_id, $Item, $logged_user_id) {
        if ($Item != "" && $College_id) {

            $SelectCollegeUsers = "SELECT t1.* "
                    . "FROM `college_users_intranet` t1 "
                    . "INNER JOIN users t2 ON t1.user_id = t2.id "
                    . "WHERE `college_id` = '$College_id' AND user_id != '$logged_user_id' "
                    . "ORDER BY t2.first_name ASC";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            $List = array();
            foreach ($SelectCollegeUsers_Result as $key => $value) {
                $intranet_user_type = $value->intranet_user_type;

                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id ";
                        if ($Item != "") {
                            $SelectDetailsOfStudent .= "AND (t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id ";
                        if ($Item != "") {
                            $SelectDetailsOfAlumni .= "AND (t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                }
            }
            foreach ($List as $key => $value) {
                $List[$key] = $this->encryption_decryption_object->EncryptThis($value);
            }
            return $List;
        }
    }

    public function CheckChatExists($user_id, $chatwith) {

        $query = "SELECT * FROM `chats` where (user_id1 ='$user_id' AND user_id2 ='$chatwith') OR (user_id1 ='$chatwith' AND user_id2 ='$user_id') LIMIT 0 , 30";
        $query_result = $this->data_fetch->data_query($query);

        if (count($query_result) > 0) {
            foreach ($query_result as $value) {
                return $value->chat_id;
            }
        } else {
            return 0;
        }
    }

//Function to check that inbox chat exist or not.
    public function checkInboxChatExist($user_id, $chatwith) {
        $query = "SELECT * FROM chat_user"
                . " INNER JOIN chats on chat_user.chat_id=chats.chat_id"
                . " WHERE ((user_id1 ='$user_id' AND user_id2 ='$chatwith') OR (user_id1 ='$chatwith' AND user_id2 ='$user_id'))"
                . " AND chat_user.message_type=1"
                . " LIMIT 0 , 30";
        $query_result = $this->db->query($query);
        // echo $this->db->last_query(); 
        $query_result = $query_result->result();
        if (count($query_result) > 0) {
            foreach ($query_result as $value) {
                return $value->chat_id;
            }
        } else {
            return 0;
        }
    }

    //Fnction to check the user is Accepted or Denied
    public function getUserChatStatus($chat_id) {
        $status = $this->db->select('chat_status')->from('chats')->where('chat_id', $chat_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        }
    }

    //get the chat response.
    public function getUserChatResponse($chat_id) {
        $chatResponse = $this->db->select('chat_response')->from('chats')->where('chat_id', $chat_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        }
    }

    public function GetChatLast30Messages($check_chat_exists) {
        $query = "SELECT * FROM ( SELECT *  FROM `chat_user` WHERE `chat_id` = $check_chat_exists ORDER BY  `chat_user`.`timestamp` DESC LIMIT 0 , 30) sub ORDER BY id ASC";
        $query_result = $this->data_fetch->data_query($query);

        return $query_result;
    }

    public function CreateChat($user_id, $chatwith) {
        $query = "INSERT INTO `chats` (`user_id1`, `user_id2`) VALUES ('$user_id', '$chatwith')";
        $query_result = $this->data_insert->data_query($query);
        if ($query_result) {
            return true;
        } else {
            return false;
        }
    }

    public function insertChat($chat_id, $user_id, $message) {

        //Update the last_timestamp.
        $query2 = "UPDATE chats SET last_timestamp=CURRENT_TIMESTAMP WHERE chat_id='$chat_id' AND (user_id1='$user_id' OR user_id2='$user_id')";
        $this->db->query($query2);

        $query = "INSERT INTO `chat_user`(`chat_id`, `user_id`, `message`, `timestamp`) "
                . "VALUES ('$chat_id', '$user_id', '$message', CURRENT_TIMESTAMP);";

        $query_result = $this->data_insert->data_query($query);
        if ($query_result) {
            return true;
        } else {
            return false;
        }
    }

    /* Function to insert inbox message */

    public function insertInboxMessage($chat_id, $senderId, $messageContent) {
        //For inbox message message tyep is 1 and for other 0.
        $message_type = 1;
        $query = "INSERT INTO `chat_user`(`chat_id`, `user_id`, `message`, `timestamp`,`message_type`) "
                . "VALUES ('$chat_id', '$senderId', '$messageContent', CURRENT_TIMESTAMP,$message_type);";

        $query_result = $this->db->query($query);
        if ($query_result) {
            return $inserted_id = $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function getLatestMessage($user_id, $chat_id, $timestamp) {
        $query = "SELECT *  FROM `chat_user` WHERE `chat_id` = $chat_id AND `timestamp` > '$timestamp' AND user_id !=  '$user_id'";
        $query_result = $this->data_fetch->data_query($query);
        return $query_result;
    }

//Code to get the latest group message.
    public function getLatestGroupMessage($user_id, $chatWith, $timestamp) {
        $query = "SELECT m.*,u.profile_picture FROM `college_chat_groups_users_messages` m "
                . "INNER JOIN users u On u.id=m.sender_id "
                . "WHERE m.college_chat_groups_id='$chatWith' AND m.timestamp > '$timestamp' AND m.sender_id != '$user_id'";
        $query_result = $this->db->query($query);
        $result = $query_result->result();
        if ($result) {
            $chat_content = $result;
        } else {
            $chat_content = "";
        }
        return $chat_content;
    }

    //Change the chat staus according to user response.
    public function changeChatStatus($chat_response, $chat_id) {
        return $query_result = $this->db->query("UPDATE `scholerspace`.`chats` SET `chat_response` = $chat_response, chat_status='1' WHERE `chats`.`chat_id` = $chat_id ");
    }

    //Get the all total number of unread message.
    public function getAllUnreadMessage($user_id) {
        $query = $this->db->query("SELECT user_id,read_status FROM chat_user WHERE chat_id IN(SELECT chat_id FROM chats WHERE user_id='$user_id' OR user_id2='$user_id') AND read_status='0' ");
        $total_unread_message = 0;
        if ($query->num_rows() >= 1) {
            $result = $query->result();
            for ($i = 0; $i < $query->num_rows(); $i++) {
                $crows = $result[$i];
                $cuser_id = $crows->user_id;
                if ($cuser_id != $user_id) {
                    $total_unread_message++;
                }
            }
        }

        return $total_unread_message;
    }

    //Change the all associated with the user to read.
    public function changeMessageStatus($user_id) {
        $result = $this->db->query("UPDATE chat_user SET read_status='1'"
                . " WHERE user_id='$user_id'"
                . " AND read_status='0'"
        );

        return $this->db->affected_rows();
    }

    //Get the all detail  of the  message receiver.
    public function getReceiverDetils($chatwith) {
        $this->db->select('id,first_name,last_name,profile_picture')->from('users')->where('id', $chatwith);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //Get the message details.
    public function getMessageDetails($id) {
        $this->db->select('first_name,last_name,profile_picture')->from('users');
        $this->db->join('chat_user', 'users.id=chat_user.user_id', 'INNER');
        $this->db->where('users.id', $id);
        $this->db->order_by('chat_user.timestamp');
        $this->db->limit(30);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //Get the all unred message realted to user
    public function GetChatAllUnreadMessages($check_chat_exists, $user_id) {

        $query = "SELECT * FROM ( SELECT *  FROM `chat_user` WHERE `chat_id` = $check_chat_exists AND 'read_status'=0 AND user_id=$user_id ORDER BY  `chat_user`.`timestamp` DESC ) sub  ORDER BY id ASC";
        $query_result = $this->db->query($query);
        // $this->db->last_query(); 
        if ($query_result->num_rows() >= 1) {
            return $query_result->num_rows();
        } else {
            return FALSE;
        }
    }

    //Get the all filtered user to show in the list.
    public function displayChatUserList($searchKey) {
        $user_id = $this->ion_auth->user()->row()->id;
//        $this->db->select('id,first_name,last_name');
//        $this->db->from('users');
//        $this->db->where('id !=', $user_id);
//        $like = array(
//            'first_name' => $searchKey,
//            'last_name' => $searchKey,
//            'email' => $searchKey
//        );
//        $this->db->or_like($like, 'after');
//        $result = $this->db->get();
        $sql = "SELECT `id`, `first_name`, `last_name` FROM (`users`) "
                . "WHERE `id` != '$user_id' "
                . "AND ( `first_name` LIKE '%$searchKey%' OR `last_name` LIKE '%$searchKey%' OR `email` LIKE '%$searchKey%')";
        $result = $this->db->query($sql)->result();

//         echo $this->db->last_query();
        if (count($result) >= 1) {
            return $result;
        } else {

            return FALSE;
        }
    }

    //Create the group chat.
    public function createChatGroup($groupName, $groupMemeberId) {

        $this->db->insert('college_chat_groups', $groupName);
        $inserted_id = $this->db->insert_id();
        // echo $inserted_id;  
        //group_chat_id
        //user_id
        //is_admin

        $collegeMembers = array();
        for ($i = 0; $i < count($groupMemeberId); $i++) {
            $collegeMembers['group_chat_id'] = $inserted_id;
            $collegeMembers['user_id'] = $groupMemeberId[$i];
            //code to check whether user_id is admin user_id.
            if ($groupMemeberId[$i] == $this->ion_auth->user()->row()->id) {
                $collegeMembers['is_admin'] = '1';
            } else {
                $collegeMembers['is_admin'] = '0';
            }
            $this->db->insert('college_chat_groups_users', $collegeMembers);
        }
    }

//Show chats group associate with the user.
    public function showChatGroup($use_id) {
        $query = "select chat_group.group_name, chat_group.group_chat_id
             from  college_chat_groups_users as group_user 
             inner join college_chat_groups as chat_group
             ON group_user.group_chat_id=chat_group.group_chat_id
             where group_user.user_id=73";
        $result = $this->db->query($query);

        if ($result->num_rows() > 0) {
//                    echo "<pre >";
//                    print_r($result->result()); die();

            return $result->result();
        } else {
            return FALSE;
        }
    }

    //Show the group details like group_name and user name associated with the group.
    public function getChatGroupDetails($groupChatId) {
        $query1 = "SELECT distinct(t1.group_chat_id),t2.group_name 
            from college_chat_groups_users as t1
            inner join college_chat_groups as t2
            ON t1.group_chat_id=t2.group_chat_id
            where t1.group_chat_id=$groupChatId";

        $result = $this->db->query($query1);
        $result = $result->result();
        $result = $result[0];
        $group_name = $result->group_name;
        $group_id = $result->group_chat_id;

        $this->db->select('user_id')->from('college_chat_groups_users')->where('group_chat_id', $group_id);
        $result2 = $this->db->get();
        if ($result2->num_rows() > 0) {
            $noOfUsers = $result2->num_rows();
            $result2 = $result2->result();
            //get the user full name .
            $userNames = array();
            for ($i = 0; $i < $noOfUsers; $i++) {

                $this->db->select("first_name,last_name")->from('users')->where('id', $result2[$i]->user_id);

                $result3 = $this->db->get();
                $result3 = $result3->result();
                $result3 = $result3['0'];
                $userName = $result3->first_name . " " . $result3->last_name;
                array_push($userNames, $userName);
            }
        }

        $groupDetails = array(
            'group_name' => $group_name,
            'group_members' => $userNames
        );
        return $groupDetails;
    }

    /*
     * Code to check whether group_id is valid group_id 
     * Code to check whether user_id is associated with the group
     */

    public function CheckGroupChatExists($user_id, $group_id) {
        $this->db->select('id');
        $this->db->from('college_chat_groups_users_messages');
        $this->db->where('college_chat_groups_id', $group_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {//group exist
            $this->db->select('college_chat_groups_users_messages.college_chat_groups_id');
            $this->db->from('college_chat_groups_users_messages');
            $this->db->join('college_chat_groups_users', 'college_chat_groups_users_messages.college_chat_groups_id=college_chat_groups_users.group_chat_id', 'INNER JOIN');
            $this->db->where('college_chat_groups_users.group_chat_id', $group_id);
            $result2 = $this->db->get();

            if ($result2->num_rows() > 0) {
                return $result->result();
            } else {
                return FALSE;
            }
        } else {//group with the provided id doesnot exist.
            return FALSE;
        }
    }

    /* code to get the latest 30 message */

    public function getGroupChatLast30Messages($group_id) {
        $query = "SELECT * FROM (SELECT * FROM college_chat_groups_users_messages WHERE college_chat_groups_id='$group_id '"
                . " ORDER By 'timestamp' DESC LIMIT 0,30 ) sub ORDER By timestamp ASC";
        $result = $this->db->query($query);
        //  get all images of associated with sender or reciever.
        $query2 = "SELECT DISTINCT sender_id,profile_picture FROM college_chat_groups_users_messages "
                . " INNER JOIN users"
                . " ON college_chat_groups_users_messages.sender_id=users.id"
                . " WHERE college_chat_groups_id='$group_id '";

        $result2 = $this->db->query($query2);

        $senderPic = $result2->result();
        $message = $result->result();

        $resultArray = array();
        $resultArray['sendePic'] = $senderPic;
        $resultArray['message'] = $message;
        return $resultArray;
//         
    }

    //code to insert the message.
    public function insertGroupChat($chat_group_id, $message, $message_sender) {
        $mesage_body = array(
            'college_chat_groups_id' => $chat_group_id,
            'message' => $message,
            'sender_id' => $message_sender
        );
        $this->db->insert('college_chat_groups_users_messages', $mesage_body);
        $result = $this->db->affected_rows();
        if ($result > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    //function to get the all message sent by use through inbox message(Create message button).
    public function getInboxSentMessages($sender_id) {

        $sql = "SELECT chat_user.*,users.id,users.first_name,users.last_name,users.profile_picture FROM chat_user"
                . " INNER JOIN users"
                . " ON chat_user.user_id=users.id"
                . " WHERE chat_user.user_id=$sender_id AND message_type=1";
        $query_result = $this->db->query($sql)->result();
//        echo $this->db->last_query(); die();
        if (count($query_result) > 0) {
            return $query_result;
        } else {
            return FALSE;
        }
    }
  
    
  //get the all group associated with the user.
    public function getAllGroups($user_id){
    $sql1="SELECT t1.group_chat_id,t1.group_name FROM college_chat_groups AS t1"
            . " INNER JOIN college_chat_groups_users AS t2"
            . " ON t1.group_chat_id=t2.group_chat_id"
            . " WHERE t2.user_id=$user_id";
    
    $sql_query1=$this->db->query($sql1);
    $result1=$sql_query1->result(); 
    
    
//   //to get the all college group.
       $college_id = $this->session->userdata('college_id');
//      $sql1="SELECT * FROM college_groups WHERE college_id=$college_id AND visible_to='Public'";
//      $sql_query=$this->db->query($sql1);
//      $result= $sql_query->result();
////      echo "<pre />";
////      print_r($result); 
//    
       $sql2="SELECT DISTINCT college_groups.id, college_groups.* FROM college_groups"
               . " INNER JOIN college_group_users ON college_groups.id=college_group_users.group_id"
               . " WHERE (college_groups.college_id=$college_id AND college_groups.visible_to='Public')"
               . " OR "
               . " (college_group_users.user_id=$user_id"
               . " AND college_groups.visible_to='Members'"
               . " AND college_group_users.college_id=$college_id)";
       
      $sql_query2=$this->db->query($sql2);
//      echo $this->db->last_query(); 
      $result2=$sql_query2->result();
//      echo "<pre />";
//      print_r($result2);
//      //die();
//      
   $result=array(); 
   if(count($result1)>0)
    {
      $result['chat_group']=$result1; 
    }
    else
    {
     $result['chat_group']="";   
    }
    if(count($result2)>0)
    {
       $result['college_group']=$result2; 
    }
    else
    {
        $result['college_group']="";
    }
    return $result;
  }
    
  

    /* ..................... Coded By: Mahadev ..................... */

    public function getAllUserDetails($users_array, $search_string = "") {
        if (count($users_array)) {
            foreach ($users_array as $value) {
                $intranet_user_type = $value->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture,t4.email, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->user_intranet_id ";
                        if ($search_string != "") {
                            $SelectDetailsOfStudent .= "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%' OR t4.email LIKE '$search_string%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->db->query($SelectDetailsOfStudent)->result();
                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture,t4.email, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->user_intranet_id ";
                        if ($search_string != "") {
                            $SelectDetailsOfAlumni .= "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%' OR t4.email LIKE '$search_string%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->db->query($SelectDetailsOfAlumni)->result();
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture,t4.email, "
                                . "t5.intranet_user_type, "
                                . "'Teacher' AS stream_name, "
                                . "'' AS stream_course_name "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->user_intranet_id ";
                        if ($search_string != "") {
                            $SelectDetailsOfTeacher .= "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%' OR t4.email LIKE '$search_string%') ";
                        }
                        $SelectDetailsOfTeacher .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->db->query($SelectDetailsOfTeacher)->result();
                        foreach ($SelectDetailsOfTeacher_Result as $value3) {
                            $List[$value3->college_users_intranet_id] = $value3;
                        }
                        break;
                }
            }
            if (!empty($List)) {
                foreach ($List as $key => $value) {
                    $List[$key] = $this->encryption_decryption_object->EncryptThis($value);
                }
                return $List;
            }
        }
    }

    /* ..................... Mahadev code ends ..................... */
}
