<?php
include('intra_leftbar.php');
?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<input type="hidden" id="fixed_group_name" value="courses"/>

<div class="col-md-6">
    <div class="boards_tab"> 
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success ! 
                        <span id="success_alert_message" style="font-size: 15px;"></span>
                    </h4>
                </div>
                <div class="select_course_table">
                    <?php if ($user_details[0]->intranet_user_type == "teacher") { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="add_courses_teacher_div">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <button id="addNewCourseBtn" class="btn btn-primary" style="width: 100%;background-color: #4A7DCA; border-radius: 0px;"><i class="fa fa-plus"></i>&nbsp; Add a New Course</button>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                    <div class="row">                        
                        <div class="col-md-12">
                            <div class="course_type" style="border-right: 1px solid #e6e6e6; width: 100%;text-align: center">
                                <?php
                                $row_count = count($my_courses);
                                $row_count = (count($past_courses) > $row_count) ? count($past_courses) : $row_count;
                                $row_count = (count($future_courses) > $row_count) ? count($future_courses) : $row_count;
                                ?>
                                <table class="courses_table" style="width: 100%;">
                                    <thead>
                                        <?php if ($user_details[0]->intranet_user_type == "teacher") { ?>
                                            <tr style="width">
                                                <td colspan="3">Published Courses</td>
                                            </tr>
                                        <?php } ?>
                                        <tr style="width">
                                            <td class="td_right_border">My Courses</td>
                                            <td>Past Courses</td>
                                            <td class="td_left_border">Future Courses</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < $row_count; $i++) {
                                            ?>
                                            <tr>
                                                <td class="td_right_border">
                                                    <?php
                                                    if (key_exists($i, $my_courses)) {
                                                        ?>
                                                        <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($my_courses[$i]->course_id); ?>"><?php print_r($my_courses[$i]->course_name); ?></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (key_exists($i, $past_courses)) {
                                                        ?>
                                                        <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($past_courses[$i]->course_id); ?>"><?php print_r($past_courses[$i]->course_name); ?></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td class="td_left_border">
                                                    <?php
                                                    if (key_exists($i, $future_courses)) {
                                                        ?>
                                                        <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($future_courses[$i]->course_id); ?>"><?php echo $future_courses[$i]->course_name; ?></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <?php if ($user_details[0]->intranet_user_type == "teacher") { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="course_type" style="border-right: 1px solid #e6e6e6; width: 100%;text-align: center">
                                    <?php
                                    $row_count = count($drafted_my_courses);
                                    $row_count = (count($drafted_past_courses) > $row_count) ? count($drafted_past_courses) : $row_count;
                                    $row_count = (count($drafted_future_courses) > $row_count) ? count($drafted_future_courses) : $row_count;
                                    ?>
                                    <table class="courses_table" style="width: 100%;">
                                        <thead>
                                            <tr style="width">
                                                <td colspan="3">Draft Courses</td>
                                            </tr>
                                            <tr style="width">
                                                <td class="td_right_border">My Courses</td>
                                                <td>Past Courses</td>
                                                <td class="td_left_border">Future Courses</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < $row_count; $i++) {
                                                ?>
                                                <tr>
                                                    <td class="td_right_border">
                                                
                                                        <?php
                                                        if (key_exists($i, $drafted_my_courses)) {
                                                            ?>
                                                            <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($drafted_my_courses[$i]->course_id); ?>"><?php print_r($drafted_my_courses[$i]->course_name); ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (key_exists($i, $drafted_past_courses)) {
                                                            ?>
                                                            <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($drafted_past_courses[$i]->course_id); ?>"><?php print_r($drafted_past_courses[$i]->course_name); ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td class="td_left_border">
                                                        <?php
                                                        if (key_exists($i, $drafted_future_courses)) {
                                                            ?>
                                                            <a href="<?php echo site_url() . "user/courses/view/" . $encryption_decryption_object->encode($drafted_future_courses[$i]->course_id); ?>"><?php echo $drafted_future_courses[$i]->course_name; ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php     
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Notifications</h5>
            <?php
            $count = 0;
            foreach ($all_unread_alerts as $value) {
                $posted_id = $uid = $value->posted_user_id;
                $timestamp = $value->timestamp;
                if ($value->group_type == "fixed") {
                    $method_name = "view_post";
                } else if ($value->group_type == "unfixed") {
                    $method_name = "view_group_post";
                }
                ?>
                <div class="activities">
                    <div class="act_img">
                        <img
                            src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                            alt=""/>
                    </div>
                    <div class="alert_details">
                        <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                            <span>
                                <?php
                                switch ($value->type) {
                                    case 'notify':
                                        echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                        break;
                                    case 'reply':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " commented on your post.";
                                        } else {
                                            echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                        }
                                        break;
                                    case 'like':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " liked your post.";
                                        } else {
                                            echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                        }
                                        break;
                                }
                                ?>
                            </span><br>
                        </a>
                        <span class="alert_timestamp">
                            <?php
                            $posted_date = date('M j Y', strtotime($timestamp));
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                $count++;
                if ($count >= 5)
                    break;
            }
            ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
$this->load->view("front-end/page_modal/courses_modal");
include('footer2.php');
?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/courses.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/fixed_group.js" type="text/javascript"></script>