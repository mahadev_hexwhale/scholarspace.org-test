<?php include('intra_leftbar.php') ?>


<div class="col-md-6">
<div class="boards_tab" style="margin-top: 20px;">
<div>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="batch">
    
<div class="update_tab">
<div>
<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">  
      <a href="#update2" aria-controls="update2" role="tab" data-toggle="tab"> 
      <span class="update_icon1"></span>Update </a>
    </li>

    <li role="presentation"> <span class="point_top"></span> 
      <a href="#poll2" aria-controls="poll2" role="tab" data-toggle="tab"> 
      <span class="update_icon2"></span>Poll </a> 
  </li>

    <li role="presentation"> <span class="point_top"></span>
     <a href="#praise2" aria-controls="praise2" role="tab" data-toggle="tab"> 
      <span class="update_icon3"></span> Praise </a> 
    </li>

  <li role="presentation"> <span class="point_top"></span>
     <a href="#announce_tab2" aria-controls="announce_tab2" role="tab" data-toggle="tab"> 
      <span class="update_icon3"></span> Announcement </a> 
    </li>

    </ul>

  <!-- Tab panes -->
  <div class="tab-content" style="margin-top: 10px;">

 <div role="tabpanel" class="tab-pane active" id="update2">
<div class="intra_course_form">
<form>

<div class="intra_select">
<select>
  <option value="Re-offering earlier Course">Re-offering Earlier Course</option>
    <option value="Fresh Course">Fresh Course</option>
</select>
</div>

<div class="course_message">
 <div class="col-md-3"><label> Title </label></div>
  <div class="col-md-9"><input type="text" class="c_text" name="title"></div>
   </div>

<div class="course_message">
 <div class="col-md-3"><label> Course brief </label></div>
  <div class="col-md-9"><textarea type="text" class="c_text" rows="5"> </textarea></div>
   </div>

<div class="course_message">
 <div class="col-md-3"><label> Start date </label></div>
  <div class="col-md-9"><input type="text" name="date" id="from" class="date_text" required=""/></div>
   </div>

<div class="course_message">
 <div class="col-md-3"><label> End date </label></div>
  <div class="col-md-9"><input type="text" name="date" id="to" class="date_text" required=""/></div>
   </div>


<div class="course_message">
 <div class="col-md-3"><label> Eligible Students </label></div>
  <div class="col-md-9"><input type="text" class="c_text" name="title"></div>
   </div>


<div class="course_message">
 <div class="col-md-3"><label> View Options </label></div>
  <div class="col-md-9"><input type="text" class="c_text" name="title"></div>
   </div>


<div class="course_message" style="margin-top:20px;">
 <div class="col-md-12">
  <button type="submit" class="green_btn">Save as draft</button>
    <button type="submit" class="blue_btn">Publish</button>
      <button type="submit" class="grey_btn">Change/Edit</button>
  </div>
   </div>


  </form>
</div>
</div>
<!-- end of first tab -->


<div role="tabpanel" class="tab-pane" id="poll2">
<form>
<div class="poll_share">
<div class="text_poll">  
<textarea class="share_text" placeholder="Whats your question ? " > 
</textarea> 
<span class="pin2"></span> 
</div>

<div class="add_company">
 <div class="company">All company</div>
<input type="text" placeholder="Add people to notify" class="poll_text">
</div>
<div class="box_a" id="open_topic2">
<input type="text" placeholder="Add topics" class="poll_text">
</div>

<div class="add_topics" id="topic2"> <span class="join_pin"></span> Add topics</div> 
<button type="submit" class="btn">Post</button>
</div>
</form>
</div>
<!-- end of second tab -->

  <div role="tabpanel" class="tab-pane" id="praise2">
<form>
<div class="poll_share">
<div class="text_poll">  
<textarea class="share_text" placeholder="Whats your question ? " > 
</textarea> 
<span class="pin2"></span> 
</div>

<div class="add_company">
 <div class="company">All company</div>
<input type="text" placeholder="Add people to notify" class="poll_text">
</div>
<div class="box_a" id="open_topic2">
<input type="text" placeholder="Add topics" class="poll_text">
</div>

<div class="add_topics" id="topic2"> <span class="join_pin"></span> Add topics</div> 
<button type="submit" class="btn">Post</button>
</div>
</form>
   </div>

<!-- end of third tab -->
  <div role="tabpanel" class="tab-pane" id="announce_tab2">
  <form action="#" method="post">
<div class="poll_share">
<div class="text_poll">  
<textarea class="share_text" placeholder="Whats your question ? " > 
</textarea> 
<span class="pin2"></span> 
</div>


<div class="add_company">
  <input type="text" placeholder="Add a group and/or people" class="add_text">
<div class="notify_pop2" id="second_notify">
<div class="row">
<div class="col-md-2">
<div class="side_icon">  
<span class="group_icon"></span>

</div>
</div>

<div class="col-md-10">
<div class="people_list">
  <p>You can only add one group</p>

<ul>
<li>
<div class="notify_img">
  <img src="image/company2.png" alt="">
   </div>
   <h5>All Company </h5> <h6>This is the default </h6>

</li>

<li>
<div class="notify_img">
  <img src="image/hr_img.png" alt="">
   </div>
<h5>HR </h5> 
</li>

</ul>
<!--end of list of group for the company -->


</div>
</div>

<div class="col-md-2">
<div class="side_icon">  
<span class="single_user"></span>
</div>
</div>

<div class="col-md-10">
<div class="people_list">

<ul>
<li> 
<div class="notify_img">
  <img src="image/recent_user3.png" alt="">
   </div>
<h5>Haneef </h5> <h6>Web developer</h6>
</li>

<li> 
<div class="notify_img">
  <img src="image/recent_user2.png" alt="">
   </div>
<h5>Andrew </h5> <h6>Web developer</h6>
 </li>

<li> 
<div class="notify_img">
  <img src="image/recent_user1.png" alt="">
   </div>
<h5>Haneef </h5> <h6>Web developer</h6>
 </li>

</ul>
</div>
</div>



</div>
</div>
<!-- add a group or people notify 2 -->

 <div class="company" id="remove_com"> 
  <span class="color_grp"></span> 
  <h6> All company </h6> 
  <span class="pop_close"></span> 
</div>
<input type="text" placeholder="Add people to notify" class="poll_text">
<!-- popup for add people to notify -->
<div class="notify_pop">
<div class="row">
<div class="col-md-2">
<div class="side_icon">  
<span class="group_icon"></span>
<span class="single_user"></span>
</div>
</div>

<div class="col-md-10">
<div class="people_list">
  <p>You can only add one group</p>
<ul>
<li> 
<div class="notify_img">
  <img src="image/recent_user3.png" alt="">
   </div>
<h5>Haneef </h5> <h6>Web developer</h6>
</li>

<li> 
<div class="notify_img">
  <img src="image/recent_user2.png" alt="">
   </div>
<h5>Andrew </h5> <h6>Web developer</h6>
 </li>

<li> 
<div class="notify_img">
  <img src="image/recent_user1.png" alt="">
   </div>
<h5>Haneef </h5> <h6>Web developer</h6>
 </li>

</ul>
</div>
</div>

</div>
</div>





</div>

<div class="box_a" id="open_topic">
<input type="text" placeholder="Add topics" class="poll_text">
</div>

<div class="add_topics" id="topic1"> <span class="join_pin"></span> Add topics</div> 
<button type="submit" class="btn">Post</button>
</div>
</form>
   </div>



</div>

</div>
</div>
<!-- end of update tab -->


  </div>
<!-- end of tab1 -->

    <div role="tabpanel" class="tab-pane" id="job">
     <h6>Campus</h6>
       <p>The institute has following major facilities:</p>
         </div>


    <div role="tabpanel" class="tab-pane" id="know">
      <h6>History</h6>
        <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
          </div>


    <div role="tabpanel" class="tab-pane" id="blog"></div>


  </div>
   </div>
    </div>


</div>

<div class="col-md-3">

<div class="right_bar">
<!-- <div class="progress_box">
<h4>Getting Started</h4>

<div class="progress">
<div class="progress-bar" role="progressbar" aria-valuenow="64"
  aria-valuemin="0" aria-valuemax="100" style="width:64%">
    <span class="sr-only">64% Complete</span>
  </div>
</div>
<h5>64%</h5>

<a href="#">Write your first post</a>
<a href="#">Get Yammer for desktop</a>
<a href="#">Get the Yammer Mobile App</a>
</div> -->
<!-- end of progress bar -->

<div class="recent_activity">
<h5>Recent Activities</h5>

<div class="activities">
 <div class="act_img">
  <img src="image/vr_img.png" alt="">
</div>  
<p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
</div>
<!-- end of 1 activity -->

<div class="activities">
 <div class="act_img">
  <img src="image/sa.png" alt="">
</div>  
<p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
</div>
<!-- end of 2 activity -->

<div class="activities">
 <div class="act_img">
  <img src="image/sm.png" alt="">
</div>  
<p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
</div>
<!-- end of 3 activity -->

<div class="activities">
 <div class="act_img">
  <img src="image/vr_blue.png" alt="">
</div>  
<p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
</div>
<!-- end of 4 activity -->

</div>
<!-- end of recent activity -->
</div>
<!-- end of right bar -->
</div>


</div>
</div>
</div>



<?php include('footer2.php') ?>


