<?php
include('intra_leftbar.php');
$get_college_admin_list_array = get_college_admin_list($this->session->userdata('college_id'));
$college_users = array();
foreach ($get_college_admin_list_array as $college_user_value) {
    $college_users[] = $college_user_value->user_id;
}
?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .edit_post_comment{
        margin-right: 5px;
    }
    .comment_label{
        margin-bottom: 5px;
    }
    .comment_reply_edit_btn{
        margin-right: 5px;
    }
</style>
<input type="hidden" id="fixed_group_name" value="internship"/>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-8">
            <div class="group_page_box" style="padding-top: 20px; padding-left: 20px; padding-right: 20px">
                <div class="profile_details" style="padding: 0px">
                    <div style="font-family: 'open_sansregular';color: #525252;">
                        <h3 style="font-size: 21px;">
                            <span id="internship_title">
                                <?php
                                echo $internship_details->post_title;
                                $posted_user = $this->ion_auth->user($internship_details->user_id)->row();
                                ?>
                            </span>
                            <button title="Edit internship"  post_id="<?php echo $encryption_decryption_object->encode($internship_details->post_id); ?>" class="btn btn-default btn-xs pull-right edit_post"><i class="fa fa-edit"></i></button>
                            <p style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">posted by 
                                <a href="#"><?php echo $posted_user->first_name . " " . $posted_user->last_name; ?></a>
                                <?php
                                $posted_date = date('M j Y', strtotime($internship_details->timestamp));
                                echo (($posted_date == date('M j Y')) ? ", today" : $posted_date) . " at " . date('h:i A', strtotime($internship_details->timestamp));
                                ?>
                            </p>
                        </h3>
                        <div id="internship_description" class="internship_description_details" style="margin-top: 10px;">
                            <?php
                            echo $internship_details->post_message;
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <?php
                                $current_user_like_status = 0;
                                $total_comment_count = 0;
                                foreach ($internship_like_status as $like_value) {
                                    if ($user->id == $like_value->user_id && $like_value->like_status == 1) {
                                        $current_user_like_status = 1;
                                    }
                                    if ($like_value->like_status == 1) {
                                        $total_comment_count++;
                                    }
                                }
                                ?>
                                <button type="button"  like_status ="<?php echo $current_user_like_status; ?>" class="btn btn-default btn-xs like_post_btn" internship_id="<?php echo $internship_id; ?>">
                                    <?php
                                    if ($current_user_like_status == 0) {
                                        ?>
                                        <i class="fa fa-thumbs-up"></i> Like
                                        <?php
                                    } else {
                                        ?>
                                        <i class="fa fa-thumbs-down"></i> Unlike
                                        <?php
                                    }
                                    ?>
                                </button>
                                <button type="button" id="write_comment_btn" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Write Comment</button>
                                <span class="job_description_details" style="color: #337ab7;<?php echo ($total_comment_count) ? "display:inline" : "display:none"; ?>"><span class="comment_count_like_span"><?php echo $total_comment_count; ?></span> Like</span>
                            </div>
                            <div class="col-md-6">
                                <span class="pull-right" style="font-family: 'open_sansregular';font-size: 12px;color: #626262;line-height: 15px;">
                                    FROM : <a id="internship_start_date" href="javascript:void(0)"><?php echo date("d-M-Y", strtotime($internship_details->start_date)); ?></a> to <a id="internship_end_date" href="#"><?php echo date("d-M-Y", strtotime($internship_details->end_date)); ?></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="comment_box" class="group_page_box" style="border: none; display: none">
                <form id="write_comment_form">
                    <div style="font-size: 21px;border: 1px solid #e8eced;font-size: 14px;padding: 10px;border: 1px solid #e8eced;font-family: 'open_sansregular';color: #626262;">
                        <?php
                        $current_user = $this->ion_auth->user()->row();
                        echo $current_user->first_name . " " . $current_user->last_name;
                        ?>
                        <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;"> writing Comment</span>
                        <span class="pull-right"><a href="javascript:void(0)" id="cancel_comment"><i class="fa fa-times"></i> cancel</a></span>
                    </div>
                    <div>
                        <textarea class="share_text" id="mytextarea"></textarea>
                        <input type="hidden" id="comment_hidden" name="comment"/>
                        <input type="hidden" name="internship_id" value="<?php echo $internship_id; ?>"/>
                    </div>
                    <div style="margin-top: 10px">
                        <button type="submit" id="comment_btn" class="btn pull-right">Comment</button>
                    </div>
                </form>
            </div>
            <div class="group_page_box" style="border: none; padding-left: 20px; padding-right: 20px">
                <h3 style="font-size: 21px; border-bottom: 1px solid #e8eced; padding-bottom: 5px;">Comments(<span id="comment_count_span"><?php echo count($internship_reply_list); ?></span>)</h3>
                <div id="comments_div">
                    <?php
                    foreach ($internship_reply_list as $value) {
                        ?>
                        <div class="comment_container">
                            <div class="comment_div">
                                <div class="profile_details" style="padding: 0px; margin-top: 5px;border-bottom: 1px solid #e8eced;">
                                    <div style="font-family: 'open_sansregular';color: #525252;">
                                        <h3 style="font-size: 21px;">
                                            <?php
                                            $posted_user = $this->ion_auth->user($value->user_id)->row();
                                            ?> 
                                            <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 14px;margin: 0px;line-height: 20px;"><a href="#"><?php echo $posted_user->first_name . " " . $posted_user->last_name; ?></a></span>
                                            <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">
                                                <?php
                                                $posted_date = date('M j Y', strtotime($value->timestamp));
                                                echo (($posted_date == date('M j Y')) ? " today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                                ?>
                                            </span>
                                            <?php
                                            if ($current_user->id == $posted_user->id || in_array($current_user->id, $college_users)) {
                                                ?>
                                                <button comment_id="<?php echo $encryption_decryption_object->encode($value->post_reply_id); ?>" class="btn btn-default btn-xs pull-right delete_post_comment"><i class="fa fa-trash"></i></button>
                                                <button comment_id="<?php echo $encryption_decryption_object->encode($value->post_reply_id); ?>" class="btn btn-default btn-xs pull-right edit_post_comment"><i class="fa fa-edit"></i></button>
                                                <?php
                                            }
                                            ?>
                                        </h3>
                                        <div id="internship_comment-<?php echo $encryption_decryption_object->encode($value->post_reply_id); ?>" class="internship_description_details" style="margin-top: 10px;">
                                            <?php
                                            echo $value->reply_message;
                                            ?>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-12">
                                                <?php
                                                $current_user_like_status = 0;
                                                $total_comment_count = 0;
                                                foreach ($internship_comment_like_status[$value->post_reply_id] as $like_value) {
                                                    if ($current_user->id == $like_value->user_id && $like_value->like_status == 1) {
                                                        $current_user_like_status = 1;
                                                    }
                                                    if ($like_value->like_status == 1) {
                                                        $total_comment_count++;
                                                    }
                                                }
                                                ?>
                                                <button type="button" like_status ="<?php echo $current_user_like_status; ?>" internship_id ="<?php echo $encryption_decryption_object->encode($value->post_id); ?>" comment_id="<?php echo $encryption_decryption_object->encode($value->post_reply_id); ?>" class="btn btn-default btn-xs comment_like_btn">
                                                    <?php
                                                    if ($current_user_like_status == 0) {
                                                        ?>
                                                        <i class="fa fa-thumbs-up"></i> Like
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <i class="fa fa-thumbs-down"></i> Unlike
                                                        <?php
                                                    }
                                                    ?>
                                                </button>
                                                <button type="button" class="btn btn-default btn-xs reply_comment_btn"><i class="fa fa-edit"></i> Reply </button>
                                                <span class="internship_description_details" style="color: #337ab7;<?php echo ($total_comment_count) ? "display:inline" : "display:none"; ?>"><span class="comment_count_like_span"><?php echo $total_comment_count; ?></span> Like</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="comment_reply_box">
                                    <form class="comment_reply_form">
                                        <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                                            <div style="font-size: 21px;border: 1px solid #e8eced;font-size: 14px;padding: 10px;border: 1px solid #e8eced;font-family: 'open_sansregular';color: #626262;">
                                                <?php
                                                $current_user = $this->ion_auth->user()->row();
                                                echo $current_user->first_name . " " . $current_user->last_name;
                                                ?>
                                                <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;"> replying to Comment</span>
                                                <span class="pull-right"><a href="javascript:void(0)" class="cancel_reply_to_comment"><i class="fa fa-times"></i> cancel</a></span>
                                            </div>
                                            <div>
                                                <textarea name="comment_reply" class="share_text comment_reply_textarea"></textarea>
                                                <input type="hidden" name="comment_id" value="<?php echo $encryption_decryption_object->encode($value->post_reply_id); ?>"/>
                                                <input type="hidden" name="internship_id" value="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"/>
                                            </div>
                                            <div class="row commet_reply_submit_button_div">
                                                <div class="col-md-12">
                                                    <button type="submit" style="margin-top: 5px" class="btn pull-right comment_reply_submit_btn btn-sm">Reply</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                                </div>
                            </div>
                            <div class="comments_reply_container" style="<?php echo (count($internship_comment_reply[$value->post_reply_id])) ? "display: block" : "display: none"; ?>">
                                <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 style="font-size: 15px;">Replies (<span class="comment_count_span"><?php echo count($internship_comment_reply[$value->post_reply_id]); ?></span>)</h4>
                                        </div>
                                    </div>
                                    <div class="row">&nbsp;</div>
                                    <div class="comment_reply_list">
                                        <?php
                                        foreach ($internship_comment_reply[$value->post_reply_id] as $comment_reply_value) {
                                            ?>
                                            <div class="comment_replies_box">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div style="font-family: 'open_sansregular';color: #525252;border-top: 1px solid #DEE3E4;">
                                                            <div class="job_description_details" style="padding: 0px">
                                                                <div class="row">
                                                                    <div class="col-md-12" id="internship_comment_reply-<?php echo $encryption_decryption_object->encode($comment_reply_value->post_reply_of_reply_id); ?>">
                                                                        <?php echo $comment_reply_value->reply_message; ?>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-bottom: 5px;">
                                                                    <?php
                                                                    $posted_user = $this->ion_auth->user($comment_reply_value->user_id)->row();
                                                                    ?>
                                                                    <div class="<?php echo ($posted_user->id == $user->id || in_array($user->id, $college_users)) ? "col-md-10" : "col-md-12"; ?>">
                                                                        <span class="pull-right" style="font-size: 11px; color: #A2AAAD; display: block">reply by
                                                                            <a href="javascript:void(0)">
                                                                                <?php
                                                                                $posted_date = date('M j Y', strtotime($comment_reply_value->timestamp));
                                                                                $posted_date = (($posted_date == date('M j Y')) ? ", today" : $posted_date) . " at " . date('h:i A', strtotime($comment_reply_value->timestamp));
                                                                                echo $posted_user->first_name . " " . $posted_user->last_name;
                                                                                ?>
                                                                            </a>
                                                                            <?php
                                                                            echo " at " . $posted_date;
                                                                            ?>
                                                                        </span>
                                                                    </div>
                                                                    <?php
                                                                    if ($posted_user->id == $user->id || in_array($user->id, $college_users)) {
                                                                        ?>
                                                                        <div class="col-md-2">
                                                                            <span class = "pull-right"><a href = "javascript:void(0)" internship_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_id); ?>" comment_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_reply_id); ?>" comment_reply_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_reply_of_reply_id); ?>" class = "comment_reply_delete_btn"><button class = "btn btn-xs"><i class = "fa fa-trash"></i> </button></a></span>
                                                                            <span class = "pull-right"><a href = "javascript:void(0)" internship_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_id); ?>" comment_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_reply_id); ?>" comment_reply_id = "<?php echo $encryption_decryption_object->encode($comment_reply_value->post_reply_of_reply_id); ?>" class = "comment_reply_edit_btn"><button class = "btn btn-xs"><i class = "fa fa-edit"></i> </button></a></span>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="right_bar">
                <div class="recent_activity" style="margin-top: 0px;">
                    <h5>Recent Activities</h5>
                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/vr_img.png" alt="">
                        </div>  
                        <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
                    </div>
                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/sa.png" alt="">
                        </div>  
                        <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
                    </div>

                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/sm.png" alt="">
                        </div>  
                        <p><a href="#">shihas Mandottil</a> has joined PHP Development</p>
                    </div>

                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/vr_blue.png" alt="">
                        </div>  
                        <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">

        </div>
        <div class="col-md-4">

        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/internship_details_modal");
?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/internship_details.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var post_count = 0;
    tinymce.init({
        selector: '#mytextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                comment_hidden = tinyMCE.get('mytextarea').getContent();
                $("#comment_hidden").val(comment_hidden);
                if (comment_hidden != '') {
                    post_count = 1;
                    $("#comment_btn").addClass("active_post_btn");
                } else {
                    $("#comment_btn").removeClass("active_post_btn");
                    post_count = 0;
                }
            });
        }
    });

    tinymce.init({
        selector: '#internshipCommentEditModalTextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                internship_comment = tinyMCE.get('internshipCommentEditModalTextarea').getContent();
                $("#internshipCommentEditModalText").val(internship_comment);
            });
        }
    });

    tinymce.init({
        selector: '#internshipCommentReplyEditModalTextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                internship_comment_reply = tinyMCE.get('internshipCommentReplyEditModalTextarea').getContent();
                $("#internshipCommentReplyEditModalText").val(internship_comment_reply);
            });
        }
    });

    tinymce.init({
        selector: '#internshipDescriptionEditModalTextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                internship_description = tinyMCE.get('internshipDescriptionEditModalTextarea').getContent();
                $("#internshipDescriptionEditModalText").val(internship_description);
            });
        }
    });

    //internshipTitleEditModalTextarea
    $("#internshipStartDateEditModalText").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#internshipEndDateEditModalText").datepicker("option", "minDate", selected)
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
    $("#internshipEndDateEditModalText").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#internshipStartDateEditModalText").datepicker("option", "maxDate", selected);
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
</script>