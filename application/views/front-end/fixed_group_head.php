<div role="tabpanel" class="tab-pane active" id="batch">
    <div class="update_tab">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">  <span class="point_top"></span> 
                    <a href="#update" aria-controls="update" role="tab" data-toggle="tab"> 
                        <span class="update_icon1"></span>Update </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="margin-top: 10px;">
                <div role="tabpanel" class="tab-pane active" id="update">
                    <div class="share_box">
                        <a href="javascript:void(0);" id="ontick_share">Share something with this group?</a>
                        <span class="pin"></span>
                    </div>
                    <div class="show_share">
                        <form id="group_post_update_form">
                            <div class="poll_share">
                                <div class="text_poll">  
                                    <textarea ata-autoresize class="share_text" id="update_textarea" name="update_message" placeholder="Whats your question ? "></textarea>
                                    <span class="pin2"></span>
                                    <div class="attach_list">
                                        <ul>
                                            <li> <a class="upload_file_from_computer"> <span class="upload_icon"></span> <h6>Upload a file from computer</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add_files" style="display: none">
                                    <ul id="file_upload_content_ul"></ul>
                                </div>
                                <div class="add_company">
                                    <!-- add a group or people notify 2 -->
                                    <div id="updateAddPeopleToNotify">
                                        <div id="notified_user_div"></div>
                                        <div class="updateAddPeopleInputDiv" id="updateAddPeopleInputDiv">
                                            <div class="new_notify_box" style="width: 100%;">
                                            <input type="text" placeholder="Add people to notify" class="poll_text" id="updateAddPeopleInput" name="note">
                                            <div class="notify_pop" style="left: 0px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="people_list" style="overflow-x: hidden;">
                                                            <ul id="users_list_ul"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- popup for add people to notify -->
                                </div>
                                <button type="submit" id="update_post" class="btn">Post</button>
                            </div>
                        </form>
                        <form id="image_upload_form" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload_post_file" id="upload_post_file" style="display:none">
                        </form>
                    </div>
                    <!-- end of share update -->
                </div>
            </div>
        </div>
    </div>