<?php include('header1.php') ?> 

<div class="body_height">

    <div class="college_details">
        <div class="row">
            <div class="col-md-1">
                <?php include('left_sidebar.php') ?>

            </div>

            <div class="col-md-9" style="margin-left: -56px;">

                <div class="about_colleges">

                    <div class="coll_info" style="border-bottom:none; margin-top: 27px;">

                        <div class="col-md-2">

                            <div class="coll_logo">

                                <img src="image/coll_logo1.png" alt="">

                            </div>

                        </div>

                        <div class="col-md-10">

                            <div class="course_details">
                                <div class="course_social">
                                    <a href="#"> Gurukul Vidyapeeth Institute of Engineering and Technology, Patiala </a>

                                    <div class="fg_share">
                                        <a href="#"> <span class="go_link"></span> </a> <a href="#"> <span class="go_share" style="margin-right: 13px;"></span> </a>
                                        <a href="#"> <span class="f_link"></span> </a> <a href="#"> <span class="f_share"></span> </a>
                                    </div>

                                </div>

                                <div class="web_location">
                                    <p> <span> Location: </span>Banur, Punjab  </p>
                                    <p> <span>Contact:</span> 91-01762-264400, 264406, 092161-55555, 094648-55555 </p>
                                    <p> <span>Website:</span> www.gurukul.cc </p>
                                    <p>Affiliated College</p>
                                </div>

                                <div class="rating"><h6>Rating </h6> <span class="star"></span>   <button class="btn">Add your Rating</button>  </div>

                            </div>


                        </div>
                    </div>
                    <!-- end of coll-info 1 -->

                    <div class="detail_tab">

                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">About the college</a> <span class="pointer"></span>   </li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Gallery </a> <span class="pointer"></span>   </li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Discussions  </a> <span class="pointer"></span>  </li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Events </a> <span class="pointer"></span>  </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">

                                    <h6>College Profile</h6>

                                    <p>Gurukul Vidyapeeth Institute of Engineering and Technology (GVIET) is located at Ramnagar- Banur in Rajpura, Patiala (Punjab).GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi. Major facilities are hostel, library, lab etc. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam augue magna, condimentum et vehicula vitae, consequat aliquam dui. Vivamus quis ornare massa. Nunc auctor ac sem nec sodales. Nunc ac tellus id diam aliquet malesuada. Aliquam et libero sed est euismod pulvinar vel in eros. Praesent eget magna non mauris maximus sodales eu at nulla. Sed ut molestie metus, in fringilla ligula. Nulla tincidunt nisl vel mi molestie, sit amet feugiat dui porttitor. Vestibulum id porta risus, mollis lacinia ipsum. Cras tincidunt, nisi sed viverra aliquet, nisl nunc egestas justo, eget facilisis mauris lectus a neque. Phasellus nulla mi, dictum nec rhoncus eget, tempor rhoncus risus.</p>


                                    <h6>History</h6>

                                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>

                                    <h6>Campus</h6>

                                    <p>The institute has following major facilities:</p>

                                    <ul>     
                                        <li> Wells stocked library </li>
                                        <li> Separate hostel facility for boys & girls  </li>
                                        <li> State of the art laboratories </li>
                                        <li> Workshop  </li>
                                        <li> Computer centre </li>
                                        <li> Digital Classroom </li>
                                        <li> Wi-Fi facility </li>
                                        <li> Bank/ATM </li>
                                        <li> Indoor & Outdoor sports facility </li>
                                        <li> Medical facility </li>
                                        <li> Transportation facility </li>
                                        <li> Cafeteria </li>

                                    </ul>



                                </div>


                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <ul>     
                                        <li> Wells stocked library </li>
                                        <li> Separate hostel facility for boys & girls  </li>
                                        <li> State of the art laboratories </li>
                                        <li> Workshop  </li>
                                        <li> Computer centre </li>
                                        <li> Digital Classroom </li>
                                        <li> Wi-Fi facility </li>
                                        <li> Bank/ATM </li>
                                        <li> Indoor & Outdoor sports facility </li>
                                        <li> Medical facility </li>
                                        <li> Transportation facility </li>
                                        <li> Cafeteria </li>

                                    </ul>

                                </div>


                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <ul>     
                                        <li> Wells stocked library </li>
                                        <li> Separate hostel facility for boys & girls  </li>
                                        <li> State of the art laboratories </li>
                                        <li> Workshop  </li>
                                        <li> Computer centre </li>
                                        <li> Digital Classroom </li>
                                        <li> Wi-Fi facility </li>
                                        <li> Bank/ATM </li>
                                        <li> Indoor & Outdoor sports facility </li>
                                        <li> Medical facility </li>
                                        <li> Transportation facility </li>
                                        <li> Cafeteria </li>

                                    </ul>

                                </div>


                                <div role="tabpanel" class="tab-pane" id="settings">

                                    <ul>     
                                        <li> Wells stocked library </li>
                                        <li> Separate hostel facility for boys & girls  </li>
                                        <li> State of the art laboratories </li>
                                        <li> Workshop  </li>
                                        <li> Computer centre </li>
                                        <li> Digital Classroom </li>
                                        <li> Wi-Fi facility </li>
                                        <li> Bank/ATM </li>
                                        <li> Indoor & Outdoor sports facility </li>
                                        <li> Medical facility </li>
                                        <li> Transportation facility </li>
                                        <li> Cafeteria </li>

                                    </ul>

                                </div>


                            </div>

                        </div>

                    </div>



                </div>
                <!-- end of about_college -->

            </div>
            <!-- end of college listing details -->

            <div class="col-md-2" style="margin-left:15px">

                <div class="refine_tree" style="padding: 29px 0px 0px;">

                    <div class="refine_search2" style="margin-bottom: 0px;">
                        <div style="border-bottom: 1px solid #d3d3d3; width:100%; float:left">
                            <h5> <span class="msg"></span> Recommended discussions</h5>
                        </div>

                        <ul>
                            <li>The plane is divided into convex heptagons of unit diameter......</li>
                            <li>How does it find the shortest path between places on a map....</li>
                            <li>Unit diameter.Prove that the number of heptagons inside.........</li>
                            <li>What is the constant of triple point of water</li>
                        </ul>


                    </div>

                </div>
                <!--  end of refine tree1 -->


                <div class="refine_tree" style="padding: 29px 0px 0px;">

                    <div class="refine_search2" style="margin-bottom: 0px;">
                        <div style="border-bottom: 1px solid #d3d3d3; width:100%; float:left">
                            <h5> <span class="article"></span> Recommended articles</h5>
                        </div>

                        <ul>
                            <li>The plane is divided into convex heptagons of unit diameter......</li>
                            <li>How does it find the shortest path between places on a map....</li>
                            <li>Unit diameter.Prove that the number of heptagons inside.........</li>
                            <li>What is the constant of triple point of water</li>
                        </ul>

                    </div>

                </div>
                <!--  end of refine tree2 -->
            </div>



        </div>

    </div>
</div>

<?php include('footer.php') ?>




