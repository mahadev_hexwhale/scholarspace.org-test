<?php
$this->load->helper('college-admin_helper');
$encryption_decryption_object = new Encryption; //autoloaded helper class inside config/autoload.php file
?><!DOCTYPE html>
<html lang="en">
    <head>
        <title>Scholar Space | Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/dev_style.css"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/tinycarousel.css" type="text/css" media="screen"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="second_header">
            <div class="second_top" style="background-image:none;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="institute_logo">
                                <a class="institute_logo_text" href="<?php echo site_url(); ?>"> <?php echo $college_details->college_name; ?> </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="by_logo">
                                <h6>Powered by</h6>  
                                <div class="logo">
                                    <a href="<?php echo site_url("/"); ?>"><img src="<?php echo base_url(); ?>assets_front/image/logo.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of top -->
        </div>
        <!-- end of second header -->
        <?php
        if (!$this->ion_auth->logged_in()) {
            ?>
            <div class="main_page_login">
                <div class="modal fade" id="myModal_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div style="border-radius: 0px" class="modal-content">
                            <div class="modal-header">
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <span style="font-family: 'PT Sans', sans-serif;
                                              font-size: 20px;
                                              color: #466677;">Login to your account</span>
                                        <span class="pull-right" style="cursor: pointer" data-dismiss="modal" aria-label="Close" >X</span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <form id="login_form_pop" action="<?php echo site_url("auth/login"); ?>" method="post" accept-charset="utf-8">
                                    <div class="login_input">
                                        <span class="error_alert incorrect_error_msg"></span>
                                        <div class="col-md-12">
                                            <label> Username <span class="username_error_alert error_alert"></span></label>
                                            <input type="text" name="identity" value="" id="identity" style="padding-left: 10px;"  class="log_text"></div>
                                    </div>
                                    <div class="login_input">
                                        <div class="col-md-12">
                                            <label> Password <span class="password_error_alert error_alert"></span></label>
                                            <input style="padding-left: 10px;"  value="" id="password" type="password" class="log_text" name="password"></div>
                                    </div>
                                    <div>
                                        <div style="margin-top: 15px;" class="col-md-6"> <a style="cursor: pointer" id="forgot_link">Forgot Password?</a> </div>
                                        <div style="margin-top: 10px;" class="col-md-6">
                                            <button type="submit" style="border-radius: 0px;width: 100%;" class="btn btn-primary">Login Here</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="login_input">
                                    <div class="col-md-12"><h6> <span>Or</span> </h6></div>
                                </div>
                                <div class="login_social">
                                    <div class="col-md-12">
                                        <div class="fb_login">
                                            <span class="fb_log"></span>
                                            <a href="#"> Login with Facebook </a>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5px;margin-bottom: 5px" class="col-md-12">
                                        <div class="go_login">
                                            <span class="go_log"></span>
                                            <a href="#"> Login with Google </a>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5px;margin-bottom: 20px;" class="col-md-12">
                                        <div style="background-color: #308cbd; text-align: center" class="go_login">
                                            <a href="<?php echo site_url("register/"); ?>" style="cursor: pointer; border-left:none"> Create a Scholarspace account </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>