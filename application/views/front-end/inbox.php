<?php include('intra_leftbar.php') ?>
<div class="col-md-9">
    <div class="boards_tab" style="margin-top: 20px;">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">

                    <div class="courses_page_tab" style="margin-top: 18px;">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                             
                                <li role="presentation" class="active">
                                    <a href="<?php echo site_url() . "user/inbox/"; ?>" >Inbox </a> 
                                </li>
                                <li role="presentation">
                                    <a href="<?php echo site_url()."user/inbox/inboxSentMessages"; ?>"><span style="padding-right: 19px;">.</span> Sent Message </a>   
                                </li>

                                <li role="presentation">
                                    <a href="<?php echo site_url()."user/inbox/chatarchive"; ?>"><span style="padding-right: 19px;">.</span> Chat Archive</a> 
                                </li> 
<!--                                <li role="presentation">
                                    <a href="#saved" aria-controls="saved" role="tab" data-toggle="tab"><span style="padding-right: 19px;">.</span> Saved Search </a>   
                                </li>-->



                                <div class="create_msg">
                                    <a href="#" class="msg_link" data-toggle="modal" data-target="#myModal2">Create message</a>
                                </div>

                                <!-- pop up for create message -->

                                <div class="group_popup">
                                    <!-- Button trigger modal -->
                                    <!-- <button type="button" class="btn btn-primary btn-lg" >
                                      Launch demo modal
                                    </button> -->


                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Create a Message</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="choose_group">
                                                        <div class="col-md-12">
                                                            <div class="choose_grp_list">
                                                                <div class="choose_grp_img">
                                                                    <img src="image/msg_grp1.png" alt="">
                                                                </div>
                                                                <p>Post in a Group</p>
                                                            </div>

                                                            <div class="choose_grp_list" style="border-left:none;">
                                                                <div class="choose_grp_img">
                                                                    <img src="image/msg_grp2.png" alt="">
                                                                </div>
                                                                <p>Send via private message</p>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="course_message">
                                                        <div class="col-md-12"><label> Add Participant </label></div>

                                                        <div class="col-md-12"><div class="user_sugges"><input type="text" class="pop_text" name="sender_name"></div></div>
                                                        <div class="AddedMembers">
                                                        </div>
                                                        <div class="notify_pop" style="display: none;left: 2px;">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="people_list">
                                                                        <ul class="ListOfMembers_home">

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="course_message">
                                                        <div class="col-md-12"><textarea type="text" class="pop_text grp_text" name="message_content"> </textarea></div>
                                                    </div>

                                                    <div class="course_message">
                                                        <div class="col-md-12">
                                                            <div class="send_status"></div> 
                                                            <div>
                                                                <a href="#"> <span class="plus_file"></span> Add Files</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                                    <div class="course_message" style="margin: 8px 0px 15px;">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-primary">Post</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" style="margin-top: 10px;">

                                <div role="tabpanel" class="tab-pane active" id="inbox">
<!--                                    <div class="msg_search_box">
                                        <form class="navbar-form" role="search">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search my messages" 
                                                       name="srch-term" id="srch_msg">
                                            </div>
                                            <button type="submit" class="btn">Search</button>
                                        </form>
                                    </div>-->

                                    <div class="unread">
                                        <!--   <h6>Unread Messages</h6>            
                                            <div class="unread_more">
                                              <h5>You've read everything in your Inbox</h5>
                                                <p>See more conversations from across your company</p>
                                                  </div> -->

                                        <h6>Recent Chat User</h6>  
                                    </div> 

                                    <div class="inbox_details"  style="overflow-y:scroll; height:279px; scroll-behavior: auto; scroll-behavior: smooth; overflow:auto;">
                                        <ul>
                                            <?php
                                            if (count($this->StudentsListFull) >= 1) {
                                                foreach ($this->StudentsListFull as $listFull) {
                                                    ?>
                                                    <div class="first_msg">
                                                        <?php $chatWith = $this->encryption_decryption_object->encode($listFull->user_id); ?>
                                                        <a href="<?php echo site_url() . "user/inbox/inboxChat/" . $chatWith; ?>"  class="message" >
                                                            <li class="clearfix" user_id="<?php echo $this->encryption_decryption_object->encode($listFull->user_id); ?>">
                                                                <div class="intra_user_img">
                                                                    <img 
                                                                        src="<?php echo base_url() . $listFull->profile_picture; ?>"
                                                                        alt="avatar"/>
                                                                        <?php //unread message no.  ?>

                                                                </div>
                                                                <div style="margin: 8px 14px; float:left;">
                                                                    <p><?php echo $listFull->fname . ' ' . $listFull->lname; ?></p>
                                                                    <p><?php echo $listFull->stream_course_name; ?></p>
                                                                </div>
                                                                <div class="user-status">
                                                                    <i class="online"></i>
                                                                </div>
                                                            </li>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            if (count($this->AluminiListFull) >= 1) {
                                                foreach ($this->AluminiListFull as $listFull) {
                                                    ?>
                                                    <div class="first_msg">
                                                        <?php $chatWith = $this->encryption_decryption_object->encode($listFull->user_id); ?>
                                                        <a href="<?php echo site_url() . "user/inbox/inboxChat/" . $chatWith; ?>"  class="message" >
                                                            <li class="clearfix">
                                                                <div class="intra_user_img">
                                                                    <img
                                                                        src="<?php echo base_url() . $listFull->profile_picture; ?>"
                                                                        alt="avatar"/>
                                                                        <?php //unread message no.     ?>

                                                                </div>
                                                                <div style="margin: 8px 14px; float:left;">

                                                                    <p><?php echo $listFull->fname . ' ' . $listFull->lname; ?></p>
                                                                    <p><?php echo $listFull->stream_course_name; ?></p>

                                                                    <div class="user-status">
                                                                        <i class="online"></i>
                                                                    </div>
                                                            </li>
                                                        </a>  
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>

                                        <!-- end of first msg 4 

   
                                      <div class="first_msg">
                                          <div class="intra_user_img">
                                              <img src="image/recent_user1.png" alt="">
                                          </div>
                                          <div style="margin: 8px 14px; float:left;"><a href="#">Text message subject </a>
                                              <p>See more conversations from across your company</p></div>
                                          <h6>Nov 27 2014</h6>
                                      </div>

                                        <!-- end of first msg 4 -->
                                    </div>
                                </div>
                                <!-- end of first tab -->


                                <div role="tabpanel" class="tab-pane" id="sent">
                                    <div class="share_box">
                                        <a href="#">Share something with this group?</a>
                                        <span class="pin"></span>
                                    </div>
                                </div>
                                <!-- end of second tab -->

                                <div role="tabpanel" class="tab-pane" id="archive">
                                    <div class="share_box">
                                        <a href="#">Share something group?</a>
                                        <span class="pin"></span>
                                    </div>
                                </div>

                                <!-- end of third tab -->
                                <div role="tabpanel" class="tab-pane" id="saved">
                                    <div class="share_box">
                                        <a href="#">Share  group?</a>
                                        <span class="pin"></span>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </div>
                    <!-- end of course teacher tab -->


                </div>
                <!-- end of tab1 -->

                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>
                    <p>The institute has following major facilities:</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>
                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="blog"></div>


            </div>
        </div>
    </div>


</div>

<!-- <div class="col-md-3">
</div> -->


</div>
</div>
</div>
<?php include('footer2.php') ?>




