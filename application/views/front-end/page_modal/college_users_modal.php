<div class="group_popup pp_inpop">
    <div class="modal fade" id="myModalppl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                    <h4 class="modal-title" id="myModalLabel">Invite Your Coworkers</h4>
                    <div class="online_search_box">
                        <form class="navbar-form" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for people" name="srch-term" id="srch_msg">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="people_pops">
                        <div class="row">
                            <ul>
                                <li>
                                    <div class="col-md-2"><h6>Andrew</h6></div>
                                    <div class="col-md-7"><p>andrew@hexwhale.com</p></div>
                                    <div class="col-md-3"> <button class="tick_btn">invite</button> </div>
                                </li>
                                <li>
                                    <div class="col-md-2"><h6>Andrew</h6></div>
                                    <div class="col-md-7"><p>andrew@hexwhale.com</p></div>
                                    <div class="col-md-3"> <button class="tick_btn">invite</button> </div>
                                </li>
                                <li>
                                    <div class="col-md-2"><h6>Andrew</h6></div>
                                    <div class="col-md-7"><p>andrew@hexwhale.com</p></div>
                                    <div class="col-md-3"> <button class="tick_btn">invite</button> </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="pp_submit">invite</button>
                </div>
            </div>
        </div>
    </div>
</div>