<!---------------Modal after pressing confirm for request----------------->
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editHostelDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="hostelDescriptionModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hostel Description</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description <span class="required_error_span"></span></label>
                                    <textarea placeholder="Write description here.." id="hostelDescriptionModalTextarea" class="share_text pop_up_taxt" title="Hostel Description" name="hostel_description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="UserRequestConfirm_Submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>