<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editFeesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editFeesModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Fees Structure</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Course : <span style="color: black;" id="courseNameModal"></span></label>
                                    <input type="hidden" id="streamHidden" name="stream" value=""/>
                                    <input type="hidden" id="streamCourseHidden" name="stream_course" value=""/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> 1st Year Fee<span class="required_error_span"></span></label>
                                    <input type="text" id="firstYearFeeText" name="first_year_fee" class="pop_text pop_up_taxt">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> 2nd Year Fee<span class="required_error_span"></span></label>
                                    <input type="text" id="secondYearFeeText" name="second_year_fee" class="pop_text pop_up_taxt">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> 3rd Year Fee <span class="required_error_span"></span></label>
                                    <input type="text" id="thirdYearFeeText" name="third_year_fee" class="pop_text pop_up_taxt">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> 4th Year Fee <span class="required_error_span"></span></label>
                                    <input type="text" id="forthYearFeeText" name="forth_year_fee" class="pop_text pop_up_taxt">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <div class="course_details" style="margin: 0px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>