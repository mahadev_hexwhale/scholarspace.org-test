<style>
    .ui-autocomplete { position: absolute; cursor: default;z-index:2000 !important;}  
</style>
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="EditAttendence_Modal" tabindex="-1" role="dialog" aria-labelledby="EditAttendence">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="EditAttendence_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Course</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Total</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="total_classes" title="Total Classes" placeholder="Enter Total Classes" id="total_classes" class="required_field pop_text pop_up_taxt" value="<?php echo $course_details->total_classes; ?>">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" value="save_as_draft" name="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--.......................... Modal to finalize a course ........................-->
<div class="group_popup">
    <div class="modal fade" id="FinalizeCourse_Modal" tabindex="-1" role="dialog" aria-labelledby="EditAttendence">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="FinalizeCourse_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Finalize the Course</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-12">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Percentage of Marks to be considered</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        Internal Assessment
                                        <input type="text" name="ia_percent" title="IA Percentage" id="ia_percent" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        Midterm
                                        <input type="text" name="midterm_percent" title="Midterm Percentage" id="midterm_percent" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        Final
                                        <input type="text" name="final_percent" title="Final Percentage" id="final_percent" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        Final marks out of
                                        <input type="text" name="final_total_marks" title="Final Total Marks" id="final_total_marks" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>

                            <label class="required_error_percent"></label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>