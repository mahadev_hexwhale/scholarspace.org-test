<!-- Job Opening edit modal -->
<style>
    .ui-autocomplete { position: absolute; cursor: default;z-index:2000 !important;}  
</style>
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="addTeacherCoursesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="addTeacherCoursesModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Course</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Course Title</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="course_title" title="Title" placeholder="Enter course title" id="jobTitleEditModalText" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Course brief</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <textarea title="Course brief" name ="couser_brief" style="width: 100%;" class="required_field c_text front-end-font"> </textarea>
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Eligible Students</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <div class="add_company" style="border-top: 1px solid #e6e6e6;">
                                            <!-- add a group or people notify 2 -->
                                            <div id="selected_users_container_div"></div>
                                            <input type="text" placeholder="Add people to here" id="eligible_students_input" class="required_field poll_text people_list_input">
                                            <!-- popup for add people to notify -->
                                            <div class="notify_pop people_list_drop_down">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="people_list">
                                                            <ul id="courses_people_list_id" class="people_list_ul"></ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <label style="margin-top: 5px;" class="required_error_label eligible_students_required_error_label"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Start date</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" title="Start date" name ="course_start_date" placeholder="Enter start date" style="width: 100%" name="date" id="start_datepicker" class="required_field date_text front-end-font">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> End date</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" title="End Date" name ="course_end_date" placeholder="Enter end date" style="width: 100%" name="date" id="end_datepicker" class="required_field date_text front-end-font">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Mid term</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="checkbox" title="Mid term" name ="midterm" id="midterm" class="date_text pop_up_taxt">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Total Classes : <input type="number" title="Total Classes" name="total_classes" id="total_classes" placeholder="Enter Total Classes" class="required_field pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Add additional Teacher</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group add_more_teacher_div">
                                        <input type="text" title="Add Additional Teacher" name ="addtional_teacher[]" placeholder="Enter email" style="width: 40%" class="pop_text pop_up_taxt auto_fill_email">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="btn btn-xs btn-default add_more_teacher"><i class="fa fa-plus"></i> Add more</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Add Assistants</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group add_more_assistant_div">
                                        <input type="text" title="Add Assistance" name ="assistants[]" placeholder="Enter email" style="width: 40%" class="pop_text pop_up_taxt auto_fill_assistant">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="btn btn-xs btn-default add_more_assistant"><i class="fa fa-plus"></i> Add more</span>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-7">
                                    <button type="submit" value="save_as_draft" name="submit" class="green_btn btn submit_btn">Save as draft</button>
                                    <button type="submit" value="publish" name="submit" class="blue_btn btn submit_btn">Publish</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">&nbsp;</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>