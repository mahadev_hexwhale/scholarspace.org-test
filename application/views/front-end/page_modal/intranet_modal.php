<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="CreatNewGroup_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="CreatNewGroup_Form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create a New Group</h4>
                    </div>
                    <div class="modal-body">
                        <div class="course_message">
                            <div class="col-md-12">
                                <label> Group Name </label>
                                <span class="group_error">*Group Name is required.</span>
                                <input type="text" class="pop_text GroupName" name="GroupName" id="GroupName">
                            </div>
                        </div>
                        <div class="course_message">
                            <div class="col-md-12">
                                <label>Group Members </label>
                                <span class="group_error grp_members_error">*Please add members to group.</span>
                                <div class="add_company" style="border: 1px solid #e6e6e6;">
                                    <div class="AddedMembers">

                                    </div>

                                    <div class="new_notify_box">
                                        <input type="text" placeholder="Add people to notify" class="poll_text addGroupMembers" id="addGroupMembers" name="note">

                                        <div class="notify_pop" style="display: none;top: 30px;left: 0px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="people_list">
                                                        <ul class="ListOfMembers">

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="course_message">
                            <div class="col-md-6">
                                <label> Visible to </label>
                                <span class="group_error" >*Please select an option.</span>
                                <div class="visible_select">
                                    <select name="VisibleTo" class="VisibleTo" id="VisibleTo">
                                        <option value="" style="display: none;">--Select--</option>
                                        <option value="Members">Members</option>
                                        <option value="Public">Public</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="course_message">
                            <div class="col-md-6">
                                <label> Who can view this content ? </label>
                                <span class="group_error">*Required.</span>
                                <div class="radio">
                                    <label><input type="radio" name="GroupAccess" class="GroupAccess" value="Public"><span>Public</span> - Anyone in this network</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="radio">
                                    <label><input type="radio" name="GroupAccess" class="GroupAccess" value="Private"><span>Private</span> - Only approved members</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary SubmitCreateGroup_Button" >Create Group</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>