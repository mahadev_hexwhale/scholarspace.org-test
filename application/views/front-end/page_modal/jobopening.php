<?php
include('intra_leftbar.php');

$get_college_admin_list_array = get_college_admin_list($this->session->userdata('college_id'));
$college_admin_users = array();
foreach ($get_college_admin_list_array as $college_user_value) {
    $college_admin_users[] = $college_user_value->user_id;
}
?>
<style type="text/css">
    .edit_post{
        margin-right: 5px;
    }
    .discription_label{
        margin-bottom: 5px;
    }
</style>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<input type="hidden" id="fixed_group_name" value="jobopening"/>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <div class="group_page_box">
                <div class="col-md-7">
                    <div class="profile_details">
                        <!--                        <div class="profile_pic">
                                                    <img src="http://localhost/scholarspace/assets_front/image/jobopening.png" alt="">
                                                </div>-->
                        <div style="font-family: 'open_sansregular';color: #525252;">
                            <h3> Job Opening </h3>
                        </div>
                    </div>
                </div>
                <!-- tab for group page begins -->
                <div class="converse_tab">
                    <div class="detail_converse">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#dote_con_tab" aria-controls="dote_con_tab" role="tab" data-toggle="tab">Job Opportunity Post</a>
                            </li>
<!--                            <div class="middle_search group_search">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Search this group" name="srch-term" id="srch-term">
                                    </div>
                                </form>
                            </div>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="boards_tab"> 
                <div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-check"></i> Success ! 
                                <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <!--                <div class="row">
                                            <div class="col-md-5">
                                                <h4>Job oppurtunities</h4>
                                            </div>
                                            <div class="col-md-7">
                                                <button id="addNewJobBtn" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus"></i> Add New Job</button>
                                            </div>
                                        </div>-->
                        <div role="tabpanel" class="tab-pane active" id="batch">
                            <div class="update_tab">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">  <span class="point_top"></span> 
                                            <a href="#update" aria-controls="update" role="tab" data-toggle="tab"> 
                                                <span class="update_icon1"></span>Update </a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="margin-top: 10px;">
                                        <div role="tabpanel" class="tab-pane active" id="update">
                                            <div class="share_box">
                                                <a href="javascript:void(0);" id="ontick_share">Add new Job Opportunities</a>
                                                <!--<span class="pin"></span>-->
                                            </div>
                                            <div class="show_share">
                                                <form id="group_post_update_form">
                                                    <div class="poll_share">
                                                        <div class="text_poll"> 
                                                            <textarea ata-autoresize class="share_text" id="update_textarea" name="job_title" placeholder="Add new Job Title ? "></textarea>
<!--                                                            <span class="pin2"></span>
                                                            <div class="attach_list">
                                                                <ul>
                                                                    <li> <a class="upload_file_from_computer"> <span class="upload_icon"></span> <h6>Upload a file from computer</h6> </a> </li>
                                                                    <li> <a href="#"> <span class="file_icon"></span> <h6>Select a file on </h6> </a> </li>
                                                                    <li> <a href="#"> <span class="note_icon"></span> <h6>Select a note on</h6> </a> </li>
                                                                </ul>
                                                            </div>-->
                                                        </div>
                                                        <div class="add_files" style="display: none">
                                                            <ul id="file_upload_content_ul"></ul>
                                                        </div>
                                                        <div>
                                                            <textarea class="share_text" id="mytextarea"></textarea>
                                                            <input type="hidden" id="job_description" name="job_description"/>
                                                        </div>
                                                        <!--<textarea class="share_text" id="mytextarea" placeholder="Whats your Annoucement ?"></textarea>-->
                                                        <!--                                                        <div class="add_company">
                                                                                                                     add a group or people notify 2 
                                                                                                                    <div id="updateAddPeopleToNotify">
                                                                                                                        <div id="notified_user_div"></div>
                                                                                                                        <div class="updateAddPeopleInputDiv" id="updateAddPeopleInputDiv">
                                                                                                                            <div class="notify_pop">
                                                                                                                                <div class="row">
                                                                                                                                    <div class="col-md-12">
                                                                                                                                        <div class="people_list" style="overflow-y: visible;">
                                                                                                                                            <ul id="users_list_ul"></ul>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <input type="text" placeholder="Add people to notify" class="poll_text" id="updateAddPeopleInput">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                     popup for add people to notify 
                                                                                                                </div>-->
                                                        <button type="submit" id="update_post" class="btn">Post</button>
                                                    </div>
                                                </form>
                                                <form id="image_upload_form" method="post" enctype="multipart/form-data">
                                                    <input type="file" name="upload_post_file" id="upload_post_file" style="display:none">
                                                </form>
                                            </div>
                                            <!-- end of share update -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="job_opening_list">
                                <ul id="job_opening_list_ul">
                                    <?php
                                    foreach ($jobopening_list as $jobopening_value) {
                                        $posted_user = $this->ion_auth->user($jobopening_value->user_id)->row();
                                        $logged_in_user = $this->ion_auth->user()->row();
                                        if ($jobopening_value->user_id == $logged_in_user->id || $jobopening_value->is_approve || in_array($logged_in_user->id, $college_admin_users) || in_array($jobopening_value->user_id, $college_admin_users)) {
                                            ?>
                                            <li class="job_opening_list_li" jobopening_id = "<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>">
                                                <h6 style="margin-bottom: 4px;">
                                                    <span id="job_title-<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>" class="job_title_span"><?php echo $jobopening_value->post_title; ?></span>
                                                    <span style="color: #8A8A8A;font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">posted by <a href="#"><?php echo $posted_user->first_name . " " . $posted_user->last_name; ?></a> 
                                                        <?php
                                                        $posted_date = date('M j Y', strtotime($jobopening_value->timestamp));
                                                        echo (($posted_date == date('M j Y')) ? ", Today" : $posted_date) . " at " . date('h:i A', strtotime($jobopening_value->timestamp));
                                                        ?>
                                                    </span>
                                                    <?php
                                                    if ($posted_user->id == $logged_in_user->id || in_array($logged_in_user->id, $college_admin_users)) {
                                                        ?>
                                                        <button post_id="<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>" class="btn btn-default btn-xs pull-right delete_post"><i class="fa fa-trash"></i></button>
                                                        <button title="Edit Job"  post_id="<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>" class="btn btn-default btn-xs pull-right edit_post"><i class="fa fa-edit"></i></button>
                                                        <?php
                                                    }
                                                    ?>    
                                                </h6>  
                                                <p id="job_description-<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>"><?php echo $jobopening_value->post_message; ?></p>
                                                <div class="jobopening_action_btn_div" style="margin-top: 5px">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <!--<button class="edit_job_opening btn btn-xs"><i class="fa fa-thumbs-up"></i></button>-->
                                                            <a href="<?php echo site_url("user/jobopening/job_details") . "?jobId=" . $encryption_decryption_object->encode($jobopening_value->post_id); ?>"><button class="edit_job_opening btn btn-xs"><i class="fa fa-external-link-square"></i> View Details</button></a>
                                                            <?php
                                                            if ($jobopening_value->is_approve == 0 && in_array($user->id, $college_admin_users) && !in_array($jobopening_value->user_id, $college_admin_users)) {
                                                                ?>
                                                                <button job_id = "<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>" class="approve_job_btn btn btn-xs"><i class="fa fa-check"></i> Approve </button>
                                                                <button job_id = "<?php echo $encryption_decryption_object->encode($jobopening_value->post_id); ?>" class="decline_job_btn btn btn-xs"><i class="fa fa-ban"></i> Decline </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <?php
                                                            if ($jobopening_value->is_approve == 0 && !in_array($jobopening_value->user_id, $college_admin_users)) {
                                                                ?>
                                                                <p class="pull-right approve_notification_p"><h6 style="color: red; font-size: 13px;"> Waiting to approve</h6></p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php // include('fixed_group_body.php')  ?>
                        <!-- end of tab1 -->
                        <div role="tabpanel" class="tab-pane" id="job">
                            <h6>Campus</h6>
                            <p>The institute has following major facilities:</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="know">
                            <h6>History</h6>
                            <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="blog"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="right_bar">
                <div class="recent_activity">
                    <h5>Recent Activities</h5>
                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/vr_img.png" alt="">
                        </div>  
                        <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
                    </div>
                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/sa.png" alt="">
                        </div>  
                        <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
                    </div>

                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/sm.png" alt="">
                        </div>  
                        <p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
                    </div>

                    <div class="activities">
                        <div class="act_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/vr_blue.png" alt="">
                        </div>  
                        <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/jobopening_modal");
?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/jobopening.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
//    tinymce.init({
//        selector: "#mytextarea"
//    });
    tinymce.init({
        selector: '#mytextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                job_description = tinyMCE.get('mytextarea').getContent();
                $("#job_description").val(job_description);
            });
        }
    });
</script>
