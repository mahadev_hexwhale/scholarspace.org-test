<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editJobModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editJobModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit internship</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title<span class="required_error_span"></span></label>
                                    <input type="text" name="job_title" id="jobTitleEditModalText" class="pop_text pop_up_taxt" title="Job Title">
                                    <input type="hidden" name="job_id" id="jobIdEditModalText" value=""/>
                                </div>
                                <div class="form-group">
                                    <label class="discription_label">Description<span class="required_error_span"></span></label>
                                    <textarea class="share_text" id="jobDescriptionEditModalTextarea"></textarea>
                                    <input type="hidden" name="job_description" id="jobDescriptionEditModalText" title="Job Description">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>