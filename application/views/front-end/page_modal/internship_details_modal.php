<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editIntershipCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editInternshipCommentModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit internship comment</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="discription_label">Comment<span class="required_error_span"></span></label>
                                    <textarea class="share_text" id="internshipCommentEditModalTextarea"></textarea>
                                    <input type="hidden" name="internship_comment" id="internshipCommentEditModalText"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editIntershipModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editInternshipModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit internship</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title<span class="required_error_span"></span></label>
                                    <input type="text" name="internship_title" id="internshipTitleEditModalText" class="pop_text pop_up_taxt" title="Internship Title">
                                    <input type="hidden" name="internship_id" id="internshipIdEditModalText" value=""/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Start Date<span class="required_error_span"></span></label>
                                    <input type="text" name="start_date" id="internshipStartDateEditModalText" class="pop_text pop_up_taxt">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>End Date<span class="required_error_span"></span></label>
                                <input type="text" name="end_date" id="internshipEndDateEditModalText" class="pop_text pop_up_taxt">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="discription_label">Description<span class="required_error_span"></span></label>
                                    <textarea class="share_text" id="internshipDescriptionEditModalTextarea"></textarea>
                                    <input type="hidden" name="internship_description" id="internshipDescriptionEditModalText" title="Internship Description">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>