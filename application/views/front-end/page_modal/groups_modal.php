<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="AddMembersToGroup_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="CreatNewGroup_Form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Members</h4>
                    </div>
                    <div class="modal-body">
                        <div class="course_message col-md-12">
                            <div class="col-md-10">
                                <span class="group_error grp_members_error">*Please add members to group.</span>
                                <div class="add_company" style="border: 1px solid #e6e6e6;">
                                    <div class="AddedMembers">

                                    </div>
                                    <div class="new_notify_box">
                                        <input type="text" placeholder="+ Add people here.." class="poll_text addMoreGroupMembers" id="addMoreGroupMembers" name="note" style="width: 53%;">
                                        <div class="notify_pop" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="people_list">
                                                        <ul class="ListOfMembers">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary addMoreGroupMembers_Button"> Add </button>
                            </div>
                        </div>

                        <?php if ($user->id == $group->group_admin_user_id && count($requests)) { ?>
                            <div class="PendingMembers members_div col-md-12" style="margin-bottom: 25px;">
                                Pending Members
                                <ul>
                                    <?php
                                    foreach ($requests as $key => $value) {
                                        ?>
                                        <li user_intranet_id="<?php echo $value->user_intranet_id; ?>">
                                            <div class="existing_user col-md-12">
                                                <div class="user_pic">
                                                    <img src="<?php echo base_url() . "" . $value->profile_picture; ?>" alt="">
                                                </div>
                                                <div class="pp_details">
                                                    <h5> 
                                                        <span class="on_dot" title="offline"></span>
                                                        <?php
                                                        echo $value->first_name . " " . $value->last_name;
                                                        echo " (" . ucfirst($value->intranet_user_type) . ")";
                                                        ?>
                                                    </h5> 
                                                    <h6>
                                                        <?php
                                                        if ($value->intranet_user_type != "teacher")
                                                            echo "<strong>" . $value->stream_name . "</strong>";
                                                        ?>
                                                    </h6>
                                                </div>
                                                <span class="btn btn-primary btn-xs request_btn ApproveRequest"><i class="fa fa-check"></i> Approve</span>
                                                <span class="btn btn-default btn-xs request_btn DenyRequest" style="margin-right: 10px;"><i class="fa fa-times"></i> Deny</span>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <div class="ExistingMembers members_div col-md-12">
                            Current Members
                            <ul>
                                <?php
                                foreach ($member as $key => $value) {
                                    ?>
                                    <li user_intranet_id="<?php echo $value->user_intranet_id; ?>">
                                        <div class="existing_user col-md-12">
                                            <div class="user_pic">
                                                <img src="<?php echo base_url() . "" . $value->profile_picture; ?>" alt="">
                                            </div>
                                            <div class="pp_details">
                                                <h5> 
                                                    <span class="on_dot" title="offline"></span>
                                                    <?php
                                                    echo $value->first_name . " " . $value->last_name;
                                                    echo " (" . ucfirst($value->intranet_user_type) . ")";
                                                    ?>
                                                </h5> 
                                                <h6>
                                                    <?php
                                                    if ($value->intranet_user_type != "teacher")
                                                        echo "<strong>" . $value->stream_name . "</strong>";
                                                    ?>
                                                </h6>
                                            </div>
                                            <span class="btn btn-default btn-xs user_delete"><i class="fa fa-trash"></i> Delete</span>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>