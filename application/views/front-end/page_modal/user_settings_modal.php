<!-- Modal -->
<div class="group_popup">
    <div class="modal fade" id="ChangeEmail_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="ChangeEmail_Form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Change Email</h4>
                    </div>
                    <div class="modal-body">
                        <div class="course_message">
                            <p style="font-size: 13px;">
                                To use a different email address with your Scholarspace account, enter it here and we'll send you a verification email. Once you've verified it, all Scholarspace emails will be sent to the new address, and you'll be able to use it to log in.
                            </p>
                        </div>
                        <div class="course_message">
                            <div class="col-md-4"><label> Current Email </label></div>
                            <div class="col-md-8"><?php echo $user->email; ?></div>
                        </div>
                        <div class="course_message">
                            <div class="col-md-4"><label> New Email </label></div>
                            <div class="col-md-8">
                                <input title="Email" id="email" type="text" class="pop_text" name="email">
                                <span class="email_error_msg" style="font-size: 12px;color: red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="course_message" style="margin: 8px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="group_popup">
    <div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                    <h4 class="modal-title" id="myModalLabel">Change Location</h4>
                </div>
                <div class="modal-body">
                    <div class="course_message">
                        <div class="col-md-4"><label> Current Location </label></div>
                        <div class="col-md-8"> <a href="#">Bangalore</a>   </div>
                    </div>
                    <div class="course_message">
                        <div class="col-md-4"><label> New Location </label></div>
                        <div class="col-md-8"><input type="text" class="pop_text" name="group_name"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="course_message" style="margin: 8px 0px 15px;">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>