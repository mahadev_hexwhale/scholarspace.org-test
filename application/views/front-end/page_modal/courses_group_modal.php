<!-- Job Opening edit modal -->
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editStudentDetailsTrackCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="addTeacherCoursesModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Grads</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group">
                                        <div class="intra_select course_select">
                                            <select class="pull-right">
                                                <option value="Re-offering earlier Course">Re-offering Earlier Course</option>
                                                <option value="Fresh Course">Fresh Course</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Course Title</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="course_title" title="Title" placeholder="Enter course title" id="jobTitleEditModalText" class="pop_text pop_up_taxt">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Course brief</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <textarea title="Course brief" name ="couser_brief" style="width: 100%;" class="c_text front-end-font"> </textarea>
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Eligible Students</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <div class="add_company" style="border-top: 1px solid #e6e6e6;">
                                            <!-- add a group or people notify 2 -->
                                            <div id="selected_users_container_div"></div>
                                            <input type="text" placeholder="Add people to notify" id="eligible_students_input" class="poll_text people_list_input">
                                            <!-- popup for add people to notify -->
                                            <div class="notify_pop people_list_drop_down">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="people_list">
                                                            <ul id="courses_people_list_id" class="people_list_ul"></ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <label style="margin-top: 5px;" class="required_error_label eligible_students_required_error_label"></label>
                                                <!--                                        <input type="text" title="Eligible Students" id="jobTitleEditModalText" class="pop_text pop_up_taxt">
                                                    <label class="required_error_label"></label>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Start date</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" title="Start date" name ="course_start_date" placeholder="Enter start date" style="width: 100%" name="date" id="start_datepicker" class="date_text front-end-font">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> End date</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" title="End Date" name ="course_end_date" placeholder="Enter end date" style="width: 100%" name="date" id="end_datepicker" class="date_text front-end-font">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-7">
                                    <button type="submit" value="save_as_draft" name="submit" class="green_btn btn submit_btn">Save as draft</button>
                                    <button type="submit" value="publish" name="submit" class="blue_btn btn submit_btn">Publish</button>
                                    <button type="button" class="grey_btn btn pull-right">Change/Edit</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">&nbsp;</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>