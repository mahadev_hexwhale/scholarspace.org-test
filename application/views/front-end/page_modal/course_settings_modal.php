<style>
    .ui-autocomplete { position: absolute; cursor: default;z-index:2000 !important;}  
</style>
<!--.......................... Modal for Adding new assistant students ............................-->
<div class="group_popup">
    <!-- Modal for  -->
    <div class="modal fade" id="AddMoreAssistants_Modal" tabindex="-1" role="dialog" aria-labelledby="AddMoreAssistants">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="Assistants_Form" class="AddMore_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title">Add Assistants</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Email</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="assistant_email" title="Enter Email" placeholder="Enter Email" class="assistant_email required_field pop_text pop_up_taxt">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label class="required_error_label"></label>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="AddMoreAssistants_Button" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--.......................... Modal for Adding new associate teachers ............................-->
<div class="group_popup">
    <!-- Modal for  -->
    <div class="modal fade" id="AddMoreAssociates_Modal" tabindex="-1" role="dialog" aria-labelledby="AddMoreAssociates">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="Associates_Form" class="AddMore_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="">Add Associate</h4>
                    </div>
                    <div class="modal-body">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Email</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="associates_email" title="Enter Email" placeholder="Enter Email" class="associates_email required_field pop_text pop_up_taxt">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label class="required_error_label"></label>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="AddMoreAssociates_Button" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--.......................... Modal for Editing course name and course brief ............................-->
<div class="group_popup">
    <!-- Modal for  -->
    <div class="modal fade" id="EditCourseDetails_Modal" tabindex="-1" role="dialog" aria-labelledby="EditCourseDetails">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="EditCourseDetails_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title">Edit Details</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Course Name</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" name="course_name" title="Course Name" placeholder="Enter Name" class="required_field pop_text pop_up_taxt" value="<?php echo $course_details->course_name; ?>">
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label>Course Brief</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <textarea rows="10" name="course_brief" title="Course Brief" placeholder="Enter Brief" class="required_field pop_text pop_up_taxt" value="<?php echo $course_details->course_brief; ?>" ><?php echo $course_details->course_brief; ?></textarea>
                                        <label class="required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="AddMoreAssistants_Button" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--.......................... Modal for Adding new members to the course ............................-->
<div class="group_popup">
    <!-- Modal for  -->
    <div class="modal fade" id="AddMoremembers_Modal" tabindex="-1" role="dialog" aria-labelledby="EditCourseDetails">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="AddMoremembers_Form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title">Add Members</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label style="display: block">&nbsp;</label>
                                    <label> Eligible Students</label>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <div class="add_company" style="border-top: 1px solid #e6e6e6;">
                                            <!-- add a group or people notify 2 -->
                                            <div id="selected_users_container_div"></div>
                                            <input type="text" placeholder="Add people to here" id="eligible_students_input" class="required_field poll_text people_list_input">
                                            <!-- popup for add people to notify -->
                                            <div class="notify_pop people_list_drop_down">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="people_list">
                                                            <ul id="courses_people_list_id" class="people_list_ul"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label style="margin-top: 5px;" class="required_error_label eligible_students_required_error_label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="AddMoreAssistants_Button" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>