<input type="hidden" name="site_url" value="<?php echo site_url(); ?>" id="site_url" />
<input type="hidden" name="controller" value="<?php echo $this->router->fetch_class(); ?>" id="controller" />

<div class="group_popup pp_inpop">
    <div class="modal fade" id="collegeChatgroupModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Group</h4>
                    <div class="online_search_box">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="people_pops create_chat_group">
                        <div class="row">
                            <ul>
                                <li>
                                    <div class="col-md-4"><h6>Enter group  Name:</h6></div>
                                    <div class="col-md-7"><input type="text" name="group_name"  id="group_name" /></div>
                                </li>
                                <li>
                                    <div class="add_company" style="border: 1px solid #e6e6e6;">
                                        <div class="new_notify_box">
                                            <input type="text" placeholder="Add group member.." class="poll_text addGroupMemebr" name="note">
                                            <div class="notify_pop" style="display: none;left: 2px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="people_list">
                                                            <ul class="ListOfMembers_home">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li> 
                                <div class="AddedMembers">

                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="pp_submit">Create Group</button>
                </div>
            </div>
        </div>
    </div>
</div>