<?php
include('intra_leftbar.php');
?>
<input type="hidden" id="college_id" value="<?php echo $this->session->userdata('college_id'); ?>">
<input type="hidden" id="fixed_group_name" value="dashboard"/>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<div class="col-md-6">
    <div class="boards_tab">
        <div>
            <div class="group_page_box batch_title"> All Groups </div>
            <div class="group_page_box all_groups">
                <ul>
                    <?php foreach ($groups_list as $value) {
                        ?>
                        <li>
                            <a class="top_name" href="<?php echo site_url() . "user/groups?grp_id=" . $this->encryption_decryption_object->encode($value->id); ?>"><?php echo $value->name; ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Notifications</h5>
            <?php
            $count = 0;
            foreach ($all_unread_alerts as $value) {
                $posted_id = $uid = $value->posted_user_id;
                $timestamp = $value->timestamp;
                if ($value->group_type == "fixed") {
                    $method_name = "view_post";
                } else if ($value->group_type == "unfixed") {
                    $method_name = "view_group_post";
                }
                ?>
                <div class="activities">
                    <div class="act_img">
                        <img
                            src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                            alt=""/>
                    </div>
                    <div class="alert_details">
                        <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                            <span>
                                <?php
                                switch ($value->type) {
                                    case 'notify':
                                        echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                        break;
                                    case 'reply':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " commented on your post.";
                                        } else {
                                            echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                        }
                                        break;
                                    case 'like':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " liked your post.";
                                        } else {
                                            echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                        }
                                        break;
                                }
                                ?>
                            </span><br>
                        </a>
                        <span class="alert_timestamp">
                            <?php
                            $posted_date = date('M j Y', strtotime($timestamp));
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                $count++;
                if ($count >= 5)
                    break;
            }
            ?>
        </div>
    </div>
</div>
<?php include('footer2.php'); ?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!---- tinymce JS ---->
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>