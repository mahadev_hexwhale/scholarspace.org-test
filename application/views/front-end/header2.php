<?php
$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
$this->output->set_header('Pragma: no-cache');
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

$this->load->helper('college-admin_helper');
$encryption_decryption_object = new Encryption;     //autoloaded helper class inside config/autoload.php file
$total_requst_count = college_membership_request();

$current_location = $this->router->fetch_class() . "/" . $this->router->fetch_method();

/* ................... Get all alerts info ................... */
$all_unread_alerts = GetUnareadNotifications();     // called from helper "notification_helper.php"
$alerts_count = count($all_unread_alerts);
$FixedAlertDetails = $this->posts_model->GetFixedPostDetails($all_unread_alerts);
$UnfixedAlertDetails = $this->posts_model->GetUnfixedPostDetails($all_unread_alerts);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Scholar Space | Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/dev_style.css"/>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url(); ?>assets_front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/chat.css"/>
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div class="second_header">
            <div class="second_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <?php // if (isset($college_details->college_image)) { ?>
                                <!--<a href="<?php echo site_url("user/home"); ?>"><img class="college_logo_img" src="<?php echo base_url() . $college_details->college_image; ?>" alt=""></a>-->
                            <?php // } else { ?>
                            <a href="<?php echo site_url("user/home"); ?>"><span class="college_default_icon"><i class="fa fa-graduation-cap "></i></span></a>
                            <?php // }
                            ?>
                            <div class="institute_logo">
                                <a class="institute_logo_text" href="<?php echo site_url("user/home"); ?>"> <?php echo $college_details->college_name; ?> </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="by_logo">
                                <h6>Powered by</h6>
                                <div class="logo">
                                    <a href="<?php echo site_url("/"); ?>"><img src="<?php echo base_url(); ?>assets_front/image/logo.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of top -->
            <div class="middle">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="intranet_menu">
                                <!-- Static navbar -->
                                <nav class="navbar navbar-default">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <!--  <a class="navbar-brand" href="#">Project name</a> -->
                                    </div>
                                    <div id="navbar" class="navbar-collapse collapse">
                                        <ul class="nav navbar-nav">
                                            <?php $url = trim(basename($_SERVER['PHP_SELF'], '.php') . PHP_EOL); ?>

                                            <li class="<?php echo ($current_location == "home/index") ? "active" : "" ?>">
                                                <a href="<?php echo site_url() . "user/home" ?>"> Home </a>
                                            </li>
                                            <li class="<?php echo ($current_location == "inbox/index") ? "active" : "" ?>">
                                                <a href="<?php echo site_url("user/inbox"); ?>"> <span class="inbox"></span> Inbox </a>
                                            </li>
                                            <li class="alerts_li <?php echo ($current_location == "notification/index") ? "active" : "" ?>">
                                                <a href="javascript:void(0);">
                                                    <span class="inbox_bell"></span>
                                                    <?php if ($alerts_count) {
                                                        ?>
                                                        <span class="counts"><?php echo $alerts_count; ?></span>
                                                    <?php }
                                                    ?>
                                                </a>
                                            </li>
                                            <?php if ($user->user_type == "college-admin" && $college_details->admin_user_id == $user->id) { ?>
                                                <li class="<?php echo ($current_location == "request/index") ? "active" : "" ?>">
                                                    <a href="<?php echo site_url("user/request"); ?>">
                                                        <i class="fa fa-user-plus"></i>&nbsp;
                                                        <?php
                                                        if ($total_requst_count) {
                                                            ?>
                                                            <span class="counts"><?php echo $total_requst_count; ?></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="alerts_popup">
                                        <div class="recent_activity">
                                            <?php
                                            $count = 0;
                                            if (count($all_unread_alerts)) {
                                                foreach ($all_unread_alerts as $value) {
                                                    $posted_id = $uid = $value->posted_user_id;
                                                    $timestamp = $value->timestamp;
                                                    if ($value->group_type == "fixed") {
                                                        $method_name = "view_post";
                                                    } else if ($value->group_type == "unfixed") {
                                                        $method_name = "view_group_post";
                                                    }
                                                    ?>
                                                    <div class="activities col-md-12" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" 
                                                         group="<?php echo $value->group_type; ?>" type="<?php echo $value->type; ?>">
                                                        <div class="act_img">
                                                            <img src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>" alt=""/>
                                                        </div>
                                                        <div class="alert_details">
                                                            <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                                                                <span>
                                                                    <?php
                                                                    switch ($value->type) {
                                                                        case 'notify':
                                                                            echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                                                            break;
                                                                        case 'reply':
                                                                            if ($value->group_type == "fixed") {
                                                                                $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                                                                $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                            } else {
                                                                                $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                                                                $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                            }
                                                                            if (count($temp_details_array) > 1) {
                                                                                $i = 0;
                                                                                $put_comma = 0;
                                                                                foreach ($temp_details_array as $value1) {
                                                                                    $uid = $value1->user_id;
                                                                                    $i++;
                                                                                    if ($uid != $posted_id) {
                                                                                        $put_comma = 1;
                                                                                        echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                                    }
                                                                                    if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                                        echo ", ";
                                                                                    }
                                                                                }
                                                                                echo " commented on your post.";
                                                                            } else {
                                                                                echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                                                            }
                                                                            break;
                                                                        case 'like':
                                                                            if ($value->group_type == "fixed") {
                                                                                $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                                                $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                            } else {
                                                                                $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                                                $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                            }
                                                                            if (count($temp_details_array) > 1) {
                                                                                $i = 0;
                                                                                $put_comma = 0;
                                                                                foreach ($temp_details_array as $value1) {
                                                                                    $uid = $value1->user_id;
                                                                                    $i++;
                                                                                    if ($uid != $posted_id) {
                                                                                        $put_comma = 1;
                                                                                        echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                                    }
                                                                                    if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                                        echo ", ";
                                                                                    }
                                                                                }
                                                                                echo " liked your post.";
                                                                            } else {
                                                                                echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                                                            }
                                                                            break;
                                                                    }
                                                                    ?>
                                                                </span><br>
                                                            </a>
                                                            <span class="alert_timestamp">
                                                                <?php
                                                                $posted_date = date('M j Y', strtotime($timestamp));
                                                                echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                                                                ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $count++;
                                                    if ($count >= 5)
                                                        break;
                                                }
                                            }else {
                                                ?>
                                                <div class="activities">
                                                    <div class="alert_details">
                                                        <center> No New Notifications.!! </center>
                                                    </div>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                        <a class="alert_popup_more col-md-12" href="<?php echo site_url('user/notification'); ?>">View All</a>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="middle_box">
                                <div class="middle_search">
                                    <div class="navbar-form" role="search">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control SearchString_Autocomplete"
                                                   placeholder="Search for people and groups" name="SearchString_Autocomplete"
                                                   id="SearchString_Autocomplete">
                                        </div>
                                    </div>


                                    <div class="chat">
                                        <?php
                                        //If use is in inbox then hide the that option in the header 
                                        if ($this->router->fetch_class() != "inbox") {
                                            ?>

                                            <a href="#" id="quickview-toggle">
                                                <i class="fa fa-comment"></i>Chat <?php if (getUnreadMessage() > 0) { ?><span class="pull-right badge badge-sucess">( <?php echo getUnreadMessage(); ?> ) </span><?php } ?>
                                            </a>
                                        <?php } ?>
                                    </div>


                                    <div class="setting_box1" id="set_box">
                                        <img class="user_menu_icon"
                                             src="<?php echo base_url("/assets_front/image/user_menu_icon.png") ?>">
                                    </div>
                                    <div class="setting_list">
                                        <ul>
                                            <li><a class="user_settings" href="<?php echo site_url('user/settings'); ?>"><h6><i
                                                            class="fa fa-cog"></i>&nbsp;Settings</h6></a></li>
                                            <li><a class="logout_user" href="#"><h6><i class="fa fa-sign-out pull-left"></i>&nbsp;Log
                                                        Out</h6></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of second header -->
        <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
        <input type="hidden" id="college_id" value="<?php echo $this->session->userdata('college_id'); ?>"/>
        <input type="hidden" id="controller_name" value="<?php echo $this->router->fetch_class(); ?>"/>
