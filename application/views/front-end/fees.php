<?php include('intra_leftbar.php') ?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet"
      type="text/css"/>

<input type="hidden" id="fixed_group_name" value="fees"/>
<div class="col-md-6">
    <div class="boards_tab">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success !
                        <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                </div>
                <?php
                include('fixed_group_head.php');
                /*
                  ?>
                  <div class="fee_structure_details">
                  <div class="row">
                  <div class="col-md-6">
                  <h3>Fee Structure 2015-2016</h3>
                  </div>
                  <div class="col-md-6">
                  <a href="<?php echo site_url("user/fees/manage"); ?>"><button id="addNewJobBtn" class="btn btn-default btn-xs pull-right"><i class="fa fa-gears"></i> Manage fee structure</button></a>
                  </div>
                  </div>
                  <div class="fee_table">
                  <table class="table">
                  <thead>
                  <tr>
                  <th>Year</th>
                  <th>1st year</th>
                  <th>2nd year</th>
                  <th>3rd year</th>
                  <th>4th year</th>
                  </tr>
                  <tr class="blue_row">
                  <th>Course</th>
                  <th>Admission fee</th>
                  <th>Tuition fee</th>
                  <th>Tuition fee</th>
                  <th>Tuition fee</th>
                  </tr>
                  </thead>

                  <tbody>
                  <tr class="color_row">
                  <td colspan="5">Under Graduate</td>
                  </tr>

                  <tr>
                  <td>B.E Computer Science</td>
                  <td>Rs.25000</td>
                  <td>Rs.125000</td>
                  <td>Rs.125000</td>
                  <td>Rs.125000</td>
                  </tr>

                  <tr>
                  <td>B.E Electronics &amp; Communication</td>
                  <td>Rs.25000</td>
                  <td>Rs.125000</td>
                  <td>Rs.125000</td>
                  <td>Rs.125000</td>
                  </tr>

                  <tr>
                  <td>B.E Mechanical</td>
                  <td>Rs.100000</td>
                  <td>Rs.25000</td>
                  <td>Rs.125000</td>
                  <td>-</td>
                  </tr>

                  <tr>
                  <td>B.E Civil</td>
                  <td>Rs.50000</td>
                  <td>Rs.25000</td>
                  <td>Rs.125000</td>
                  <td>-</td>
                  </tr>

                  <tr>
                  <td>B.E Information Technology</td>
                  <td>-</td>
                  <td>Rs.75000</td>
                  <td>Rs.75000</td>
                  <td>Rs.75000</td>
                  </tr>
                  </tbody>
                  </table>

                  <table class="table" style="margin-top: 29px;">
                  <tbody>
                  <tr class="color_row">
                  <td colspan="5">Post Graduate</td>
                  </tr>
                  <tr>
                  <td>M.Tech(VLSI Embedded System)</td>
                  <td>-</td>
                  <td>Rs.175000</td>
                  <td>Rs.150000</td>
                  <td>Rs.150000</td>
                  </tr>

                  <tr>
                  <td>M.Tech(Computer Science)</td>
                  <td>-</td>
                  <td>Rs.175000</td>
                  <td>-</td>
                  <td>-</td>
                  </tr>

                  <tr>
                  <td>M.Tech(Digital &amp; Networking)</td>
                  <td>-</td>
                  <td>Rs.175000</td>
                  <td>-</td>
                  <td>-</td>
                  </tr>

                  <tr>
                  <td>M.Tech(Computer &amp; Networking)</td>
                  <td>-</td>
                  <td>Rs.175000</td>
                  <td>-</td>
                  <td>-</td>
                  </tr>

                  <tr>
                  <td>M.Tech(Machine Design)</td>
                  <td>Rs.175000</td>
                  <td>Rs.175000</td>
                  <td>-</td>
                  <td>-</td>
                  </tr>
                  </tbody>
                  </table>

                  </div>
                  </div>
                  <?php */
                include('fixed_group_body.php');
                ?>
                <!-- end of tab1 -->
                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>

                    <p>The institute has following major facilities:</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>

                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical
                        University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="blog"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Notifications</h5>
            <?php
            $count = 0;
            foreach ($all_unread_alerts as $value) {
                $posted_id = $uid = $value->posted_user_id;
                $timestamp = $value->timestamp;
                if ($value->group_type == "fixed") {
                    $method_name = "view_post";
                } else if ($value->group_type == "unfixed") {
                    $method_name = "view_group_post";
                }
                ?>
                <div class="activities">
                    <div class="act_img">
                        <img
                            src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                            alt=""/>
                    </div>
                    <div class="alert_details">
                        <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                            <span>
                                <?php
                                switch ($value->type) {
                                    case 'notify':
                                        echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                        break;
                                    case 'reply':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " commented on your post.";
                                        } else {
                                            echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                        }
                                        break;
                                    case 'like':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " liked your post.";
                                        } else {
                                            echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                        }
                                        break;
                                }
                                ?>
                            </span><br>
                        </a>
                        <span class="alert_timestamp">
                            <?php
                            $posted_date = date('M j Y', strtotime($timestamp));
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                $count++;
                if ($count >= 5)
                    break;
            }
            ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/jobopening_modal");
?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js"
        type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/fixed_group.js"
        type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/jobopening.js" type="text/javascript"></script>-->