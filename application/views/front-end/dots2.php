<?php include('intra_leftbar.php') ?>

<div class="col-md-9">
    <div class="group_page_box">

        <div class="col-md-7">
            <div class="profile_details">
                <div class="profile_pic">
                    <img src="image/profile_pic.png" alt="">
                    <div class="join_plus"> + Join</div>
                </div>
                <div class="group_tag">
                    <h4>Business Process Exellence</h4>
                    <h6>Public Group</h6>
                </div>

            </div>
        </div>

        <div class="col-md-5">
            <div class="admin_details">
                <h6>Members 230</h6>

                <ul>
                    <li>
                        <div class="admin_image">
                            <img src="image/recent_user2.png" alt="">
                            <div class="admin_plus">Admin</div>
                        </div>
                    </li>

                    <li>
                        <div class="admin_image">
                            <img src="image/recent_user3.png" alt="">
                            <div class="admin_plus">Admin</div>
                        </div>
                    </li>

                    <li>
                        <div class="admin_image">
                            <img src="image/intra_user1.png" alt="">
                            <div class="admin_plus">Admin</div>
                        </div>
                    </li>

                    <li>
                        <div class="admin_image">
                            <img src="image/recent_user4.png" alt="">
                            <div class="admin_plus">Admin</div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>

        <!-- tab for group page begins -->

        <div class="converse_tab">
            <div class="detail_converse">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#dote_con_tab" aria-controls="dote_con_tab" role="tab" data-toggle="tab">CONVERSATIONS</a>
                    </li>
                    <li role="presentation">
                        <a href="#dote_file_tab" aria-controls="dote_file_tab" role="tab" data-toggle="tab">FILES</a> 
                    </li>
                    <li role="presentation">
                        <a href="#dote_note_tab" aria-controls="dote_note_tab" role="tab" data-toggle="tab"> NOTES </a>
                    </li>

                    <div class="middle_search group_search">
                        <form class="navbar-form" role="search">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                </div>
                                <input type="text" class="form-control" placeholder="Search this group" name="srch-term" id="srch-term">
                            </div>
                        </form>
                    </div>

                </ul>

            </div>
        </div>
        <!-- end of converse tab -->
    </div>
    <!-- end of group_page -->

    <!-- content of group page tab -->

    <div class="converse_content">
        <div class="converse_tab">
            <div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="dote_con_tab">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="update_tab" style="margin-top: 17px;">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active">  <span class="point_top"></span> 
                                                <a href="#dote_update" aria-controls="dote_update" role="tab" data-toggle="tab"> 
                                                    <span class="update_icon1"></span>Update </a>
                                            </li>

                                            <li role="presentation"> <span class="point_top"></span> 
                                                <a href="#dote_poll" aria-controls="dote_poll" role="tab" data-toggle="tab"> 
                                                    <span class="update_icon2"></span>Poll </a> 
                                            </li>

                                            <li role="presentation"> <span class="point_top"></span>
                                                <a href="#dote_praise" aria-controls="dote_praise" role="tab" data-toggle="tab"> 
                                                    <span class="update_icon3"></span> Praise </a> 
                                            </li>

                                            <li role="presentation"> <span class="point_top"></span>
                                                <a href="#dote_announce" aria-controls="dote_announce" role="tab" data-toggle="tab"> 
                                                    <span class="update_icon4"></span> Announcement </a> 
                                            </li>

                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content" style="margin-top: 10px;">

                                            <div role="tabpanel" class="tab-pane active" id="dote_update">

                                                <div class="share_box">
                                                    <a href="#">Share something with this group?</a>
                                                    <span class="pin"></span>
                                                </div>



                                            </div>
                                            <!-- end of first tab -->


                                            <div role="tabpanel" class="tab-pane" id="dote_poll">
                                                <form action="#" method="post">
                                                    <div class="poll_share">
                                                        <div class="text_poll">  
                                                            <textarea class="share_text" placeholder="Whats your question ? " > 
                                                            </textarea> 
                                                            <span class="pin2"></span> 
                                                        </div>

                                                        <div class="box_a">
                                                            <span class="answer_box"> A </span>
                                                            <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                                        </div>

                                                        <div class="box_a">
                                                            <span class="answer_box"> B</span>
                                                            <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                                        </div>

                                                        <div class="box_a">
                                                            <span class="answer_box"> C </span>
                                                            <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                                        </div>

                                                        <div class="add_company">
                                                            <input type="text" placeholder="Add a group and/or people" class="add_text">
                                                            <div class="notify_pop2" id="second_notify">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="group_icon"></span>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">
                                                                            <p>You can only add one group</p>

                                                                            <ul>
                                                                                <li>
                                                                                    <div class="notify_img">
                                                                                        <img src="image/company2.png" alt="">
                                                                                    </div>
                                                                                    <h5>All Company </h5> <h6>This is the default </h6>

                                                                                </li>

                                                                                <li>
                                                                                    <div class="notify_img">
                                                                                        <img src="image/hr_img.png" alt="">
                                                                                    </div>
                                                                                    <h5>HR </h5> 
                                                                                </li>

                                                                            </ul>
                                                                            <!--end of list of group for the company -->


                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="single_user"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">

                                                                            <ul>
                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user3.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user2.png" alt="">
                                                                                    </div>
                                                                                    <h5>Andrew </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user1.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                            <!-- add a group or people notify 2 -->

                                                            <div class="company"> 
                                                                <span class="color_grp"></span> 
                                                                <h6> All company </h6> 
                                                                <span class="pop_close"></span> 
                                                            </div>
                                                            <input type="text" placeholder="Add people to notify" class="poll_text">
                                                            <!-- popup for add people to notify -->
                                                            <div class="notify_pop">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="group_icon"></span>
                                                                            <span class="single_user"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">
                                                                            <p>You can only add one group</p>
                                                                            <ul>
                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user3.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user2.png" alt="">
                                                                                    </div>
                                                                                    <h5>Andrew </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user1.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="box_a">
                                                            <input type="text" placeholder="Add topics" class="poll_text" name="topic">
                                                        </div>

                                                        <div class="add_topics"> <span class="join_pin"></span> Add topics</div> 
                                                        <button type="submit" class="btn">Post</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- end of second tab -->

                                            <div role="tabpanel" class="tab-pane" id="dote_praise">
                                                <form>
                                                    <div class="poll_share">
                                                        <div class="text_poll">  
                                                            <textarea class="share_text" placeholder="Whats your question ? " > 
                                                            </textarea> 
                                                            <span class="pin2"></span> 
                                                        </div>

                                                        <div class="add_company">
                                                            <div class="company">All company</div>
                                                            <input type="text" placeholder="Add people to notify" class="poll_text" name="note">
                                                        </div>
                                                        <div class="box_a">
                                                            <input type="text" placeholder="Add topics" class="poll_text">
                                                        </div>

                                                        <div class="add_topics"> <span class="join_pin"></span> Add topics</div> 
                                                        <button type="submit" class="btn">Post</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- end of third tab -->
                                            <div role="tabpanel" class="tab-pane" id="dote_announce">
                                                <form action="#" method="post">
                                                    <div class="poll_share">
                                                        <div class="text_poll">  
                                                            <textarea class="share_text" placeholder="Whats your question ? " > 
                                                            </textarea> 
                                                            <span class="pin2"></span> 
                                                        </div>


                                                        <div class="add_company">
                                                            <input type="text" placeholder="Add a group and/or people" class="add_text" name="group">
                                                            <div class="notify_pop2">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="group_icon"></span>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">
                                                                            <p>You can only add one group</p>

                                                                            <ul>
                                                                                <li>
                                                                                    <div class="notify_img">
                                                                                        <img src="image/company2.png" alt="">
                                                                                    </div>
                                                                                    <h5>All Company </h5> <h6>This is the default </h6>

                                                                                </li>

                                                                                <li>
                                                                                    <div class="notify_img">
                                                                                        <img src="image/hr_img.png" alt="">
                                                                                    </div>
                                                                                    <h5>HR </h5> 
                                                                                </li>

                                                                            </ul>
                                                                            <!--end of list of group for the company -->


                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="single_user"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">

                                                                            <ul>
                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user3.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user2.png" alt="">
                                                                                    </div>
                                                                                    <h5>Andrew </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user1.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                            <!-- add a group or people notify 2 -->

                                                            <div class="company"> 
                                                                <span class="color_grp"></span> 
                                                                <h6> All company </h6> 
                                                                <span class="pop_close"></span> 
                                                            </div>
                                                            <input type="text" placeholder="Add people to notify" class="poll_text" name="note">
                                                            <!-- popup for add people to notify -->
                                                            <div class="notify_pop">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="side_icon">  
                                                                            <span class="group_icon"></span>
                                                                            <span class="single_user"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-10">
                                                                        <div class="people_list">
                                                                            <p>You can only add one group</p>
                                                                            <ul>
                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user3.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user2.png" alt="">
                                                                                    </div>
                                                                                    <h5>Andrew </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                                <li> 
                                                                                    <div class="notify_img">
                                                                                        <img src="image/recent_user1.png" alt="">
                                                                                    </div>
                                                                                    <h5>Haneef </h5> <h6>Web developer</h6>
                                                                                </li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>

                                                        <div class="box_a">
                                                            <input type="text" placeholder="Add topics" class="poll_text" name="topic">
                                                        </div>

                                                        <div class="add_topics"> <span class="join_pin"></span> Add topics</div> 
                                                        <button type="submit" class="btn">Post</button>
                                                    </div>
                                                </form>
                                            </div>



                                        </div>

                                    </div>
                                </div>
                                <!-- end of update tab -->


                                <!-- commentors tab begins -->
                                <div class="second_commentors_tab">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <div class="tab_pop">See all conversations in your network</div>
                                                <a href="#dote_top_tab" aria-controls="dote_top_tab" role="tab" data-toggle="tab">
                                                    Top .</a> </li>
                                            <li role="presentation"><a href="#dote_all_tab" aria-controls="dote_all_tab" role="tab" data-toggle="tab">All .</a></li>
                                            <li role="presentation"><a href="#dote_follow_tab" aria-controls="dote_follow_tab" role="tab" data-toggle="tab">Following
                                                </a> </li>

                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content" style="margin-top: 14px;">
                                            <div role="tabpanel" class="tab-pane active" id="dote_top_tab">
                                                <div class="global_details">
                                                    <h4> <a href="#"> Global Telecom </a> </h4>

                                                    <div class="user_popup">
                                                        <span class="point_top"></span> 
                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="intra_user_img" style="padding: 4px 9px;">
                                                                    <img src="image/company.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="pop_user_info">
                                                                    <h5>All Company</h5>
                                                                    <h6>Public group</h6>
                                                                    <h6>This is the default group for everyone</h6>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="pop_send">
                                                                    <a href="#">Send Message</a>
                                                                    <a href="#">Following</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="top_user_img">
                                                                <img src="image/intra_user1.png">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <div class="top_user_details">
                                                                <h5> <a href="#" id="top_name"> Swapna Srivastava </a> </h5>
                                                                <div class="user_popup" id="res_top_name">
                                                                    <span class="point_top"></span> 
                                                                    <div class="row">

                                                                        <div class="col-md-3">
                                                                            <div class="intra_user_img" style="padding: 4px 9px;">
                                                                                <img src="image/recent_user1.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            <div class="pop_user_info">
                                                                                <h5>Haneef</h5>
                                                                                <h6>Web developer</h6>
                                                                                <h6>Groups: Web development design</h6>
                                                                                <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-12">
                                                                            <div class="pop_send">
                                                                                <a href="#">Send Message</a>
                                                                                <a href="#">Following</a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>


                                                                <h6>November 12, 2013 at 11:13am</h6>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae elit vel nisi lobortis pellentesque quis in lacus. Donec eu dignissim nunc, sed venenatis neque. Ut ultrices magna id ullamcorper vestibulum. Nam molestie nec justo ac rutrum......</p>

                                                                <a href="#" title="Like">Like.</a>
                                                                <a href="#" title="Reply">Reply.</a>
                                                                <a href="#" title="Share">Share.</a>
                                                                <a href="#" title="More">More</a>

                                                                <div class="commenting_box">
                                                                    <div class="joined">
                                                                        <a href="#"> <span class="join_pin"></span> joined</a>

                                                                        <div class="edit"><a href="#">Edit Topics</a></div>

                                                                    </div>

                                                                    <div class="reply_box">
                                                                        <div class="announce_img">
                                                                            <img src="image/recent_user1.png" alt="">
                                                                        </div>
                                                                        <div class="share_box2" id="comment_in1">
                                                                            <a href="javascript:void(0);" >Write a reply...</a>
                                                                            <span class="reply_pin"></span>
                                                                        </div>

                                                                        <div class="more_share" id="more_in1">
                                                                            <form action="#" method="post">
                                                                                <a href="#">Vishnu Ravi is replying to Haneef</a>
                                                                                <div class="text_poll2">  
                                                                                    <textarea class="share_text2"></textarea> 
                                                                                    <span class="pin2"></span> 
                                                                                    <input type="text" placeholder="Notify additional people" class="poll_text" name="note">
                                                                                </div>
                                                                                <button type="submit" class="btn">Post</button>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- end of global details 1 -->

                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="dote_all_tab">
                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="dote_follow_tab">
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <!--  end of commentors tab -->


                            </div>

                            <div class="col-md-4">
                                <div class="right_bar">

                                    <div class="recent_activity">
                                        <h5>Recent Activities</h5>

                                        <div class="activities">
                                            <div class="act_img">
                                                <img src="image/vr_img.png" alt="">
                                            </div>  
                                            <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
                                        </div>
                                        <!-- end of 1 activity -->

                                        <div class="activities">
                                            <div class="act_img">
                                                <img src="image/sa.png" alt="">
                                            </div>  
                                            <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
                                        </div>
                                        <!-- end of 2 activity -->

                                        <div class="activities">
                                            <div class="act_img">
                                                <img src="image/sm.png" alt="">
                                            </div>  
                                            <p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
                                        </div>
                                        <!-- end of 3 activity -->

                                        <div class="activities">
                                            <div class="act_img">
                                                <img src="image/vr_blue.png" alt="">
                                            </div>  
                                            <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
                                        </div>
                                        <!-- end of 4 activity -->

                                    </div>
                                    <!-- end of recent activity -->
                                </div>
                                <!-- end of right bar -->
                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="dote_file_tab"></div>

                    <div role="tabpanel" class="tab-pane" id="dote_note_tab"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer2.php') ?>