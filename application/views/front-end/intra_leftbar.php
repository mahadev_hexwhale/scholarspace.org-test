<?php
include('header2.php');
$user = $this->ion_auth->user()->row();
?>
<div class="intranet_details">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="intra_sidebar">
                    <div class="intra_user">
                        <a href="<?php echo base_url("user/settings"); ?>">
                            <div class="intra_user_img">
                                <img src="<?php echo base_url() . $user->profile_picture; ?>" alt="">
                            </div>
                            <h6>
                                <?php
                                echo $user->first_name . " " . $user->last_name;
                                ?>
                            </h6>
                        </a>
                    </div>
                    <div class="boards_tab">
                        <div class="groups">
                            <a href="#"> Groups </a> 
                            <a href="#" data-toggle="modal" data-target="#CreatNewGroup_Modal"><span class="plus"></span></a>
                        </div>
                        <div id="board_list">
                            <ul>
                                <li class="<?php echo ("user/" . $current_location == "user/home/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/home/"); ?>">Home</a></li>
                                <li class="<?php echo ("user/" . $current_location == "user/dashboard/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/dashboard/"); ?>">College Community</a></li>
                                <?php if ($this->user_details->intranet_user_type != "teacher") { ?>
                                    <li class="<?php echo ("user/" . $current_location == "user/batchboard/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/batchboard/"); ?>">My Batchboard</a></li>
                                <?php } ?>
                                <?php if ($this->user_details->intranet_user_type == "alumni") { ?>
                                    <li class="<?php echo ("user/" . $current_location == "user/alumni/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/alumni/"); ?>">Alumni</a></li>
                                <?php } ?>
                                <?php if ($user->user_type == "college-admin" && $college_details->admin_user_id == $user->id) { ?>
                                    <li class="<?php echo ("user/" . $current_location == "user/teacher/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/teacher"); ?>">Add Teacher</a></li>
                                    <li class="<?php echo ("user/" . $current_location == "user/request/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/request/"); ?>">Users Request
                                            <?php
                                            if ($total_requst_count) {
                                                ?>
                                                <span class="board_no"><?php echo $total_requst_count; ?></span>
                                                <?php
                                            }
                                            ?>
                                        </a> 
                                    </li>
                                <?php } ?>
                                <li class="<?php echo ("user/" . $current_location == "user/members_list/college") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/members_list/college/"); ?>">College Users</a></li>
                                <!--<li><a href="<?php echo site_url("user/dashboard/"); ?>">College Profile</a></li>-->
                            </ul>
                        </div>
                    </div>
                    <?php
                    //Get the list of groups. The method is been called from college-admin_helper.php
                    $GroupList = GroupList_Method();
                    ?>
                    <div class="browse_pop"> <span class="br_icon"></span> <a href="#">Browse Groups</a> </div>
                    <div class="create_pop"> 
                        <ul style="margin-top: 0px;">
                            <?php
                            $i = 0;
                            foreach ($GroupList as $key => $value) {
                                $i++;
                                $CheckIfMemberOfGroup = CheckIfMemberOfGroup_Method($this->encryption_decryption_object->encode($value->id));
                                if ($CheckIfMemberOfGroup == 1 || $value->group_admin_user_id == $user->id) {
                                    ?>
                                    <li><a href="<?php echo base_url() . "user/groups?grp_id=" . $this->encryption_decryption_object->encode($value->id); ?>"><i class="fa fa-users"></i> <?php echo $value->name; ?></a></li>
                                    <?php if ($i > 2) { ?>
                                        <li><a style="color:#337AB7;" href="<?php echo base_url() . "user/groups/view"; ?>"> more...</a></li>
                                        <?php
                                        break;
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <!--<div class="create_pop"> <span class="br_icon2"></span> <a href="#" data-toggle="modal" data-target="#CreatNewGroup_Modal" id="CreatNewGroup_Button">Create Group</a> </div>-->
                    <div class="create_pop"> 
                        <ul>
                            <li class="tools_text">Tools</li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/courses/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/courses"); ?>">Courses</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/jobopening/index" || "user/" . $current_location == "user/jobopening/job_details") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/jobopening"); ?>">Job Opening</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/internship/index" || "user/" . $current_location == "user/internship/internship_details") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/internship"); ?>">Internship</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/hostel/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/hostel"); ?>">Hostel</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/fees/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/fees"); ?>">Fees</a></li>
                        </ul>
                    </div>
                </div>
            </div>