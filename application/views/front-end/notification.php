<?php
//echo "<pre />";
//print_r($all_posts);die();
include('intra_leftbar.php');
?>
<div class="col-md-9">
    <div class="row">
        <div class="col-lg-12">
            <div class="announce">
                <h5>
                    <i class="fa fa-bell"></i>&nbsp; All Notification
                    <button title="Mark All as read" type="button" class="btn btn-primary btn-xs markAllAsRead_button pull-right">
                        <i class="fa fa-check"></i>
                    </button>
                </h5>
                <br/>

                <div class="tab-content">
                    <?php
                    if (count($all_posts)) {
                        foreach ($all_posts as $value) {
                            $posted_id = $uid = $value->posted_user_id;
                            $timestamp = $value->timestamp;
                            ?>
                            <div class="user_announce alert_content_div <?php
                            if ($value->read_status != 1) {
                                echo "unread_alert";
                            }
                            ?>" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>"
                                 group="<?php echo $value->group_type; ?>" type="<?php echo $value->type; ?>">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <div class="alert_img">
                                            <img src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-11">
                                        <div class="alert_details">
                                            <h6>
                                                <?php
                                                if ($value->group_type == "fixed") {
                                                    $method_name = "view_post";
                                                } else if ($value->group_type == "unfixed") {
                                                    $method_name = "view_group_post";
                                                }
                                                ?>
                                                <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                                                    <span>
                                                        <?php
                                                        switch ($value->type) {
                                                            case 'notify':
                                                                echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                                                break;
                                                            case 'reply':
                                                                if ($value->group_type == "fixed") {
                                                                    $temp_details_array = $FixedPostDetails['posts_reply_list'][$value->post_id];
                                                                    $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                } else {
                                                                    $temp_details_array = $UnfixedPostDetails['posts_reply_list'][$value->post_id];
                                                                    $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                }
                                                                if (count($temp_details_array) > 1) {
                                                                    $i = 0;
                                                                    $put_comma = 0;
                                                                    foreach ($temp_details_array as $value1) {
                                                                        $uid = $value1->user_id;
                                                                        $i++;
                                                                        if ($uid != $posted_id) {
                                                                            $put_comma = 1;
                                                                            echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                        }
                                                                        if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                            echo ", ";
                                                                        }
                                                                    }
                                                                    echo " commented on your post.";
                                                                } else {
                                                                    echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                                                }
                                                                break;
                                                            case 'like':

                                                                if ($value->group_type == "fixed") {
                                                                    $temp_details_array = $FixedPostDetails['posts_like_all_user'][$value->post_id];
                                                                    $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                } else {
                                                                    $temp_details_array = $UnfixedPostDetails['posts_like_all_user'][$value->post_id];
                                                                    $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                }
                                                                if (count($temp_details_array) > 1) {
                                                                    $i = 0;
                                                                    $put_comma = 0;
                                                                    foreach ($temp_details_array as $value1) {
                                                                        $uid = $value1->user_id;
                                                                        $i++;
                                                                        if ($uid != $posted_id) {
                                                                            $put_comma = 1;
                                                                            echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                        }
                                                                        if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                            echo ", ";
                                                                        }
                                                                    }
                                                                    echo " liked your post.";
                                                                } else {
                                                                    echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                                                }
                                                                break;
                                                        }
                                                        ?> 
                                                    </span><br>
                                                    <span class="alert_timestamp">
                                                        <?php
                                                        $posted_date = date('M j Y', strtotime($timestamp));
                                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                                                        ?>
                                                    </span>
                                                </a>
                                            </h6>
                                            <?php if ($value->read_status != 1) { ?>
                                                <button title="Mark as read" type="button"
                                                        uid="<?php echo $this->encryption_decryption_object->encode($uid); ?>"
                                                        class="btn btn-primary btn-xs markAsRead_button pull-right mark_as_read">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/notification.js"
type="text/javascript"></script>