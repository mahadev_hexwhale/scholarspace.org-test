<?php

include 'header.php';
?>
<style type="text/css">
    .txt_box_style{
        width: 100%!important;
        height: 36px!important;
        border: 2px solid #a9bec8!important;
    }
    .heading{
        text-transform: uppercase;
        font-family: 'PT Sans', sans-serif;
        font-size: 15px;
        color: #fff!important;
        background-color: #308cbd;
        width: 100%;
        background-color: #308cbd;
        height: 47px;
        padding: 15px 29px;
        /* border-left: 1px solid #cfcfcf; */
        margin: 25px 0px;
    }
</style>
<div class="body_height">
    <div class="banner_content" id="bck_image">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-warning" style="height: 500px;">
                        <div class="box-header">
                            <h5 class="heading">New Member Registration</h5>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <form role="form">
                                <!-- text input -->
                                <div class="row">
                                    <div class="col-lg-10">
                                        <table>
                                            <tr>
                                                <td style="width:40%">First Name</td>
                                                <td  style="width:60%">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control txt_box_style" placeholder="I'm Looking for Engineering College" name="srch-term" id="srch-term1">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <?php

    include 'footer.php';
    ?>