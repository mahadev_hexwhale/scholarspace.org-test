<?php
include('intra_leftbar.php');
$college_id = $this->session->userdata('college_id');
?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">

<input type="hidden" id="grpID" value="<?php echo $this->encryption_decryption_object->encode($group->id); ?>">
<input type="hidden" id="members_json" value='<?php print_r(json_encode($member)); ?>'/>

<div class="col-md-9">
    <div class="group_page_box">
        <div class="col-md-7">
            <div class="profile_details">
                <div class="profile_pic">
                    <?php if ($group->group_icon_image != "") { ?>
                        <img src="<?php echo base_url() . "" . $group->group_icon_image; ?>" alt="">
                    <?php } else { ?>
                        <img src="<?php echo base_url() . "assets_front/image/profile_pic.png"; ?>" alt="">
                        <?php
                    }
                    if ($user->id != $group->group_admin_user_id) {
                        // if user is not admin then check if user is already a member of this group
                        // if no then show him join icon so that he can send a request to join group
                        $LoggedUserIsMember = 0;
                        foreach ($member as $key => $value) {
                            if ($this->encryption_decryption_object->is_valid_input($value->user_id) == $user->id)
                                $LoggedUserIsMember = 1;
                        }
                        if ($LoggedUserIsMember == 0) {
                            // Check if the user has already requested to join group
                            $IsRequestedForGroup = CheckIfRequestedForGroup_Method($this->encryption_decryption_object->encode($group->id));

                            if ($IsRequestedForGroup == 1) {
                                ?>
                                <div class="join_plus" style="cursor: default"> Requested</div>
                                <?php
                            } else {
                                ?>
                                <div class="join_plus JoinGroupNow_Button"> + Join</div>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
                <div class="group_tag">
                    <h4>
                        <?php echo $group->name; ?>
                        <?php
                        if ($user->id == $group->group_admin_user_id) {
                            //show settings icon for admin
                            ?>
                            <a href="<?php echo base_url() . "user/groups/settings?grp_id=" . $this->encryption_decryption_object->encode($group->id) ?>"><i
                                    class="fa fa-cog grp_settings"></i></a>
                            <?php } ?>
                    </h4>
                    <h6>
                        <?php echo ucfirst($group->group_access); ?> Group
                        <?php
                        $LoggedUserIsMember = 0;
                        foreach ($member as $key => $value) {
                            if ($this->encryption_decryption_object->is_valid_input($value->user_id) == $user->id)
                                $LoggedUserIsMember = 1;
                        }
                        if ($LoggedUserIsMember == 1 && $user->id != $group->group_admin_user_id) {
                            ?>
                            <span style="color: rgba(0, 0, 0, 0.2);">|</span>
                            <span class="join_leave">
                                <span class="joined_group"><i class="fa fa-check"></i> Joined </span>
                                <span class="leave_group"><i class="fa fa-minus"></i> Leave </span>
                            </span>
                            <?php
                        }
                        ?>

                    </h6>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="admin_details">
                <h6>Members <?php echo count($member); ?></h6>
                <ul>
                    <?php
                    // get randomly, the group members to show at the top of the page
                    if ($user->id != $group->group_admin_user_id) {

                        $count = count($member) > 16 ? 16 : count($member);
                        $rand_keys = array_rand($member, $count);
                        if (is_array($rand_keys)) {
                            //if multiple members are available
                            foreach ($rand_keys as $key => $value) {
                                ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . "" . $member[$value]->profile_picture; ?>"
                                             alt="">

                                        <div class="admin_plus"><?php echo $member[$value]->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            //if single members are available
                            ?>
                            <li>
                                <div class="admin_image">
                                    <img src="<?php echo base_url() . "" . $member[$rand_keys]->profile_picture; ?>"
                                         alt="">

                                    <div class="admin_plus"><?php echo $member[$rand_keys]->first_name; ?></div>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        // show add more button for admin
                        $count = count($member) > 15 ? 15 : count($member);
                        $rand_keys = array_rand($member, $count);
                        if (is_array($rand_keys)) {
                            //if multiple members are available
                            foreach ($rand_keys as $key => $value) {
                                ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . "" . $member[$value]->profile_picture; ?>"
                                             alt="">

                                        <div class="admin_plus"><?php echo $member[$value]->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            //if single members are available
                            ?>
                            <li>
                                <div class="admin_image">
                                    <img src="<?php echo base_url() . "" . $member[$rand_keys]->profile_picture; ?>"
                                         alt="">

                                    <div class="admin_plus"><?php echo $member[$rand_keys]->first_name; ?></div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        <li>
                            <div class="admin_image">
                                <button class="add_member_button" id="AddMembersToGroup_Button"
                                        grpID='<?php echo $this->encryption_decryption_object->encode($group->id); ?>'>
                                    <i class="fa fa-user-plus"></i></button>
                            </div>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>

        <!-- tab for group page begins -->
        <div class="converse_tab">
            <div class="detail_converse">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#dote_con_tab" aria-controls="dote_con_tab" role="tab"
                           data-toggle="tab">CONVERSATIONS</a>
                    </li>
                    <li role="presentation">
                        <a href="#dote_info_tab" aria-controls="dote_info_tab" role="tab" data-toggle="tab">INFO</a>
                    </li>
                    <li role="presentation">
                        <a href="#dote_file_tab" aria-controls="dote_file_tab" role="tab" data-toggle="tab">FILES</a>
                    </li>
                    <div class="middle_search group_search">
                        <form id="SearchThisGroup_Form" class="navbar-form" role="search">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                                <input type="text" class="form-control SearchString" placeholder="Search this group"
                                       name="SearchString" id="SearchString">
                            </div>
                        </form>
                    </div>
                </ul>
            </div>
        </div>
    </div>

    <div class="converse_content">
        <div class="converse_tab">
            <div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="dote_con_tab">
                        <div class="row">
                            <div class="col-md-8">
                                <?php if ($LoggedUserIsMember == 1 || $user->id == $group->group_admin_user_id) { ?>
                                    <div class="update_tab" style="margin-top: 17px;">
                                        <div>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="post_type active"><span
                                                        class="point_top"></span>
                                                    <a href="#update" aria-controls="update" role="tab"
                                                       data-toggle="tab">
                                                        <span class="update_icon1"></span>Update
                                                    </a>
                                                </li>
                                                <li role="presentation" class="post_type"><span
                                                        class="point_top"></span>
                                                    <a href="#poll" aria-controls="poll" role="tab" data-toggle="tab">
                                                        <span class="update_icon2"></span>Poll
                                                    </a>
                                                </li>
                                                <li role="presentation" class="post_type"><span
                                                        class="point_top"></span>
                                                    <a href="#praise" aria-controls="praise" role="tab"
                                                       data-toggle="tab">
                                                        <span class="update_icon3"></span> Praise
                                                    </a>
                                                </li>
                                                <li role="presentation" class="post_type"><span
                                                        class="point_top"></span>
                                                    <a href="#announce" aria-controls="announce" role="tab"
                                                       data-toggle="tab">
                                                        <span class="update_icon4"></span> Announcement
                                                    </a>
                                                </li>
                                            </ul>

                                            <div class="tab-content" style="margin-top: 10px;">

                                                <!--...........................Tab for Update.................................-->
                                                <div id="update" role="tabpanel" class="tab-pane category active">
                                                    <div class="share_box">
                                                        <a href="javascript:void(0);" id="ontick_share">Share an update
                                                            with this group?</a>
                                                        <span class="pin"></span>
                                                    </div>
                                                    <div class="show_share">
                                                        <form id="update_form" action="#" method="POST">
                                                            <div class="poll_share">
                                                                <div class="text_poll">
                                                                    <textarea id="update_content"
                                                                              class="share_text update_content"></textarea>
                                                                    <span class="pin2 upload_file_from_computer"></span>
                                                                </div>
                                                                <div class="add_files" style="display: none">
                                                                    <ul class="file_upload_content_ul"></ul>
                                                                </div>
                                                                <div class="add_company"
                                                                     style="border: 1px solid #e6e6e6;">
                                                                    <div class="AddedMembers">
                                                                        <div intranet_id='' class='company addAllGroup'>
                                                                            <h6><?php echo $group->name; ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="new_notify_box">
                                                                        <input type="text"
                                                                               placeholder="Add people to notify.."
                                                                               class="poll_text addMembersHere"
                                                                               name="note">

                                                                        <div class="notify_pop"
                                                                             style="display: none;left: 2px;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="people_list">
                                                                                        <ul class="ListOfMembers">

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit"
                                                                        class="btn btn-primary post_update" disabled="">
                                                                    Post
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <form class="image_upload_form" method="post"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="upload_post_file"
                                                                   class="upload_post_file" style="display:none">
                                                        </form>
                                                    </div>
                                                </div>

                                                <!--...........................Tab for Poll..............................-->
                                                <div id="poll" role="tabpanel" class="tab-pane category">
                                                    <div class="share_box">
                                                        <a href="javascript:void(0);" id="ontick_share">Share a poll
                                                            with this group?</a>
                                                        <span class="pin"></span>
                                                    </div>
                                                    <div class="show_share">
                                                        <form id="poll_form" action="#" method="POST">
                                                            <div class="poll_share">
                                                                <div class="text_poll">
                                                                    <textarea id="poll_question"
                                                                              class="share_text poll_question"
                                                                              placeholder="Whats your question ?"></textarea>
                                                                    <span class="pin2 upload_file_from_computer"></span>
                                                                </div>
                                                                <div class="poll_post_answer_div">
                                                                    <div class="box_a">
                                                                        <span class="answer_box">A</span>
                                                                        <input type="text" placeholder="Answer"
                                                                               class="poll_text poll_answer"
                                                                               name="answer[]">
                                                                    </div>
                                                                    <div class="box_a">
                                                                        <span class="answer_box">B</span>
                                                                        <input type="text" placeholder="Answer"
                                                                               class="poll_text poll_answer"
                                                                               name="answer[]">
                                                                    </div>
                                                                </div>
                                                                <div class="box_a">
                                                                    <span
                                                                        class="add_more add_more_answer"> Add More </span>
                                                                </div>
                                                                <div class="add_files" style="display: none">
                                                                    <ul class="file_upload_content_ul"></ul>
                                                                </div>
                                                                <div class="add_company"
                                                                     style="border: 1px solid #e6e6e6;">
                                                                    <div class="AddedMembers">
                                                                        <div intranet_id='' class='company addAllGroup'>
                                                                            <h6><?php echo $group->name; ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="new_notify_box">
                                                                        <input type="text"
                                                                               placeholder="Add people to notify.."
                                                                               class="poll_text addMembersHere"
                                                                               name="note">

                                                                        <div class="notify_pop"
                                                                             style="display: none;left: 2px;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="people_list">
                                                                                        <ul class="ListOfMembers">

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary post_poll"
                                                                        disabled=""> Post
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <form class="image_upload_form" method="post"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="upload_post_file"
                                                                   class="upload_post_file" style="display:none">
                                                        </form>
                                                    </div>
                                                </div>

                                                <!--------------------------------Tab for Praise----------------------------------->
                                                <div id="praise" role="tabpanel" class="tab-pane category">
                                                    <div class="share_box">
                                                        <a href="javascript:void(0);" id="ontick_share">Praise people
                                                            from this group?</a>
                                                        <span class="pin"></span>
                                                    </div>
                                                    <div class="show_share">
                                                        <form id="praise_form" action="#" method="POST">
                                                            <div class="poll_share">
                                                                <div class="text_poll">
                                                                    <div class="add_company Praising_div"
                                                                         style="border: 1px solid #e6e6e6; min-height: 50px;">
                                                                        <div class="Praise_AddedMembers">

                                                                        </div>
                                                                        <div class="new_notify_box" style="width: 70%;">
                                                                            <input type="text"
                                                                                   placeholder="Add people to notify.."
                                                                                   class="poll_text PraiseMembers"
                                                                                   name="note">

                                                                            <div class="praise_notify_pop"
                                                                                 style="display: none;left: 2px;">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="praise_people_list">
                                                                                            <ul class="Praise_ListOfMembers">

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <span class="pin2 upload_file_from_computer"></span>
                                                                </div>
                                                                <div class="text_poll">
                                                                    <div class="praise_image">
                                                                        <span class="praise_prev"></span>

                                                                        <div class="praise_badge">
                                                                            <?php foreach ($badges as $key => $value) { ?>
                                                                                <img class="current_praise_badge"
                                                                                     badge_id="<?php echo $encryption_decryption_object->encode($value->id); ?>"
                                                                                     src="<?php echo base_url() . $value->image; ?>"
                                                                                     alt="">
                                                                                 <?php }
                                                                                 ?>
                                                                        </div>
                                                                        <span class="praise_next"></span>
                                                                    </div>
                                                                    <textarea class="praise_text" id="praise_content"
                                                                              placeholder="What are you praising them for ? "></textarea>
                                                                </div>
                                                                <div class="add_files" style="display: none">
                                                                    <ul class="file_upload_content_ul"></ul>
                                                                </div>
                                                                <div class="add_company"
                                                                     style="border: 1px solid #e6e6e6;">
                                                                    <div class="AddedMembers">
                                                                        <div intranet_id='' class='company addAllGroup'>
                                                                            <h6><?php echo $group->name; ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="new_notify_box">
                                                                        <input type="text"
                                                                               placeholder="Add people to notify.."
                                                                               class="poll_text addMembersHere"
                                                                               name="note">

                                                                        <div class="notify_pop"
                                                                             style="display: none;left: 2px;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="people_list">
                                                                                        <ul class="ListOfMembers">

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit"
                                                                        class="btn btn-primary post_praise" disabled="">
                                                                    Post
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <form class="image_upload_form" method="post"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="upload_post_file"
                                                                   class="upload_post_file" style="display:none">
                                                        </form>
                                                    </div>
                                                </div>

                                                <!--...........................Tab for Announce.............................-->
                                                <div id="announce" role="tabpanel" class="tab-pane category">
                                                    <div class="share_box">
                                                        <a href="javascript:void(0);" id="ontick_share">Make an
                                                            announcement in this group?</a>
                                                        <span class="pin"></span>
                                                    </div>
                                                    <div class="show_share">
                                                        <form id="announce_form" action="#" method="post">
                                                            <div class="poll_share">
                                                                <div class="text_poll">
                                                                    <textarea class="share_text announce_heading"
                                                                              placeholder="Whats your Annoucement ?"></textarea>
                                                                    <textarea id="announce_textpad"
                                                                              class="announce_textpad"
                                                                              placeholder="Place some text here"></textarea>
                                                                    <span class="pin2 upload_file_from_computer"></span>
                                                                </div>
                                                                <div class="add_files" style="display: none">
                                                                    <ul class="file_upload_content_ul"></ul>
                                                                </div>
                                                                <div class="add_company"
                                                                     style="border: 1px solid #e6e6e6;">
                                                                    <div class="AddedMembers">
                                                                        <div intranet_id='' class='company addAllGroup'>
                                                                            <h6><?php echo $group->name; ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="new_notify_box">
                                                                        <input type="text"
                                                                               placeholder="Add people to notify.."
                                                                               class="poll_text addMembersHere"
                                                                               name="note">

                                                                        <div class="notify_pop"
                                                                             style="display: none;left: 2px;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="people_list">
                                                                                        <ul class="ListOfMembers">

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit"
                                                                        class="btn btn-primary post_announce"
                                                                        disabled=""> Post
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <form class="image_upload_form" method="post"
                                                              enctype="multipart/form-data">
                                                            <input type="file" name="upload_post_file"
                                                                   class="upload_post_file" style="display:none">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <!-- commentors tab begins -->
                                <div class="commentors_tab">
                                    <div>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="PostFilter active" id="allPosts"><a
                                                    href="#all_tab" aria-controls="all_tab" role="tab"
                                                    data-toggle="tab">All .</a></li>
                                            <li role="presentation" class="PostFilter" id="topPosts"><a href="#top_tab"
                                                                                                        aria-controls="top_tab"
                                                                                                        role="tab"
                                                                                                        data-toggle="tab">Top
                                                    .</a></li>
                                            <li role="presentation" class="PostFilter" id="followingPosts"><a
                                                    href="#follow_tab" aria-controls="follow_tab" role="tab"
                                                    data-toggle="tab">Following</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content" style="margin-top: 14px;">
                                            <div role="tabpanel" class="tab-pane active" id="all_tab">
                                                <div id="all_post_content" class="global_details">
                                                    <?php
                                                    foreach ($all_posts['posts_list'] as $value) {
                                                        ?>
                                                        <div class="post_content_div"
                                                             id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <div class="top_user_img">
                                                                        <img
                                                                            src="<?php echo base_url() . $value->profile_picture; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <div class="top_user_details">
                                                                        <h5><a class="top_name"
                                                                               uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>"
                                                                               href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>"><?php echo $value->first_name . " " . $value->last_name; ?></a>
                                                                        </h5>

                                                                        <div class="user_popup res_top_name"></div>
                                                                        <h6>
                                                                            <span class="post_time"
                                                                                  timestamp="<?php echo strtotime($value->timestamp); ?>">
                                                                                      <?php
                                                                                      $posted_date = date('M j Y', strtotime($value->timestamp));
                                                                                      echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                                                                      ?>
                                                                            </span>
                                                                        </h6>
                                                                        <?php
                                                                        switch ($value->type) {
                                                                            case 'update':
                                                                                ?>
                                                                                <div class="post_type_div"
                                                                                     id="<?php echo $encryption_decryption_object->encode($all_posts['posts_update_details'][$value->post_id][0]->update_id); ?>">
                                                                                    <p class="post_message_p">
                                                                                        <?php
                                                                                        if (str_word_count($all_posts['posts_update_details'][$value->post_id][0]->update_content) >= 110) {
                                                                                            ?>
                                                                                            <span
                                                                                                class="truncated_body">
                                                                                                    <?php
                                                                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                                    ?>
                                                                                            </span>
                                                                                            <span class="complete_body">
                                                                                                <?php
                                                                                                echo html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                                                ?>
                                                                                            </span>
                                                                                            <?php
                                                                                        } else {
                                                                                            echo html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content);
                                                                                        }
                                                                                        ?>
                                                                                    </p>
                                                                                </div>
                                                                                <?php
                                                                                break;
                                                                            case 'poll':
                                                                                if ($all_posts['posts_poll_answer_vote_current_user'][$value->post_id]) {
                                                                                    ?>
                                                                                    <div class="post_type_div"
                                                                                         id="<?php echo $encryption_decryption_object->encode($all_posts['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                                                        <p class="poll_question_p">
                                                                                            <?php echo $all_posts['posts_poll_details'][$value->post_id][0]->question; ?>
                                                                                        </p>

                                                                                        <div class="poll_answer_div">
                                                                                            <?php
                                                                                            $poll_id = $all_posts['posts_poll_details'][$value->post_id][0]->poll_id;
                                                                                            $total_votes = 0;
                                                                                            foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                                $vote_count = $all_posts['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                                                                $total_votes += $vote_count;
                                                                                            }
                                                                                            foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                                $vote_count = $all_posts['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                                                                if ($vote_count == 0) {
                                                                                                    $answer_percentage = 0;
                                                                                                } else {
                                                                                                    $answer_percentage = (100 * $vote_count) / $total_votes;
                                                                                                }
                                                                                                ?>
                                                                                                <div class="ans_option">
                                                                                                    <p class="poll_answer_p"
                                                                                                       id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?></p>

                                                                                                    <div
                                                                                                        class="progress">
                                                                                                        <div
                                                                                                            class="progress-bar progress-bar-green"
                                                                                                            role="progressbar"
                                                                                                            aria-valuenow="40"
                                                                                                            aria-valuemin="0"
                                                                                                            aria-valuemax="100"
                                                                                                            style="width: <?php echo $answer_percentage; ?>%"></div>
                                                                                                    </div>
                                                                                                    <p class="poll_answer_p ans_percentage_p"><?php echo round($answer_percentage); ?>
                                                                                                        %</p>
                                                                                                </div>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                            <div
                                                                                                class="total_vote_div"><?php echo $total_votes; ?>
                                                                                                total votes
                                                                                                <!--<a href="javascript:void(0);" class="change_vote_button"> · Change Vote</a>--></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <div class="post_type_div"
                                                                                         id="<?php echo $encryption_decryption_object->encode($all_posts['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                                                        <p class="poll_question_p">
                                                                                            <?php echo $all_posts['posts_poll_details'][$value->post_id][0]->question; ?>
                                                                                        </p>

                                                                                        <div class="poll_answer_div">
                                                                                            <form
                                                                                                class="poll_answer_form">
                                                                                                    <?php
                                                                                                    $poll_id = $all_posts['posts_poll_details'][$value->post_id][0]->poll_id;
                                                                                                    foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                                        ?>
                                                                                                    <p class="poll_answer_p"
                                                                                                       id="<?php echo $encryption_decryption_object->encode($value1->ans_id); ?>">
                                                                                                        <label>
                                                                                                            <input
                                                                                                                type="radio"
                                                                                                                name="answer"
                                                                                                                class="radio_answer"
                                                                                                                value="<?php echo $encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?>
                                                                                                        </label>
                                                                                                    </p>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($LoggedUserIsMember == 1 || $user->id == $group->group_admin_user_id) {
                                                                                                    ?>
                                                                                                    <button
                                                                                                        type="submit"
                                                                                                        name="submit_vote"
                                                                                                        class="btn btn-primary submit_vote">
                                                                                                        Vote
                                                                                                    </button>
                                                                                                <?php } ?>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                break;
                                                                            case 'praise':
                                                                                ?>
                                                                                <div class="post_type_div"
                                                                                     id="<?php echo $encryption_decryption_object->encode($all_posts['posts_praise_details'][$value->post_id][0]->praise_id); ?>">
                                                                                    <div class="row praised_div">
                                                                                        <div
                                                                                            class="col-md-2 praise_div_image">
                                                                                            <img
                                                                                                src="<?php echo base_url() . "" . $all_posts['posts_praise_details'][$value->post_id][0]->badge_image; ?>"
                                                                                                alt="">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-10 praise_div_details">
                                                                                            <p class="row praise_member_p">
                                                                                                <span
                                                                                                    class="praised_text">Praised</span>
                                                                                                    <?php
                                                                                                    $array_count = count($all_posts['posts_praise_member_details'][$value->post_id]);
                                                                                                    $running_count = 0;
                                                                                                    foreach ($all_posts['posts_praise_member_details'][$value->post_id] as $member_value) {
                                                                                                        $running_count++;
                                                                                                        echo $this->ion_auth->user($member_value->praised_user_id)->row()->first_name . " " . $this->ion_auth->user($member_value->praised_user_id)->row()->last_name;
                                                                                                        if ($running_count != $array_count) {
                                                                                                            echo ", ";
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                            </p><br>

                                                                                            <p class="row praise_content_p">
                                                                                                "
                                                                                                <?php
                                                                                                if (str_word_count($all_posts['posts_praise_details'][$value->post_id][0]->praise_content) >= 110) {
                                                                                                    ?>
                                                                                                    <span
                                                                                                        class="truncated_body">
                                                                                                            <?php
                                                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <span
                                                                                                        class="complete_body">
                                                                                                            <?php
                                                                                                            echo html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    echo html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content);
                                                                                                }
                                                                                                ?>
                                                                                                "
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php
                                                                                break;
                                                                            case 'announce':
                                                                                ?>
                                                                                <div class="post_type_div"
                                                                                     id="<?php echo $encryption_decryption_object->encode($all_posts['posts_announce_details'][$value->post_id][0]->announce_id); ?>">
                                                                                    <div class="post_announce_div">
                                                                                        <div class="row">
                                                                                            <div
                                                                                                class="col-md-12 announce_head_div">
                                                                                                <i class="fa fa-bullhorn"></i>
                                                                                                <?php echo $all_posts['posts_announce_details'][$value->post_id][0]->heading; ?>
                                                                                            </div>
                                                                                            <div
                                                                                                class="col-md-12 announce_content_div">
                                                                                                    <?php
                                                                                                    if (str_word_count($all_posts['posts_announce_details'][$value->post_id][0]->content) >= 110) {
                                                                                                        ?>
                                                                                                    <span
                                                                                                        class="truncated_body">
                                                                                                            <?php
                                                                                                            //                                                                                                        echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content, ENT_QUOTES), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr(($all_posts['posts_announce_details'][$value->post_id][0]->content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <span
                                                                                                        class="complete_body">
                                                                                                            <?php
                                                                                                            echo html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse </a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    echo str_replace('<br />', '', html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content));
                                                                                                }
                                                                                                ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php
                                                                                break;
                                                                        }

                                                                        $file_count = count($all_posts['posts_file_list'][$value->post_id]);
                                                                        if ($file_count) {
                                                                            ?>
                                                                            <div class="post_file_div">
                                                                                <div class="row">
                                                                                    <?php
                                                                                    $count = 1;
                                                                                    $inner_count = 2;
                                                                                    foreach ($all_posts['posts_file_list'][$value->post_id] as $file_value) {
                                                                                        if ($count == 1) {
                                                                                            if ($file_value->file_type == "jpg") {
                                                                                                ?>
                                                                                                <div class="col-md-12">
                                                                                                    <a class="fancybox-thumbs"
                                                                                                       data-fancybox-group="thumb"
                                                                                                       href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                                                        <img
                                                                                                            style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                                            src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <div class="col-md-4"
                                                                                                     style="padding-right: 19px;">
                                                                                                    <span
                                                                                                        class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                                                    <img
                                                                                                        style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                                        src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                                                </div>
                                                                                                <?php
                                                                                            }
                                                                                            $count++;
                                                                                        } else {
                                                                                            if ($inner_count == 2) {
                                                                                                ?>
                                                                                                <!--<div class="col-md-12">-->
                                                                                                <?php
                                                                                            }
                                                                                            if ($file_value->file_type == "jpg") {
                                                                                                ?>
                                                                                                <div class="col-md-4"
                                                                                                     style="padding-right: 19px;">
                                                                                                    <a class="fancybox-thumbs"
                                                                                                       data-fancybox-group="thumb"
                                                                                                       href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                                                        <img
                                                                                                            style="margin-bottom: 2px; width: 100%; object-fit: cover;"
                                                                                                            src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <div class="col-md-4"
                                                                                                     style="padding-right: 19px;">
                                                                                                    <span
                                                                                                        class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                                                    <img
                                                                                                        style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                                        src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                                                </div>
                                                                                                <?php
                                                                                            }
                                                                                            if ($file_count == $inner_count) {
                                                                                                ?>
                                                                                                <!--</div>-->
                                                                                                <?php
                                                                                            }
                                                                                            $inner_count++;
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        if (count($all_posts['posts_notified_users_list'][$value->post_id])) {
                                                                            ?>
                                                                            <div class="notified_user_div">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div
                                                                                            class="notify_user_heading">
                                                                                            cc:
                                                                                        </div>
                                                                                        <div class="notify_user_body">
                                                                                            <?php
                                                                                            $array_count = count($all_posts['posts_notified_users_list'][$value->post_id]);
                                                                                            $running_count = 0;
                                                                                            foreach ($all_posts['posts_notified_users_list'][$value->post_id] as $user_id) {
                                                                                                $running_count++;
                                                                                                $user_details = $this->ion_auth->user($user_id)->row();
                                                                                                ?>
                                                                                                <a user="<?php echo $this->encryption_decryption_object->encode($user_id); ?>"
                                                                                                   href="">
                                                                                                       <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                                </a>
                                                                                                <?php
                                                                                                if ($running_count != $array_count) {
                                                                                                    echo ",";
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <?php if ($LoggedUserIsMember == 1 || $user->id == $group->group_admin_user_id) { ?>
                                                                            <div class="post_action_div"
                                                                                 post_id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                                                                <a class="post_like"
                                                                                   href="javascript:void(0);"
                                                                                   like_count="<?php echo count($all_posts['posts_like_all_user'][$value->post_id]); ?>"
                                                                                   like_action="<?php echo ($all_posts['posts_like_current_user'][$value->post_id] == 1) ? "unlike" : "like"; ?>"
                                                                                   title="Like"><?php echo ($all_posts['posts_like_current_user'][$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                                                                <a href="javascript:void(0);"
                                                                                   class="reply_link" title="Reply">Reply.</a>
                                                                                <a href="javascript:void(0);"
                                                                                   follow_action="<?php echo ($all_posts['posts_follow_current_user'][$value->post_id] == 1) ? "unfollow" : "follow"; ?>"
                                                                                   class="post_follow"
                                                                                   title="Follow"><?php echo ($all_posts['posts_follow_current_user'][$value->post_id] == 1) ? "Unfollow." : "Follow."; ?></a>
                                                                                   <?php if ($all_posts['posts_current_user'][$value->post_id]) { ?>
                                                                                    <a href="javascript:void(0);"
                                                                                       class="click_more" title="More">More</a>
                                                                                    <div class="more_list">
                                                                                        <ul>
                                                                                            <li class="delete_post"><a
                                                                                                    href="javascript:void(0);">
                                                                                                    <h6><span
                                                                                                            class="stop_icon"></span>
                                                                                                        Delete </h6></a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="commenting_box">
                                                                                <div
                                                                                    id="post_like_div-<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                                    class="post_like_div"
                                                                                    style="display: <?php echo (!empty($all_posts['posts_like_all_user'][$value->post_id])) ? "block" : "none"; ?>">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div
                                                                                                id="post_like_body-<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                                                class="post_like_body">
                                                                                                <i class="fa fa-thumbs-up"></i>
                                                                                                <?php
                                                                                                $array_count = count($all_posts['posts_like_all_user'][$value->post_id]);
                                                                                                $running_count = 0;
                                                                                                if ($all_posts['posts_like_current_user'][$value->post_id] == 1) {
                                                                                                    echo "You";
                                                                                                    if (count($all_posts['posts_like_all_user'][$value->post_id]) > 1) {
                                                                                                        echo ",";
                                                                                                    }
                                                                                                    foreach ($all_posts['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                                                        $running_count++;
                                                                                                        $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                                                        if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                                                            ?>
                                                                                                            <span>
                                                                                                                <?php
                                                                                                                echo $user_details->first_name . " " . $user_details->last_name;
                                                                                                                if ($running_count != $array_count) {
                                                                                                                    echo ", ";
                                                                                                                }
                                                                                                                ?>
                                                                                                            </span>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                    if (count($all_posts['posts_like_all_user'][$value->post_id]) >= 1) {
                                                                                                        foreach ($all_posts['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                                                            $running_count++;
                                                                                                            $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                                                            if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                                                                ?>
                                                                                                                <span>
                                                                                                                    <?php
                                                                                                                    echo $user_details->first_name . " " . $user_details->last_name;
                                                                                                                    if ($running_count != $array_count) {
                                                                                                                        echo ", ";
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </span>
                                                                                                                <?php
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                like this.
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="all_reply">
                                                                                    <?php
                                                                                    foreach ($all_posts['posts_reply_list'][$value->post_id] as $post_reply_key => $post_reply_value) {
                                                                                        $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                                                        ?>
                                                                                        <div
                                                                                            class="post_content_reply_div">
                                                                                            <div class="row">
                                                                                                <div class="col-md-2">
                                                                                                    <div
                                                                                                        class="top_user_img pull-right">
                                                                                                        <img
                                                                                                            class="reply_user_img"
                                                                                                            src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-10">
                                                                                                    <div
                                                                                                        class="top_user_details">
                                                                                                        <h5>
                                                                                                            <a class="top_name"
                                                                                                               uid="<?php echo $this->encryption_decryption_object->encode($user_details->user_id); ?>"
                                                                                                               href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($user_details->user_id); ?>"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a>
                                                                                                        </h5>
                                                                                                        <h6>
                                                                                                            <span
                                                                                                                class="post_time"
                                                                                                                timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                                                                    <?php
                                                                                                                    $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                                                                    echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                                                                    ?>
                                                                                                            </span>
                                                                                                        </h6>

                                                                                                        <div>
                                                                                                            <p class="post_message_p">
                                                                                                                <?php
                                                                                                                if (str_word_count($post_reply_value->reply_content) >= 80) {
                                                                                                                    ?>
                                                                                                                    <span
                                                                                                                        class="truncated_body">
                                                                                                                            <?php
                                                                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                                                                            ?>
                                                                                                                    </span>
                                                                                                                    <span
                                                                                                                        class="complete_body">
                                                                                                                            <?php
                                                                                                                            echo $post_reply_value->reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                                                                            ?>
                                                                                                                    </span>
                                                                                                                    <?php
                                                                                                                } else {
                                                                                                                    echo $post_reply_value->reply_content;
                                                                                                                }
                                                                                                                ?>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                        if (count($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id])) {
                                                                                                            ?>
                                                                                                            <div
                                                                                                                class="reply_post_notified_user_div">
                                                                                                                <div
                                                                                                                    class="row">
                                                                                                                    <div
                                                                                                                        class="col-md-12">
                                                                                                                        <div
                                                                                                                            class="reply_post_notify_user_heading">
                                                                                                                            cc:
                                                                                                                        </div>
                                                                                                                        <div
                                                                                                                            class="reply_post_notify_user_body">
                                                                                                                                <?php
                                                                                                                                $array_count = count($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id]);
                                                                                                                                $running_count = 0;
                                                                                                                                foreach ($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id] as $user_id) {
                                                                                                                                    $running_count++;
                                                                                                                                    $user_details = $this->ion_auth->user($user_id->user_id)->row();
                                                                                                                                    ?>
                                                                                                                                <a user="<?php echo $this->encryption_decryption_object->encode($user_id->user_id); ?>"
                                                                                                                                   href="">
                                                                                                                                       <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                                                                </a>
                                                                                                                                <?php
                                                                                                                                if ($running_count != $array_count) {
                                                                                                                                    echo ",";
                                                                                                                                }
                                                                                                                            }
                                                                                                                            ?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <?php
                                                                                                        }
                                                                                                        ?>
                                                                                                        <div
                                                                                                            post_id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                                                            post_reply_id="<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                                            class="post_action_div">
                                                                                                            <a class="post_reply_like"
                                                                                                               href="javascript:void(0);"
                                                                                                               like_count="<?php echo count($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]); ?>"
                                                                                                               like_action="<?php echo ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>"
                                                                                                               title="Like"><?php echo ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                                                            <a href="javascript:void(0);"
                                                                                                               class="click_more"
                                                                                                               title="More">More</a>

                                                                                                            <div
                                                                                                                class="more_list">
                                                                                                                <ul>
                                                                                                                    <li class="delete_post_reply">
                                                                                                                        <a href="javascript:void(0);">
                                                                                                                            <h6>
                                                                                                                                <span
                                                                                                                                    class="stop_icon"></span>
                                                                                                                                Delete
                                                                                                                            </h6>
                                                                                                                        </a>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div
                                                                                                id="post_reply_like_div-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                                style="display: <?php echo (!empty($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>"
                                                                                                class="post_reply_like_div">
                                                                                                <div class="row">
                                                                                                    <div
                                                                                                        class="col-md-2"></div>
                                                                                                    <div
                                                                                                        class="col-md-10">
                                                                                                        <div
                                                                                                            id="post_reply_like_body-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                                            class="post_like_body">
                                                                                                            <i class="fa fa-thumbs-up"></i>
                                                                                                            <span
                                                                                                                class="current_user_post_reply_like">
                                                                                                                    <?php
                                                                                                                    //current user like
                                                                                                                    if ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1 && count($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                                                                        ?>
                                                                                                                    You and
                                                                                                                    <?php
                                                                                                                } else if ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                                                                    ?>
                                                                                                                    You
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </span>
                                                                                                            like this.
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <!-- reply comment form  -->
                                                                                <div class="reply_box"
                                                                                     id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                                                                    <div class="announce_img">
                                                                                        <img
                                                                                            src="<?php echo base_url() . $user->profile_picture; ?>"
                                                                                            alt="">
                                                                                    </div>
                                                                                    <div class="share_box2 comment_in1">
                                                                                        <a href="javascript:void(0);">Write
                                                                                            a reply...</a>
                                                                                    </div>
                                                                                    <div class="more_share">
                                                                                        <form class="reply_form">
                                                                                            <a href="#"><?php echo $user->first_name . " " . $user->last_name; ?>
                                                                                                is replying.</a>

                                                                                            <div class="text_poll2">
                                                                                                <input type="hidden"
                                                                                                       name="post_id"
                                                                                                       value="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"/>
                                                                                                <textarea
                                                                                                    name="reply_message"
                                                                                                    class="share_text2 reply_textarea"></textarea>
                                                                                            </div>
                                                                                            <div
                                                                                                class="text_poll2 reply_post_text_poll2">
                                                                                                <div
                                                                                                    class="AddedCCMembersOfReply"></div>
                                                                                                <input type="text"
                                                                                                       placeholder="Notify additional people.."
                                                                                                       class="poll_text addCCMembersToReply"
                                                                                                       name="note">

                                                                                                <div class="notify_pop"
                                                                                                     style="display: none;left: 0px;">
                                                                                                    <div class="row">
                                                                                                        <div
                                                                                                            class="col-md-12">
                                                                                                            <div
                                                                                                                class="people_list">
                                                                                                                <ul class="Reply_ListOfMembers">

                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <button type="submit"
                                                                                                    class="btn btn-primary reply_post_button"
                                                                                                    disabled=""> Post
                                                                                            </button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div class="scroller_loading_div">
                                                    <img class="scroller_loading_img"
                                                         src="<?php echo base_url(); ?>assets_front/image/scroll_loader.GIF"
                                                         alt=""/>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="top_tab"></div>
                                            <div role="tabpanel" class="tab-pane" id="follow_tab"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="right_bar">
                                    <div class="recent_activity">
                                        <h5>Notifications</h5>
                                        <?php
                                        $count = 0;
                                        foreach ($all_unread_alerts as $value) {
                                            $posted_id = $uid = $value->posted_user_id;
                                            $timestamp = $value->timestamp;
                                            if ($value->group_type == "fixed") {
                                                $method_name = "view_post";
                                            } else if ($value->group_type == "unfixed") {
                                                $method_name = "view_group_post";
                                            }
                                            ?>
                                            <div class="activities">
                                                <div class="act_img">
                                                    <img
                                                        src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                                                        alt=""/>
                                                </div>
                                                <div class="alert_details">
                                                    <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                                                        <span>
                                                            <?php
                                                            switch ($value->type) {
                                                                case 'notify':
                                                                    echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                                                    break;
                                                                case 'reply':
                                                                    if ($value->group_type == "fixed") {
                                                                        $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                                                        $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                    } else {
                                                                        $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                                                        $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                                    }
                                                                    if (count($temp_details_array) > 1) {
                                                                        $i = 0;
                                                                        $put_comma = 0;
                                                                        foreach ($temp_details_array as $value1) {
                                                                            $uid = $value1->user_id;
                                                                            $i++;
                                                                            if ($uid != $posted_id) {
                                                                                $put_comma = 1;
                                                                                echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                            }
                                                                            if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                                echo ", ";
                                                                            }
                                                                        }
                                                                        echo " commented on your post.";
                                                                    } else {
                                                                        echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                                                    }
                                                                    break;
                                                                case 'like':
                                                                    if ($value->group_type == "fixed") {
                                                                        $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                                        $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                    } else {
                                                                        $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                                        $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                                    }
                                                                    if (count($temp_details_array) > 1) {
                                                                        $i = 0;
                                                                        $put_comma = 0;
                                                                        foreach ($temp_details_array as $value1) {
                                                                            $uid = $value1->user_id;
                                                                            $i++;
                                                                            if ($uid != $posted_id) {
                                                                                $put_comma = 1;
                                                                                echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                                            }
                                                                            if ($i != count($temp_details_array) && $put_comma == "1") {
                                                                                echo ", ";
                                                                            }
                                                                        }
                                                                        echo " liked your post.";
                                                                    } else {
                                                                        echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                                                    }
                                                                    break;
                                                            }
                                                            ?>
                                                        </span><br>
                                                    </a>
                                                    <span class="alert_timestamp">
                                                        <?php
                                                        $posted_date = date('M j Y', strtotime($timestamp));
                                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <?php
                                            $count++;
                                            if ($count >= 5)
                                                break;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dote_info_tab">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="EditGroupInfo_Form" method="POST" action="">
                                    <div class="announce">
                                        <h5>
                                            <i class="fa fa-info-circle"></i> <?php echo $group->name; ?> Info
                                            <?php if ($user->id == $group->group_admin_user_id) { ?>
                                                <div class="group_info_edit_button_div">
                                                    <button type="button"
                                                            class="btn btn-default btn-xs pull-right group_info_edit_button">
                                                        <i class="fa fa-pencil"></i> Edit
                                                    </button>
                                                </div>
                                                <div class="group_info_save_button_div" style="display: none;">
                                                    <button type="submit"
                                                            class="btn btn-default btn-xs pull-right save_info_button">
                                                        Save
                                                    </button>
                                                    <a href="javascript:void(0);" class="pull-right btn-xs cancel_edit">Cancel</a>
                                                </div>
                                            <?php } ?>
                                        </h5>
                                        <div class="total_anounce">
                                            <div class="col-md-12">
                                                <div class="course_message">
                                                    <div class="group_info_div">
                                                        <?php
                                                        if ($group->group_info == "") {
                                                            echo "<div class='temp_p'><p style='font-size: 11px;color: #757575;'>Click 'edit' (above and to the right) to give users more information about this Group. You can add things like a description of what this Group is about, what kinds of things people should post here, and resources for group members.</p></div>";
                                                        } else {
                                                            ?>
                                                            <div>
                                                                <?php
                                                                echo html_entity_decode($group->group_info);
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="group_info_textpad_div" style="display: none;">
                                                        <textarea id="group_info_textpad" class="group_info_textpad"
                                                                  placeholder="Place some text here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dote_file_tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="announce">
                                    <h5>
                                        <i class="fa fa-info-circle"></i> File list
                                    </h5>
                                    <br>

                                    <div class="total_anounce">
                                        <div class="box-body table-responsive no-padding">
                                            <table id="group_files_table"
                                                   class="table table-bordered table-hover dataTable"
                                                   aria-describedby="group_files_table_info">
                                                <thead>
                                                    <tr>
                                                        <th width="50%">Name</th>
                                                        <th width="10%">Type</th>
                                                        <th width="20%">Uploaded By</th>
                                                        <th width="20%">Uploaded On</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($files as $value) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                $temp = explode("/", $value->file_path);
                                                                $temp_name = explode("-", $temp[3]);
                                                                $file_name = explode(".", $temp_name[1]);
                                                                ?>
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb"
                                                                   href="<?php echo base_url() . $value->file_path; ?>">
                                                                       <?php echo $file_name[0]; ?>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <?php echo $file_name[1]; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $value->first_name . " " . $value->last_name; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $value->timestamp; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include('footer2.php');
include('page_modal/groups_modal.php');
?>
<!---- Supporting JS ---->
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/groups.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/group_posts.js" type="text/javascript"></script>

<!---- Fancybox JS ---->
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!---- tinymce JS ---->
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>

<!---- Datatable JS ---->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<!---- highlight JS ---->
<script src="<?php echo base_url(); ?>assets/plugins/highlight/jquery.highlight-5.closure.js" type="text/javascript"></script>
