<?php include('intra_leftbar.php') ?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>

<input type="hidden" id="fixed_group_name" value="fees"/>
<div class="col-md-9">
    <div class="boards_tab"> 
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success ! 
                        <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                </div>
                <div class="fee_structure_details">
                    <div class="row">
                        <div class="col-md-6">
                            <h3><i class="fa fa-gears"></i> Manage Fee Structure</h3>  
                        </div>
                        <!--                        <div class="col-md-6">
                                                    <button id="addNewFeesBtn" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus"></i> Add New Fee Structure</button>
                                                </div>-->
                    </div>
                    <div class="row" style="border-top: 1px solid #e6e6e6; margin-top: 15px;">&nbsp;</div>
                    <div class="fees_box">
                        <div class="fees_year_head fees_body_active">
                            <div class="row">
                                <div class="col-md-6">
                                    <a style="text-decoration: none"><h4 style="color: #5c5c5c;font-family: 'open_sanslight';font-size: 18px;margin: 0px;"><i class="fa fa-circle-o"></i>&nbsp; Fee Structure 2015-2016</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="fees_year_body" style="display: block;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="fee_table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Year</th>
                                                    <th>1st year</th>
                                                    <th>2nd year</th> 
                                                    <th>3rd year</th>
                                                    <th>4th year</th>
                                                    <th>Action</th>
                                                </tr>
                                                <tr class="blue_row">
                                                    <th>Course</th>
                                                    <th>Admission fee</th>
                                                    <th>Tuition fee</th>
                                                    <th>Tuition fee</th>
                                                    <th>Tuition fee</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                foreach ($college_stream_course_list as $key => $value) {
                                                    foreach ($value as $inner_value) {
                                                        ?>
                                                        <tr stream="<?php echo $encryption_decryption_object->encode($key); ?>" stream_course = "<?php echo $encryption_decryption_object->encode($inner_value->course_id); ?>">
                                                            <td class="course_name"><?php echo $stream_name[$key] . " - " . $inner_value->title; ?></td>
                                                            <td class="first_year" fee="">Rs.25000</td>
                                                            <td class="second_year" fee="">Rs.125000</td>
                                                            <td class="third_year" fee="">Rs.125000</td>
                                                            <td class="forth_year" fee="">Rs.125000</td>
                                                            <td>
                                                                <button class="edit_fees btn btn-default btn-xs pull-right"><i class="fa fa-edit"></i></button>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of tab1 -->
                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>
                    <p>The institute has following major facilities:</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>
                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="blog"></div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/manage_fees_modal");
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/manage_fees.js" type="text/javascript"></script>