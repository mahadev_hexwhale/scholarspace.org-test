<?php
include('intra_leftbar.php');
?>
<input type="hidden" id="college_id" value="<?php echo $this->session->userdata('college_id'); ?>">
<input type="hidden" id="fixed_group_name" value="dashboard"/>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<div class="col-md-6">
    <div class="boards_tab">
        <div>
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">
                    <div class="commentors_tab">
                        <div>
                            <!-- Tab panes -->
                            <div class="tab-content" style="margin-top: 14px;">
                                <div role="tabpanel" class="tab-pane active" id="all_tab">
                                    <div id="all_post_content" class="global_details">
                                        <?php
                                        foreach ($home_posts as $value) {
                                            ?>
                                            <div class="post_content_div" id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>" gid = "<?php echo $encryption_decryption_object->encode($value->group_id); ?>">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="top_user_img">
                                                            <img src="<?php echo base_url() . $this->ion_auth->user($value->user_id)->row()->profile_picture; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="top_user_details">
                                                            <h5>
                                                                <a class="top_name"
                                                                   uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>"
                                                                   href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>"><?php echo $this->ion_auth->user($value->user_id)->row()->first_name . " " . $this->ion_auth->user($value->user_id)->row()->last_name; ?>
                                                                </a>
                                                            </h5>

                                                            <h5>
                                                                <span class="top_group_name">posted in </span>
                                                                <a class="top_name"
                                                                   gid="<?php echo $this->encryption_decryption_object->encode($value->group_id); ?>"
                                                                   href="<?php echo site_url() . "user/groups?grp_id=" . $this->encryption_decryption_object->encode($value->group_id); ?>"><?php echo $group_names[$value->group_id]; ?>
                                                                </a>
                                                            </h5>
                                                            <div class="user_popup res_top_name"></div>
                                                            <h6>
                                                                <span class="post_time" timestamp="<?php echo strtotime($value->timestamp); ?>">
                                                                    <?php
                                                                    $posted_date = date('M j Y', strtotime($value->timestamp));
                                                                    echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                                                    ?>
                                                                </span>
                                                            </h6>
                                                            <?php
                                                            switch ($value->type) {
                                                                case 'update':
                                                                    ?>
                                                                    <div class="post_type_div" id="<?php echo $encryption_decryption_object->encode($all_posts['posts_update_details'][$value->post_id][0]->update_id); ?>">
                                                                        <p class="post_message_p">
                                                                            <?php
                                                                            if (str_word_count($all_posts['posts_update_details'][$value->post_id][0]->update_content) >= 110) {
                                                                                ?>
                                                                                <span class="truncated_body">
                                                                                    <?php
                                                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                    ?>
                                                                                </span>
                                                                                <span class="complete_body">
                                                                                    <?php
                                                                                    echo html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                                    ?>
                                                                                </span>
                                                                                <?php
                                                                            } else {
                                                                                echo html_entity_decode($all_posts['posts_update_details'][$value->post_id][0]->update_content);
                                                                            }
                                                                            ?>
                                                                        </p>
                                                                    </div>
                                                                    <?php
                                                                    break;
                                                                case 'poll':
                                                                    if ($all_posts['posts_poll_answer_vote_current_user'][$value->post_id]) {
                                                                        ?>
                                                                        <div class="post_type_div"
                                                                             id="<?php echo $encryption_decryption_object->encode($all_posts['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                                            <p class="poll_question_p">
                                                                                <?php echo $all_posts['posts_poll_details'][$value->post_id][0]->question; ?>
                                                                            </p>

                                                                            <div class="poll_answer_div">
                                                                                <?php
                                                                                $poll_id = $all_posts['posts_poll_details'][$value->post_id][0]->poll_id;
                                                                                $total_votes = 0;
                                                                                foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                    $vote_count = $all_posts['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                                                    $total_votes += $vote_count;
                                                                                }
                                                                                foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                    $vote_count = $all_posts['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                                                    if ($vote_count == 0) {
                                                                                        $answer_percentage = 0;
                                                                                    } else {
                                                                                        $answer_percentage = (100 * $vote_count) / $total_votes;
                                                                                    }
                                                                                    ?>
                                                                                    <div class="ans_option">
                                                                                        <p class="poll_answer_p"
                                                                                           id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?></p>

                                                                                        <div class="progress">
                                                                                            <div
                                                                                                class="progress-bar progress-bar-green"
                                                                                                role="progressbar"
                                                                                                aria-valuenow="40"
                                                                                                aria-valuemin="0"
                                                                                                aria-valuemax="100"
                                                                                                style="width: <?php echo $answer_percentage; ?>%"></div>
                                                                                        </div>
                                                                                        <p class="poll_answer_p ans_percentage_p"><?php echo round($answer_percentage); ?>
                                                                                            %</p>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                                <div
                                                                                    class="total_vote_div"><?php echo $total_votes; ?>
                                                                                    total votes
                                                                                    <!--<a href="javascript:void(0);" class="change_vote_button"> · Change Vote</a>--></div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <div class="post_type_div"
                                                                             id="<?php echo $encryption_decryption_object->encode($all_posts['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                                            <p class="poll_question_p">
                                                                                <?php echo $all_posts['posts_poll_details'][$value->post_id][0]->question; ?>
                                                                            </p>

                                                                            <div class="poll_answer_div">
                                                                                <form class="poll_answer_form">
                                                                                    <?php
                                                                                    $poll_id = $all_posts['posts_poll_details'][$value->post_id][0]->poll_id;
                                                                                    foreach ($all_posts['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                                        ?>
                                                                                        <p class="poll_answer_p"
                                                                                           id="<?php echo $encryption_decryption_object->encode($value1->ans_id); ?>">
                                                                                            <label>
                                                                                                <input type="radio"
                                                                                                       name="answer"
                                                                                                       class="radio_answer"
                                                                                                       value="<?php echo $encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?>
                                                                                            </label>
                                                                                        </p>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <button type="submit"
                                                                                            name="submit_vote"
                                                                                            class="btn btn-primary submit_vote">
                                                                                        Vote
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    break;
                                                                case 'praise':
                                                                    ?>
                                                                    <div class="post_type_div"
                                                                         id="<?php echo $encryption_decryption_object->encode($all_posts['posts_praise_details'][$value->post_id][0]->praise_id); ?>">
                                                                        <div class="row praised_div">
                                                                            <div class="col-md-2 praise_div_image">
                                                                                <img
                                                                                    src="<?php echo base_url() . "" . $all_posts['posts_praise_details'][$value->post_id][0]->badge_image; ?>"
                                                                                    alt="">
                                                                            </div>
                                                                            <div class="col-md-10 praise_div_details">
                                                                                <p class="row praise_member_p">
                                                                                    <span
                                                                                        class="praised_text">Praised</span>
                                                                                        <?php
                                                                                        $array_count = count($all_posts['posts_praise_member_details'][$value->post_id]);
                                                                                        $running_count = 0;
                                                                                        foreach ($all_posts['posts_praise_member_details'][$value->post_id] as $member_value) {
                                                                                            $running_count++;
                                                                                            echo $this->ion_auth->user($member_value->praised_user_id)->row()->first_name . " " . $this->ion_auth->user($member_value->praised_user_id)->row()->last_name;
                                                                                            if ($running_count != $array_count) {
                                                                                                echo ", ";
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                </p><br>

                                                                                <p class="row praise_content_p">"
                                                                                    <?php
                                                                                    if (str_word_count($all_posts['posts_praise_details'][$value->post_id][0]->praise_content) >= 110) {
                                                                                        ?>
                                                                                        <span class="truncated_body">
                                                                                            <?php
                                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                            ?>
                                                                                        </span>
                                                                                        <span class="complete_body">
                                                                                            <?php
                                                                                            echo html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                                            ?>
                                                                                        </span>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo html_entity_decode($all_posts['posts_praise_details'][$value->post_id][0]->praise_content);
                                                                                    }
                                                                                    ?>
                                                                                    "
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    break;
                                                                case 'announce':
                                                                    ?>
                                                                    <div class="post_type_div"
                                                                         id="<?php echo $encryption_decryption_object->encode($all_posts['posts_announce_details'][$value->post_id][0]->announce_id); ?>">
                                                                        <div class="post_announce_div">
                                                                            <div class="row">
                                                                                <div
                                                                                    class="col-md-12 announce_head_div">
                                                                                    <i class="fa fa-bullhorn"></i>
                                                                                    <?php echo $all_posts['posts_announce_details'][$value->post_id][0]->heading; ?>
                                                                                </div>
                                                                                <div
                                                                                    class="col-md-12 announce_content_div">
                                                                                        <?php
                                                                                        if (str_word_count($all_posts['posts_announce_details'][$value->post_id][0]->content) >= 110) {
                                                                                            ?>
                                                                                        <span class="truncated_body">
                                                                                            <?php
                                                                                            $content = html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content);
                                                                                            $content = preg_replace('/<p[^>]*>/', '', $content);
                                                                                            $content = preg_replace('/\s+?(\S+)?$/', '', substr($content, 0, 500));
                                                                                            echo "<p>" . $content . "</p>";
                                                                                            echo "<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                                            ?>
                                                                                        </span>
                                                                                        <span class="complete_body">
                                                                                            <?php
                                                                                            echo html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse </a>";
                                                                                            ?>
                                                                                        </span>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo str_replace('<br />', '', html_entity_decode($all_posts['posts_announce_details'][$value->post_id][0]->content));
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    break;
                                                            }

                                                            $file_count = count($all_posts['posts_file_list'][$value->post_id]);
                                                            if ($file_count) {
                                                                ?>
                                                                <div class="post_file_div">
                                                                    <div class="row">
                                                                        <?php
                                                                        $count = 1;
                                                                        $inner_count = 2;
                                                                        foreach ($all_posts['posts_file_list'][$value->post_id] as $file_value) {
                                                                            if ($count == 1) {
                                                                                if ($file_value->file_type == "jpg") {
                                                                                    ?>
                                                                                    <div class="col-md-12">
                                                                                        <a class="fancybox-thumbs"
                                                                                           data-fancybox-group="thumb"
                                                                                           href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                                            <img
                                                                                                style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                                src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                                        </a>
                                                                                    </div>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <div class="col-md-4"
                                                                                         style="padding-right: 19px;">
                                                                                        <span
                                                                                            class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                                        <img
                                                                                            style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                            src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                $count++;
                                                                            } else {
                                                                                if ($inner_count == 2) {
                                                                                    ?>
                                                                                    <!--<div class="col-md-12">-->
                                                                                    <?php
                                                                                }
                                                                                if ($file_value->file_type == "jpg") {
                                                                                    ?>
                                                                                    <div class="col-md-4"
                                                                                         style="padding-right: 19px;">
                                                                                        <a class="fancybox-thumbs"
                                                                                           data-fancybox-group="thumb"
                                                                                           href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                                            <img
                                                                                                style="margin-bottom: 2px; width: 100%; object-fit: cover;"
                                                                                                src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                                        </a>
                                                                                    </div>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <div class="col-md-4"
                                                                                         style="padding-right: 19px;">
                                                                                        <span
                                                                                            class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                                        <img
                                                                                            style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;"
                                                                                            src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                if ($file_count == $inner_count) {
                                                                                    ?>
                                                                                    <!--</div>-->
                                                                                    <?php
                                                                                }
                                                                                $inner_count++;
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            if (count($all_posts['posts_notified_users_list'][$value->post_id])) {
                                                                ?>
                                                                <div class="notified_user_div">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="notify_user_heading">cc:</div>
                                                                            <div class="notify_user_body">
                                                                                <?php
                                                                                $array_count = count($all_posts['posts_notified_users_list'][$value->post_id]);
                                                                                $running_count = 0;
                                                                                foreach ($all_posts['posts_notified_users_list'][$value->post_id] as $user_id) {
                                                                                    $running_count++;
                                                                                    $user_details = $this->ion_auth->user($user_id)->row();
                                                                                    ?>
                                                                                    <a user="<?php echo $this->encryption_decryption_object->encode($user_id); ?>"
                                                                                       href="">
                                                                                           <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                    </a>
                                                                                    <?php
                                                                                    if ($running_count != $array_count) {
                                                                                        echo ",";
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php // if ($user->id == $group->group_admin_user_id) {     ?>
                                                            <div class="post_action_div"
                                                                 post_id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                                                <a class="post_like" href="javascript:void(0);"
                                                                   like_count="<?php echo count($all_posts['posts_like_all_user'][$value->post_id]); ?>"
                                                                   like_action="<?php echo ($all_posts['posts_like_current_user'][$value->post_id] == 1) ? "unlike" : "like"; ?>"
                                                                   title="Like"><?php echo ($all_posts['posts_like_current_user'][$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                                                <a href="javascript:void(0);" class="reply_link"
                                                                   title="Reply">Reply.</a>
                                                                <a href="javascript:void(0);"
                                                                   follow_action="<?php echo ($all_posts['posts_follow_current_user'][$value->post_id] == 1) ? "unfollow" : "follow"; ?>"
                                                                   class="post_follow"
                                                                   title="Follow"><?php echo ($all_posts['posts_follow_current_user'][$value->post_id] == 1) ? "Unfollow." : "Follow."; ?></a>
                                                                   <?php if ($all_posts['posts_current_user'][$value->post_id]) { ?>
                                                                    <a href="javascript:void(0);" class="click_more"
                                                                       title="More">More</a>
                                                                    <div class="more_list">
                                                                        <ul>
                                                                            <li class="delete_post"><a
                                                                                    href="javascript:void(0);"><h6><span
                                                                                            class="stop_icon"></span>
                                                                                        Delete </h6></a></li>
                                                                        </ul>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="commenting_box">
                                                                <div
                                                                    id="post_like_div-<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                    class="post_like_div"
                                                                    style="display: <?php echo (!empty($all_posts['posts_like_all_user'][$value->post_id])) ? "block" : "none"; ?>">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div
                                                                                id="post_like_body-<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                                class="post_like_body">
                                                                                <i class="fa fa-thumbs-up"></i>
                                                                                <span class="current_user_like">
                                                                                    <?php
                                                                                    $array_count = count($all_posts['posts_like_all_user'][$value->post_id]);
                                                                                    $running_count = 0;

                                                                                    if ($all_posts['posts_like_current_user'][$value->post_id] == 1) {
                                                                                        echo "You";
                                                                                        if (count($all_posts['posts_like_all_user'][$value->post_id]) > 1) {
                                                                                            echo ",";
                                                                                        }
                                                                                        foreach ($all_posts['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                                            $running_count++;
                                                                                            $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                                            if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                                                ?>
                                                                                                <span>
                                                                                                    <?php
                                                                                                    echo $user_details->first_name . " " . $user_details->last_name;
                                                                                                    if ($running_count != $array_count) {
                                                                                                        echo ", ";
                                                                                                    }
                                                                                                    ?>
                                                                                                </span>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        if (count($all_posts['posts_like_all_user'][$value->post_id]) >= 1) {
                                                                                            foreach ($all_posts['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                                                $running_count++;
                                                                                                $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                                                if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                                                    ?>
                                                                                                    <span>
                                                                                                        <?php
                                                                                                        echo $user_details->first_name . " " . $user_details->last_name;
                                                                                                        if ($running_count != $array_count) {
                                                                                                            echo ", ";
                                                                                                        }
                                                                                                        ?>
                                                                                                    </span>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    like this.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="all_reply">
                                                                    <?php
                                                                    foreach ($all_posts['posts_reply_list'][$value->post_id] as $post_reply_key => $post_reply_value) {
                                                                        $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                                        ?>
                                                                        <div class="post_content_reply_div">
                                                                            <div class="row">
                                                                                <div class="col-md-2">
                                                                                    <div
                                                                                        class="top_user_img pull-right">
                                                                                        <img class="reply_user_img"
                                                                                             src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <div class="top_user_details">
                                                                                        <h5><a class="top_name"
                                                                                               uid="<?php echo $this->encryption_decryption_object->encode($user_details->user_id); ?>"
                                                                                               href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($user_details->user_id); ?>"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a>
                                                                                        </h5>
                                                                                        <h6>
                                                                                            <span class="post_time" timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                                                <?php
                                                                                                $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                                                echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                                                ?>
                                                                                            </span>
                                                                                        </h6>

                                                                                        <div>
                                                                                            <p class="post_message_p">
                                                                                                <?php
                                                                                                if (str_word_count($post_reply_value->reply_content) >= 80) {
                                                                                                    ?>
                                                                                                    <span
                                                                                                        class="truncated_body">
                                                                                                            <?php
                                                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <span
                                                                                                        class="complete_body">
                                                                                                            <?php
                                                                                                            echo $post_reply_value->reply_content . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                                                            ?>
                                                                                                    </span>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    echo $post_reply_value->reply_content;
                                                                                                }
                                                                                                ?>
                                                                                            </p>
                                                                                        </div>
                                                                                        <?php
                                                                                        if (count($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id])) {
                                                                                            ?>
                                                                                            <div
                                                                                                class="reply_post_notified_user_div">
                                                                                                <div class="row">
                                                                                                    <div
                                                                                                        class="col-md-12">
                                                                                                        <div
                                                                                                            class="reply_post_notify_user_heading">
                                                                                                            cc:
                                                                                                        </div>
                                                                                                        <div class="reply_post_notify_user_body">
                                                                                                            <?php
                                                                                                            $array_count = count($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id]);
                                                                                                            $running_count = 0;
                                                                                                            foreach ($all_posts['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id] as $user_id) {
                                                                                                                $running_count++;
                                                                                                                $user_details = $this->ion_auth->user($user_id->user_id)->row();
                                                                                                                ?>
                                                                                                                <a user="<?php echo $this->encryption_decryption_object->encode($user_id->user_id); ?>" href="">
                                                                                                                    <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                                                </a>
                                                                                                                <?php
                                                                                                                if ($running_count != $array_count) {
                                                                                                                    echo ",";
                                                                                                                }
                                                                                                            }
                                                                                                            ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <div
                                                                                            post_id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"
                                                                                            post_reply_id="<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                            class="post_action_div">
                                                                                            <a class="post_reply_like"
                                                                                               href="javascript:void(0);"
                                                                                               like_count="<?php echo count($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]); ?>"
                                                                                               like_action="<?php echo ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>"
                                                                                               title="Like"><?php echo ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                                               <?php if ($all_posts['posts_current_user'][$value->post_id] || $post_reply_value->user_id == $this->ion_auth->user()->row()->id) { ?>
                                                                                                <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                                                                <div class="more_list">
                                                                                                    <ul>
                                                                                                        <li class="delete_post_reply">
                                                                                                            <a href="javascript:void(0);">
                                                                                                                <h6>
                                                                                                                    <span class="stop_icon"></span> Delete
                                                                                                                </h6>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                id="post_reply_like_div-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                style="display: <?php echo (!empty($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>"
                                                                                class="post_reply_like_div">
                                                                                <div class="row">
                                                                                    <div class="col-md-2"></div>
                                                                                    <div class="col-md-10">
                                                                                        <div
                                                                                            id="post_reply_like_body-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>"
                                                                                            class="post_like_body">
                                                                                            <i class="fa fa-thumbs-up"></i>
                                                                                            <span
                                                                                                class="current_user_post_reply_like">
                                                                                                    <?php
                                                                                                    //current user like
                                                                                                    if ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1 && count($all_posts['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                                                        ?>
                                                                                                    You and
                                                                                                    <?php
                                                                                                } else if ($all_posts['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                                                    ?>
                                                                                                    You
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            </span>
                                                                                            like this.
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <!-- reply comment form  -->
                                                                <div class="reply_box"
                                                                     id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                                                    <div class="announce_img">
                                                                        <img
                                                                            src="<?php echo base_url() . $this->user->profile_picture; ?>"
                                                                            alt="">
                                                                    </div>
                                                                    <div class="share_box2 comment_in1">
                                                                        <a href="javascript:void(0);">Write a
                                                                            reply...</a>
                                                                    </div>
                                                                    <div class="more_share">
                                                                        <form class="reply_form">
                                                                            <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?>
                                                                                is replying.</a>

                                                                            <div class="text_poll2">
                                                                                <input type="hidden" name="post_id"
                                                                                       value="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"/>
                                                                                <textarea name="reply_message"
                                                                                          class="share_text2 reply_textarea"></textarea>
                                                                            </div>
                                                                            <div
                                                                                class="text_poll2 reply_post_text_poll2">
                                                                                <div
                                                                                    class="AddedCCMembersOfReply"></div>
                                                                                <input type="text"
                                                                                       placeholder="Notify additional people.."
                                                                                       class="poll_text addCCMembersToReply"
                                                                                       name="note">

                                                                                <div class="notify_pop"
                                                                                     style="display: none;left: 0px;">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="people_list">
                                                                                                <ul class="Reply_ListOfMembers">

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <button type="submit"
                                                                                    class="btn btn-primary reply_post_button"
                                                                                    disabled=""> Post
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php // }     ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="scroller_loading_div">
                                        <img class="scroller_loading_img"
                                             src="<?php echo base_url(); ?>assets_front/image/scroll_loader.GIF"
                                             alt=""/>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="top_tab"></div>
                                <div role="tabpanel" class="tab-pane" id="follow_tab"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Notifications</h5>
            <?php
            $count = 0;
            foreach ($all_unread_alerts as $value) {
                $posted_id = $uid = $value->posted_user_id;
                $timestamp = $value->timestamp;
                if ($value->group_type == "fixed") {
                    $method_name = "view_post";
                } else if ($value->group_type == "unfixed") {
                    $method_name = "view_group_post";
                }
                ?>
                <div class="activities">
                    <div class="act_img">
                        <img
                            src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                            alt=""/>
                    </div>
                    <div class="alert_details">
                        <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                            <span>
                                <?php
                                switch ($value->type) {
                                    case 'notify':
                                        echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                        break;
                                    case 'reply':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " commented on your post.";
                                        } else {
                                            echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                        }
                                        break;
                                    case 'like':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " liked your post.";
                                        } else {
                                            echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                        }
                                        break;
                                }
                                ?>
                            </span><br>
                        </a>
                        <span class="alert_timestamp">
                            <?php
                            $posted_date = date('M j Y', strtotime($timestamp));
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                $count++;
                if ($count >= 5)
                    break;
            }
            ?>
        </div>
    </div>
</div>
<?php include('footer2.php'); ?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/intranet_posts.js" type="text/javascript"></script>

<!---- tinymce JS ---->
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>