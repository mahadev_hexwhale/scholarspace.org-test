<?php
include('intra_leftbar.php');
$finalize = $course_details->finalize;
$midterm = $course_details->midterm;
?>
<input type="hidden" id="course_id" value="<?php echo $this->encryption_decryption_object->encode($course_details->id); ?>">
<div class="col-md-9">
    <div class="group_page_box">
        <div class="col-md-7">
            <button type="button" name="EditCourseDetails" class="btn btn-xs pull-right EditCourseDetails" data-toggle="modal" data-target="#EditCourseDetails_Modal"><i class="fa fa-pencil"></i> Edit </button>
            <div class="course_name">
                <a href="<?php echo site_url() . "user/courses/view/" . $this->encryption_decryption_object->encode($course_details->id); ?>"><?php echo $course_details->course_name; ?></a>
            </div>
            <?php if ($course_details->save_and_draft == 1) { ?>
                <a href="javascript:void(0);" class="publish_course">Publish now</a>
            <?php } ?>
            <div class="course_desc">
                <h4><?php echo $course_details->course_brief; ?></h4>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-12">
                <?php if (!empty($assistants)) { ?>
                    <div class="course_assistants admin_details">
                        Assistants
                        <ul>
                            <?php foreach ($assistants as $value) { ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . $this->ion_auth->user($value->user_id)->row()->profile_picture; ?>" alt="">
                                        <div class="admin_plus"><?php echo $this->ion_auth->user($value->user_id)->row()->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                            <li><button type="button" name="AddMoreAssistants" class="btn btn-xs AddMoreAssistants" data-toggle="modal" data-target="#AddMoreAssistants_Modal"><i class="fa fa-plus"></i></button></li>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <div class = "col-md-12">
                <?php if (!empty($associates)) {
                    ?>
                    <div class="course_associates admin_details">
                        Associates
                        <ul>
                            <?php foreach ($associates as $value) { ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . $this->ion_auth->user($value->user_id)->row()->profile_picture; ?>" alt="">
                                        <div class="admin_plus"><?php echo $this->ion_auth->user($value->user_id)->row()->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                            <li><button type="button" name="AddMoreAssociates" class="btn btn-xs AddMoreAssociates" data-toggle="modal" data-target="#AddMoreAssociates_Modal"><i class="fa fa-plus"></i></button></li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="bck_setlist">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#attendance_tab" aria-controls="attendance_tab" role="tab" data-toggle="tab"><span class="min_img1"></span> Attendance</a>
            </li>
            <li role="presentation">
                <a href="#ia_marks_tab" aria-controls="ia_marks_tab" role="tab" data-toggle="tab"><span class="min_img2"></span> IA Marks </a>
            </li>
            <?php if ($midterm == "1") { ?>
                <li role="presentation">
                    <a href="#midterm_marks_tab" aria-controls="midterm_marks_tab" role="tab" data-toggle="tab"><span class="min_img3"></span> Midterm Marks</a>
                </li>
            <?php } ?>
            <li role="presentation">
                <a href="#external_marks_tab" aria-controls="external_marks_tab" role="tab" data-toggle="tab"><span class="min_img3"></span> External Marks</a>
            </li>
            <li role="presentation">
                <a href="#final_marks_tab" aria-controls="final_marks_tab" role="tab" data-toggle="tab"><span class="min_img3"></span> Final Marks</a>
            </li>
            <li role="presentation" class="pull-right">
                <button type="button" name="AddMoremembers" class="btn btn-xs AddMoremembers" data-toggle="modal" data-target="#AddMoremembers_Modal"><i class="fa fa-plus"></i> Add Members</button>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="attendance_tab">
            <div class="profile_form">
                <form id="MemberAttendance_Form" method="POST">
                    <h3>Attendance
                        <button type="submit" name="SaveAttendance" class="btn btn-success pull-right SaveAttendance_Button"> Save</button>
                    </h3>
                    <div class="col-md-3" style="padding: 0px">
                        <table id="MemberAttendance_Table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    ?>
                                    <tr uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>">
                                        <td class="td_height_28">
                                            <span>
                                                <a href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                    <?php
                                                    echo $value->first_name . " " . $value->last_name;
                                                    ?>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-9" style="padding: 0px;overflow-x: scroll;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <?php
                                    $col_num = 0;
                                    foreach ($classes_array as $class => $date) {
                                        $col_num++;
                                        ?>
                                        <td>
                                            <span class="marks_num"><?php echo $col_num . "."; ?></span>
                                            <span class="marks_date_th"><?php echo date('d/m/Y', strtotime($date)); ?></span>
                                        </td>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    $uid = $value->user_id;
                                    ?>
                                    <tr uid="<?php echo $this->encryption_decryption_object->encode($uid); ?>">
                                        <?php
                                        foreach ($classes_array as $class => $date) {
                                            $attendance = $user_attendance[$uid][$class];
                                            ?>
                                            <td class="attendance_td">
                                                <label class="radio-inline attend_radio pull-left">
                                                    <input type="radio" name="<?php echo "attendance//" . $this->encryption_decryption_object->encode($class) . "//" . $this->encryption_decryption_object->encode($uid); ?>" class="attendance_radio present" value="1" <?php if ($attendance == "1") echo "checked"; ?>>P
                                                </label>
                                                <label class="radio-inline attend_radio pull-left">
                                                    <input type="radio" name="<?php echo "attendance//" . $this->encryption_decryption_object->encode($class) . "//" . $this->encryption_decryption_object->encode($uid); ?>" class="attendance_radio absent" value="0" <?php if ($attendance == "0") echo "checked"; ?>>A
                                                </label>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="ia_marks_tab">
            <div class="profile_form">
                <form id="MemberIAMarks_Form" method="POST">
                    <h3>IA Marks
                        <button type="submit" name="SaveIAMarks" class="btn btn-success pull-right SaveIAMarks_Button"> Save</button>
                    </h3>
                    <div class="col-md-3" style="padding: 0px">
                        <table id="MemberIAMarks_Table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="150">Student</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    ?>
                                    <tr uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>">
                                        <td class="td_height_28">
                                            <span>
                                                <a href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                    <?php
                                                    echo $value->first_name . " " . $value->last_name;
                                                    ?>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-9" style="padding: 0px;overflow-x: scroll;">
                        <table id="" class="table table-striped table-bordered fix_width_600">
                            <thead>
                                <tr>
                                    <?php
                                    $col_num = 0;
                                    foreach ($tests_array as $test => $test_info) {
                                        $col_num++;
                                        ?>
                                        <th>
                                            <span class="marks_num"><?php echo $col_num . "."; ?></span>
                                            <span class="marks_date_th"><?php echo date('d/m/Y', strtotime($test_info->date)); ?></span>
                                            <span class="marks_th_span">&nbsp;&nbsp;&nbsp;<?php echo $test_info->total_marks; ?></span>
                                        </th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    $uid = $value->user_id;
                                    ?>
                                    <tr uid="<?php echo $this->encryption_decryption_object->encode($uid); ?>">
                                        <?php
                                        foreach ($tests_array as $test => $date) {
                                            $marks = $user_iamarks[$uid][$test];
                                            ?>
                                            <td class="iamarks_td">
                                                <input type="text" name="<?php echo 'iatest//' . $this->encryption_decryption_object->encode($test) . '//' . $this->encryption_decryption_object->encode($uid); ?>" value="<?php echo $marks ?>" class="width_132">
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <?php if ($midterm == "1") { ?>
            <div role="tabpanel" class="tab-pane" id="midterm_marks_tab">
                <div class="profile_form">
                    <form id="MemberMidtermMarks_Form" class="Marks_Form" fid="midterm" method="POST">
                        <h3> Midterm Marks
                            <button type="submit" name="SaveMidtermMarks" class="btn btn-success pull-right SaveMidtermMarks_Button"> Save</button>
                        </h3>
                        <div class="col-md-12" style="padding: 0px">
                            <table id="MemberMidtermMarks_Table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Student</th>
                                        <th>Marks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($group_student_list as $key => $value) {
                                        $uid = $value->user_id;
                                        if ($midterm_marks[$uid] != "") {
                                            $marks = $midterm_marks[$uid]->obtained_marks;
                                            $total_marks = $midterm_marks[$uid]->total_marks;
                                            ?>
                                            <tr uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                <td>
                                                    <span>
                                                        <a href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                            <?php
                                                            echo $value->first_name . " " . $value->last_name;
                                                            ?>
                                                        </a>
                                                    </span>
                                                </td>
                                                <td class="midterm_marks_td marks_td">
                                                    <input type="text" class="input_width_70" name="<?php echo 'midterm//' . $this->encryption_decryption_object->encode($uid); ?>" value="<?php echo $marks ?>"> out of <?php echo $total_marks; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        <?php } ?>
        <div role="tabpanel" class="tab-pane" id="external_marks_tab">
            <div class="profile_form">
                <form id="MemberExternalMarks_Form" class="Marks_Form" fid="external" method="POST">
                    <h3> External Marks
                        <button type="submit" name="SaveExternalMarks" class="btn btn-success pull-right SaveExternalMarks_Button"> Save</button>
                    </h3>
                    <div class="col-md-12" style="padding: 0px">
                        <table id="MemberExternalMarks_Table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    $uid = $value->user_id;
                                    if ($external_marks[$uid] != "") {
                                        $marks = $external_marks[$uid]->obtained_marks;
                                        $total_marks = $external_marks[$uid]->total_marks;
                                        ?>
                                        <tr uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>">
                                            <td>
                                                <span>
                                                    <a href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                        <?php
                                                        echo $value->first_name . " " . $value->last_name;
                                                        ?>
                                                    </a>
                                                </span>
                                            </td>
                                            <td class="external_marks_td marks_td">
                                                <input type="text" class="input_width_70" name="<?php echo 'external//' . $this->encryption_decryption_object->encode($uid); ?>" value="<?php echo $marks ?>"> out of <?php echo $total_marks; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="final_marks_tab">
            <div class="profile_form">
                <form id="MemberFinalMarks_Form" class="Marks_Form" fid="final" method="POST">
                    <h3> Final Marks
                        <button type="submit" name="SaveFinalMarks" class="btn btn-success pull-right SaveFinalMarks_Button"> Save</button>
                    </h3>
                    <div class="col-md-12" style="padding: 0px">
                        <table id="MemberFinalMarks_Table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($group_student_list as $key => $value) {
                                    $uid = $value->user_id;
                                    if ($final_marks[$uid] != "") {
                                        $marks = $final_marks[$uid]->obtained_marks;
                                        $total_marks = $final_marks[$uid]->total_marks;
                                        ?>
                                        <tr uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>">
                                            <td>
                                                <span>
                                                    <a href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>">
                                                        <?php
                                                        echo $value->first_name . " " . $value->last_name;
                                                        ?>
                                                    </a>
                                                </span>
                                            </td>
                                            <td class="external_marks_td marks_td">
                                                <input type="text" class="input_width_70" name="<?php echo 'external//' . $this->encryption_decryption_object->encode($uid); ?>" value="<?php echo $marks ?>"> out of <?php echo $total_marks; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
$this->load->view("front-end/page_modal/course_settings_modal");
include('footer2.php');
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/courses.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/course_settings.js" type="text/javascript"></script>