<?php
include('intra_leftbar.php');
?>
<div class="col-md-9">
    <div class="boards_tab" style="margin-top: 20px;">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">
                    <div class="bck_page_profile">
                        <div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bck_setlist">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"> <a href="#a" aria-controls="a" role="tab" data-toggle="tab"> <span class="min_img1"></span> Profile</a> </li>
                                            <li role="presentation"><a href="#b" aria-controls="b" role="tab" data-toggle="tab"> <span class="min_img2"></span>  Password </a> </li>
                                            <li role="presentation"><a href="#c" aria-controls="c" role="tab" data-toggle="tab"> <span class="min_img3"></span> My Colleges</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <!-- Tab for Profile -->
                                        <div role="tabpanel" class="tab-pane active" id="a">
                                            <div class="profile_form">
                                                <h3>Profile</h3>
                                                <form id="ProfileSettings_Form" action="#" method="post" enctype="multipart/form-data">
                                                    <h6>Basics</h6>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-offset-4 col-md-4">
                                                                <div class="profile_class record">
                                                                    <img id="default_image" src="<?php echo base_url() . $user->profile_picture; ?>" class="img-thumbnail pro_image" name="pro_image"/>
                                                                    <span id="profile_pic_edit" class="btn-xs profile_pic_edit"> Change Image </span>
                                                                </div>
                                                                <input type='file' id="profile_upload" name="profile_pic" style="display: none;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>First Name:</label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" name="fname" class="pro_text fname" value="<?php echo $user->first_name; ?>">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Last Name:</label>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" name="lname" class="pro_text lname" value="<?php echo $user->last_name; ?>">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Email:</label>  
                                                            </div>
                                                            <div class="col-md-9">                                                              
                                                                <?php echo $user->email; ?> <a href="#" class="msg_link email" data-toggle="modal" data-target="#ChangeEmail_Modal"> Change</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <p>Info</p>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Location:</label>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" class="pro_txter location" name="location" value="<?php echo $user->location; ?>">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Phone:</label>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" class="pro_txter phone" name="phone" value="<?php echo $phone = $user->phone == 0 ? "" : $user->phone; ?>">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Birthday:</label>  
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input style="width: 55%;" type="text" title="Birthday" name="dob" value="<?php echo date('d-m-Y', strtotime($user->dob)); ?>" class="date_text dob" readonly="">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-7">
                                                                <a href="<?php echo site_url("user/settings"); ?>" class="btn pull-right"> Cancel</a>
                                                                <button type="submit" class="btn pull-right"> Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>


                                        <!-- Tab for Change Password -->
                                        <div role="tabpanel" class="tab-pane" id="b">
                                            <div class="my_coll profile_form">
                                                <h4>Change password</h4>
                                                <form id="ChangePassword_Form" action="#" method="POST">
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Old password</label>  
                                                            </div>
                                                            <div class="col-md-5">
                                                                <input type="password" name="old_password" class="pro_text old_password">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>New password</label>  
                                                            </div>
                                                            <div class="col-md-5">
                                                                <input type="password" name="new_password" class="pro_text new_password">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Confirm New password</label>  
                                                            </div>
                                                            <div class="col-md-5">
                                                                <input type="password" name="confirm_password" class="pro_text confirm_password">
                                                                <span class="profile_error_msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basic_form">
                                                        <div class="row">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-5">
                                                                <div class="error_msg"></div>
                                                                <button type="submit" class="btn">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>  
                                        </div>

                                        <!-- Tab for My colleges -->
                                        <div role="tabpanel" class="tab-pane" id="c">
                                            <?php
                                            foreach ($colleges_list as $value) {
                                                ?>
                                                <div class="my_coll">
                                                    <div class="my_coll_box">
                                                        <h5><?php echo $value->college_name; ?></h5>
                                                        <?php if (isset($value->intranet_user_type)) { ?>
                                                            <div style="padding:13px;">
                                                                <p>
                                                                    <b>
                                                                        <?php
                                                                        if ($value->intranet_user_type != "teacher")
                                                                            echo $value->stream_name;
                                                                        echo " (" . ucfirst($value->intranet_user_type) . ")";
                                                                        ?>
                                                                    </b>
                                                                </p>
                                                                <p><?php
                                                                    if ($value->intranet_user_type != "teacher")
                                                                        echo $value->stream_course_name;
                                                                    ?>
                                                                </p>
                                                                <p><?php
                                                                    if ($value->intranet_user_type == "student")
                                                                        echo $temp_array[$value->semester_or_year] . " " . $value->study_type;
                                                                    else if ($value->intranet_user_type == "alumni")
                                                                        echo "Pass out year: " . $value->year_of_passing;
                                                                    ?></p>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div style="padding:13px;">
                                                                <p><b>College-Admin</b></p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="d">
                                            sertertaeaewre    
                                        </div>
                                    </div>
                                </div>
                                <!-- end of form  -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of tab1 -->
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php include('footer2.php') ?>
<?php $this->load->view('front-end/page_modal/user_settings_modal'); ?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/user_settings.js" type="text/javascript"></script>

