<?php
include('intra_leftbar.php');
$get_college_admin_list_array = get_college_admin_list($this->session->userdata('college_id'));
$college_admin_users = array();
foreach ($get_college_admin_list_array as $college_user_value) {
    $college_admin_users[] = $college_user_value->user_id;
}
?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .edit_post{
        margin-right: 5px;
    }
    .discription_label{
        margin-bottom: 5px;
    }
    #internshipDescriptionEditModalTextarea_ifr{
        height: 100%;
    }
</style>
<input type="hidden" id="fixed_group_name" value="internship"/>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <div class="group_page_box">
                <div class="col-md-7">
                    <div class="profile_details">
                        <div style="font-family: 'open_sansregular';color: #525252;">
                            <h3>Internship</h3>
                        </div>
                    </div>
                </div>
                <!-- tab for group page begins -->
                <div class="converse_tab">
                    <div class="detail_converse">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#internship_tab" class="internship_tab" content_type="internship_post" aria-controls="dote_con_tab" role="tab" data-toggle="tab"> Internship Post</a>
                            </li>
                            <li role="presentation">
                                <a href="#favorite_internship_tab" class="internship_tab" content_type="favorite_post" aria-controls="dote_con_tab" role="tab" data-toggle="tab">Shortlisted Post</a>
                            </li>
                            <li role="presentation">
                                <a href="#archive_internship_tab" class="internship_tab" content_type="archive_post" aria-controls="dote_con_tab" role="tab" data-toggle="tab">Archive Post</a>
                            </li>
                            <!--<div class="middle_search group_search">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Search this group" name="srch-term" id="srch-term">
                                    </div>
                                </form>
                            </div>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="boards_tab"> 
                <div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-check"></i> Success ! 
                                <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="internship_tab">
                            <div class="update_tab">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">  <span class="point_top"></span> 
                                            <a href="#update" aria-controls="update" role="tab" data-toggle="tab"> 
                                                <span class="update_icon1"></span>Update </a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="margin-top: 10px;">
                                        <div role="tabpanel" class="tab-pane active" id="update">
                                            <div class="share_box">
                                                <a href="javascript:void(0);" id="ontick_share">Add new Internship</a>
                                                <!--<span class="pin"></span>-->
                                            </div>
                                            <div class="show_share">
                                                <form id="group_post_update_form">
                                                    <div class="poll_share">
                                                        <div class="text_poll"> 
                                                            <textarea ata-autoresize class="share_text" id="update_textarea" name="internship_title" placeholder="Add new Intership Title ? "></textarea>
                                                            <!--<span class="pin2"></span>-->
                                                            <div class="attach_list">
                                                                <ul>
                                                                    <li> <a class="upload_file_from_computer"> <span class="upload_icon"></span> <h6>Upload a file from computer</h6> </a> </li>
                        <!--                                            <li> <a href="#"> <span class="file_icon"></span> <h6>Select a file on </h6> </a> </li>
                                                                    <li> <a href="#"> <span class="note_icon"></span> <h6>Select a note on</h6> </a> </li>-->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="add_files" style="display: none">
                                                            <ul id="file_upload_content_ul"></ul>
                                                        </div>
                                                        <div>
                                                            <textarea class="share_text" id="mytextarea"></textarea>
                                                            <input type="hidden" id="internship_description" name="internship_description"/>
                                                        </div>
                                                        <div class="add_company" style="font-family: 'PT Sans', sans-serif !important;font-size: 14px !important;min-height: 30px !important;">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <input type="text" title="Start date" name="internship_start_date" placeholder="Enter start date" style="width: 100%" id="start_datepicker" class="date_text front-end-font">
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input type="text" title="End date" name="internship_end_date" placeholder="Enter end date" style="width: 100%" id="end_datepicker" class="date_text front-end-font">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--<textarea class="share_text" id="mytextarea" placeholder="Whats your Annoucement ?"></textarea>-->
                                                        <!--<div class="add_company">
                                                             add a group or people notify 2 
                                                            <div id="updateAddPeopleToNotify">
                                                                <div id="notified_user_div"></div>
                                                                <div class="updateAddPeopleInputDiv" id="updateAddPeopleInputDiv">
                                                                    <div class="notify_pop">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="people_list" style="overflow-y: visible;">
                                                                                    <ul id="users_list_ul"></ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" placeholder="Add people to notify" class="poll_text" id="updateAddPeopleInput">
                                                                </div>
                                                            </div>
                                                             popup for add people to notify 
                                                        </div>-->
                                                        <button type="submit" id="update_post" class="btn">Post</button>
                                                    </div>
                                                </form>
                                                <form id="image_upload_form" method="post" enctype="multipart/form-data">
                                                    <input type="file" name="upload_post_file" id="upload_post_file" style="display:none">
                                                </form>
                                            </div>
                                            <!-- end of share update -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="job_opening_list">
                                <ul id="internship_list_ul">
                                    <?php
                                    $internship_count = count($internship_list);
                                    if ($internship_count) {
                                        foreach ($internship_list as $internship_value) {
                                            $posted_user = $this->ion_auth->user($internship_value->user_id)->row();
                                            $logged_in_user = $this->ion_auth->user()->row();
                                            if ($internship_value->user_id == $logged_in_user->id || $internship_value->is_approve || in_array($logged_in_user->id, $college_admin_users) || in_array($internship_value->user_id, $college_admin_users)) {
                                                ?>
                                                <li class="internship_list_li" internship_id = "<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>">
                                                    <h6 style="margin-bottom: 4px;">
                                                        <span id="internship_title-<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="internship_title_span"><?php echo $internship_value->post_title; ?></span>
                                                        <?php
                                                        if ($posted_user->id == $logged_in_user->id || in_array($logged_in_user->id, $college_admin_users)) {
                                                            ?>
                                                            <button title="Delete internship" post_id="<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="btn btn-default btn-xs pull-right delete_post"><i class="fa fa-trash"></i></button>
                                                            <!--<button title="Edit internship"  post_id="<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="btn btn-default btn-xs pull-right edit_post"><i class="fa fa-edit"></i></button>-->
                                                            <?php
                                                        }
                                                        ?>
                                                        <button title="<?php echo ($user_favorite_internship[$internship_value->post_id]) ? "Deshortlist" : "Shortlist"; ?>" favorite_status="<?php echo $user_favorite_internship[$internship_value->post_id]; ?>" post_id="<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="btn btn-default btn-xs pull-right <?php echo ($user_favorite_internship[$internship_value->post_id]) ? "make_favorite_btn" : "favorite_btn"; ?> favorite_internship_btn"><i class="fa fa-star"></i></button>
                                                        <p style="color: #8A8A8A;font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">posted by <a href="#"><?php echo $posted_user->first_name . " " . $posted_user->last_name; ?></a> 
                                                            <?php
                                                            $posted_date = date('M j Y', strtotime($internship_value->timestamp));
                                                            echo (($posted_date == date('M j Y')) ? ", Today" : $posted_date) . " at " . date('h:i A', strtotime($internship_value->timestamp));
                                                            ?>
                                                        </p>
                                                    </h6>
                                                    <!--<p id="internship_description-<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>"><?php echo $internship_value->post_message; ?></p>-->
                                                    <div class="internship_action_btn_div" style="margin-top: 5px">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <!--<button class="edit_job_opening btn btn-xs"><i class="fa fa-thumbs-up"></i></button>-->
                                                                <a href="<?php echo site_url("user/internship/internship_details") . "?internshipId=" . $encryption_decryption_object->encode($internship_value->post_id); ?>"><button class="edit_job_opening btn btn-xs"><i class="fa fa-external-link-square"></i> View Details</button></a>
                                                                <?php
                                                                if ($internship_value->is_approve == 0 && in_array($user->id, $college_admin_users) && !in_array($internship_value->user_id, $college_admin_users)) {
                                                                    ?>
                                                                    <button internship_id = "<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="approve_internship_btn btn btn-xs"><i class="fa fa-check"></i> Approve </button>
                                                                    <button internship_id = "<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" class="decline_internship_btn btn btn-xs"><i class="fa fa-ban"></i> Decline </button>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($internship_value->is_approve == 0 && !in_array($internship_value->user_id, $college_admin_users) && !in_array($user->id, $college_admin_users)) {
                                                                    ?>
                                                                    <span style="color: red; font-size: 13px;"> Waiting to approve</span>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <span class="pull-right" style="font-family: 'open_sansregular';font-size: 12px;color: #626262;line-height: 15px;">
                                                                    FROM : <a id="internship_start_date-<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" href="javascript:void(0)"><?php echo date("d-M-Y", strtotime($internship_value->start_date)); ?></a> to <a id="internship_end_date-<?php echo $encryption_decryption_object->encode($internship_value->post_id); ?>" href="#"><?php echo date("d-M-Y", strtotime($internship_value->end_date)); ?></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <p style="font-family: open_sanslight;font-size: 13px;color: #525252;margin: 0px;">No post found.</p>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php // include('fixed_group_body.php')  ?>
                        <!-- end of tab1 -->
                        <div role="tabpanel" class="tab-pane" id="favorite_internship_tab">
                            <div class="job_opening_list">
                                <div id="favorite_internship_loading_img" style="display: block" class="img_loading_div">
                                    <img class="scroller_loading_img" src="<?php echo base_url(); ?>assets_front/image/scroll_loader.GIF" alt=""/>
                                </div>
                                <ul id="favorite_internship_list_ul"></ul>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="archive_internship_tab">
                            <div class="job_opening_list">
                                <div id="archive_internship_loading_img" class="img_loading_div">
                                    <img class="scroller_loading_img" src="<?php echo base_url(); ?>assets_front/image/scroll_loader.GIF" alt=""/>
                                </div>
                                <ul id="archive_internship_list_ul"></ul>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="blog"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="right_bar">
                <div class="recent_activity">
                    <h5>Notifications</h5>
                    <?php
                    $count = 0;
                    foreach ($all_unread_alerts as $value) {
                        $posted_id = $uid = $value->posted_user_id;
                        $timestamp = $value->timestamp;
                        if ($value->group_type == "fixed") {
                            $method_name = "view_post";
                        } else if ($value->group_type == "unfixed") {
                            $method_name = "view_group_post";
                        }
                        ?>
                        <div class="activities">
                            <div class="act_img">
                                <img
                                    src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                                    alt=""/>
                            </div>
                            <div class="alert_details">
                                <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                                    <span>
                                        <?php
                                        switch ($value->type) {
                                            case 'notify':
                                                echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                                break;
                                            case 'reply':
                                                if ($value->group_type == "fixed") {
                                                    $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                                    $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                } else {
                                                    $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                                    $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                                }
                                                if (count($temp_details_array) > 1) {
                                                    $i = 0;
                                                    $put_comma = 0;
                                                    foreach ($temp_details_array as $value1) {
                                                        $uid = $value1->user_id;
                                                        $i++;
                                                        if ($uid != $posted_id) {
                                                            $put_comma = 1;
                                                            echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                        }
                                                        if ($i != count($temp_details_array) && $put_comma == "1") {
                                                            echo ", ";
                                                        }
                                                    }
                                                    echo " commented on your post.";
                                                } else {
                                                    echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                                }
                                                break;
                                            case 'like':
                                                if ($value->group_type == "fixed") {
                                                    $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                    $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                } else {
                                                    $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                                    $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                                }
                                                if (count($temp_details_array) > 1) {
                                                    $i = 0;
                                                    $put_comma = 0;
                                                    foreach ($temp_details_array as $value1) {
                                                        $uid = $value1->user_id;
                                                        $i++;
                                                        if ($uid != $posted_id) {
                                                            $put_comma = 1;
                                                            echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                        }
                                                        if ($i != count($temp_details_array) && $put_comma == "1") {
                                                            echo ", ";
                                                        }
                                                    }
                                                    echo " liked your post.";
                                                } else {
                                                    echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                                }
                                                break;
                                        }
                                        ?>
                                    </span><br>
                                        </a>
                                <span class="alert_timestamp">
                                    <?php
                                    $posted_date = date('M j Y', strtotime($timestamp));
                                    echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                                    ?>
                                </span>
                            </div>
                        </div>
                        <?php
                        $count++;
                        if ($count >= 5)
                            break;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/internship_page_modal");
?>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>  
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/internship.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
//    tinymce.init({
//        selector: "#mytextarea"
//    });
    tinymce.init({
        selector: '#mytextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                internship_description = tinyMCE.get('mytextarea').getContent();
                $("#internship_description").val(internship_description);
            });
        }
    });

    tinymce.init({
        selector: '#internshipDescriptionEditModalTextarea',
        menubar: false,
        statusbar: false,
        plugins: [
            "autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste jbimages autoresize"
        ],
        autoresize_bottom_margin: 25,
        relative_urls: false,
        content_css: base_url + "assets_front/css/dev_style.css",
        toolbar: "bold italic bullist link unlink",
        setup: function (ed) {
            ed.on('keyup', function (e) {
                internship_description = tinyMCE.get('internshipDescriptionEditModalTextarea').getContent();
                $("#internshipDescriptionEditModalText").val(internship_description);
            });
        }
    });

    //internshipTitleEditModalTextarea
    $("#start_datepicker").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#end_datepicker").datepicker("option", "minDate", selected)
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
    $("#end_datepicker").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#start_datepicker").datepicker("option", "maxDate", selected);
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });

    //internshipTitleEditModalTextarea
    $("#internshipStartDateEditModalText").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#internshipEndDateEditModalText").datepicker("option", "minDate", selected)
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
    $("#internshipEndDateEditModalText").datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selected) {
            $("#internshipStartDateEditModalText").datepicker("option", "maxDate", selected);
        }
    }).attr('readonly', 'true').keypress(function (event) {
        event.preventDefault();
    });
</script>
