<?php $this->load->view('front-end/header'); ?>
<div class="body_height">
    <div class="college_details">
        <div class="container">
            <div class="new_registration">
                <h3>Reset Password</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact_info_reg">
                            <form id="reset_password_form" action="<?php echo site_url("forgot"); ?>" method="post">
                                <div class="success_alert alert alert-success alert-dismissable" style="display: none; padding: 8px;margin-bottom: 10px;">
                                    <h4 style="padding: 0px; margin: 0px;">
                                        <i class="icon fa fa-check"></i> Your password is updated Successfully. You can now login with your new password.
                                        <span id="success_alert_message" style="font-size: 15px;"></span>
                                    </h4>
                                </div>
                                <div class="danger_alert alert alert-danger alert-dismissable" style="display: none;padding: 8px;margin-bottom: 10px;">
                                    <h4 style="padding: 0px; margin: 0px;">
                                        <i class="icon fa fa-warning"></i> Error updating your password..! Please try again. 
                                        <span id="danger_alert_message" style="font-size: 15px;"></span>
                                    </h4>
                                </div>
                                <div class="col-md-8">
                                    <div class="course_message">
                                        <div class="col-md-3"><label>Password</label></div>
                                        <div class="col-md-5">
                                            <input type="password" name="password" class="reg_text password" id="password" placeholder="Enter password">
                                            <input type="hidden" id="uid" value="<?php echo $this->encryption_decryption_object->encode($user_id); ?>">
                                            <input type="hidden" id="cid" value="<?php echo $this->uri->segment(3); ?>">
                                        </div>
                                        <div class="col-md-4 error_msg"><?php echo form_error('email'); ?></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><label>Confirm Password</label></div>
                                        <div class="col-md-5">
                                            <input type="password" name="confirm_password" class="reg_text confirm_password" id="confirm_password" placeholder="Confirm password">
                                        </div>
                                        <div class="col-md-4 error_msg"><?php echo form_error('email'); ?></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-5">
                                            <button type="submit" style="border-radius: 0px;width: 100%;" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php $this->load->view('front-end/footer'); ?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/reset_forgot_password.js" type="text/javascript"></script>
