<?php include('intra_leftbar.php') ?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet"
      type="text/css"/>
<input type="hidden" id="fixed_group_name" value="hostel"/>
<input type="hidden" id="hotel_description_hidden"
       value="<?php echo (count($hostel_description)) ? $hostel_description[0]->hostel_description : ""; ?>"/>
<div class="col-md-6">
    <div class="boards_tab">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success !
                        <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                </div>
                <?php include('fixed_group_head.php') ?>
                <!--                <div class="hostel_details">
                    <div class="hostel_heading_div">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="hostel_heading_h3">My Hostel</h3>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default btn-xs pull-right hostel_details_edit_btn"><i class="fa fa-edit"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="hostel_info"> 
                        <p id="hostel_description_details">
                            <?php
                //                            if (count($hostel_description)) {
                //                                echo $hostel_description[0]->hostel_description;
                //                            } else {
                //                                echo "Description not addedd.";
                //                            }
                ?>
                        </p>
                    </div>
                </div>-->
                <?php include('fixed_group_body.php') ?>
                <!-- end of tab1 -->

                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>

                    <p>The institute has following major facilities:</p>
                </div>

                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>

                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical
                        University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>

                <div role="tabpanel" class="tab-pane" id="blog"></div>
            </div>
        </div>
    </div>


</div>

<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Notifications</h5>
            <?php
            $count = 0;
            foreach ($all_unread_alerts as $value) {
                $posted_id = $uid = $value->posted_user_id;
                $timestamp = $value->timestamp;
                if ($value->group_type == "fixed") {
                    $method_name = "view_post";
                } else if ($value->group_type == "unfixed") {
                    $method_name = "view_group_post";
                }
                ?>
                <div class="activities">
                    <div class="act_img">
                        <img
                            src="<?php echo base_url() . $this->ion_auth->user($posted_id)->row()->profile_picture; ?>"
                            alt=""/>
                    </div>
                    <div class="alert_details">
                        <a href="<?php echo base_url() . "user/posts/" . $method_name . "/" . $this->encryption_decryption_object->encode($value->post_id); ?>">
                            <span>
                                <?php
                                switch ($value->type) {
                                    case 'notify':
                                        echo $this->ion_auth->user($posted_id)->row()->first_name . " " . $this->ion_auth->user($posted_id)->row()->last_name . " has tagged you in a post.";
                                        break;
                                    case 'reply':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_reply_list'][$value->post_id];
                                            $uid = $reply_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " commented on your post.";
                                        } else {
                                            echo $this->ion_auth->user($reply_user_id)->row()->first_name . " " . $this->ion_auth->user($reply_user_id)->row()->last_name . " commented on your post.";
                                        }
                                        break;
                                    case 'like':
                                        if ($value->group_type == "fixed") {
                                            $temp_details_array = $FixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        } else {
                                            $temp_details_array = $UnfixedAlertDetails['posts_like_all_user'][$value->post_id];
                                            $uid = $liked_user_id = $temp_details_array[0]->user_id;
                                        }
                                        if (count($temp_details_array) > 1) {
                                            $i = 0;
                                            $put_comma = 0;
                                            foreach ($temp_details_array as $value1) {
                                                $uid = $value1->user_id;
                                                $i++;
                                                if ($uid != $posted_id) {
                                                    $put_comma = 1;
                                                    echo $this->ion_auth->user($uid)->row()->first_name . " " . $this->ion_auth->user($uid)->row()->last_name . "";
                                                }
                                                if ($i != count($temp_details_array) && $put_comma == "1") {
                                                    echo ", ";
                                                }
                                            }
                                            echo " liked your post.";
                                        } else {
                                            echo $this->ion_auth->user($liked_user_id)->row()->first_name . " " . $this->ion_auth->user($liked_user_id)->row()->last_name . " liked your post.";
                                        }
                                        break;
                                }
                                ?>
                            </span><br>
                        </a>
                        <span class="alert_timestamp">
                            <?php
                            $posted_date = date('M j Y', strtotime($timestamp));
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($timestamp));
                            ?>
                        </span>
                    </div>
                </div>
                <?php
                $count++;
                if ($count >= 5)
                    break;
            }
            ?>
        </div>
    </div>
</div>


</div>
</div>
</div>
<?php include('footer2.php') ?>
<?php include('page_modal/hostel_page_modal.php') ?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js"
        type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/fixed_group.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/hostel_page.js"
        type="text/javascript"></script>