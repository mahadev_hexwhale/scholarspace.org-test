<?php
include('intra_leftbar.php');
?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

<input type="hidden" id="grpID" value="<?php echo $this->encryption_decryption_object->encode($group->id); ?>">
<input type="hidden" id="members_json" value='<?php print_r(json_encode($member)); ?>'/>

<div class="col-md-9">
    <div class="group_page_box">
        <div class="col-md-7">
            <div class="profile_details">
                <div class="profile_pic">
                    <img src="<?php echo base_url() . "" . $group->group_icon_image; ?>" alt="">
                    <?php
                    if ($user->id != $group->group_admin_user_id) {
                        // if user is not admin then check if user is already a member of this group
                        // if no then show him join icon so that he can send a request to join group
                        $LoggedUserIsMember = 0;
                        foreach ($member as $key => $value) {
                            if ($value->user_id == $user->id)
                                $LoggedUserIsMember = 1;
                        }
                        if ($LoggedUserIsMember == 0) {
                            ?>
                            <div class="join_plus"> + Join</div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="group_tag">
                    <h4>
                        <?php echo $group->name; ?>
                    </h4>
                    <h6>
                        <?php echo $group->group_access; ?> Group 
                        <?php
                        $LoggedUserIsMember = 0;
                        foreach ($member as $key => $value) {
                            if ($value->user_id == $user->id)
                                $LoggedUserIsMember = 1;
                        }
                        if ($LoggedUserIsMember == 1) {
                            ?>
                            <span style="color: rgba(0, 0, 0, 0.2);">|</span>
                            <span class="join_leave">
                                <span class="joined_group"><i class="fa fa-check"></i> Joined </span>
                                <span class="leave_group"><i class="fa fa-minus"></i> Leave </span>
                            </span>
                            <?php
                        }
                        ?>
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="admin_details">
                <h6>Members <?php echo count($member); ?></h6>
                <ul>
                    <?php
                    // get randomly, the group members to show at the top of the page
                    if ($user->id != $group->group_admin_user_id) {

                        $count = count($member) > 16 ? 16 : count($member);
                        $rand_keys = array_rand($member, $count);
                        if (is_array($rand_keys)) {
                            //if multiple members are available
                            foreach ($rand_keys as $key => $value) {
                                ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . "" . $member[$value]->profile_picture; ?>" alt="">
                                        <div class="admin_plus"><?php echo $member[$value]->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            //if single members are available
                            ?>
                            <li>
                                <div class="admin_image">
                                    <img src="<?php echo base_url() . "" . $member[$rand_keys]->profile_picture; ?>" alt="">
                                    <div class="admin_plus"><?php echo $member[$rand_keys]->first_name; ?></div>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        // show add more button for admin
                        $count = count($member) > 15 ? 15 : count($member);
                        $rand_keys = array_rand($member, $count);
                        if (is_array($rand_keys)) {
                            //if multiple members are available
                            foreach ($rand_keys as $key => $value) {
                                ?>
                                <li>
                                    <div class="admin_image">
                                        <img src="<?php echo base_url() . "" . $member[$value]->profile_picture; ?>" alt="">
                                        <div class="admin_plus"><?php echo $member[$value]->first_name; ?></div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            //if single members are available
                            ?>
                            <li>
                                <div class="admin_image">
                                    <img src="<?php echo base_url() . "" . $member[$rand_keys]->profile_picture; ?>" alt="">
                                    <div class="admin_plus"><?php echo $member[$rand_keys]->first_name; ?></div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        <li>
                            <div class="admin_image">
                                <button class="add_member_button" id="AddMembersToGroup_Button" grpID = '<?php echo $group->id; ?>'><i class="fa fa-user-plus"></i></button>
                            </div>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>

        <!-- tab for group page begins -->
        <div class="converse_tab">
            <div class="detail_converse">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#dote_con_tab" aria-controls="dote_con_tab" role="tab" data-toggle="tab">CONVERSATIONS</a>
                    </li>
                    <li role="presentation">
                        <a href="#dote_info_tab" aria-controls="dote_info_tab" role="tab" data-toggle="tab"> INFO </a>
                    </li>
                    <li role="presentation">
                        <a href="#dote_file_tab" aria-controls="dote_file_tab" role="tab" data-toggle="tab">FILES</a> 
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="converse_content">
        <div class="converse_tab">
            <div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="dote_con_tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="announce">
                                    <h5>
                                        <i class="fa fa-pencil"></i> Edit Group 
                                        <?php if ($user->id = $group->group_admin_user_id) { ?>
                                            <button class="btn btn-default btn-xs pull-right DeleteGroup"><i class="fa fa-trash"></i> Delete group</button>
                                        <?php } ?>
                                    </h5>
                                    <div class="total_anounce">
                                        <div class="col-md-12">
                                            <form id="EditGroup_Form" method="POST" enctype="multipart/form-data" action="<?php echo base_url() . "user/groups/UpdateGroupSettings_Method"; ?>">
                                                <div class="col-md-12">
                                                    <div class="course_message">
                                                        <div class="col-md-2"><label for="">Group Name </label></div>
                                                        <div class="col-md-5">
                                                            <input type="hidden" id="hidden_name" value="<?php echo $group->name; ?>">
                                                            <input type="text" id="group_name" name="group_name" value="<?php echo $group->name; ?>" class="reg_text" placeholder="Enter Group Name">
                                                        </div>
                                                        <div class="col-md-5 error_msg"></div>
                                                    </div>
                                                    <div class="course_message">
                                                        <div class="col-md-2"><label for="">Group Image </label></div>
                                                        <div class="col-md-2">
                                                            <div class="grp_img_loading">
                                                                <img src="<?php echo base_url() . "assets_front/image/728.gif"; ?>">
                                                            </div>
                                                            <img id="group_icon_image" class="group_icon_image" src="<?php echo base_url() . "" . $group->group_icon_image; ?>">
                                                            <input type="hidden" name="hidden_image_path" id="hidden_image_path" value="">
                                                        </div>
                                                        <div class="col-md-5"><input type="file" name="group_image" class="group_image" id="group_image"></div>
                                                    </div>
                                                    <div class="">
                                                        <div class="col-md-2"><label class="font_15_grp">Members</label></div>
                                                        <div class="col-md-2">
                                                            <span class="font_open_13"><?php echo count($member); ?> members</span>
                                                        </div>
                                                        <div class="col-md-5 announce_details">
                                                            <a href="" data-toggle="modal" data-target="#AddMembersToGroup_Modal">Add / Remove</a>
                                                        </div>
                                                    </div>
                                                    <div class="course_message">
                                                        <div class="col-md-5 radio">
                                                            <label><b>Who can view this content? </b></label>
                                                            <label><input type="radio" name="VisibleTo" class="VisibleTo" value="Public" <?php if ($group->visible_to == "Public") echo "checked"; ?>><span>Public</span> - Anyone in this network</label>
                                                            <label><input type="radio" name="VisibleTo" class="VisibleTo" value="Members" <?php if ($group->visible_to == "Members") echo "checked"; ?>><span>Private</span> - Only approved members </label>
                                                        </div>
                                                    </div>
                                                    <div class="course_message">
                                                        <div class="col-md-5 radio">
                                                            <label><b>Who can join this group? </b></label>
                                                            <label><input type="radio" name="GroupAccess" class="GroupAccess" value="Public" <?php if ($group->group_access == "Public") echo "checked"; ?>> Anyone in this network</label>
                                                            <label><input type="radio" name="GroupAccess" class="GroupAccess" value="Private" <?php if ($group->group_access == "Private") echo "checked"; ?>> Only those approved by an admin</label>
                                                        </div>
                                                    </div>

                                                    <div class="reg_agree">
                                                        <div class="col-md-12"> 
                                                            <div class="checkbox">
                                                                <button type="submit" class="btn btn-success">Save Changes</button>
                                                                <a href="<?php echo base_url() . "user/groups/settings?grp_id=" . $encryption_decryption_object->encode($group->id); ?>" class="btn btn-success">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dote_info_tab">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="EditGroupInfo_Form" method="POST" action="">
                                    <div class="announce">
                                        <h5>
                                            <i class="fa fa-info-circle"></i> <?php echo $group->name; ?> Info
                                            <?php if ($user->id == $group->group_admin_user_id) { ?>
                                                <div class="group_info_edit_button_div">
                                                    <button type="button" class="btn btn-default btn-xs pull-right group_info_edit_button"><i class="fa fa-pencil"></i> Edit</button>
                                                </div>
                                                <div class="group_info_save_button_div" style="display: none;">
                                                    <button type="submit" class="btn btn-default btn-xs pull-right save_info_button">Save</button>
                                                    <a href="javascript:void(0);" class="pull-right btn-xs cancel_edit">Cancel</a>
                                                </div>
                                            <?php } ?>
                                        </h5>
                                        <div class="total_anounce">
                                            <div class="col-md-12">
                                                <div class="course_message">
                                                    <div class="group_info_div">
                                                        <?php
                                                        if ($group->group_info == "") {
                                                            echo "<div class='temp_p'><p style='font-size: 11px;color: #757575;'>Click 'edit' (above and to the right) to give users more information about this Group. You can add things like a description of what this Group is about, what kinds of things people should post here, and resources for group members.</p></div>";
                                                        } else {
                                                            ?>
                                                            <div>
                                                                <?php
                                                                echo html_entity_decode($group->group_info);
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="group_info_textpad_div" style="display: none;">
                                                        <textarea id="group_info_textpad" class="group_info_textpad" placeholder="Place some text here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dote_file_tab"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include('footer2.php');
    include ('page_modal/groups_modal.php');
    ?>
    <script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/groups.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
    <!---- Fancybox JS ---->
    <script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <!---- Datatable JS ---->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>