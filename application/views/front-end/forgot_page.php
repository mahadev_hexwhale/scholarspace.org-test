<?php $this->load->view('front-end/header'); ?>
<div class="body_height">
    <div class="college_details padding_300">
        <div class="container">
            <div class="new_registration">
                <h3>Forgot Password</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact_info_reg">
                            <form id="forgot_password_form" action="<?php echo site_url("forgot"); ?>" method="post">
                                <div class="success_alert alert alert-success alert-dismissable" style="display: none; padding: 8px;margin-bottom: 10px;">
                                    <h4 style="padding: 0px; margin: 0px;"> Email is sent successfully with the activation link.
                                        <i class="icon fa fa-check"></i>  
                                        <span id="success_alert_message" style="font-size: 15px;"></span>
                                    </h4>
                                </div>
                                <div class="danger_alert alert alert-danger alert-dismissable" style="display: none;padding: 8px;margin-bottom: 10px;">
                                    <h4 style="padding: 0px; margin: 0px;">
                                        <i class="icon fa fa-warning"></i> Error..! Please try again. 
                                        <span id="danger_alert_message" style="font-size: 15px;"></span>
                                    </h4>
                                </div>
                                <div class="col-md-8">
                                    <div class="course_message">
                                        <div class="col-md-12">
                                            Please enter your email here. A link will be sent to this email with the verification link.
                                        </div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><label>Email</label></div>
                                        <div class="col-md-5">
                                            <input type="text" name="email" class="reg_text email" id="email" placeholder="Enter email">
                                        </div>
                                        <div class="col-md-4 error_msg"><?php echo form_error('email'); ?></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-5">
                                            <button type="submit" style="border-radius: 0px;width: 100%;" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php $this->load->view('front-end/footer'); ?>
<?php
$FormSubmit = $this->session->flashdata('FormSubmit');
if ($FormSubmit == 'success') {
    ?>
    <script type="text/javascript">
        $('.success_alert').fadeIn();
        setTimeout(function () {
            $('.success_alert').fadeOut();
        }, 4000);
    </script>
<?php } else if ($FormSubmit == 'failure') { ?>
    <script type="text/javascript">
        $('.danger_alert').fadeIn();
        setTimeout(function () {
            $('.danger_alert').fadeOut();
        }, 3000);
    </script>
<?php } ?>

<script type="text/javascript">
    $('input').focus(function () {
        $('.error_msg').html('');
    });
</script>