<?php include('intra_leftbar.php') ?>


<div class="col-md-9">
    <div class="boards_tab" style="margin-top: 20px;">
        <div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">

                    <div class="courses_page_tab" style="margin-top: 18px;">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" >
                                    <a href="<?php echo site_url() . "user/inbox/"; ?>">Inbox </a> 
                                </li>
                                <li role="presentation" class="active">
                                    <a href="<?php echo site_url()."user/inbox/inboxSentMessages"; ?>"><span style="padding-right: 19px;">.</span> Sent Message </a>   
                                </li>
                                <li role="presentation">
                                    <a href="<?php echo site_url() . "user/inbox/chatarchive"; ?>"><span style="padding-right: 19px;">.</span> Chat Archive</a> 
                                </li>
<!--                                
                                <div class="group_popup">
                                    <!-- Button trigger modal -->
                                    <!-- <button type="button" class="btn btn-primary btn-lg" >
                                      Launch demo modal
                                    </button> -->

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Create a Message</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="choose_group">
                                                        <div class="col-md-12">
                                                            <div class="choose_grp_list">
                                                                <div class="choose_grp_img">
                                                                    <img src="image/msg_grp1.png" alt="">
                                                                </div>
                                                                <p>Post in a Group</p>
                                                            </div>

                                                            <div class="choose_grp_list" style="border-left:none;">
                                                                <div class="choose_grp_img">
                                                                    <img src="image/msg_grp2.png" alt="">
                                                                </div>
                                                                <p>Send via private message</p>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="course_message">
                                                        <div class="col-md-12"><label> Add Participant </label></div>
                                                        <div class="col-md-12"><input type="text" class="pop_text" name="group_name"></div>
                                                    </div>


                                                    <div class="course_message">
                                                        <div class="col-md-12"><textarea type="text" class="pop_text grp_text" name="group_name"> </textarea></div>
                                                    </div>

                                                    <div class="course_message">
                                                        <div class="col-md-12"> 
                                                            <div>
                                                                <a href="#"> <span class="plus_file"></span> Add Files</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                                    <div class="course_message" style="margin: 8px 0px 15px;">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-primary">Post</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </ul>


                            <!-- Tab panes -->
                            <div class="tab-content" style="margin-top: 10px;">

                                <div role="tabpanel" class="tab-pane active" id="inbox">
<!--                                    <div class="msg_search_box">
                                        <form class="navbar-form" role="search">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search my messages" 
                                                       name="srch-term" id="srch_msg">
                                            </div>
                                            <button type="submit" class="btn">Search</button>
                                        </form>
                                    </div>-->

                                    <div class="unread">
                                        <?php $send_to = $send_to[0]; ?>
                                        <h6 class="user-name"  user_id="<?php echo $this->encryption_decryption_object->encode($send_to->id); ?>"  >
                                            <?php
                                            if (count($send_to) >= 1) {
                                                echo $send_to->first_name . " " . $send_to->last_name;
                                            }
                                            ?>
                                        </h6> 
                                     
                                      <!--hidden filed for receiver detail-->
                                      <input type="hidden" id="receiver_profile_picture" name="receiver_profile_picture" 
                                             value="<?php if(isset($send_to->profile_picture) && !empty($send_to->profile_picture))
                                                 { echo site_url().$send_to->profile_picture; } ?>" />
                                      <input type="hidden" id="receiver_user_id" name="receiver_user_id" 
                                             value="<?php echo $this->encryption_decryption_object->encode($send_to->id); ?>" />
                                      <input type="hidden" id="receiver_user_name" name="receiver_user_name"
                                             value="<?php if(isset($send_to->first_name) && !empty($send_to->last_name) )
                                                 { echo $send_to->first_name. " ".$send_to->last_name; } ?>" />
                                     
                                      <!--Hidden field for sender detail -->
                                      <input type="hidden" id="sender_profile_picture" name="sender_profile_picture"
                                            value="<?php if(isset($send_from['profile_picture']) && !empty($send_from['profile_picture']))
                                                { echo site_url().$send_from['profile_picture']; } ?>" />
                                      <input type="hidden" id="sender_name" name="sender_name" 
                                             value="<?php if((isset($send_from['first_name']) && !empty($send_from['first_name']))&&(isset($send_from['last_name']) && !empty($send_from['last_name'])) )
                                                 { echo $send_from['first_name']." ".$send_from['last_name']; } ?>" />
                                      <input type="hidden" id="sender_user_id" name="sender_user_id" 
                                             value="<?php if(isset($send_from['user_id']) && !empty($send_from['user_id']))
                                                 { echo $send_from['user_id']; } ?>" />
                                                             
                                     
                                    </div> 

                                    <div class="inbox_details" style="overflow-y:scroll; height:279px; scroll-behavior: auto; scroll-behavior: smooth; overflow:auto;">
                                        <!--<p>August 1, 2015</p> -->

                                        <?php
                                        if ((count($chat_content) >= 1) && !empty($chat_content)) {
                                            foreach ($chat_content as $chatContent) {
                                                //   echo "<pre />"; print_r($chatContent);
                                                ?>
                                                <div class="first_msg">      
                                                    <?php
                                                    //code to check wether message is  message from send or receiver 
                                                    if ($chatContent->user_id == $send_to->id) {
                                                        ?>
                                                        <div class="intra_user_img" >
                                                            <img src="<?php echo base_url() . $send_to->profile_picture; ?>" alt="">

                                                        </div>
                                                        <div style="margin: 0px 14px; float:left;" id="name" ><a href="#"><?php echo $send_to->first_name . " " . $send_to->last_name; ?> </a>

                                                            <p style="margin-top: 13px;margin-left: 25px;"><?php echo $chatContent->message; ?></p>
                                                        </div>  
                                                        <h6><?php echo $chatContent->timestamp; ?></h6>
                                                    <?php } else {
                                                        ?>

                                                        <div class="intra_user_img">
                                                            <img src="<?php echo base_url() . $send_from['profile_picture']; ?>" alt="">
                                                        </div>
                                                        <div style="margin: 0px 14px; float:left;"><a href="#"><?php echo $send_from['first_name'] . " " . $send_from['last_name']; ?> </a>
                                                            <p style="margin-top: 13px;margin-left: 25px;">  <?php echo $chatContent->message; ?></p>

                                                        </div> 
                                                        <h6><?php echo $chatContent->timestamp; ?></h6>

                                                    <?php }
                                                    ?>                                        
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            echo "<p>" . "There is not message exist" . "</p>";
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                            <!-- end of first tab -->


                        </div>

                        <div id="chatbox">
                            <textarea class="form-control form-white send-message" style="" placeholder="Write a reply" 
                                      aria-label="Write a reply..." class="uiTextareaNoResize uiTextareaAutogrow _1rv" 
                                      aria-controls="webMessengerRecentMessages"
                                      aria-describedby="webMessengerHeaderName" name="message_body" 
                                      role="textbox" rows="3" onkeydown="">
                            </textarea>

                        </div>    



                    </div>
                </div>
                <!-- end of course teacher tab -->


            </div>
            <!-- end of tab1 -->
          <div role="tabpanel" class="tab-pane" id="blog"></div>
        </div>
    </div>
</div>


</div>

<!-- <div class="col-md-3">
</div> -->


</div>
</div>
</div>

<!--<script type="text/javaScript" >
//var timestampChat=
var chatwith=('#receiver_user_id').val();
alert(chatwith);
ajaxCallCheckNewMessages(timestampChat, chatwith)

</script>-->

<?php include('footer2.php') ?>

<script type="text/javascript">
$(document).ready(function(){
 inboxChat()
});
</script>