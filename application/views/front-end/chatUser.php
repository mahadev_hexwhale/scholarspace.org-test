<?php
$AluminiListFull = $this->AluminiListFull;
$StudentsListFull = $this->StudentsListFull;
$TeacherListFull = $this->TeacherListFull;
$collegeChatGroupName = $this->collegeChatGroupName;

?>

<div id="quickview-sidebar" class="">
    <div class="quickview-header">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#chat" data-toggle="tab">Chat</a></li>
        </ul>
    </div>
    <div class="quickview">
        <div class="tab-content">
            <div class="tab-pane fade active in" id="chat">
                <div class="chat-body current">
                    <div class="chat-search">

                        <div class="append-icon">
                            <input type="text" class="form-control getList" placeholder="Search college users...">
                            <i class="icon-magnifier"></i>
                            <div class="chat-list hide dropdown-chat">
                                <ul class="ListOfMembersChat">

                                </ul>
                            </div>
                        </div>

                    </div>


                    <div class="chat-groups">
                        <div class="title">GROUP CHATS</div> 
                        <button type="button" name="AddMoreAssistants" class="btn btn-xs AddMoreAssistants" data-toggle="modal" data-target="#collegeChatgroupModel"><i class="fa fa-plus"><a href="#"><b></b></a></i></button>
                        <div id="GroupList" class="collapse in">
                        <ul>
                            <?php
                            if(!empty($collegeChatGroupName)){ 
                            if (count($collegeChatGroupName) > 0) { $count=0;
                                foreach ($collegeChatGroupName as $GroupName) {
                                    $count++;
                                    ?>
                                    <input type="hidden" name="list_of_chat_member" value="<?php ?>" />
                                    <li class="group_chat_id" id="<?php echo $this->encryption_decryption_object->encode($GroupName->group_chat_id); ?>"><?php echo $GroupName->group_name; ?></li>
                                    <?php
                                 if($count>3){break; }
                              }
                             if($count>3)
                             {?>                               
                              <a href="#" id="ShowAll"><h5>Show All</h5></a>    
                            <?php } 
                            }
                            
                            }
                            ?> 
                        </ul>
                        </div>      
                    </div>

                    
                    
                    <!--group details popup -->
                    <div class="groupDetails"  >
                        <div class="row"> 
                            <div class="col-md-12">
                               <h4 style="Font-size:18;color:#006622" ></h4>
                                <div class="pop_user_info">
                                    <ul style=" text-transform: capitalize;font-size:12px;">
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <!--Group details popup end-->

                   <div class="title">RECENT CHATS</div>
                    <div class="chat-list get-height-chat">
                        <ul>
                            <?php
                            if (count($StudentsListFull) >= 1) {
                                foreach ($StudentsListFull as $listFull) {
                                    ?>
                                    <li class="clearfix" user_id="<?php echo $this->encryption_decryption_object->encode($listFull->user_id) ?>">
                                        <div class="user-img">
                                            <img
                                                src="<?php echo base_url() . $listFull->profile_picture; ?>"
                                                alt="avatar"/>

                                            <?php if (getUnreadMsgCount($this->encryption_decryption_object->encode($listFull->user_id))) { ?>
                                                <div class="pull-right badge badge-danger">
                                                    <?php
                                                    echo getUnreadMsgCount($this->encryption_decryption_object->encode($listFull->user_id));
                                                    ?>
                                                </div>
                                            <?php } ?>

                                        </div>
                                        <div class="user-details">
                                            <div
                                                class="user-name"><?php echo $listFull->fname . ' ' . $listFull->lname; ?></div>
                                            <div class="user-txt"><?php echo $listFull->stream_course_name; ?></div>
                                        </div>
                                        <div class="user-status">
                                            <i class="online"></i>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            if (count($AluminiListFull) >= 1) {
                                foreach ($AluminiListFull as $listFull) {
                                    ?>
                                    <li class="clearfix">
                                        <div class="user-img">
                                            <img
                                                src="<?php echo base_url() . $listFull->profile_picture; ?>"
                                                alt="avatar"/>
                                                <?php if (getUnreadMsgCount($this->encryption_decryption_object->encode($listFull->user_id))) { ?>
                                                <div class="pull-right badge badge-danger">
                                                    <?php
                                                    echo getUnreadMsgCount($this->encryption_decryption_object->encode($listFull->user_id));
                                                    ?>
                                                </div>
                                            <?php } ?>

                                        </div>
                                        <div class="user-details">
                                            <div
                                                class="user-name"><?php echo $listFull->fname . ' ' . $listFull->lname; ?></div>
                                            <div class="user-txt"><?php echo $listFull->stream_course_name; ?></div>
                                        </div>
                                        <div class="user-status">
                                            <i class="online"></i>
                                        </div>
                                    </li>

                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="chat-conversation">
                    <div class="conversation-header">
                        <div class="user clearfix">
                            <div class="chat-back">
                                <i class="fa fa-fw fa-mail-reply"></i>
                                <!-- <i class="icon-action-undo"></i> -->
                            </div>
                            <div class="user-details">
                                <div class="user-name" style="text-align: justify;margin-left: 37px;text-transform: capitalize;font-size: 15px;font-weight: 600;">James Miller</div>
                                <div class="user-txt" style="font-size: 9px;padding: 6px 15px;line-height: 8px;font-weight: 900;">On the road again...</div>
                            </div>
                        </div>
                    </div>
                    <div class="conversation-body">
                        <ul id="messages_chat">

                        </ul>
                    </div>
                    <div class="conversation-message">
                        <input type="text" placeholder="Your message..." class="form-control form-white send-message"/>
                        <!-- <div class="item-footer clearfix">-->
                        <!--     <div class="footer-actions">-->
                        <!--         <i class="icon-rounded-marker"></i>-->
                        <!--         <i class="icon-rounded-camera"></i>-->
                        <!--         <i class="icon-rounded-paperclip-oblique"></i>-->
                        <!--         <i class="icon-rounded-alarm-clock"></i>-->
                        <!--     </div>-->
                        <!-- </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("front-end/page_modal/college_chatgroup_modal") ?>
