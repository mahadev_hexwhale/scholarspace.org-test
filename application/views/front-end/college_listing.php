<?php include('header1.php') ?>
<div class="body_height">
<div class="college_details">

<div class="row">
<div class="col-md-1">
<?php include('left_sidebar.php') ?>

</div>



<div class="col-md-9" style="margin-left: -56px;">

<div class="about_colleges">
<div class="coll_details_top">

<div class="row">
<div class="col-md-8">
<div class="search_result">
<ul>
<li> <div class="result_box"> <h6> Engineering </h6> <span class="close_btn"></span> </div>  <div class="result_box"> <h6>Maharashtra</h6> <span class="close_btn"></span>  </div> <p> 3517 Colleges </p> </li>
<li> <a href="#">List of Top Engineering Colleges In India based on Ranking</a> </li>

</ul>

</div>
</div>



<div class="col-md-4">

<div class="search_result">
 <p> Go to  Quick Links: <a href="#">Top 10 colleges</a> <a href="#"> Btech </a> <a href="#"> Mtech </a> <!-- <a href="#"> Colleges in mumbai </a> -->   </p> 

</div>

</div>



</div>
</div>
<!-- end of colleges top -->

<div class="institute_details">

<div class="first_coll">

<div class="col-md-12">

<div class="course_details">
<a href="#"> Gurukul Vidyapeeth Institute of Engineering and Technology, Patiala </a>

<div class="rating"><h6>Rating </h6>   <span class="star"></span> </div>

</div>
</div>


<div class="coll_info">

<div class="col-md-2">

<div class="coll_logo">

<img src="image/coll_logo1.png" alt="">

</div>

</div>

<div class="col-md-10">

<div class="coll_table">

<table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>COURSES</th>
        <th>FEES</th>
        <th>DURATION</th>
         <th>RATING</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>B.Tech - Aeronautical Engineering</td>
        <td>0.2 lakhs</td>
        <td>4-5 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>ME/M.Tech</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>M.Phil/P.</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
    </tbody>
  </table>


</div>
</div>
</div>
<!-- end of coll-info 1 -->

</div>
<!-- end of first-coll-info 1 -->


<div class="first_coll">

<div class="col-md-12">

<div class="course_details">
<a href="#"> World College of Technology and Management, Gurgaon. </a>

<div class="rating"><h6>Rating </h6>   <span class="star"></span> </div>

</div>
</div>


<div class="coll_info">

<div class="col-md-2">

<div class="coll_logo">

<img src="image/coll_logo2.png" alt="">

</div>

</div>

<div class="col-md-10">

<div class="coll_table">

<table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>COURSES</th>
        <th>FEES</th>
        <th>DURATION</th>
         <th>RATING</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>B.Tech - Aeronautical Engineering</td>
        <td>0.2 lakhs</td>
        <td>4-5 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>ME/M.Tech</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>M.Phil/P.</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
    </tbody>
  </table>


</div>
</div>
</div>
<!-- end of coll-info 1 -->

</div>
<!-- end of first-coll-info 2 -->


<div class="first_coll">

<div class="col-md-12">

<div class="course_details">
<a href="#"> Indian Institute of Technology, Kanpur </a>

<div class="rating"><h6>Rating </h6>   <span class="star"></span> </div>

</div>
</div>


<div class="coll_info">

<div class="col-md-2">

<div class="coll_logo">

<img src="image/coll_logo3.png" alt="">

</div>

</div>

<div class="col-md-10">

<div class="coll_table">

<table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>COURSES</th>
        <th>FEES</th>
        <th>DURATION</th>
         <th>RATING</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>B.Tech - Aeronautical Engineering</td>
        <td>0.2 lakhs</td>
        <td>4-5 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>ME/M.Tech</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>M.Phil/P.</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
    </tbody>
  </table>


</div>
</div>
</div>
<!-- end of coll-info 1 -->

</div>
<!-- end of first-coll-info 3 -->



<div class="first_coll">

<div class="col-md-12">

<div class="course_details">
<a href="#"> Indian Institute of Technology, Roorkee </a>

<div class="rating"><h6>Rating </h6>   <span class="star"></span> </div>

</div>
</div>


<div class="coll_info">

<div class="col-md-2">

<div class="coll_logo">

<img src="image/coll_logo4.png" alt="">

</div>

</div>

<div class="col-md-10">

<div class="coll_table">

<table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>COURSES</th>
        <th>FEES</th>
        <th>DURATION</th>
         <th>RATING</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>B.Tech - Aeronautical Engineering</td>
        <td>0.2 lakhs</td>
        <td>4-5 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>ME/M.Tech</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
      <tr>
        <td>M.Phil/P.</td>
        <td>0.4 lakhs</td>
        <td>2-3 Years</td>
        <td>9.4 /10</td>
      </tr>
    </tbody>
  </table>


</div>
</div>
</div>
<!-- end of coll-info 1 -->

</div>
<!-- end of first-coll-info 4 -->


</div>


<div class="coll_pagination">
<ul class="pagination">
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>

<ul class="pager">
<li class="previous"><a href="#"></a></li>
<li class="next"><a href="#"></a></li>
</ul>
</div>
<!-- end of pagination -->


</div>
</div>
<!-- end of college listing details -->

<div class="col-md-2" style="margin-left: 15px;">

<div class="refine_tree">
<form action="#" method="post">
<div class="refine_search">
<h4>Refine Your Search</h4>

<div class="flow_search">

<ul>
<li> 
<span class="flow_line"></span>
<div class="flow_icon">1</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">

<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>
</div>

</li>

<li> 
<span class="flow_line"></span>
<div class="flow_icon">2</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">

<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>

</div>

</li>


<li> 
<span class="flow_line"></span>
<div class="flow_icon">3</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">
	
<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>

<button class="btn">SEARCH</button>
</div>

</li>
</ul>
</div>
</div>

<!-- refine search 1 ends -->


<div class="refine_search">
<h4>Refine Your Search</h4>

<div class="flow_search">

<ul>
<li> 
<span class="flow_line"></span>
<div class="flow_icon">1</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">

<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>
</div>

</li>

<li> 
<span class="flow_line"></span>
<div class="flow_icon">2</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">

<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>

</div>

</li>


<li> 
<span class="flow_line"></span>
<div class="flow_icon">3</div>  
<span class="flow_line"></span>

<div class="refine_dropdown">
  
<select>
  <option value="volvo">What degree interested in ?</option>
  <option value="Mtech">Mtech</option>
 </select>

<button class="btn">SEARCH</button>
</div>

</li>
</ul>
</div>
</div>
<!-- refine search 2 ends -->
</form>
</div>

</div>


</div>
</div>
</div>
<?php include('footer.php') ?>





