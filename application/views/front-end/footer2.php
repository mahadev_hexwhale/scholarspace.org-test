<!-- .........footer begins............ -->
<!-- BEGIN Chat SIDEBAR -->
<?php include 'chatUser.php'; ?>
<!-- END Chat SIDEBAR -->
</body>
<?php include ('page_modal/intranet_modal.php'); ?>
<?php include ('page_modal/create_message_modal.php'); ?>

<script src="<?php echo base_url(); ?>assets_front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets_front/js/html5shiv.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_front/js/respond.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var site_url = $("#site_url").val();
    var base_url = $("#base_url").val();
</script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/footer2.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets_front/custom_assets/login.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/intranet_group.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_front/custom_assets/chat.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/inbox.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/college_chatgroup.js" type="text/javascript"></script>
</html>

<?php
if (($this->session->flashdata('post_id')) != "") {
    ?>
    <script type="text/javascript">
        var post_id = "<?php echo $this->session->flashdata('post_id'); ?>";
        $('html, body').animate({
            scrollTop: $('div#' + post_id).offset().top - 120
        }, 800);
    </script>
    <?php
}

