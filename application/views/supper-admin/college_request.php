<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-share-square-o"></i>
            College Request
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li><li><a href="#"> College Request</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <table id="college_request_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                <thead>
                                    <tr>
                                        <th>College Name</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Zipcode</th>
                                        <th style="width: 18%">Action</th>
                                    </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    foreach ($requested_college_list as $key => $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->college_name; ?></td>
                                            <td><?php echo $value->college_city; ?></td>
                                            <td><?php echo $value->college_state; ?></td>
                                            <td><?php echo $value->college_zipcode; ?></td>
                                            <td>
                                                <button college_id="<?php echo $value->id; ?>" class="btn btn-success btn-xs accept_college_request_btn"><i class="fa fa-check"></i> Accept</button>
                                                <button college_id="<?php echo $value->id; ?>" class="btn btn-danger btn-xs ignore_college_details_btn"><i class="fa fa-ban"></i> Ignore</button>
                                                <button college_id="<?php echo $value->id; ?>" college_name="<?php echo $value->college_name; ?>" college_zipcode="<?php echo $value->college_zipcode; ?>" college_state="<?php echo $value->college_state; ?>" college_city="<?php echo $value->college_city; ?>" class="btn btn-primary btn-xs edit_college_details_btn"><i class="fa fa-edit"></i> Edit</button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>College Name</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Zipcode</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" value="<?php echo site_url(); ?>" id="site_url">
<input type="hidden" value="<?php echo base_url(); ?>" id="base_url">
<?php
$this->load->view('supper-admin/footer');
$this->load->view('supper-admin/college_request_modal');
?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>assets/custom_assets/formValidation.js'></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/college_request.js" type="text/javascript"></script>
<?php
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("<?php echo $session_flash; ?>");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("<?php echo $session_flash; ?>");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>