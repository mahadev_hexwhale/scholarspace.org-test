<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
    #selectbox{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
    }
    .select2-selection{
        border-radius: 0px !important;
    }

    .select2-container--default .select2-selection--multiple 
    {
        border:1px solid #d2d6de;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple
    {
        border-color:#3C8DBC!important;
    }


</style>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Register College</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Search College</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">
                                            Basic Information
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">College Name</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter College Name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Address</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Address">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">City</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter City">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Type of Institute</label>
                                                    <select class="form-control">
                                                        <option>-- Choose option --</option>
                                                        <option>option 1</option>
                                                        <option>option 2</option>
                                                        <option>option 3</option>
                                                        <option>option 4</option>
                                                        <option>option 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group select2-wrapper">
                                                    <label for="exampleInputEmail1">Courses Offered by stream</label>
                                                    <!--<select id="selectbox" multiple class="user_type" required="" data-search="true" aria-required="true" tabindex="-1" placeholder="Select Customers" title="">-->
                                                    <select id="selectbox" class="form-control input-lg" multiple="multiple" placeholder="Choose options">
                                                        <option>option 1</option>
                                                        <option>option 2</option>
                                                        <option>option 3</option>
                                                        <option>option 4</option>
                                                        <option>option 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Naac accredition</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Naac accredition">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" class="">
                                            Contact Info
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Naac accredition">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Phone</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Naac accredition">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Fax</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Naac accredition">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" class="">
                                            Infrastructure
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Industry Interaction</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Fee</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Placement Details</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Quality of Students and Crowd</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">International Exposure</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">International / India Tieups</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" class="">
                                            Gallery
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Videos :  <a style="margin-left: 208px;cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label style="margin-top: 10px">
                                                    Images :  
                                                </label>
                                            </div>
                                            <div class="col-md-8">

                                                <div class="form-group">
                                                    <label for="exampleInputFile" style="margin-top: 10px;margin-bottom: 17px;">Choose an image to upload</label>
                                                    <input type="file" id="exampleInputFile" multiple="">
                                                    <!--     <p class="help-block">Example block-level help text here.</p> -->
                                                </div>
                                            </div>
                                        </div>     




                                        <div class="row">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" class="">
                                            Facilities
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Clubs and Activities : 
                                                </label>
                                                <a style="font-weight:700;cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" class="">
                                            Other Facilities
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-2">
                                                <label>
                                                    Hostel Facility yes / no :
                                                </label>
                                            </div>
                                            <div class="col-md-2" style="margin-top: -10px;">
                                                <div class="radio">
                                                    <label class="pull-left">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                                                        Yes
                                                    </label>
                                                    <label style="margin-left: 40px;">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="2" placeholder="Description..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-2">
                                                <label>
                                                    Aicte approved or not :
                                                </label>
                                            </div>
                                            <div class="col-md-2" style="margin-top: -10px;">
                                                <div class="radio">
                                                    <label class="pull-left">
                                                        <input type="radio" name="optionsRadioss" id="optionsRadios1" value="option1" checked="">
                                                        Yes
                                                    </label>
                                                    <label style="margin-left: 40px;">
                                                        <input type="radio" name="optionsRadioss" id="optionsRadios1" value="option1" >
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="2" placeholder="Description..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Clubs and Activities : 
                                                </label>
                                                <a style="font-weight:700;cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Laboratories :  
                                                </label>
                                                <a style="font-weight:700; cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Festivals :  
                                                </label>
                                                <a style="font-weight:700; cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                            <div class="col-md-4">
                                                <label>
                                                    Extra Curricular :  
                                                </label>
                                                <a style="font-weight:700; cursor: pointer" class="pull-right"><i class="fa fa-plus"></i> Add More </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Scholarships</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Facility to Stay near by</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Sports Infrastructure</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Library</label>
                                                    <textarea class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" class="">
                                            Faculty <span style="margin-left: 10px; font-size: 15px;"><i class="fa fa-plus"></i> Add More</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="box-body">
                                        <div class="row" style="padding-bottom: 10px;">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Enter Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="1" placeholder="Enter Description ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div>

            <div class="submission">
                <div class="col-md-10"></div>   
                <div class="col-md-2">
                    <div class="form-group">
                        <button class="btn btn-block btn-primary pull-right">Save Details</button>
                    </div>
                </div>
            </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
$this->load->view('supper-admin/footer');
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
    $('#selectbox').select2();
</script>