<?php
$this->load->view("data-entry/header");
$this->load->view("data-entry/sidebar");
?>
<link href="<?php echo base_url(); ?>assets/custom_assets/pages_css/data-entry/select2.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<style type="text/css">
    #selectbox{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
    }
    .select2-selection{
        border-radius: 0px !important;
    }
    .select2-container{
        width: 100% !important;
    }
    .select2-container--default .select2-selection--multiple 
    {
        border:1px solid #d2d6de;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple
    {
        border-color:#3C8DBC!important;
    }
    .head_of_stream{
        background-color: #334b58;
        color:#fff;
    }
    .show_auto_search{
        z-index: 10357;
        width: 91.6%;
        position: absolute;
        list-style-type: none;
        background-color: #ECF0F5;
    }
</style>
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-edit"></i>
            Register College
        </h1> 
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Register College</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <input type="hidden" id="current_choosed_option_select2_id" value=""/>
        <form method="post" id="college_registration_form" enctype="multipart/form-data" action="<?php echo site_url("data-entry/college/save_college_registration"); ?>" role="form">
            <div class="success_alert alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
                <span id="success_alert_message"></span>
            </div>
            <div class="danger_alert alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <span id="danger_alert_message"></span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div id="extra_flag_field_div"></div>
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-success">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">
                                                Basic Information
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>College Name * <span class="required_error_span"></span></label>
                                                        <div><input type="text" id="college_id" name="college_name" class="form-control text_required fetch_names" placeholder="Enter College Name"></div><!-- /.input group -->
                                                        <ul class="show_auto_search"></ul>
                                                        <input type="hidden" name="existing_college_id" id="existing_college_id" value="0"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Type of Institute *<span class="required_error_span"></span></label>
                                                        <div>
                                                            <select id="institute_type" name="institute_type" class="form-control select_required">
                                                                <option value="">-- Choose option --</option>
                                                                <?php
                                                                foreach ($institution_types as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Naac accredition *<span class="required_error_span"></span></label>
                                                        <div><input type="text" id="naac_accredition" name="naac_accredition" class="form-control text_required" placeholder="Enter Naac accredition"></div><!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Address 1 *<span class="required_error_span"></span></label>
                                                        <div><input type="text" id="address_1" name="address_1" class="form-control text_required" placeholder="Enter Address 1"></div><!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Address 2</label>
                                                        <input type="text" id="address_2" name="address_2" class="form-control" placeholder="Enter Address">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>City *<span class="required_error_span"></span></label>
                                                        <div><input type="text" id="city" name="city" class="form-control text_required" placeholder="Enter City"></div><!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">State *<span class="required_error_span"></span></label>
                                                        <div>
                                                            <select id="state" name="state" class="form-control select_required">
                                                                <option value="">-- Choose State --</option>
                                                                <?php
                                                                foreach ($state_list as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value->title; ?>"><?php echo $value->title; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Zipcode *<span class="required_error_span"></span></label>
                                                        <div><input type="text" id="zipcode" name="zipcode" class="form-control text_required" placeholder="Enter Zipcode"></div><!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel box box-success">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" class="">
                                                Stream and Courses
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true">
                                        <div class="box-body">
                                            <!--                                            <div class="row">
                                                                                            <div class="col-md-4">
                                                                                                <div class="form-group">
                                                                                                    <label for="exampleInputEmail1">Courses Offered by stream *<span class="required_error_span"></span></label>
                                                                                                    <div>
                                                                                                        <select id="selectbox" multiple="multiple" name="course_offer[]" class="form-control select_required select_courses_offered"  placeholder="Choose options">
                                            <?php
                                            foreach ($stream_list as $value) {
                                                ?>
                                                                                                                                                                                                                                                                                                        <option value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>
                                                <?php
                                            }
                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div id="courses_offered_by"></div>
                                                                                        </div>-->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Courses Offered by stream *<span class="required_error_span"></span></label>
                                                        <div>
                                                            <select id="stream_selectbox" multiple="multiple" name="stream_offer[]" class="form-control select_required"  placeholder="Choose options">
                                                                <?php
                                                                foreach ($stream_list as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value->id . "-existing"; ?>"><?php echo $value->title; ?></option>
                                                                    <?php
                                                                }
                                                                foreach ($contemporary_stream_list as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value->id . "-contemporary"; ?>"><?php echo $value->title; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="row" id="courses_content">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" class="">
                                                    Contact Info
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Email *<span class="required_error_span"></span></label>
                                                            <div><input type="text" name="email" id="email" class="form-control text_required" placeholder="Enter Email"></div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Phone *<span class="required_error_span"></span></label>
                                                            <div><input type="text" name="phone" id="phone" class="form-control text_required" placeholder="Enter Phone"></div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Fax</label>
                                                            <input type="text" name="fax" class="form-control"  id="fax" placeholder="Enter Naac accredition">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" class="">
                                                    Infrastructure
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Industry Interaction <span class=""></span></label>
                                                            <div>
                                                                <textarea class="form-control" id="industry_interaction" name="industry_interaction" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Fee <span class=""></span></label>
                                                            <div>
                                                                <textarea name="fee" id="fee" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Placement Details <span class=""></span></label>
                                                            <div>
                                                                <textarea name="placement_details" id="placement_details" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Quality of Students and Crowd <span class=""></span></label>
                                                            <div>
                                                                <textarea name="quality_of_student_and_crowd" id="quality_of_students_and_crowd" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">International Exposure <span class=""></span></label>
                                                            <div>
                                                                <textarea name="international_exposure" id="international_exposure" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">International / India Tie ups <span class=""></span></label>
                                                            <div>
                                                                <textarea name="international_or_india_tie_up" id="international_or_india_tie_ups" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" class="">
                                                    Gallery
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label>
                                                            Videos :  <a style="margin-left: 208px;cursor: pointer" class="pull-right add-more-video-btn"><i class="fa fa-plus"></i> Add More </a>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input name="video[]" type="text" class="form-control" id="video_one" placeholder="Enter Title">
                                                        </div>
                                                    </div>
                                                    <div class="more_content" id="more_video"></div>
                                                </div>
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label style="margin-top: 10px">
                                                            Images :  
                                                        </label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputFile" style="margin-top: 10px;margin-bottom: 17px;">Choose an image to upload</label>
                                                            <input name="images[]" type="file" id="exampleInputFile" multiple="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 more_content" id="img_content"></div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" class="">
                                                    Facilities
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label>
                                                            Clubs and Activities : 
                                                        </label>
                                                        <a style="font-weight:700;cursor: pointer" class="pull-right add_more_club_and_activities_btn"><i class="fa fa-plus"></i> Add More </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="clubs_activity_title[]" class="form-control" id="clubs_activity_title_one" placeholder="Enter Title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="clubs_activity_description[]" class="form-control" id="clubs_activity_description_one" placeholder="Enter Description">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="more_content" id="more_club_and_activities"></div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" class="">
                                                    Other Facilities
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-2">
                                                        <label>
                                                            Hostel Facility yes / no :
                                                        </label>
                                                    </div>
                                                    <div class="col-md-2" style="margin-top: -10px;">
                                                        <div class="radio">
                                                            <label class="pull-left">
                                                                <input type="radio" name="hostel_ckeck" class="hostel_facilities_radio" value="yes" checked="">
                                                                Yes
                                                            </label>
                                                            <label style="margin-left: 40px;">
                                                                <input type="radio" name="hostel_ckeck" class="hostel_facilities_radio" value="no" >
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group hostel_description_div">
                                                            <textarea name="hostal_facility_description" id="hostel_facility" class="form-control" rows="2" placeholder="Description..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-2">
                                                        <label>
                                                            Aicte approved or not :
                                                        </label>
                                                    </div>
                                                    <div class="col-md-2" style="margin-top: -10px;">
                                                        <div class="radio">
                                                            <label class="pull-left">
                                                                <input type="radio" name="aicte_approved_check" class="aicte_approved_radio" value="yes" checked="">
                                                                Yes
                                                            </label>
                                                            <label style="margin-left: 40px;">
                                                                <input type="radio" name="aicte_approved_check" class="aicte_approved_radio" value="no">
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group aicte_approved_or_not_div">
                                                            <textarea name="aicte_approved_or_not" id="aicte_approved" class="form-control" rows="2" placeholder="Description..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label>
                                                            Laboratories :  
                                                        </label>
                                                        <a style="font-weight:700; cursor: pointer" class="pull-right add_more_laboratories"><i class="fa fa-plus"></i> Add More </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="laboratories_title[]" class="form-control" id="laboratories_title_one" placeholder="Enter Title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <textarea name="laboratories_description[]" id="laboratories_description_one" class="form-control" rows="1" placeholder="Description ..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="more_content" id="more_laboratories"></div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label>
                                                            Festivals :  
                                                        </label>
                                                        <a style="font-weight:700; cursor: pointer" class="pull-right add_festivals"><i class="fa fa-plus"></i> Add More </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="festivals_title[]" class="form-control" id="festivals_title_one" placeholder="Enter Title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <textarea  name="festivals_description[]"class="form-control" id="festivals_description_one" rows="1" placeholder="Description ..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="more_content" id="more_festivals"></div>
                                                </div>
                                                <div class="row">&nbsp;</div>
                                                <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                                                    <div class="col-md-4">
                                                        <label>
                                                            Extra Curricular :  
                                                        </label>
                                                        <a style="font-weight:700; cursor: pointer" class="pull-right add_extra_curricular"><i class="fa fa-plus"></i> Add More </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="extra_curricular_title[]" class="form-control" id="extra_curricular_title_one" placeholder="Enter Title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <textarea  name="extra_curricular_description[]"class="form-control" id="extra_curricular_description_one" rows="1" placeholder="Description ..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="more_content" id="more_extra_curricular"></div>
                                                </div>
                                                <div class="row">&nbsp;</div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Scholarships</label>
                                                            <textarea name="scholarships" id="scholarships" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Facility to Stay near by</label>
                                                            <textarea name="facility_stay_near_by" id="facility_to_stay_near_by" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Sports Infrastructure</label>
                                                            <textarea name="sports_infrastructure" id="sports_infrastructure" class="form-control" rows="2" placeholder="Enter ..."></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Library</label>
                                                            <textarea name="library" class="form-control" id="library" rows="2" placeholder="Enter ..."></textarea>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" class="">
                                                    Faculty 
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="box-body">
                                                <div class="row" style="padding-bottom: 10px;">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <a style="font-weight:700; cursor: pointer; margin-left: 30%;" class="add_faculty"><i class="fa fa-plus"></i> Add More </a>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" name="faculty_title[]" class="form-control" id="faculty_title_one" placeholder="Enter Title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <textarea name="faculty_description[]" class="form-control" id="faculty_description_one" rows="1" placeholder="Enter Description ..."></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="more_content" id="more_faculty"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">&nbsp;</div>
                                    <div class="row submission">
                                        <div class="col-md-10"></div>   
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button type="submit" id="college_details_save_btn" class="btn btn-block btn-primary pull-right">Save Details</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">&nbsp;</div>
                                </div>
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                    </div>
                </div>
        </form>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<input type="hidden" value="<?php echo site_url(); ?>" id="site_url">
<?php
$this->load->view("data-entry/footer");
?>
<!--<script src="<?php echo base_url(); ?>assets/custom_assets/select2.js"></script>-->
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/data-entry/select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/data-entry/register.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/formValidation.js" type="text/javascript"></script>
<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
<script src="<?php echo base_url(); ?>assets/custom_assets/jquery-ui-with-custom-autocomplete.js" type="text/javascript"></script>
<?php
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("College Added Successfully.");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("Failed To Add College Details.");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>