<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $encryption_decryption_object = null;
    public $permissions;
    public $user = null;

    public function __construct() {
        parent::__construct();

        $token = $this->input->post('token');
        $this->load->model('posts_model');

        if ($token == "") {
            $this->user = $this->ion_auth->user()->row();
            if (!$this->ion_auth->logged_in()) {
                redirect('/');
            }
        } else {
            $ecr_token = md5($token);
            $user_id = $this->input->post('user_id');
            $this->user = $this->ion_auth->user($user_id)->row();
            $token_from_db = $this->user->android_token;
            if ($token_from_db == "" || $ecr_token != $token_from_db) {
                // login error
                exit();
            }
        }

        $this->load->library('My_PHPMailer');
    }

    public function send_email($email, $subject, $body) {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "jogendra@hexwhale.com";
        $mail->Password = "getinside";
        $mail->setFrom('jogendra@hexwhale.com', 'Scholler Space');
        $mail->addReplyTo('jogendra@hexwhale.com', 'Scholler Space');
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->addAddress($email);
        $mail->send();
    }

    public function get_unfixed_group_post($GroupID) {
        $college_id = $this->session->userdata('college_id');

        //current user
        $user_id = $this->user->user_id;

        $GetAllPosts = "SELECT t1.*,t1.id as post_id, "
                . "t2.first_name, t2.last_name, t2.profile_picture "
                . "FROM college_unfixed_group_posts t1 "
                . "INNER JOIN users t2 ON t1.user_id = t2.id "
                . "WHERE group_id = '$GroupID' AND college_id = '$college_id' "
                . "group by t1.id "
                . "ORDER BY t1.timestamp DESC ";
//                . "LIMIT 0,4";
        $GetAllPosts_Result = $this->data_fetch->data_query($GetAllPosts);
        $data['posts_list'] = $GetAllPosts_Result;

        $data['posts_file_list'] = array();
        $data['posts_reply_list'] = array();
        $data['posts_like_current_user'] = array();
        $data['posts_follow_current_user'] = array();
        $data['posts_like_all_user'] = array();
        $data['posts_reply_like_all_user'] = array();
        $data['posts_reply_like_current_user'] = array();
        $data['posts_notified_users_list'] = array();
        $data['posts_reply_notified_users_list'] = array();

        $data['posts_update_details'] = array();
        $data['posts_poll_details'] = array();
        $data['posts_poll_answer_details'] = array();
        $data['posts_poll_answer_vote_details'] = array();
        $data['posts_praise_details'] = array();
        $data['posts_praise_member_details'] = array();
        $data['posts_announce_details'] = array();

        //fetch POST reply and like list
        foreach ($data['posts_list'] as $value) {
            $post_id = $value->post_id;

            //post like for the current user
            $sql_query = "SELECT `id` FROM `college_unfixed_group_posts` "
                    . "WHERE `id` = '$post_id' AND `user_id` = '$user_id' "
                    . "AND `college_id` = '$college_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['posts_current_user'][$post_id] = 1;
            } else {
                $data['posts_current_user'][$post_id] = 0;
            }

            switch ($value->type) {
                case 'update':
                    $sql_query = "SELECT id AS update_id, update_content FROM college_unfixed_group_update_post_details "
                            . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_update_details'][$post_id] = $query_result;
                    break;
                case 'poll':
                    $sql_query = "SELECT id AS poll_id, question FROM college_unfixed_group_poll_post_details "
                            . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_poll_details'][$post_id] = $query_result;

                    foreach ($data['posts_poll_details'][$post_id] as $poll_value) {
                        $poll_id = $poll_value->poll_id;
                        $sql_query = "SELECT id as ans_id, answer FROM college_unfixed_group_poll_post_answer_details "
                                . "WHERE post_id = '$post_id' AND poll_id = '$poll_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_poll_answer_details'][$post_id][$poll_id] = $query_result;

                        foreach ($data['posts_poll_answer_details'][$post_id][$poll_id] as $poll_ans_value) {
                            $poll_ans_id = $poll_ans_value->ans_id;
                            $sql_query = "SELECT count(id) as vote_count, voted_user_id FROM college_unfixed_group_poll_post_answer_vote_details "
                                    . "WHERE post_id = '$post_id' AND poll_id = '$poll_id' AND ans_id = '$poll_ans_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_poll_answer_vote_details'][$post_id][$poll_id][$poll_ans_id] = $query_result;
                        }

                        $sql_query = "SELECT `id` FROM `college_unfixed_group_poll_post_answer_vote_details` "
                                . "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if (count($query_result)) {
                            $data['posts_poll_answer_vote_current_user'][$post_id] = 1;
                        } else {
                            $data['posts_poll_answer_vote_current_user'][$post_id] = 0;
                        }
                    }
                    break;
                case 'praise':
                    $sql_query = "SELECT t1.id AS praise_id,t1.praise_content,t1.badge_id, "
                            . "t2.image as badge_image "
                            . "FROM college_unfixed_group_praise_post_details t1 "
                            . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                            . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_praise_details'][$post_id] = $query_result;

                    foreach ($data['posts_praise_details'][$post_id] as $praise_value) {
                        $praise_id = $praise_value->praise_id;

                        $sql_query = "SELECT member_user_id as praised_user_id FROM college_unfixed_group_praise_post_member_details "
                                . "WHERE post_id = '$post_id' AND praise_id = '$praise_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_praise_member_details'][$post_id] = $query_result;
                    }
                    break;
                case 'announce':
                    $sql_query = "SELECT id AS announce_id, heading,content FROM college_unfixed_group_announce_post_details "
                            . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_announce_details'][$post_id] = $query_result;
                    break;
            }
            //post files list
            $sql_query = "SELECT `id`,`file_type`,`file_path` "
                    . "FROM `college_unfixed_group_posts_files` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_file_list'][$post_id] = $query_result;

            //post reply msg
            $sql_query = "SELECT id AS post_reply_id,`user_id`,`reply_content`,`like_count`,`timestamp` "
                    . "FROM `college_unfixed_group_post_reply` "
                    . "WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_reply_list'][$post_id] = $query_result;

            //post reply like
            foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                $post_reply_id = $inner_value->post_reply_id;

                $sql_query = "SELECT `user_id` FROM `college_unfixed_group_post_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' "
                        . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_unfixed_group_post_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' "
                        . "AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                } else {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                }

                //post reply notified user list
                $sql_query = "SELECT `user_id` FROM `college_unfixed_group_post_reply_notify_user_details` "
                        . "WHERE `post_id` = '$post_id' "
                        . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                if (count($query_result)) {
                    $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                } else {
                    $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                }
            }

            //post reply like
            $sql_query = "SELECT id AS post_reply_id,`user_id` "
                    . "FROM `college_unfixed_group_post_reply_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            foreach ($query_result as $value) {
                $data['posts_reply_like'][$post_id][$value->post_reply_id] = $value->user_id;
            }

            //post like for all the user
            $sql_query = "SELECT `id`,`user_id` FROM `college_unfixed_group_post_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_like_all_user'][$post_id] = $query_result;

            //post like for the current user
            $sql_query = "SELECT `id` FROM `college_unfixed_group_post_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['posts_like_current_user'][$post_id] = 1;
            } else {
                $data['posts_like_current_user'][$post_id] = 0;
            }

            //post follow for the current user
            $sql_query_follow = "SELECT `id` FROM `college_unfixed_group_posts_follow` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
            $sql_query_follow_result = $this->data_fetch->data_query($sql_query_follow);

            if (count($sql_query_follow_result)) {
                $data['posts_follow_current_user'][$post_id] = 1;
            } else {
                $data['posts_follow_current_user'][$post_id] = 0;
            }

            //Post notified user list
            $sql_query = "SELECT `user_id` FROM `college_unfixed_group_posts_notify_user_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            if (count($query_result)) {
                foreach ($query_result as $value) {
                    $data['posts_notified_users_list'][$post_id][] = $value->user_id;
                }
            } else {
                $data['posts_notified_users_list'][$post_id] = array();
            }
        }
        $data['user'] = $this->user;
        return $data;
    }

    public function get_fixed_group_post($group) {
        $college_id = $this->session->userdata('college_id');

        //current user
        $user_id = $this->user->user_id;

        $GetAllPosts = "SELECT t1.*,t1.post_id as post_id, "
                . "t2.first_name, t2.last_name, t2.profile_picture "
                . "FROM college_fixed_group_posts t1 "
                . "INNER JOIN users t2 ON t1.user_id = t2.id "
                . "WHERE post_group_name = '$group' AND college_id = '$college_id' "
                . "group by t1.post_id "
                . "ORDER BY t1.timestamp DESC ";
//                . "LIMIT 0,4";
        $GetAllPosts_Result = $this->data_fetch->data_query($GetAllPosts);
        $data['posts_list'] = $GetAllPosts_Result;

        $data['posts_file_list'] = array();
        $data['posts_reply_list'] = array();
        $data['posts_like_current_user'] = array();
        $data['posts_follow_current_user'] = array();
        $data['posts_like_all_user'] = array();
        $data['posts_reply_like_all_user'] = array();
        $data['posts_reply_like_current_user'] = array();
        $data['posts_notified_users_list'] = array();
        $data['posts_reply_notified_users_list'] = array();

        $data['posts_update_details'] = array();
        $data['posts_poll_details'] = array();
        $data['posts_poll_answer_details'] = array();
        $data['posts_poll_answer_vote_details'] = array();
        $data['posts_praise_details'] = array();
        $data['posts_praise_member_details'] = array();
        $data['posts_announce_details'] = array();

        //fetch POST reply and like list
        foreach ($data['posts_list'] as $value) {
            $post_id = $value->post_id;

            //post like for the current user
            $sql_query = "SELECT post_id as id FROM `college_fixed_group_posts` "
                    . "WHERE `post_id` = '$post_id' AND `user_id` = '$user_id' "
                    . "AND `college_id` = '$college_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['posts_current_user'][$post_id] = 1;
            } else {
                $data['posts_current_user'][$post_id] = 0;
            }

            switch ($value->type) {
                case 'update':
                    $sql_query = "SELECT id AS update_id, update_content FROM college_fixed_group_update_post_details "
                            . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_update_details'][$post_id] = $query_result;
                    break;
                case 'poll':
                    $sql_query = "SELECT id AS poll_id, question FROM college_fixed_group_poll_post_details "
                            . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_poll_details'][$post_id] = $query_result;

                    foreach ($data['posts_poll_details'][$post_id] as $poll_value) {
                        $poll_id = $poll_value->poll_id;
                        $sql_query = "SELECT id as ans_id, answer FROM college_fixed_group_poll_post_answer_details "
                                . "WHERE post_id = '$post_id' AND poll_id = '$poll_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_poll_answer_details'][$post_id][$poll_id] = $query_result;

                        foreach ($data['posts_poll_answer_details'][$post_id][$poll_id] as $poll_ans_value) {
                            $poll_ans_id = $poll_ans_value->ans_id;
                            $sql_query = "SELECT count(id) as vote_count, voted_user_id FROM college_fixed_group_poll_post_answer_vote_details "
                                    . "WHERE post_id = '$post_id' AND poll_id = '$poll_id' AND ans_id = '$poll_ans_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_poll_answer_vote_details'][$post_id][$poll_id][$poll_ans_id] = $query_result;
                        }

                        $sql_query = "SELECT `id` FROM `college_fixed_group_poll_post_answer_vote_details` "
                                . "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if (count($query_result)) {
                            $data['posts_poll_answer_vote_current_user'][$post_id] = 1;
                        } else {
                            $data['posts_poll_answer_vote_current_user'][$post_id] = 0;
                        }
                    }
                    break;
                case 'praise':
                    $sql_query = "SELECT t1.id AS praise_id,t1.praise_content,t1.badge_id, "
                            . "t2.image as badge_image "
                            . "FROM college_fixed_group_praise_post_details t1 "
                            . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                            . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_praise_details'][$post_id] = $query_result;

                    foreach ($data['posts_praise_details'][$post_id] as $praise_value) {
                        $praise_id = $praise_value->praise_id;

                        $sql_query = "SELECT member_user_id as praised_user_id FROM college_fixed_group_praise_post_member_details "
                                . "WHERE post_id = '$post_id' AND praise_id = '$praise_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_praise_member_details'][$post_id] = $query_result;
                    }
                    break;
                case 'announce':
                    $sql_query = "SELECT id AS announce_id, heading,content FROM college_fixed_group_announce_post_details "
                            . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_announce_details'][$post_id] = $query_result;
                    break;
            }

            //post files list
            $sql_query = "SELECT `id`,`file_type`,`file_path` "
                    . "FROM `college_fixed_group_posts_files` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_file_list'][$post_id] = $query_result;

            //post reply msg
            $sql_query = "SELECT post_reply_id AS post_reply_id,`user_id`,`reply_message`,`reply_like`,`timestamp` "
                    . "FROM `college_fixed_group_posts_reply` "
                    . "WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_reply_list'][$post_id] = $query_result;

            //post reply like
            foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                $post_reply_id = $inner_value->post_reply_id;

                $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' "
                        . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_fixed_group_posts_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' "
                        . "AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                } else {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                }

                //post reply notified user list
                $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_notify_user_details` "
                        . "WHERE `post_id` = '$post_id' "
                        . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                if (count($query_result)) {
                    $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                } else {
                    $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                }
            }

            //post reply like
            $sql_query = "SELECT post_reply_id AS post_reply_id,`user_id` "
                    . "FROM `college_fixed_group_posts_reply_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            foreach ($query_result as $value) {
                $data['posts_reply_like'][$post_id][$value->post_reply_id] = $value->user_id;
            }

            //post like for all the user
            $sql_query = "SELECT `id`,`user_id` FROM `college_fixed_group_posts_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_like_all_user'][$post_id] = $query_result;

            //post like for the current user
            $sql_query = "SELECT `id` FROM `college_fixed_group_posts_like_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['posts_like_current_user'][$post_id] = 1;
            } else {
                $data['posts_like_current_user'][$post_id] = 0;
            }

            //post follow for the current user
            $sql_query_follow = "SELECT `id` FROM `college_fixed_group_posts_follow` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
            $sql_query_follow_result = $this->data_fetch->data_query($sql_query_follow);

            if (count($sql_query_follow_result)) {
                $data['posts_follow_current_user'][$post_id] = 1;
            } else {
                $data['posts_follow_current_user'][$post_id] = 0;
            }

            //Post notified user list
            $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_notify_user_details` "
                    . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            if (count($query_result)) {
                foreach ($query_result as $value) {
                    $data['posts_notified_users_list'][$post_id][] = $value->user_id;
                }
            } else {
                $data['posts_notified_users_list'][$post_id] = array();
            }
        }
        $data['user'] = $this->user;
        return $data;
    }

}

class UserClass extends MY_Controller {

    public $AluminiListFull = array();
    public $StudentsListFull = array();
    public $TeacherListFull = array();
    public $college_id;
    public $collegeChatGroupName = array();
    public $user_details;

    public function __construct() {
        parent::__construct();
        $this->college_id = $this->session->userdata('college_id');
        $this->load->helper('college-admin_helper');
        $this->user_details = GetUserDetails();
        
        $this->load->model('usersearch');
        $this->AluminiListFull = $this->usersearch->getUserList($this->college_id, 'alumni');
        $this->StudentsListFull = $this->usersearch->getUserList($this->college_id, 'student');
        $this->TeacherListFull = $this->usersearch->getUserList($this->college_id, 'teacher');
        $this->collegeChatGroupName = $this->usersearch->showChatGroup($this->ion_auth->user()->row()->id);
    }

    public function FilterCollegeMembers_Method() {
        $Item = $this->input->post('Item');
        $logged_user_id = $this->ion_auth->user()->row()->id;
        echo json_encode($this->usersearch->FilterCollegeMembers_Method($this->college_id, $Item, $logged_user_id));
    }

    //Working on (Bhashker)
    public function getChatUser() {

        $chatwith = $this->input->post('ChatWith');
        $chatwith = $this->encryption_decryption_object->is_valid_input($chatwith);


        $chat_content = array();
        $user_id = $this->ion_auth->user()->row()->id;

        $check_chat_exists = $this->usersearch->CheckChatExists($user_id, $chatwith);
        if ($check_chat_exists != 0) {
            //Code to make all message to  read.        
            $this->usersearch->changeMessageStatus($user_id);
            $chat_status = $this->usersearch->getUserChatStatus($check_chat_exists);
            $chat_status = $chat_status[0];

            //Check the user response wheter is valid to check the user or not.
            if ($this->checkValidChatUser($check_chat_exists)) {
                $chat_content = $this->usersearch->GetChatLast30Messages($check_chat_exists);
                if (count($chat_content) >= 1) {

                    foreach ($chat_content as $chat) {
                        $chat->status = $chat_status->chat_status;
                    }

                    foreach ($chat_content as $key => $value) {
                        $value->user_id = $this->encryption_decryption_object->encode($value->user_id);
                    }
                }

                echo json_encode($chat_content);
            } else {
                $chat_content = "";
                echo json_encode($chat_content);
            }
        } else {

            $create_new_chat = $this->usersearch->CreateChat($user_id, $chatwith);
        }

        return;
    }

    //Function Modified by Bhashkar
    public function inserChat() {

        $chat_user_id = $this->input->post('Chat_user_id');
        $message = $this->input->post('chat_message');
        $chatwith = $this->encryption_decryption_object->is_valid_input($chat_user_id);
        $user_id = $this->ion_auth->user()->row()->id;

        $chat_id = $this->usersearch->CheckChatExists($user_id, $chatwith);

        //check the vaid chat user.
        $isValid = $this->checkValidChatUser($chat_id);

        if ($isValid) {

            $message = $this->input->post('chat_message');
            $user_id = $this->ion_auth->user()->row()->id;

            $insertChat = $this->usersearch->insertChat($chat_id, $user_id, $message);
            if ($insertChat) {
                echo $timestamp = date('Y-m-d G:i:s');

                //echo json_encode($timestamp);
            }
            return;
        } else {//User in Denied to chat with the sender. hide the chat window.//Start work tomorow.
            echo $timestamp = ""; //Show the message and hide the chat box.  
            // echo json_encode($timestamp);
        }
    }

    public function getLatestMessage() {
        $chat_user_id = $this->input->post('ChatWith');
        $timestamp = $this->input->post('timeStamp');
        if ($timestamp == "" || $timestamp == -1) {
            $timestamp = date('Y-m-d G:i:s');
            return;
        }
        $chatwith = $this->encryption_decryption_object->is_valid_input($chat_user_id);
        $user_id = $this->ion_auth->user()->row()->id;
        $chat_id = $this->usersearch->CheckChatExists($user_id, $chatwith);
        //get the chat status.

        $chatStatus = $this->usersearch->getUserChatStatus($chat_id);
        $chatStatus = $chatStatus[0];

        if ($chatStatus->chat_status == 0) {
            $chat_content = "Chat is not permitted";
            echo json_encode($chat_content);
        } else {
            if ($chat_id != 0) {
                $chat_content = $this->usersearch->getLatestMessage($user_id, $chat_id, $timestamp);
                foreach ($chat_content as $key => $value) {
                    $value->user_id = $this->encryption_decryption_object->encode($value->user_id);
                }
                echo json_encode($chat_content);
            }
        }
    }

    //Gets the userchat response and change the chat status.
    public function changeChatResponse() {
        $chat_resp = $this->input->post('chatresponse');
        $chat_with = $this->input->post('chatwith');

        $chatwith = $this->encryption_decryption_object->is_valid_input($chat_with);
        $user_id = $this->ion_auth->user()->row()->id;
        $chat_id = $this->usersearch->CheckChatExists($user_id, $chatwith);
        echo $this->usersearch->changeChatStatus($chat_resp, $chat_id);
    }

    //Change the message status to read.
    public function changMessageStatus() {
        $user_id = $this->ion_auth->user()->row()->id;
        echo $affectedRows = $this->usersearch->changeMessageStatus($user_id);
    }

    //function to check the valid chat user.
    public function checkValidChatUser($chat_id) {
        $isNewUser = $this->usersearch->getUserChatStatus($chat_id);   //0:New user1:Old user.               
        $isNewUser = $isNewUser[0]->chat_status;

        if ($isNewUser == 0) {//New user
            return TRUE;
        } else {//Old User. check furthe for chat response.
            $response = $this->usersearch->getUserChatResponse($chat_id);
            $chatResponse = $response[0]->chat_response;
            if ($chatResponse == 1 || $chatResponse == 2 || $chatResponse == 0) {//For new, accept and ignore 
                return TRUE;
            } else {//If rejected
                return FALSE;
            }
        }
    }

//Function to get the all the use.
    public function displayChatUserList() {
        $searchKey = $this->input->post('Item');
        $userlist = $this->usersearch->displayChatUserList($searchKey);
        //echo "<pre />";
        //print_R($userlist);
        // echo $userlist; 
        echo json_encode($userlist);
    }

//Fnction to create group 
    public function createChatGroup() {
        $groupName = $this->input->post('groupName');
        $groupMemeberId = $this->input->post('chatGroupMember');
        $user_id = $this->ion_auth->user()->row()->id;
        array_push($groupMemeberId, $user_id);
        sort($groupMemeberId);

        $groupName = array(
            'group_name' => $groupName,
            'created_at' => date("Y-m-d")
        );

        $result = $this->usersearch->createChatGroup($groupName, $groupMemeberId);
        echo json_encode($result);
    }

    //To get the group detial
    public function getChatGroupDetails() {
        $groupChatId = $this->input->post('groupChatId');
         $groupChatId=$this->encryption_decryption_object->is_valid_input($groupChatId);
        $result = $this->usersearch->getChatGroupDetails($groupChatId);
        echo json_encode($result);
    }

    /* get the  old 30 message of group chat

     */

    public function getGroupChatUsers() {
        $group_id = $this->input->post('groupChatId');
//        echo json_encode($group_id); 
//        $user_id = $this->ion_auth->user()->row()->id;
//        $group_chat_id = $this->usersearch->CheckGroupChatExists($user_id, $group_id);
        //echo json_encode($group_chat_id);  
         
        if ($group_id){
            //group and group user is exist.
            //get the last30 messages.
            $message_content = $this->usersearch->getGroupChatLast30Messages($group_id);
//           echo "<pre />"; print_r($message_content ); 
          $chat_contentArr=array();
          for($j=0; $j<count($message_content['sendePic']); $j++)
           {
              
              for($i=0; $i<count($message_content['message']); $i++)
              {
                $chat_contentArr[$i]['id']=$message_content['message'][$i]->id;
                $chat_contentArr[$i]['message']= $message_content['message'][$i]->message;
                $chat_contentArr[$i]['sender_id']=$message_content['message'][$i]->sender_id;
                $chat_contentArr[$i]['timestamp']= $message_content['message'][$i]->timestamp;
                               
                if($message_content['message'][$i]->sender_id==$message_content['sendePic'][$j]->sender_id)
                 {
                     $chat_contentArr[$i]['profile_pic']=$message_content['sendePic'][$j]->profile_picture;
                 }       
              }
           }
//           echo "<pre />"; print_R($chat_contentArr); die();
           if (count($chat_contentArr)>0){
                echo json_encode($chat_contentArr);
            }
        }
    }

    //code ot insert the group chat message.
    public function insertGroupChat() {

        $chat_group_id = $this->input->post('group_id');
        $message = $this->input->post('chat_message');
        $message_sender = $this->ion_auth->user()->row()->id;
        $insertGroupChat = $this->usersearch->insertGroupChat($chat_group_id, $message, $message_sender);
        if ($insertGroupChat) {
            echo $timestamp = date('Y-m-d G:i:s');
        } else {
            //User in Denied to chat with the sender. hide the chat window.//Start work tomorow.
            echo $timestamp = ""; //Show the message and hide the chat box.  
            // echo json_encode($timestamp);
        }
    }

    public function getLatestGroupMessage() {
        $chat_user_id = $this->input->post('ChatWith');
        $timestamp = $this->input->post('timeStamp');
        if ($timestamp == "" || $timestamp == -1) {
            $timestamp = date('Y-m-d G:i:s');
            return;
        }
        $chatWith = $chat_user_id;
        $user_id = $this->ion_auth->user()->row()->id;
        $chat_content = $this->usersearch->getLatestGroupMessage($user_id, $chatWith, $timestamp);
  
        if(count($chat_content)>0){        
         foreach ($chat_content as $key => $value) {
                    $value->college_chat_groups_id = $this->encryption_decryption_object->encode($value->college_chat_groups_id);
                }
           echo json_encode($chat_content);
        }
       else 
       {
          $chat_content = "";
          echo json_encode($chat_content);
       }
         
    }
    
    
 //Show all group.   
  public function showAllGroup(){
   $user_id = $this->ion_auth->user()->row()->id;
   $groups=$this->usersearch->getAllGroups($user_id); 
//   echo "<pre />";
//   print_r($groups);  die();
   foreach($groups['chat_group'] as $key=>$value){
      $value->group_chat_id=$this->encryption_decryption_object->encode($value->group_chat_id);
  }
  foreach($groups['college_group'] as $key=>$value){
      $value->id=$this->encryption_decryption_object->encode($value->id);
  }
  echo JSON_encode($groups);

  }  
    

}
