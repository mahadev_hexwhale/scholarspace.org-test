<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()) {
            redirect("/");
        }
        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library
        $this->load->helper(array('url', 'html', 'form'));  // load url,html,form helpers
        $this->load->helper('college-admin_helper');
    }

    function index() {
        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[100]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|exact_length[10]|callback_numeric_wcomma|is_unique[' . $tables['users'] . '.phone]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front-end/register');
        } else {
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $profile_picture = "assets_front/image/user_profile_pic/user_default_profile.jpg";
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'user_type' => "front-end-user",
                'user_type_id' => 3,
                'phone' => $this->input->post('phone'),
                'encoded_pwd' => base64_encode($password),
                'profile_picture' => $profile_picture
            );
            if ($this->ion_auth->register($username, $password, $email, $additional_data)) {
                $remember = 0;

                $subject = "Scholarspace Registration Details";
                $body = " <div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
                            <div style='width:680px'>
                                <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
                                    <img src='http://scholarspace.org/test/img/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
                                </div>
                                <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear <span style='font-size: 18px;'>$username</span> </p>
                                <p style='margin-top:0px;margin-bottom:15px; color:#000000 !important;width:100%;'>Thank you for registering yourself at Scholarspace</p>
                                <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Please login to your account using below mentioned link and credentials. &nbsp;&nbsp;<a href='http://scholarspace.org/test/' target='_blank'>Scholar Space</a>
                                </p>
                                <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
                                    <thead>
                                        <tr>
                                            <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222' colspan='2'>Login Details</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b>Username:</b> $email </td>
                                            <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> Password:</b> $password </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
                                <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
                                <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholar Space.</p>
                                <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
                            </div>
                        </div>";
                send_email($email, $subject, $body);
                if ($this->ion_auth->login($email, $password, $remember)) {
                    redirect("/");
                }
            } else {
                $this->session->set_flashdata('item_error', 'Request can not completed, Please try again later.');
                redirect(current_url());
            }
        }
    }

    function check_unique_email() {
        if (!empty($_POST)) {
            $email = $this->input->post('email');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'email', $email)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function check_unique_phone() {
        if (!empty($_POST)) {
            $phone = $this->input->post('mobile');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'phone', $phone)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function captcha_validation() {
        $this->load->library('recaptcha_library');
//        $publickey = "6LfFBQgTAAAAALCGsNxskGwNGHyaDCGvNBtB1vZz";
//        $privatekey = "6LfFBQgTAAAAAJY0R7ju830AwccRPfAIcm_BEvDk";

        $publickey = "6LczpBMTAAAAAB_9eGK5n57sdVa2TFfhKmJPeigX";
        $privatekey = "6LczpBMTAAAAAKMb3q6teX0XqHvcd4NWUgSt0FsW";

# the response from reCAPTCHA
        $resp = null;
# the error code from reCAPTCHA, if any
        $error = null;

# was there a reCAPTCHA response?
        if ($_POST["recaptcha_response_field"]) {
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
            echo $resp->is_valid;
        }
    }

}

?>