<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "super-admin") {
            show_404();
        }
    }

    public function index() {
        $this->load->view('supper-admin/home');
    }

}
