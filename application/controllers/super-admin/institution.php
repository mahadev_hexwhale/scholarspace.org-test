<?php

Class Institution extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "super-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        $sql_query = "SELECT * FROM `institution_types`";
        $data['institution_type_list'] = $this->data_fetch->data_query($sql_query);
        $this->load->view('supper-admin/viewInstitution', $data);
    }

    public function submit_add_institute() {
        if (!empty($_POST)) {
            $institution = $this->input->post('institution');
            $sql_query = "INSERT INTO `institution_types`(`title`,`active_status`) VALUES ('$institution','1')";
            $query_result = $this->data_insert->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Institute Type Added Successfully.");
                redirect("super-admin/institution/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to add intitution Type.");
                redirect("super-admin/institution/", 'refresh');
            }
        }
    }

    public function submit_edit_institution() {
        if (!empty($_POST)) {
            $institution_title = $this->input->post('institution_title');
            $institution_id = $this->input->post('institution_id');

            $sql_query = "UPDATE `institution_types` SET `title` = '$institution_title' WHERE `id`='$institution_id'";
            $query_result = $this->data_insert->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Institute type updated Successfully.");
                redirect("super-admin/institution/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to update intitution Type.");
                redirect("super-admin/institution/", 'refresh');
            }
        }
    }

    public function update_institution_active_status() {
        if (!empty($_POST)) {
            $active_status = ($this->input->post('active_status') == '1') ? '0' : '1';
            $institution_id = $this->input->post('institution_id');
            $sql_query = "UPDATE `institution_types` SET `active_status` = '$active_status' WHERE `id`='$institution_id'";
            $query_result = $this->data_update->data_query($sql_query);
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function delete_institution() {
        if (!empty($_POST)) {
            $institution_id = $this->input->post('institution_id');
            $sql_query = "DELETE FROM `institution_types` WHERE `id` = '$institution_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Institution deleted Successfully.");
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}
