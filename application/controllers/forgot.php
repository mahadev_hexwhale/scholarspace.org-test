<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot extends CI_Controller {

    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()) {
            redirect("/");
        }
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('college-admin_helper');

        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            // if der is any error
            $this->load->view('front-end/forgot_page');
        } else {
            $email = strtolower($this->input->post('email'));

            if ($email != "") {
                // get the details of the user with this email_id
                $this->db->select('id,first_name,last_name,email');
                $this->db->where('email', $email);
                $query_result = $this->db->get('users')->row();
                $user_name = $query_result->first_name . " " . $query_result->last_name;
                $user_id = $query_result->id;

                if (count($query_result)) {
                    $date = date('Y-m-d H:i:s');
                    $code = $date . "_" . uniqid(); // set the code for further verification process
                    // update table with code
                    $data = array(
                        'forgotten_password_code' => $code,
                        'forgotten_password_time' => $date
                    );
                    $this->db->where('email', $email);
                    $UpdateUser = $this->db->update('users', $data);

                    if ($UpdateUser) {
                        // send a email with link 
                        $link = site_url() . "forgot/ResetPassword/" . $this->encryption_decryption_object->encode($code);
                        $subject = "Reset Password for Scholarspace Account";
                        $body = "<div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
                                    <div style='width:680px'>
                                        <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
                                            <img src='http://scholarspace.org/test/img/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
                                        </div>
                                        <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear <span style='font-size: 18px;'> $user_name </span> </p>
                                        <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Plese click on the given link below to reset your password.
                                        </p>
                                        <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
                                            <tbody>
                                                <tr>
                                                    <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b>Reset link: </b> $link </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
                                        <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
                                        <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholar Space.</p>
                                        <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
                                    </div>
                                </div>";
                        send_email($email, $subject, $body);
                    }
                    // send flashdata to show success msg
                    $this->session->set_flashdata('FormSubmit', 'success');
                    redirect('forgot', 'refresh');
                } else {
                    // send flashdata to show failure msg
                    $this->session->set_flashdata('FormSubmit', 'failure');
                    redirect('forgot', 'refresh');
                }
            } else {
                $this->session->set_flashdata('item_error', 'Request can not completed, Please try again later.');
                redirect(current_url());
            }
        }
    }

    public function ResetPassword($code) {
        $code = $this->encryption_decryption_object->is_valid_input($code);
        if ($code) {
            // Get the user having this code
            $this->db->select('*');
            $this->db->where('forgotten_password_code', $code);
            $GetUserDetails = $this->db->get('users')->row();

            if (count($GetUserDetails)) {
                $data['user_id'] = $GetUserDetails->id;
                $this->load->view('front-end/reset_forgot_password', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function UpdatePassword_Method() {
        $post_data = $this->input->post();

        if (!empty($post_data)) {
            $uid = $this->encryption_decryption_object->is_valid_input($this->input->post('uid'));
            $code = $this->encryption_decryption_object->is_valid_input($this->input->post('code'));

            if ($uid && $code) {
                $password = $this->input->post('password');
                $confirm_password = $this->input->post('confirm_password');
                if ($password == $confirm_password) {
                    $password = $this->ion_auth_model->hash_password($password);

                    // update password
                    $data = array(
                        'password' => $password
                    );
                    $this->db->where('id', $uid);
                    $UpdateUser = $this->db->update('users', $data);

                    if ($UpdateUser) {
                        // update forgotten_password_code to empty
                        $data = array(
                            'forgotten_password_code' => "",
                            'forgotten_password_time' => ""
                        );
                        $this->db->where('id', $uid);
                        $UpdateUser = $this->db->update('users', $data);
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

}
