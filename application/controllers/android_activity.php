<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Android_activity extends MY_Controller {

    public $AluminiListFull = array();
    public $StudentsListFull = array();
    public $TeacherListFull = array();
    public $college_id;

//    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->load->model('android_model');
        $this->load->model('data_fetch');
        $this->load->model('data_insert');

        $this->college_id = $this->input->post('college_id');
        $user_id = $this->input->post('user_id');
        $this->AluminiListFull = $this->android_model->getUserList($this->college_id, 'alumni', $user_id);
        $this->StudentsListFull = $this->android_model->getUserList($this->college_id, 'student', $user_id);
        $this->TeacherListFull = $this->android_model->getUserList($this->college_id, 'teacher', $user_id);
//        $this->encryption_decryption_object = new Encryption();
//        $token = $this->input->post('token');
//        if ($token == "") {
//            echo "Invalid request";
//        } else{
//            $user_id=$this->input->post('user_id');    
//            $this->user = $this->ion_auth->user($user_id)->row();        
//            $token_from_db = $this->user->android_token;
//            if($token_from_db==""||$token!=$token_from_db){
//                // login error
//                $response["connection"] = "wrong";
//                print(json_encode($response));
//                exit();
//            }
//        }
    }

    function index() {
        
    }

    function get_recent_chats() {
        $user_id = $this->input->post('user_id');

        $result = $this->android_model->get_recent_chat_users($user_id);
        
        $res = array();
        $response["id"] = array();
        $response["name"] = array();
        if ($result) {
            foreach ($result as $r) {
                $response["id"][] = $r->id;
                $response["name"][] = $r->first_name . " " . $r->last_name;
            }
        }
        print(json_encode($response));
    }

    public function get_chat_history() {

        $chatwith = $this->input->post('person_id');
//        print_r($chatwith); die();
//        $this->encryption_decryption_object->encode($chatwith);
//        $chatwith = $this->encryption_decryption_object->encode($person_id);
//        $chatwith = $this->encryption_decryption_object->is_valid_input($chatwith);
        $chat_content = array();
        $user_id = $this->input->post('user_id');

//
        $check_chat_exists = $this->android_model->CheckChatExists($user_id, $chatwith);
        if ($check_chat_exists != 0) {
            $chat_content = $this->android_model->GetChatLast30Messages($check_chat_exists);
            $response["user_id"] = array();
            $response["message"] = array();
            $response["timestamp"] = array();
            if (!empty($chat_content)) {
                foreach ($chat_content as $cc) {
                    $response["user_id"][] = $cc->user_id;
                    $response["message"][] = $cc->message;
                    $response["timestamp"][] = $cc->timestamp;
                }
                print_r(json_encode($response));
                die();
            }
        } else {
            $create_new_chat = $this->android_model->CreateChat($user_id, $chatwith);
        }
        return;
    }

    public function save_chat_message() {

        $chatwith = $this->input->post('person_id');
        $user_id = $this->user->id;
        $chat_id = $this->android_model->CheckChatExists($user_id, $chatwith);
        $message = $this->input->post('chat_message');

        $insertChat = $this->android_model->insertChat($chat_id, $user_id, $message);
        if ($insertChat) {
            $response["isSaved"] = true;
            $timestamp = date('Y-m-d G:i:s');
            $response["time_stamp"] = $timestamp;
            print(json_encode($response));
            $update_result = $this->android_model->updateRecentTime($timestamp, $chat_id);
            //echo $this->ion_auth->user()->row()->id;;
        } else {
            $response["isSaved"] = false;
            print(json_encode($response));
        }
        return;
    }

    public function getLatestMessage() {

        $chatwith = $this->input->post('person_id');
        $timestamp = $this->input->post('time_stamp');
        if ($timestamp == "" || $timestamp == -1) {
            $timestamp = date('Y-m-d G:i:s');
            //echo json_encode($timestamp);
            return;
        }
        $user_id = $this->user->id;
        $chat_id = $this->android_model->CheckChatExists($user_id, $chatwith);
        if ($chat_id != 0) {
            $chat_content = $this->android_model->getLatestMessage($user_id, $chat_id, $timestamp);
            if (!empty($chat_content)) {
                $response["isMessage"] = true;
                foreach ($chat_content as $ch) {
                    $response["user_id"] = $ch->user_id;
                    $response["message"] = $ch->message;
                    $response["timestamp"] = $ch->timestamp;
                }
            } else {
                $response["isMessage"] = false;
            }
            print(json_encode($response));
        }
    }

    public function getAllUsers() {
        $key_word = $this->input->post('key_word');
        $key_word = trim($key_word);
        $result = $this->android_model->allUsers($key_word, $this->user->id);

        if (!empty($result)) {
            $response["isList"] = true;
            $response["id"] = array();
            $response["name"] = array();
            foreach ($result as $r) {
                $response["id"][] = $r->id;
                $response["name"][] = $r->first_name . " " . $r->last_name;
            }
            print(json_encode($response));
        } else {
            $response["isList"] = false;
            print(json_encode($response));
        }
    }

}
