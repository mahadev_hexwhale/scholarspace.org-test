<?php

Class Settings extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');

        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->encryption_decryption_object = new Encryption();
    }

    public function index() {
        $data['user'] = $this->user;
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $data['college_details'] = $this->college_details;
        $colleges = array();

        $query = "SELECT * FROM college_users_intranet WHERE user_id = '$user_id'";
        $query_result = $this->data_fetch->data_query($query);
        foreach ($query_result as $value) {
            $colleges[$value->college_id] = $value;
        }

        $query = "SELECT * FROM college_admin WHERE user_id = '$user_id'";
        $query_result = $this->data_fetch->data_query($query);
        foreach ($query_result as $value1) {
            $colleges[$value1->college_id] = $value1;
        }

        $TotalList = array();
        foreach ($colleges as $value) {
            if (isset($value->intranet_user_type)) {
                $intranet_user_type = $value->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t5.intranet_user_type, "
                                . "t6.college_name "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "INNER JOIN college t6 ON t6.id = '$value->college_id' "
                                . "WHERE t1.college_users_intranet_id = $value->id ";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);
                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $TotalList[] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t5.intranet_user_type, "
                                . "t6.college_name "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "INNER JOIN college t6 ON t6.id = '$value->college_id' "
                                . "WHERE college_users_intranet_id = $value->id ";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $TotalList[] = $value2;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t5.intranet_user_type, "
                                . "t6.college_name "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "INNER JOIN college t6 ON t6.id = '$value->college_id' "
                                . "WHERE college_users_intranet_id = $value->id ";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value3) {
                            $TotalList[] = $value3;
                        }
                        break;
                }
            } else {
                $SelectDetails = "SELECT college_name FROM college WHERE id = '$value->college_id'";
                $SelectDetails_Result = $this->data_fetch->data_query($SelectDetails);
                foreach ($SelectDetails_Result as $value3) {
                    $TotalList[] = $value3;
                }
            }
        }
        $data['temp_array'] = array('', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh');
        $data['colleges_list'] = $TotalList;
        $this->load->view("front-end/user_settings", $data);
    }

    public function ChangeEmail_Method() {
        $user = $this->user;
        $username = $this->user->first_name . " " . $this->user->last_name;
        $old_email = $user->email;
        $college_id = $this->session->userdata('college_id');
        $postdata = $this->input->post();
        $new_email = $this->input->post('email');

        if (!empty($postdata) && $new_email != "") {
            $key_for_activation = date('dmYHis');

            $select_query = "SELECT * FROM temp_email_change WHERE user_id = '$user->id'";
            $select_query_result = $this->data_fetch->data_query($select_query);

            if (count($select_query_result)) {
                $update_query = "UPDATE temp_email_change "
                        . "SET old_email = '$old_email', new_email = '$new_email', activation_key = '$key_for_activation' "
                        . "WHERE user_id = '$user->id' AND status = '0'";
                $update_query_result = $this->data_update->data_query($update_query);
            } else {
                $insert_query = "INSERT INTO temp_email_change "
                        . "(user_id,college_id,old_email,new_email,activation_key,status) "
                        . "VALUES('$user->id','$college_id','$old_email','$new_email','$key_for_activation','0')";
                $update_query_result = $this->data_insert->data_query($insert_query);
            }

            if ($update_query_result) {
                $email = $old_email;
                $subject = "Email verification";
                $body = "<div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
                        <div style='width:680px'>
                        <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
                                <img src='" . site_url() . "img/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
                            </div>
                        <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear $username</p>
                            <p style='margin-top:0px;margin-bottom:15px; color:#000000 !important;width:100%;'>Congratulations! You have been registered for Scholarspace</p>
                            <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Please login to your account using below mentioned link and credentials.<a href='" . site_url() . "test/' target='_blank'> Scholarspace</a>                                
                        </p>
                        <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
                        <thead>
                        <tr>
                        <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222' colspan='2'>Email changed successfully.</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b> Previous email: </b> $old_email </td>
                        </tr>
                        <tr>
                        <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> New email: </b> $new_email </td>
                        </tr>
                        <tr>
                        <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> Click here to verify: </b>" . site_url() . "?uid=" . $this->encryption_decryption_object->encode($user->id) . "&key=" . $this->encryption_decryption_object->encode($key_for_activation) . " </td>
                        </tr>
                        </tbody>
                        </table>
                        <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
                        <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
                        <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholler Space.</p>
                        <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
                        </div>
                        </div>";
                send_email($old_email, $subject, $body);
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpadateSettings_Method() {
        $college_id = $this->session->userdata('college_id');
        $user = $this->user;
        $user_id = $this->user->id;
        $post_data = $this->input->post();
        if (!empty($post_data) && $college_id != "") {
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $location = $this->input->post('location');
            $phone = $this->input->post('phone');
            $dob = $this->input->post('dob');
            $dob = date("Y-m-d", strtotime($dob));

            if ($_FILES['profile_pic']['name']) {
                $target = "assets_front/image/user_profile_pic/";
                $target.=date("dmYhsi") . "-" . basename($_FILES['profile_pic']['name']);
                if (move_uploaded_file($_FILES['profile_pic']['tmp_name'], $target)) {
                    $profile_pic = $target;
                } else {
                    echo 0;
                }
            }

            $query = "UPDATE users "
                    . "SET first_name = '$fname', last_name='$lname', location='$location', phone='$phone', dob='$dob' ";
            if (isset($profile_pic))
                $query .= ", profile_picture = '$profile_pic' ";
            $query .= "WHERE id = '$user_id'";
            $query_result = $this->data_update->data_query($query);
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpadatePassword_Method() {
        $college_id = $this->session->userdata('college_id');
        $user_id = $this->user->id;
        $post_data = $this->input->post();
        $identity = $this->session->userdata('identity');

        if (!empty($post_data) && $college_id != "") {

            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');

            if ($new_password == $confirm_password) {
                $password_matches = $this->ion_auth->hash_password_db($user_id, $old_password);
                if ($password_matches) {
                    $changed = $this->ion_auth->change_password($identity, $old_password, $new_password);
                    if ($changed) {
                        echo "Password changed successfully.";
                    } else {
                        echo "Something went wrong.. Try Again!!";
                    }
                } else {
                    echo "Current password in incorrect!!";
                }
            } else {
                echo "Passwords doesn't match. Please try again.";
            }
        } else {
            echo "Something went wrong.. Try Again!!";
        }
    }

    function check_unique_phone() {
        if (!empty($_POST)) {
            $phone = $this->input->post('mobile');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'phone', $phone)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function check_unique_email() {
        if (!empty($_POST)) {
            $email = $this->input->post('email');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'email', $email)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

}
