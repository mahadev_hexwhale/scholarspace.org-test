<?php

Class Setting extends UserClass {

    private $user = null;
    private $college_details = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;
        $this->load->view("front-end/setting", $data);
    }

}
