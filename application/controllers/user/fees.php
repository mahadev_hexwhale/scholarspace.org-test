<?php

/* class is to mainten the user request from the college admin */

Class Fees extends MY_Fixedgroup {
    
    private $college_details = null;

    function __construct() {
        parent::__construct();
        $this->group_name = "fees";
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails(); // Function is getting called by helper
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $data = $this->get_group_post(); //get all the post for this group from MY_Fixedgroup cotroller
        $data['college_details'] = $this->college_details;

        $college_id = $this->college_id;
        $this->load->view("front-end/fees", $data);
    }

    function manage() {
        $college_id = $this->college_id;

        $sql_query = "SELECT a.`stream_id`, b.`title` FROM `college_streams` as a INNER JOIN `stream` as b ON a.`stream_id`=b.`id` WHERE `college_id` = '246'";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['college_stream_list'] = $query_result;
        $data['stream_name'] = array();
        $data['college_stream_course_list'] = array();

        //get college stream courses
        foreach ($data['college_stream_list'] as $value) {
            $stream_id = $value->stream_id;

            $sql_query = "SELECT a.`course_id`, b.`title` FROM `college_stream_course` AS a INNER JOIN `stream_courses` as b ON a.`course_id` = b.`id` WHERE a.`stream_id` = '$stream_id' AND a.`college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['stream_name'][$stream_id] = $value->title;

            $data['college_stream_course_list'][$stream_id] = $query_result;
        }
        $this->load->view("front-end/manage_fees", $data);
    }

    function edit_fees_submit() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['stream']) && $this->encryption_decryption_object->is_valid_input($posted_data['stream']) && !empty($posted_data['stream_courses']) && $this->encryption_decryption_object->is_valid_input($posted_data['stream_courses'])) {
            $college_id = $this->college_id;

            $stream_id = $this->encryption_decryption_object->is_valid_input($posted_data['stream']);
            $stream_courses_id = $this->encryption_decryption_object->is_valid_input($posted_data['stream_courses']);

            $first_year_fee = $posted_data['first_year_fee'];
            $second_year_fee = $posted_data['second_year_fee'];
            $third_year_fee = $posted_data['third_year_fee'];
            $forth_year_fee = $posted_data['forth_year_fee'];

            //check whether entry is there or not
            $sql_query = "SELECT `id` FROM `college_fee_structure_years` WHERE `stream_id` = '$stream_id' AND `stream_course_id` = '$stream_courses_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                
            } else {
                $sql_query = "INSERT INTO `college_fee_structure_years`(`first_year_fee`, `second_year_fee`, `third_year_fee`, `forth_year_fee`, `stream_id`, `stream_course_id`, `college_id`) VALUES('$first_year_fee','$second_year_fee','$third_year_fee','$forth_year_fee','$stream_id','$stream_courses_id','$college_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            }
        }
    }

    function get_user_list_to_notify() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['flag'])) {
            $college_id = $this->college_id;
            $sql_query = "SELECT b.`id`,b.`email`,b.`first_name`,b.`last_name`,b.`profile_picture`,"
                    . "c.`stream_id`,c.`stream_course_id`,c.`study_type`,c.`semester_or_year`,"
                    . " d.`title`, e.`title` FROM `college_users_intranet` as a INNER JOIN `users` as b ON a.`user_id` = b.`id`"
                    . " INNER JOIN `college_student_users_intranet` as c ON a.`id` = c.`college_users_intranet_id` "
                    . "INNER JOIN `stream` as d ON c.`stream_id`= d.`id` "
                    . "INNER JOIN `stream_courses` as e ON c.`stream_course_id` = e.`id` "
                    . "WHERE a.`college_id` = '$college_id' AND a.`intranet_user_type` = 'student'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $user_list = array();

            foreach ($query_result as $value) {
                $user_list[$this->encryption_decryption_object->encode($value->id)] = $value;
            }
            echo json_encode($user_list);
        }
    }

}

?>