<?php

Class User_profile extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->encryption_decryption_object = new Encryption();
    }

    public function index() {
        $data['user'] = $this->user;
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $data['college_details'] = $this->college_details;

        $this->load->view("front-end/user_profile", $data);
    }

    public function view($profile_id) {
        $data['user'] = $this->user;
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $data['college_details'] = $this->college_details;

        $profile_id = $this->encryption_decryption_object->is_valid_input($profile_id);
        if ($profile_id) {

            // Get user details
            $query_info = "SELECT t1.id as profile_id,t1.email,t1.first_name,t1.last_name,t1.phone,t1.location,t1.profile_picture,t1.dob "
                    . "FROM users t1 "
                    . "WHERE t1.id = '$profile_id'";
            $result_query_info = $this->data_fetch->data_query($query_info);
            $data['profile_info'] = $result_query_info[0];

            // Get college details which he is a member
            $GetColleges = "SELECT t1.*,t1.id AS user_intranet_id, t2.college_name, t2.id as college_id "
                    . "FROM college_users_intranet t1 "
                    . "INNER JOIN college t2 ON t1.college_id = t2.id "
                    . "WHERE user_id = '$profile_id'";
            $data['my_colleges'] = $GetColleges_result = $this->data_fetch->data_query($GetColleges);

            // Get detailed info of stream, course etc..,
            $my_colleges_info = array();
            foreach ($GetColleges_result as $value) {
                switch ($value->intranet_user_type) {
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.title AS stream_name, "
                                . "t3.title AS stream_course_name, "
                                . "t4.intranet_user_type "
                                . "FROM college_alumni_users_intranet t1 "
                                . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                                . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                                . "INNER JOIN college_users_intranet AS t4 ON t1.college_users_intranet_id = t4.id "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        $my_colleges_info[$value->college_id] = $SelectDetailsOfAlumni_Result[0];
                        break;
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.title AS stream_name, "
                                . "t3.title AS stream_course_name, "
                                . "t4.intranet_user_type "
                                . "FROM college_student_users_intranet t1 "
                                . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                                . "INNER JOIN stream_courses as t3 ON t1.stream_course_id = t3.id "
                                . "INNER JOIN college_users_intranet t4 ON t1.college_users_intranet_id = t4.id "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);
                        $my_colleges_info[$value->college_id] = $SelectDetailsOfStudent_Result[0];
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.* "
                                . "FROM college_teacher_users_intranet t1 "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        $my_colleges_info[$value->college_id] = $SelectDetailsOfTeacher_Result[0];
                        break;
                }
            }
            $data['temp_array'] = array('', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh');
            $data['my_colleges_info'] = $my_colleges_info;

            // Get all groups in which he is a member
            $GetGroup = "SELECT t1.id as group_id,t1.name,t1.group_admin_user_id,t1.group_icon_image "
                    . "FROM college_groups t1 "
                    . "INNER JOIN college_group_users t2 ON t2.group_id = t1.id "
                    . "WHERE visible_to = 'Public' AND group_access IN('Public','Private') "
                    . "AND t2.user_id = '$profile_id'";
            $GetGroup_Result = $this->data_fetch->data_query($GetGroup);
            $data['groups_list'] = $GetGroup_Result;

            // Get all the users which he is following
            $GetFollowings = "SELECT followed_by_user_id,following_user_id FROM college_users_follow WHERE followed_by_user_id = '$profile_id'";
            $data['following_list'] = $this->data_fetch->data_query($GetFollowings);

            // Get all the users who are following him
            $GetFollowers = "SELECT followed_by_user_id,following_user_id FROM college_users_follow WHERE following_user_id = '$profile_id'";
            $data['followers_list'] = $this->data_fetch->data_query($GetFollowers);

            // Get all the posts made by the user
            $GetHomePosts = "SELECT t1.*, t2.first_name, t2.last_name, t2.profile_picture "
                    . "FROM college_fixed_group_posts t1 "
                    . "INNER JOIN users t2 ON t1.user_id = t2.id "
                    . "WHERE t1.user_id = '$profile_id' AND t1.college_id = '$college_id' AND post_group_name = 'home' "
                    . "GROUP BY t1.post_id "
                    . "ORDER BY t1.timestamp DESC ";
            $GetHomePosts_Result = $this->data_fetch->data_query($GetHomePosts);
            $data['user_posts'] = $GetHomePosts_Result;
            $PostsArray = array();
            foreach ($GetHomePosts_Result as $value) {
                $PostsArray[$value->post_id] = $value->post_id;
            }
            $data['all_posts'] = $this->posts_model->get_all_posts('fixed', $PostsArray);

            $this->load->view("front-end/user_profile", $data);
        } else {
            show_404();
        }
    }

}
