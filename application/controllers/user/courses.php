<?php

/* class is to maintain the user request from the college admin */

Class Courses extends UserClass {

    public $user = null;
    public $college_id = null;
    public $college_details = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->load->model('courses_model');
        $this->load->model('usersearch');
        $this->load->model('posts_model');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->group_name = "courses";
        $this->user = $this->ion_auth->user()->row();
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->college_id = $this->session->userdata('college_id');
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {

        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;
        $user_id = $this->user->id;

        $query = "SELECT * FROM college_users_intranet WHERE user_id = '$user_id'";
        $data['user_details'] = $this->data_fetch->data_query($query);

        if ($data['user_details'][0]->intranet_user_type == "teacher") {
            /* ...............Get all the Published courses of teacher................ */
            $data['past_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "past", "published"); // Past Courses
            $data['my_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "present", "published"); // Present Courses
            $data['future_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "future", "published"); // Future Courses

            /* ...............Get all Drafted the courses of teacher................ */
            $data['drafted_past_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "past", "drafted"); // Past Courses
            $data['drafted_my_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "present", "drafted"); // Present Courses
            $data['drafted_future_courses'] = $this->courses_model->GetTeacherCourses($college_id, $user_id, "future", "drafted"); // Future Courses
        } else {
            /* ...............Get all Drafted the courses of member................ */
            $data['past_courses'] = $this->courses_model->GetMemberCourses($college_id, $user_id, "past", "published"); // Past Courses
            $data['my_courses'] = $this->courses_model->GetMemberCourses($college_id, $user_id, "present", "published"); // Present Courses
            $data['future_courses'] = $this->courses_model->GetMemberCourses($college_id, $user_id, "future", "published"); // Future Courses
        }
        /* Load view page */
        $this->load->view("front-end/courses", $data);
    }

    public function GetTeacherEmail() {
        $college_id = $this->college_id;
        $query_variable = $this->input->post("email");
        $query = "SELECT t1.user_id, " . "t2.email " . "FROM college_users_intranet t1 " . "INNER JOIN users t2 ON t2.id = t1.user_id " . "WHERE t1.college_id = '$college_id' AND t1.intranet_user_type = 'teacher' " . "AND t2.email LIKE '$query_variable%' LIMIT 10";
        $teacher_details_array = $this->data_fetch->data_query($query);

        $teacher_details = array();
        foreach ($teacher_details_array as $key => $value) {
            $teacher_details[$value->user_id] = $value->email;
        }
        echo json_encode($teacher_details);
    }

    public function GetStudentEmail() {
        $college_id = $this->college_id;
        $query_variable = $this->input->post("email");
        $query = "SELECT t1.user_id, " . "t2.email " . "FROM college_users_intranet t1 " . "INNER JOIN users t2 ON t2.id = t1.user_id " . "WHERE t1.college_id = '$college_id' AND t1.intranet_user_type = 'student' " . "AND t2.email LIKE '$query_variable%' LIMIT 10";
        $teacher_details_array = $this->data_fetch->data_query($query);

        $teacher_details = array();
        foreach ($teacher_details_array as $key => $value) {
            $teacher_details[$value->user_id] = $value->email;
        }
        echo json_encode($teacher_details);
    }

    public function view($course_id) {
        $data['user'] = $this->user;
        $user_id = $this->user->id;
        $data['college_details'] = $this->college_details;

        //Get all badge images
        $data['badges'] = $this->data_fetch->getBadges();

        $query = "SELECT * FROM college_users_intranet WHERE user_id = '$user_id'";
        $data['user_details'] = $this->data_fetch->data_query($query);

        $course_id = $this->encryption_decryption_object->is_valid_input($course_id);
        if ($course_id) {
            //get college details
            $sql_query = "SELECT * FROM `college_course` WHERE `id` = '$course_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['course_details'] = $query_result[0];

                //get all student list
                $sql_query = "SELECT ccgm.`user_id`, ud.`first_name`, ud.`last_name`, cui.`id` as `intranet_id` " . "FROM `college_course_members` as ccgm " . "INNER JOIN `users` as ud ON ccgm.`user_id`= ud.`id` " . "INNER JOIN `college_users_intranet` as cui ON ccgm.`user_id`= cui.`user_id` " . "WHERE `course_id` = '$course_id' AND ccgm.user_type IN('member','assistant-student')";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['group_student_list'] = $query_result;

                $sql_query = "SELECT t2.`title` as stream_name, t3.`title` as stream_course_name, t1.`college_users_intranet_id`, " . "t1.`study_type`, t1.`semester_or_year`, t4.user_id " . "FROM `college_student_users_intranet` AS t1 " . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` " . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` " . "INNER JOIN college_users_intranet t4 ON t4.id = t1.college_users_intranet_id " . "WHERE t1.`college_users_intranet_id` IN " . "(SELECT cui.`id` as `intranet_id` FROM `college_course_members` as ccgm " . "INNER JOIN `users` as ud ON ccgm.`user_id`= ud.`id` " . "INNER JOIN `college_users_intranet` as cui ON ccgm.`user_id`= cui.`user_id` " . "WHERE `course_id` = '$course_id' AND ccgm.user_type IN('member','assistant-student'))";
                $query_result = $this->data_fetch->data_query($sql_query);

                $attendance = array();
                $marks = array();
                $final_marks = array();
                $midterm_marks = array();
                $grand_final_marks = array();
                $student_details_array = array();
                $FixedPostsArray = array();
                foreach ($query_result as $value) {
                    $student_details_array[$value->college_users_intranet_id] = $value;

                    // Get Attendance of a user
                    $query1 = "SELECT count(t1.id) AS classes, t1.user_id " . "FROM `college_course_members_attendance` t1 " . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$value->user_id' AND attendance = '1'";
                    $query1_result = $this->data_fetch->data_query($query1);
                    foreach ($query1_result as $attend) {
                        $attendance[$value->user_id] = $attend->classes;
                    }

                    // Get obtained marks of a user
                    $query3 = "SELECT DISTINCT(test_id), SUM(obtained_marks) AS obtained_marks " . "FROM `college_course_members_marks` t1 " . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$value->user_id'";
                    $query3_result = $this->data_fetch->data_query($query3);
                    foreach ($query3_result as $mark) {
                        $marks[$value->user_id] = $mark->obtained_marks;
                    }

                    // Get Midterm marks of a user
                    $query4 = "SELECT * " . "FROM `college_course_members_midterm_marks` t1 " . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$value->user_id'";
                    $query4_result = $this->data_fetch->data_query($query4);
                    foreach ($query4_result as $mark) {
                        $midterm_marks[$value->user_id] = $mark->obtained_marks;
                    }

                    // Get External marks of a user
                    $query5 = "SELECT * " . "FROM `college_course_members_finalmarks` t1 " . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$value->user_id'";
                    $query5_result = $this->data_fetch->data_query($query5);
                    foreach ($query5_result as $mark) {
                        $final_marks[$value->user_id] = $mark->obtained_marks;
                    }

                    // Get Final marks of a user
                    $query6 = "SELECT * "
                            . "FROM `college_course_members_total_final_marks` t1 "
                            . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$value->user_id'";
                    $query6_result = $this->data_fetch->data_query($query6);
                    foreach ($query6_result as $mark) {
                        $grand_final_marks[$value->user_id] = $mark->obtained_marks;
                    }
                }

                // Get total IA marks
                $query2 = "SELECT DISTINCT(test_id), t1.total_marks "
                        . "FROM `college_course_members_marks` t1 "
                        . "WHERE t1.course_id = '$course_id'";
                $query2_result = $this->data_fetch->data_query($query2);
                $total_marks = 0;
                foreach ($query2_result as $mark) {
                    $total_marks += $mark->total_marks;
                }
                $data['total_ia_marks'] = $total_marks;

                // Get total Midterm marks
                $query2 = "SELECT DISTINCT(id), t1.total_marks " . "FROM `college_course_members_midterm_marks` t1 " . "WHERE t1.course_id = '$course_id' " . "GROUP BY course_id";
                $query2_result = $this->data_fetch->data_query($query2);
                if (count($query2_result))
                    $data['total_midterm_marks'] = $query2_result[0]->total_marks;
                else
                    $data['total_midterm_marks'] = 0;

                // Get total Final marks
                $query2 = "SELECT DISTINCT(id), t1.total_marks " . "FROM `college_course_members_finalmarks` t1 " . "WHERE t1.course_id = '$course_id' " . "GROUP BY course_id";
                $query2_result = $this->data_fetch->data_query($query2);
                if (count($query2_result))
                    $data['total_external_marks'] = $query2_result[0]->total_marks;
                else
                    $data['total_external_marks'] = 0;

                // Get total Final marks
                $query2 = "SELECT DISTINCT(id), t1.total_marks " . "FROM `college_course_members_total_final_marks` t1 " . "WHERE t1.course_id = '$course_id' " . "GROUP BY course_id";
                $query2_result = $this->data_fetch->data_query($query2);
                if (count($query2_result))
                    $data['total_final_marks'] = $query2_result[0]->total_marks;
                else
                    $data['total_final_marks'] = 0;

                // Get total Conducted classes
                $query = "SELECT DISTINCT(class_id) AS classes FROM `college_course_members_attendance` WHERE course_id = '$course_id' ";
                $query_result = $this->data_fetch->data_query($query);
                if (count($query_result))
                    $data['TotalConductedClasses'] = count($query_result);
                else
                    $data['TotalConductedClasses'] = 0;

                // Get all the posts from this course
                $sql_query = "SELECT t2.* FROM `college_course_posts` t1 "
                        . "INNER JOIN college_fixed_group_posts t2 ON t2.post_id = t1.post_id "
                        . "WHERE t1.course_id = '$course_id' " 
                        . "ORDER BY timestamp DESC";
                $query_result3 = $this->data_fetch->data_query($sql_query);
                $data['course_posts'] = $query_result3;
                foreach ($query_result3 as $value) {
                    $FixedPostsArray[$value->post_id] = $value->post_id;
                }

                if (count($FixedPostsArray)) {
                    $FixedPostDetails = $this->posts_model->get_all_posts("fixed", $FixedPostsArray);
                    $data['all_posts'] = $FixedPostDetails;
                }
                $data['group_student_details'] = $student_details_array;
                $data['attendance'] = $attendance;
                $data['marks'] = $marks;
                $data['final_marks'] = $final_marks;
                $data['midterm_marks'] = $midterm_marks;
                $data['grand_final_marks'] = $grand_final_marks;
            }
            $this->load->view("front-end/course_page", $data);
        }
    }

    public function settings($course_id) {
        $data['user'] = $this->user;
        $user_id = $this->user->id;
        $data['college_details'] = $this->college_details;

        $query = "SELECT * FROM college_users_intranet WHERE user_id = '$user_id'";
        $user_details = $this->data_fetch->data_query($query);

        if ($user_details[0]->intranet_user_type == "teacher") {
            $course_id = $this->encryption_decryption_object->is_valid_input($course_id);

            if ($course_id) {
                //get college details
                $sql_query = "SELECT * FROM `college_course` WHERE `id` = '$course_id'";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['course_details'] = $query_result[0];

                    //get all student list
                    $sql_query = "SELECT ccgm.`user_id`, ud.`first_name`, ud.`last_name`, cui.`id` as `intranet_id` "
                            . "FROM `college_course_members` as ccgm "
                            . "INNER JOIN `users` as ud ON ccgm.`user_id`= ud.`id` "
                            . "INNER JOIN `college_users_intranet` as cui ON ccgm.`user_id`= cui.`user_id` "
                            . "WHERE `course_id` = '$course_id' AND ccgm.user_type IN('member','assistant-student')";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['group_student_list'] = $query_result;

                    $sql_query = "SELECT t2.`title` as stream_name, t3.`title` as stream_course_name, t1.`college_users_intranet_id`, "
                            . "t1.`study_type`, t1.`semester_or_year`, t4.user_id "
                            . "FROM `college_student_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN college_users_intranet t4 ON t4.id = t1.college_users_intranet_id "
                            . "WHERE t1.`college_users_intranet_id` IN "
                            . "(SELECT cui.`id` as `intranet_id` FROM `college_course_members` as ccgm "
                            . "INNER JOIN `users` as ud ON ccgm.`user_id`= ud.`id` "
                            . "INNER JOIN `college_users_intranet` as cui ON ccgm.`user_id`= cui.`user_id` " . "WHERE `course_id` = '$course_id' AND ccgm.user_type IN('member','assistant-student'))";
                    $query_result = $this->data_fetch->data_query($sql_query);

                    $getAssistants = "SELECT user_id,user_type FROM college_course_members "
                            . "WHERE course_id = '$course_id' AND user_type = 'assistant-student'";
                    $getAssistants_result = $this->data_fetch->data_query($getAssistants);
                    $data['assistants'] = $getAssistants_result;

                    $getAssociates = "SELECT user_id,user_type FROM college_course_members "
                            . "WHERE course_id = '$course_id' AND user_type = 'associate-teacher'";
                    $getAssociates_result = $this->data_fetch->data_query($getAssociates);
                    $data['associates'] = $getAssociates_result;

                    $user_iamarks = $midterm_marks = $external_marks = $final_marks = array();
                    $tests_array = $classes_array = array();
                    $student_details_array = array();
                    $user_attendance = array();

                    foreach ($query_result as $value) {
                        $student_details_array[$value->college_users_intranet_id] = $value;

                        // Get Attendance of a user
                        $query1 = "SELECT class_id, attendance FROM `college_course_members_attendance` "
                                . "WHERE course_id = '$course_id' AND user_id = '$value->user_id'";
                        $query1_result = $this->data_fetch->data_query($query1);
                        foreach ($query1_result as $attend) {
                            $user_attendance[$value->user_id][$attend->class_id] = $attend->attendance;
                        }

                        $query_iamarks = "SELECT test_id, obtained_marks,total_marks FROM `college_course_members_marks` "
                                . "WHERE course_id = '$course_id' AND user_id = '$value->user_id'";
                        $query_iamarks_result = $this->data_fetch->data_query($query_iamarks);
                        foreach ($query_iamarks_result as $mark) {
                            $user_iamarks[$value->user_id][$mark->test_id] = $mark->obtained_marks;
                        }
                        // Get Midterm marks of a user
                        $midterm_marks[$value->user_id] = $this->courses_model->GetMarksofMember($course_id, $value->user_id, "midterm");
                        // Get External marks of a user
                        $external_marks[$value->user_id] = $this->courses_model->GetMarksofMember($course_id, $value->user_id, "external");
                        // Get Final marks of a user
                        $final_marks[$value->user_id] = $this->courses_model->GetMarksofMember($course_id, $value->user_id, "final");
                    }

                    $data['group_student_details'] = $student_details_array;
                    $data['external_marks'] = $external_marks;
                    $data['final_marks'] = $final_marks;
                    $data['midterm_marks'] = $midterm_marks;
                    $data['grand_final_marks'] = $final_marks;
                    $data['user_attendance'] = $user_attendance;
                    $data['user_iamarks'] = $user_iamarks;
                }
                $query = "SELECT DISTINCT(class_id),date FROM college_course_members_attendance WHERE course_id = '$course_id'";
                $query_result = $this->data_fetch->data_query($query);
                foreach ($query_result as $value) {
                    $classes_array[$value->class_id] = $value->date;
                }
                $data['classes_array'] = $classes_array;

                $query = "SELECT DISTINCT(test_id),date,total_marks FROM college_course_members_marks WHERE course_id = '$course_id'";
                $query_result = $this->data_fetch->data_query($query);
                foreach ($query_result as $value) {
                    $tests_array[$value->test_id] = $value;
                }
                $data['tests_array'] = $tests_array;

                $this->load->view("front-end/course_settings", $data);
            }
        } else {
            show_404();
        }
    }

    function get_eligible_student() {
        $final_list = array();
        $posted_data = $this->input->post();

        if ($posted_data['flag'] == "success") {
            $filter = $this->input->post('filter') != "" ? $this->input->post('filter') : "";
            $course_id = $this->input->post('course_id');
            $college_id = $this->college_id;

            //count array list
            $number_to_word = array(1 => "First", 2 => "Second", 3 => "Third", 4 => "Forth", 5 => "Fifth", 6 => "Sixth", 7 => "Seventh", 8 => "Eighth", 9 => "Ninth", 10 => "Tenth", 11 => "Eleventh", 12 => "Twelth");

            /* ............................. Student List ............................ */
            $sql_query = "SELECT b.id, b.email, b.first_name, b.last_name, b.profile_picture,"
                    . "c.stream_id,c.stream_course_id,c.study_type,c.semester_or_year, "
                    . "d.title as stream_title, e.title as course_title "
                    . "FROM college_users_intranet as a "
                    . "INNER JOIN users as b ON a.user_id = b.id "
                    . "INNER JOIN college_student_users_intranet as c ON a.id = c.college_users_intranet_id "
                    . "INNER JOIN stream as d ON c.stream_id= d.id "
                    . "INNER JOIN stream_courses as e ON c.stream_course_id = e.id "
                    . "LEFT JOIN college_course_members ccm ON ccm.user_id = b.id "
                    . "WHERE a.college_id = '$college_id' AND a.intranet_user_type = 'student' ";

            // if request coming from a course settings page
            if ($course_id != "") {
                $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
                if ($course_id) {
                    // get the user of that course
                    $GetCourseUsers = "SELECT user_id FROM college_course_members WHERE course_id = '$course_id'";
                    $GetCourseUsers_result = $this->data_fetch->data_query($GetCourseUsers);
                    if (count($GetCourseUsers_result)) {
                        $users = array();
                        foreach ($GetCourseUsers_result as $value) {
                            $users[] = $value->user_id;
                        }
                    }
                    $users = implode(',', $users);
                    // exclude those users from result
                    $sql_query .= "AND b.id NOT IN ($users) ";
                }
            }

            // if request is coming on keyup we get the search string
            if ($filter != "") {
                $sql_query .= "AND (b.first_name LIKE '$filter%' OR b.last_name LIKE '$filter%' OR b.email LIKE '$filter%') ";
            }
            $query_result = $this->data_fetch->data_query($sql_query);

            $user_list = array();
            foreach ($query_result as $value) {
                $user_list[$this->encryption_decryption_object->encode($value->id)] = $value;
            }
            $final_list['student_list'] = $user_list;

            /* ............................. Student List Batch wise ............................ */
            $sql_query = "SELECT s.title as stream_name, s_c.title as stream_course_name, "
                    . "c_s_u_i.study_type, c_s_u_i.college_users_intranet_id, c_s_u_i.semester_or_year, u.* "
                    . "FROM college_student_users_intranet as c_s_u_i "
                    . "INNER JOIN stream as s ON c_s_u_i.stream_id = s.id "
                    . "INNER JOIN stream_courses as s_c ON c_s_u_i.stream_course_id = s_c.id "
                    . "INNER JOIN college_users_intranet as c_u_i ON c_s_u_i.college_users_intranet_id = c_u_i.id "
                    . "RIGHT JOIN users as u ON c_u_i.user_id = u.id "
                    . "WHERE c_u_i.college_id = '$college_id' AND c_u_i.intranet_user_type = 'student' ";

            // if request coming from a course settings page
            if ($course_id != "") {
                $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
                if ($course_id) {
                    // get the user of that course
                    $GetCourseUsers = "SELECT user_id FROM college_course_members WHERE course_id = '$course_id'";
                    $GetCourseUsers_result = $this->data_fetch->data_query($GetCourseUsers);
                    if (count($GetCourseUsers_result)) {
                        $users = array();
                        foreach ($GetCourseUsers_result as $value) {
                            $users[] = $value->user_id;
                        }
                    }
                    $users = implode(',', $users);
                    // exclude those users from result
                    $sql_query .= "AND u.id NOT IN ($users) ";
                }
            }
            // if request is coming on keyup we get the search string
            if ($filter != "") {
                $sql_query .= "AND (s.`title` LIKE '$filter%' OR s_c.`title` LIKE '$filter%' ) ";
            }
            $query_result = $this->data_fetch->data_query($sql_query);

            foreach ($query_result as $value) {
                $final_list['student_batch_list']["<h5>" . $value->stream_name . " (" . $number_to_word[$value->semester_or_year] . " " . ucfirst($value->study_type) . ")</h5><h6>" . $value->stream_course_name . "</h6>"][] = $this->encryption_decryption_object->encode($value->id);
            }
            echo json_encode($final_list);
        }
    }

    function save_course() {
        $posted_data = $this->input->post();

        if (!empty($posted_data)) {
            $college_id = $this->college_id;
            $logged_user_id = $this->user->id;

            $course_title = ($posted_data['course_title']);
            $couser_brief = htmlentities($posted_data['couser_brief']);

            $course_start_date = date("Y-m-d", strtotime(str_replace("/", "-", $posted_data['course_start_date'])));
            $course_end_date = date("Y-m-d", strtotime(str_replace("/", "-", $posted_data['course_end_date'])));
            $total_classes = $posted_data['total_classes'];
            $midterm = $this->input->post('midterm') == "on" ? "1" : "0";

            $selected_users = $posted_data['selected_users'];
            $selected_users_group = $posted_data['selected_users_group'];

            $addtional_teacher = $this->input->post('addtional_teacher');
            $assistants = $this->input->post('assistants');

            if ($posted_data['submit'] == "save_as_draft") {
                $save_as_draft = 1;
            } else if ($posted_data['submit'] == "publish") {
                $save_as_draft = 0;
            }
            $group_member_list_array = array();
            foreach (json_decode($selected_users) as $key => $value) {
                $group_user_id = $this->encryption_decryption_object->is_valid_input($key);
                if ($group_user_id) {
                    $group_member_list_array[] = $group_user_id;
                }
            }

            foreach (json_decode($selected_users_group) as $key => $users_list) {
                $users_list = explode(',', $users_list);
                foreach ($users_list as $value) {
                    $value = trim($value);
                    $group_user_id = $this->encryption_decryption_object->is_valid_input($value);
                    if ($group_user_id) {
                        $group_member_list_array[] = $group_user_id;
                    }
                }
            }
            $group_member_list_array = array_unique($group_member_list_array);
            $timestamp = date("Y-m-d H:i:s");

            /* Add new Course */
            $table_name = 'college_course';
            $insert_data = array(
                'college_id' => $college_id,
                'created_teacher_id' => $logged_user_id,
                'start_date' => $course_start_date,
                'end_date' => $course_end_date,
                'total_classes' => $total_classes,
                'midterm' => $midterm,
                'course_name' => $course_title,
                'course_brief' => $couser_brief,
                'created_date' => $timestamp,
                'save_and_draft' => $save_as_draft
            );
            $query_result = $this->data_insert->add_fields($insert_data, $table_name);
            if ($query_result) {
                $inserted_group_id = $query_result;

                // Insert admin to the course
                $sql_query = "INSERT INTO `college_course_members`(`course_id`, `user_id`, `user_type`, `activate`) "
                        . "VALUES('$inserted_group_id', '$logged_user_id', 'course-admin', '1')";
                $query_result = $this->data_insert->data_query($sql_query);

                // Insert all the associate teachers to table
                if (count($addtional_teacher) > 0 && $addtional_teacher[0] != "") {
                    foreach ($addtional_teacher as $value) {
                        $GetUserID = "SELECT id FROM users WHERE email = '$value'";
                        $GetUserID_result = $this->data_fetch->data_query($GetUserID);
                        $user_id = $GetUserID_result[0]->id;

                        $sql_query = "INSERT INTO `college_course_members`(`course_id`, `user_id`, `user_type`) " . "VALUES('$inserted_group_id', '$user_id', 'associate-teacher')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }

                $user_id_array = array();
                // Insert all the assistant students to table
                if (count($assistants) > 0 && $assistants[0] != "") {
                    foreach ($assistants as $value) {
                        $GetUserID = "SELECT id FROM users WHERE email = '$value'";
                        $GetUserID_result = $this->data_fetch->data_query($GetUserID);
                        $user_id = $GetUserID_result[0]->id;
                        $user_id_array[] = $GetUserID_result[0]->id;
                        $sql_query = "INSERT INTO `college_course_members`(`course_id`, `user_id`, `user_type`) " . "VALUES('$inserted_group_id', '$user_id', 'assistant-student')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }

                /* Insert course group Member */
                foreach ($group_member_list_array as $key => $value) {
                    if (!in_array($value, $user_id_array)) {
                        $sql_query = "INSERT INTO `college_course_members`(`course_id`, `user_id`, `user_type`) VALUES('$inserted_group_id', '$value', 'member')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        }
    }

    public function EditTotalClasses_method() {
        $post_data = $this->input->post();

        if (!empty($post_data) && $this->input->post('total_classes') != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));

            if ($course_id) {
                $total_classes = $this->input->post('total_classes');
                $sql_query = "UPDATE college_course SET total_classes = '$total_classes' WHERE id = '$course_id'";
                $query_result = $this->data_update->data_query($sql_query);
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function FinalizeCourse_Method() {
        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['midterm_percent'] != "" && $post_data['final_percent'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $ia_percent = $this->input->post('ia_percent');
                $midterm_percent = $this->input->post('midterm_percent');
                $final_percent = $this->input->post('final_percent');

                $final_total_marks = $this->input->post('final_total_marks');
                $date = date('Y-m-d');

                // Get total IA marks
                $GetIATotalMarks = "SELECT DISTINCT(test_id), t1.total_marks "
                        . "FROM `college_course_members_marks` t1 "
                        . "WHERE t1.course_id = '$course_id'";
                $GetIATotalMarks_result = $this->data_fetch->data_query($GetIATotalMarks);
                $total_ia_marks = 0;
                foreach ($GetIATotalMarks_result as $mark) {
                    $total_ia_marks += $mark->total_marks;
                }
                
                $query = "SELECT * FROM college_course_members WHERE course_id = '$course_id' AND user_type = 'member'";
                $result = $this->data_fetch->data_query($query);
                foreach ($result as $value) {
                    $user_id = $value->user_id;

                    // Calculate the IA exam marks
                    $ia_marks = 0;
                    $GetTotalObtainedIAMarks = "SELECT DISTINCT(test_id), SUM(obtained_marks) AS obtained_marks "
                            . "FROM `college_course_members_marks` t1 "
                            . "WHERE t1.course_id = '$course_id' AND t1.user_id = '$user_id' "
                            . "LIMIT 0,1";
                    $GetTotalObtainedIAMarks_result = $this->data_fetch->data_query($GetTotalObtainedIAMarks);
                    if (count($GetTotalObtainedIAMarks_result)) {
                        $obtained_ia_marks = $GetTotalObtainedIAMarks_result[0]->obtained_marks;
                        if ($obtained_ia_marks)
                            $ia_marks = ($obtained_ia_marks * $ia_percent) / $total_ia_marks;
                    }

                    // Calculate the midterm exam marks
                    $midterm_marks = 0;
                    $getMidetermMarks = "SELECT * FROM college_course_members_midterm_marks " . "WHERE course_id = '$course_id' AND user_id = '$user_id'";
                    $getMidetermMarks_result = $this->data_fetch->data_query($getMidetermMarks);
                    if (count($getMidetermMarks_result)) {
                        $obtained_midterm_marks = $getMidetermMarks_result[0]->obtained_marks;
                        $total_midterm_marks = $getMidetermMarks_result[0]->total_marks;
                        $midterm_marks = ($obtained_midterm_marks * $midterm_percent) / $total_midterm_marks;
                    }

                    // Calculate the external exam marks
                    $final_marks = 0;
                    $getFinalMarks = "SELECT * FROM college_course_members_finalmarks " . "WHERE course_id = '$course_id' AND user_id = '$user_id'";
                    $getFinalMarks_result = $this->data_fetch->data_query($getFinalMarks);
                    if (count($getFinalMarks_result)) {
                        $obtained_final_marks = $getFinalMarks_result[0]->obtained_marks;
                        $total_final_marks = round($getFinalMarks_result[0]->total_marks);
                        $final_marks = round(($obtained_final_marks * $final_percent) / $total_final_marks);
                    }
                    if ($ia_marks != 0 && $final_marks != 0) {
                        $total_obtained_final_mark = $ia_marks + $midterm_marks + $final_marks;
                        $insert = "INSERT INTO college_course_members_total_final_marks "
                                . "(course_id, user_id, obtained_marks, total_marks, midterm_percent, final_percent, date) "
                                . "VALUES('$course_id', '$user_id', '$total_obtained_final_mark', '$final_total_marks', '$midterm_percent', '$final_percent', '$date')";
                        $insert_result = $this->data_insert->data_query($insert);
                        if ($insert_result) {
                            $update = "UPDATE college_course SET finalize = '1' WHERE id = '$course_id'";
                            $update_result = $this->data_update->data_query($update);
                            echo 1;
                        }
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function AddStudentAttendance_Method() {
        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['course_id'] != "" && $post_data['updated_date'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            $StudentsArray = json_decode($this->input->post('StudentsArray'));
            $updated_date = date('Y-m-d', strtotime($this->input->post('updated_date')));

            if ($course_id) {
                $query = "SELECT class_id FROM college_course_members_attendance ORDER BY id DESC LIMIT 1";
                $query_result = $this->data_fetch->data_query($query);
                if (count($query_result)) {
                    $class_id = $query_result[0]->class_id;
                    $class_id++;
                } else {
                    $class_id = 1;
                }

                foreach ($StudentsArray as $user_id => $attendance) {
                    $user_id = $this->encryption_decryption_object->is_valid_input($user_id);
                    if ($user_id) {
                        $query = "INSERT INTO college_course_members_attendance(course_id, user_id, class_id, attendance, date) " . "VALUES('$course_id', '$user_id', '$class_id', '$attendance', '$updated_date')";
                        $query_result = $this->data_insert->data_query($query);
                    } else {
                        echo 0;
                        die();
                    }
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function AddStudentMarks_Method() {
        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['course_id'] != "" && $post_data['total_marks'] != "" && $post_data['updated_date'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            $total_marks = $this->input->post('total_marks');
            $StudentsArray = json_decode($this->input->post('StudentsArray'));
            $updated_date = date('Y-m-d', strtotime($this->input->post('updated_date')));

            if ($course_id) {
                $query = "SELECT test_id FROM college_course_members_marks ORDER BY id DESC LIMIT 1";
                $query_result = $this->data_fetch->data_query($query);
                if (count($query_result)) {
                    $test_id = $query_result[0]->test_id;
                    $test_id++;
                } else {
                    $test_id = 1;
                }

                foreach ($StudentsArray as $user_id => $marks) {
                    $user_id = $this->encryption_decryption_object->is_valid_input($user_id);
                    if ($user_id) {
                        $query = "INSERT INTO college_course_members_marks(course_id, user_id, test_id, total_marks, obtained_marks, date) " . "VALUES('$course_id', '$user_id', '$test_id', '$total_marks', '$marks', '$updated_date')";
                        $query_result = $this->data_insert->data_query($query);
                    } else {
                        echo 0;
                        die();
                    }
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function AddStudentFinalMarks_Method() {
        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['course_id'] != "" && $post_data['total_marks'] != "" && $post_data['updated_date'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            $total_marks = $this->input->post('total_marks');
            $StudentsArray = json_decode($this->input->post('StudentsArray'));
            $updated_date = date('Y-m-d', strtotime($this->input->post('updated_date')));

            if ($course_id) {
                foreach ($StudentsArray as $user_id => $marks) {
                    $user_id = $this->encryption_decryption_object->is_valid_input($user_id);
                    if ($user_id) {
                        $query = "SELECT id FROM `college_course_members_finalmarks` WHERE course_id = '$course_id' AND user_id = '$user_id'";
                        $query_result = $this->data_fetch->data_query($query);
                        if (count($query_result)) {
                            $query1 = "UPDATE college_course_members_finalmarks " . "SET total_marks = '$total_marks', obtained_marks = '$marks', date = '$updated_date' " . "WHERE course_id = '$course_id' AND user_id = '$user_id'";
                            $query1_result = $this->data_update->data_query($query1);
                        } else {
                            $query2 = "INSERT INTO college_course_members_finalmarks(course_id, user_id, total_marks, obtained_marks, date) " . "VALUES('$course_id', '$user_id', '$total_marks', '$marks', '$updated_date')";
                            $query2_result = $this->data_insert->data_query($query2);
                        }
                    } else {
                        echo 0;
                        die();
                    }
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function AddStudentMidtermMarks_Method() {
        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['course_id'] != "" && $post_data['total_marks'] != "" && $post_data['updated_date'] != "") {

            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            $total_marks = $this->input->post('total_marks');
            $StudentsArray = json_decode($this->input->post('StudentsArray'));
            $updated_date = date('Y-m-d', strtotime($this->input->post('updated_date')));

            if ($course_id) {
                foreach ($StudentsArray as $user_id => $marks) {
                    $user_id = $this->encryption_decryption_object->is_valid_input($user_id);
                    if ($user_id) {
                        $query = "SELECT id FROM `college_course_members_midterm_marks` WHERE course_id = '$course_id' AND user_id = '$user_id'";
                        $query_result = $this->data_fetch->data_query($query);
                        if (count($query_result)) {
                            $query1 = "UPDATE college_course_members_midterm_marks " . "SET total_marks = '$total_marks', obtained_marks = '$marks', date = '$updated_date' " . "WHERE course_id = '$course_id' AND user_id = '$user_id'";
                            $query1_result = $this->data_update->data_query($query1);
                        } else {
                            $query2 = "INSERT INTO college_course_members_midterm_marks(course_id, user_id, total_marks, obtained_marks, date) " . "VALUES('$course_id', '$user_id', '$total_marks', '$marks', '$updated_date')";
                            $query2_result = $this->data_insert->data_query($query2);
                        }
                    } else {
                        echo 0;
                        die();
                    }
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpdateMemberAttendance_Method() {
        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['course_id'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $attendance_array = $this->input->post('attendance_array');
                foreach ($attendance_array as $key => $value) {
                    $temp = explode("//", $key);
                    $class_id = $this->encryption_decryption_object->is_valid_input($temp[1]);
                    $user_id = $this->encryption_decryption_object->is_valid_input($temp[2]);
                    if ($class_id && $user_id) {
                        $updateAttendance = "UPDATE college_course_members_attendance SET attendance = '$value' "
                                . "WHERE class_id = '$class_id' AND user_id = '$user_id'";
                        $updateAttendance_result = $this->data_insert->data_query($updateAttendance);
                    }
                }
                if ($updateAttendance_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpdateMemberIAMarks_Method() {
        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['course_id'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $iamarks_array = $this->input->post('iamarks_array');
                foreach ($iamarks_array as $key => $value) {
                    $temp = explode("//", $key);
                    $test_id = $this->encryption_decryption_object->is_valid_input($temp[1]);
                    $user_id = $this->encryption_decryption_object->is_valid_input($temp[2]);
                    if ($test_id && $user_id) {
                        $updateIAMarks = "UPDATE college_course_members_marks SET obtained_marks = '$value' "
                                . "WHERE test_id = '$test_id' AND user_id = '$user_id'";
                        $updateIAMarks_result = $this->data_insert->data_query($updateIAMarks);
                    }
                }
                if ($updateIAMarks_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpdateMemberMarks_Method() {
        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['course_id'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $marks_array = $this->input->post('marks_array');
                $exam = $this->input->post('exam');

                foreach ($marks_array as $key => $value) {
                    $temp = explode("//", $key);
                    $user_id = $this->encryption_decryption_object->is_valid_input($temp[1]);
                    if ($user_id) {
                        switch ($exam) {
                            case 'midterm':
                                $updateMarks = "UPDATE college_course_members_midterm_marks ";
                                break;
                            case 'external':
                                $updateMarks = "UPDATE college_course_members_finalmarks ";
                                break;
                            case 'final':
                                $updateMarks = "UPDATE college_course_members_total_final_marks ";
                                break;
                        }
                        $updateMarks .= "SET obtained_marks = '$value' WHERE user_id = '$user_id'";
                        $updateMarks_result = $this->data_insert->data_query($updateMarks);
                    }
                }
                if ($updateMarks_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function AddMoreToCourse_Method() {

        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['user_type'] != "") {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $email = $this->input->post('email');
                $user_type = $this->input->post('user_type');
                $user_type = ($user_type == 'Assistants_Form') ? 'assistant-student' : 'associate-teacher';

                //check if already existing
                $query = "SELECT t1.user_id,t1.user_type FROM college_course_members t1 "
                        . "INNER JOIN users t2 ON t2.id = t1.user_id "
                        . "WHERE t2.email = '$email' AND t1.course_id = '$course_id'";
                $query_result = $this->data_fetch->data_query($query);
                if (count($query_result)) {
                    // if existing update
                    $uid = $query_result[0]->user_id;
                    $user_type = $query_result[0]->user_type;

                    // if user is not a course admin
                    if ($user_type != "course-admin") {
                        $updateUser = "UPDATE college_course_members SET user_type = '$user_type' "
                                . "WHERE user_id = '$uid' AND course_id = '$course_id'";
                        $updateUser_result = $this->data_update->data_query($updateUser);
                        if ($updateUser_result) {
                            echo "User updated successfully.";
                        } else {
                            echo 0;
                        }
                    } else {
                        echo "Course admin cannot be updated.";
                    }
                } else {
                    // get the user_id from users table
                    $GetUserID = "SELECT id FROM users WHERE email = '$email' LIMIT 0,1";
                    $GetUserID_result = $this->data_fetch->data_query($GetUserID);
                    if (count($GetUserID_result)) {
                        $uid = $GetUserID_result[0]->id;

                        // Insert to the course with user_type
                        $insertAssistant = "INSERT INTO college_course_members "
                                . "(course_id,user_id,user_type) "
                                . "VALUES ('$course_id','$uid','$user_type')";
                        $insertAssistant_result = $this->data_insert->data_query($insertAssistant);
                        if ($insertAssistant_result) {
                            echo "User added successfully.";
                        } else {
                            echo 0;
                        }
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function EditCourseDetails_Method() {

        $postdata = $this->input->post();
        if (!empty($postdata)) {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            $course_name = $this->input->post('course_name');
            $course_brief = $this->input->post('course_brief');
            if ($course_id && $course_name != "" && $course_brief != "") {
                $data_array = array('course_name' => $course_name, 'course_brief' => $course_brief);
                $where_array = array('id' => $course_id);
                $query_result = $this->data_update->update_fields('college_course', $data_array, $where_array);
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            }
        } else {
            echo 0;
        }
    }

    public function AddMoremembers_Method() {

        $postdata = $this->input->post();
        if (!empty($postdata)) {
            $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
            if ($course_id) {
                $selected_users = $this->input->post('selected_users');
                $selected_users_group = $this->input->post('selected_users_group');

                $group_member_list_array = array();
                foreach (json_decode($selected_users) as $key => $value) {
                    $group_user_id = $this->encryption_decryption_object->is_valid_input($key);
                    if ($group_user_id) {
                        $group_member_list_array[] = $group_user_id;
                    }
                }

                foreach (json_decode($selected_users_group) as $key => $users_list) {
                    $users_list = explode(',', $users_list);
                    foreach ($users_list as $value) {
                        $value = trim($value);
                        $group_user_id = $this->encryption_decryption_object->is_valid_input($value);
                        if ($group_user_id) {
                            $group_member_list_array[] = $group_user_id;
                        }
                    }
                }
                $group_member_list_array = array_unique($group_member_list_array);

                foreach ($group_member_list_array as $uid) {
                    // insert user to course
                    $sql_query = "INSERT INTO `college_course_members`(`course_id`, `user_id`, `user_type`, `activate`) "
                            . "VALUES('$course_id', '$uid', 'member', '0')";
                    $query_result = $this->data_insert->data_query($sql_query);
                    if ($query_result) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function GetCourseMembers_Method() {
        $logged_user_id = $this->ion_auth->user()->row()->id;
        $college_id = $this->session->userdata('college_id');

        $PostData = $this->input->post();
        $course_id = $this->encryption_decryption_object->is_valid_input($this->input->post('course_id'));
        $search_string = $this->input->post('search_string') != false ? $this->input->post('search_string') : "";

        if (!empty($PostData) && $college_id && $course_id) {
            // Get all the users from this course
            $SelectCourseUsers = "SELECT ccm.user_id, cui.id as user_intranet_id, "
                    . "cui.intranet_user_type "
                    . "FROM college_course_members AS ccm "
                    . "INNER JOIN college_users_intranet AS cui ON cui.user_id = ccm.user_id "
                    . "WHERE ccm.course_id = '$course_id' AND cui.user_id != '$logged_user_id'";
            $SelectCourseUsers_Result = $this->data_fetch->data_query($SelectCourseUsers);

            // get details of users
            $List = $this->usersearch->getAllUserDetails($SelectCourseUsers_Result, $search_string);
            echo json_encode($List);
        }
    }

}

?>