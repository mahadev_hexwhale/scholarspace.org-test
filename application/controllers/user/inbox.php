<?php

/* class is to mainten the user request from the college admin */

Class Inbox extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;
    public $data = array();
    public $AluminiListFull = array();
    public $StudentsListFull = array();
    public $TeacherListFull = array();

    //public $college_id;

    function __construct() {
        parent::__construct();

        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->encryption_decryption_object = new Encryption();
        $this->college_id = $this->session->userdata('college_id');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails(); // Function is getting called by helper

        $this->college_id = $this->session->userdata('college_id');
        $this->load->model('usersearch');
        $this->AluminiListFull = $this->usersearch->getUserList($this->college_id, 'alumni');
        $this->StudentsListFull = $this->usersearch->getUserList($this->college_id, 'student');
        $this->TeacherListFull = $this->usersearch->getUserList($this->college_id, 'teacher');
    }

    public function index() {
        $this->data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $this->data['college_details'] = $this->college_details;
        $this->load->view('front-end/inbox', $this->data);
    }

    public function chatarchive() {
        $this->data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $this->data['college_details'] = $this->college_details;
        $this->load->view('front-end/chatarchive', $this->data);
    }



    public function chatarchive_msg($chatWith) {
        $this->data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $this->data['college_details'] = $this->college_details;

        $chatwith = $this->encryption_decryption_object->is_valid_input($chatWith);

        //Code  to get the first name  and last name of user to chat.
        $receiverDetails = $this->usersearch->getReceiverDetils($chatwith);

        $sendererDetail = array(
            'user_id' => $user_id = $this->ion_auth->user()->row()->id,
            'first_name' => $this->ion_auth->user()->row()->first_name,
            'last_name' => $this->ion_auth->user()->row()->last_name,
            'profile_picture' => $this->ion_auth->user()->row()->profile_picture
        );

        //Code to get the message sender and receiver detail.
        $chat_content = array();
        $user_id = $this->ion_auth->user()->row()->id;

        $check_chat_exists = $this->usersearch->CheckChatExists($user_id, $chatwith);
        if ($check_chat_exists != 0) {

            $chat_status = $this->usersearch->getUserChatStatus($check_chat_exists);
            $chat_status = $chat_status[0];

            //Check the user response wheter is valid to check the user or not.
            if ($this->checkValidChatUser($check_chat_exists)) {

                $chat_content = $this->usersearch->GetChatLast30Messages($check_chat_exists);

                if (count($chat_content) >= 1) {

                    foreach ($chat_content as $chat) {
                        $chat->status = $chat_status->chat_status;
                    }
                }

                $this->data['chat_content'] = $chat_content;
                $this->data['send_to'] = $receiverDetails;
                $this->data['send_from'] = $sendererDetail;
                // echo "<pre />"; print_R($this->data) ; die();
                $this->load->view('front-end/chatarchive_msg', $this->data);
            } else {
                $chat_content = "";
                $this->data['send_to'] = $receiverDetails;
                $this->data['send_from'] = $sendererDetail;
                $this->data['chat_content'] = $chat_content;
                $this->load->view('front-end/chatarchive_msg', $this->data);
            }
        }
        /* else {

          $create_new_chat = $this->usersearch->CreateChat($user_id, $chatwith);
          } */

        return;
    }

    //function to display the user message and chat box to chat.
    public function inboxChat($chatWith) {
        $this->data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $this->data['college_details'] = $this->college_details;

        $chatwith = $this->encryption_decryption_object->is_valid_input($chatWith);

        //Code  to get the first name  and last name of user to chat.
        $receiverDetails = $this->usersearch->getReceiverDetils($chatwith);

        $sendererDetail = array(
            'user_id' => $user_id = $this->ion_auth->user()->row()->id,
            'first_name' => $this->ion_auth->user()->row()->first_name,
            'last_name' => $this->ion_auth->user()->row()->last_name,
            'profile_picture' => $this->ion_auth->user()->row()->profile_picture
        );

        //Code to get the message sender and receiver detail.
        $chat_content = array();
        $user_id = $this->ion_auth->user()->row()->id;
        $check_chat_exists = $this->usersearch->checkInboxChatExist($user_id, $chatwith);
        if ($check_chat_exists != 0) {

            $chat_status = $this->usersearch->getUserChatStatus($check_chat_exists);
            $chat_status = $chat_status[0];

            //Check the user response wheter is valid to check the user or not.
            if ($check_chat_exists) {

                $chat_content = $this->usersearch->GetChatLast30Messages($check_chat_exists);

                if (count($chat_content) >= 1) {

                    foreach ($chat_content as $chat) {
                        $chat->status = $chat_status->chat_status;
                    }
                }

                $this->data['chat_content'] = $chat_content;
                $this->data['send_to'] = $receiverDetails;
                $this->data['send_from'] = $sendererDetail;
                // echo "<pre />"; print_R($this->data) ; die();
                $this->load->view('front-end/inbox_message', $this->data);
            } else {
                $chat_content = "";
                $this->data['send_to'] = $receiverDetails;
                $this->data['send_from'] = $sendererDetail;
                $this->data['chat_content'] = $chat_content;
                $this->load->view('front-end/inbox_message', $this->data);
            }
        }else{
          $chat_content = "";
        $this->data['send_to'] = $receiverDetails;
        $this->data['send_from'] = $sendererDetail;
        $this->data['chat_content'] = $chat_content;
        $this->load->view('front-end/inbox_message', $this->data);
        }

//         else {
//
//          $create_new_chat = $this->usersearch->CreateChat($user_id, $chatwith);
//          } 

        return;
    }
    
    
    
    //Get the all send message.
    public function inboxSentMessages(){
        
         $this->data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $this->data['college_details'] = $this->college_details;

//        $chatwith = $this->encryption_decryption_object->is_valid_input($chatWith);
//
//        //Code  to get the first name  and last name of user to chat.
//        $receiverDetails = $this->usersearch->getReceiverDetils($chatwith);

        $sendererDetail = array(
            'user_id' => $user_id = $this->ion_auth->user()->row()->id,
            'first_name' => $this->ion_auth->user()->row()->first_name,
            'last_name' => $this->ion_auth->user()->row()->last_name,
            'profile_picture' => $this->ion_auth->user()->row()->profile_picture
        );

           $chat_content = $this->usersearch->getInboxSentMessages($logged_user_id);
//               echo "<pre />"; print_R($chat_content); die();
                if (count($chat_content) >= 1) {

                  

               $this->data['chat_content'] = $chat_content;
//                $this->data['send_to'] = $receiverDetails;
               $this->data['send_from'] = $sendererDetail;
//                // echo "<pre />"; print_R($this->data) ; die();
                $this->load->view('front-end/inbox_sent_messages', $this->data);
           } else {
                $chat_content = "";
//                $this->data['send_to'] = $receiverDetails;
                $this->data['send_from'] = $sendererDetail;
                $this->data['chat_content'] = $chat_content;
                $this->load->view('front-end/inbox_sent_messages', $this->data);
           }
//        }
//
//        $chat_content = "";
//        $this->data['send_to'] = $receiverDetails;
//        $this->data['send_from'] = $sendererDetail; 
//        $this->data['chat_content'] = $chat_content;
//        $this->load->view('front-end/inbox_sent_messages', $this->data);
//
////         else {
//
//          $create_new_chat = $this->usersearch->CreateChat($user_id, $chatwith);
//          } 

        return;
        
         
    }
    
    
    
    

    //Function to get the all the use.
    public function displayChatUserList() {
        $searchKey = $this->input->post('Item');
        $userlist = $this->usersearch->displayChatUserList($searchKey);
        //echo "<pre />";
        //print_R($userlist);
        // echo $userlist; 
        echo json_encode($userlist);
    }

    //To send the inbox private message.
    public function insertInboxMessage() {
        $SenderName = $this->input->post('senderName');
        $senderId = $this->ion_auth->user()->row()->id;
        $messageContent = $this->input->post('SenderMessage');
        $receiverId = $this->input->post('reaciver');

        $chat_id = $this->usersearch->CheckChatExists($senderId, $receiverId);
        if ($chat_id) {
            $inboxChat = $this->usersearch->insertInboxMessage($chat_id, $senderId, $messageContent);
            if ($inboxChat) {
                $timestamp = date('Y-m-d G:i:s');
                echo json_encode($timestamp);
            } else {
                $timestamp = "";
                echo json_encode($timestamp);
            }
        }
    }

}
?>


