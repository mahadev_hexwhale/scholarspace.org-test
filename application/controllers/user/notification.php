<?php

/* class is to mainten the user request from the college admin */

Class Notification extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');

        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }

        $this->encryption_decryption_object = new Encryption();

        $this->load->model('data_update');
        $this->load->model('posts_model');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails(); // Function is getting called by helper
    }

    function index() {
        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;

        /* .................... Notification List for logged in user .................... */
        $data['all_posts'] = $all_posts = GetAllNotifications();     // Function is getting called by helper "notifications_helper.php"
        $data['FixedPostDetails'] = $this->posts_model->GetFixedPostDetails($all_posts);
        $data['UnfixedPostDetails'] = $this->posts_model->GetUnfixedPostDetails($all_posts);
        $this->load->view("front-end/notification", $data);
    }

    public function MarkAsRead_Method() {
        $data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        $data['college_details'] = $this->college_details;
        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['type'] != "" && $post_data['group_type'] != "") {
            $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
            $group_type = $this->input->post('group_type');
            $type = $this->input->post('type');

            if ($post_id && $group_type != "") {

                if ($group_type == 'fixed') {
                    switch ($type) {
                        case 'notify':
                            $UpdateQuery = "UPDATE college_fixed_group_posts_notify_user_details "
                                    . "SET read_status = '1' WHERE post_id = '$post_id' AND user_id = '$logged_user_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                        case 'like':
                            $UpdateQuery = "UPDATE college_fixed_group_posts_like_details "
                                    . "SET read_status = '1' WHERE post_id = '$post_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                        case 'reply':
                            $UpdateQuery = "UPDATE college_fixed_group_posts_reply "
                                    . "SET read_status = '1' WHERE post_id = '$post_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                    }
                } else if ($group_type == 'unfixed') {
                    switch ($type) {
                        case 'notify':
                            $UpdateQuery = "UPDATE college_unfixed_group_posts_notify_user_details "
                                    . "SET read_status = '1' WHERE post_id = '$post_id' AND user_id = '$logged_user_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                        case 'like':
                            $UpdateQuery = "UPDATE college_unfixed_group_post_like_details "
                                    . "SET read_status = '1' WHERE post_id = '$post_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                        case 'reply':
                            $UpdateQuery = "UPDATE college_unfixed_group_post_reply "
                                    . "SET read_status = '1' WHERE post_id = '$post_id'";
                            $UpdateQuery_result = $this->data_update->data_query($UpdateQuery);
                            break;
                    }
                }
                if ($UpdateQuery_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function MarkAllAsRead_Method() {
        $college_id = $this->session->userdata('college_id');
        $user_id = $this->user->id;

        if ($college_id != "" && $user_id != "") {

            $fixed_array = $this->input->post('fixed_array');
            $unfixed_array = $this->input->post('unfixed_array');

            if ($fixed_array != "") {
                foreach ($fixed_array as $post_id) {
                    $post_id = $this->encryption_decryption_object->is_valid_input($post_id);
                    if ($post_id) {
                        $update_query = "UPDATE college_fixed_group_posts main_tb "
                                . "LEFT JOIN college_fixed_group_posts_notify_user_details fix_notify_tb ON main_tb.post_id = fix_notify_tb.post_id "
                                . "LEFT JOIN college_fixed_group_posts_like_details fix_like_tb ON main_tb.post_id = fix_like_tb.post_id "
                                . "LEFT JOIN college_fixed_group_posts_reply fix_reply_tb ON main_tb.post_id = fix_reply_tb.post_id "
                                . "SET fix_notify_tb.read_status = '1',fix_like_tb.read_status = '1',fix_reply_tb.read_status = '1' "
                                . "WHERE main_tb.post_id = '$post_id'";
                        $update_query_result = $this->data_update->data_query($update_query);
                        if ($update_query_result) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    }
                }
            }

            if (!empty($unfixed_array) && $unfixed_array != "") {
                foreach ($fixed_array as $post_id) {
                    $post_id = $this->encryption_decryption_object->is_valid_input($post_id);
                    if ($post_id) {
                        $update_query = "UPDATE college_unfixed_group_posts main_tb "
                                . "LEFT JOIN college_unfixed_group_posts_notify_user_details notify_tb ON main_tb.id = notify_tb.post_id "
                                . "LEFT JOIN college_unfixed_group_post_like_details like_tb ON main_tb.id = like_tb.post_id "
                                . "LEFT JOIN college_unfixed_group_post_reply reply_tb ON main_tb.id = reply_tb.post_id "
                                . "SET notify_tb.read_status = '1',like_tb.read_status = '1',reply_tb.read_status = '1' "
                                . "WHERE main_tb.id = '$post_id'";
                        $update_query_result = $this->data_update->data_query($update_query);
                        if ($update_query_result) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    }
                }
            }
        }
    }

}

?>