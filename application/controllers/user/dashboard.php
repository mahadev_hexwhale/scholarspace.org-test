<?php

Class Dashboard extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;
    public $all_unread_alerts = array();
    public $FixedPostDetails = array();
    public $UnfixedPostDetails = array();

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');
        $this->encryption_decryption_object = new Encryption();

        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('usersearch');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();   // Function is getting called by helper
    }

    public function index() {
        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;

        $FixedPostsArray = array();
        $GetAllPosts = "SELECT * FROM college_fixed_group_posts WHERE college_id = '$college_id' AND post_group_name = 'home' ORDER BY timestamp DESC";
        $GetAllPosts_Result = $this->data_fetch->data_query($GetAllPosts);
        $data['home_posts'] = $GetAllPosts_Result;
        foreach ($GetAllPosts_Result as $value) {
            $FixedPostsArray[$value->post_id] = $value->post_id;
        }
        $data['all_posts'] = $this->posts_model->get_all_posts("fixed", $FixedPostsArray);

        //Get all badge images
        $data['badges'] = $this->data_fetch->getBadges();

        // Get all members of college and sort according to timestamp
        $SelectCollegeUsersAll = "SELECT *, id AS user_intranet_id "
                . "FROM `college_users_intranet` "
                . "WHERE `college_id` = '$college_id' "
                . "ORDER BY timestamp DESC";
        $SelectCollegeUsersAll_Result = $this->data_fetch->data_query($SelectCollegeUsersAll);

        // Get all the details of the college user
        $data['college_member'] = $this->usersearch->getAllUserDetails($SelectCollegeUsersAll_Result);

        $this->load->view("front-end/intranet", $data);
    }

    public function GetUserDetails() {
        $logged_user_id = $this->user->id;
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;

        $post_data = $this->input->post();

        if (!empty($post_data) && $post_data['user_id'] != "") {
            $user_id = $this->encryption_decryption_object->is_valid_input($this->input->post('user_id'));

            $GetUser = "SELECT *,id as user_intranet_id FROM college_users_intranet "
                    . "WHERE user_id = '$user_id' AND college_id = '$college_id' LIMIT 0,1";
            $GetUser_Result = $this->data_fetch->data_query($GetUser);

            $GetDetails_Result = $this->usersearch->getAllUserDetails($GetUser_Result);

            $GetFollowing = "SELECT * FROM college_users_follow "
                    . "WHERE followed_by_user_id = '$logged_user_id' AND following_user_id = '$user_id'";
            $GetFollowing_Result = $this->data_fetch->data_query($GetFollowing);

            $following = 0;
            if (count($GetFollowing_Result)) {
                $following = 1;
            }
            foreach ($GetDetails_Result as $key => $value) {
                $send_array[0] = $GetDetails_Result[$key];
            }
            $send_array[1] = $following;

            echo json_encode($send_array);
        }
    }

}

?>