<?php

/* class is to mainten the user request from the college admin */

Class Hostel extends MY_Fixedgroup {

    private $college_details = null;

    function __construct() {
        parent::__construct();
        $this->group_name = "hostel";
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $data = $this->get_group_post(); //get all the post for this group from MY_Fixedgroup cotroller
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;

        //get hostel description
        $sql_query = "SELECT `hostel_description` FROM `college_hostel_description` WHERE `college_id` = '$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['hostel_description'] = $query_result;

        $this->load->view("front-end/hostel_page", $data);
    }

    function hostel_description_submit() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['hostel_description'])) {
            $college_id = $this->college_id;

            $hostel_description = mysql_real_escape_string($posted_data['hostel_description']);

            //check for college hostel details existance
            $sql_query = "SELECT `id` FROM `college_hostel_description` WHERE `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            //if exist then update else insert
            if (count($query_result)) {
                $sql_query = "UPDATE `college_hostel_description` SET `hostel_description` = '$hostel_description' WHERE `college_id` = '$college_id'";
                $query_result = $this->data_update->data_query($sql_query);
            } else {
                $sql_query = "INSERT INTO `college_hostel_description`(`hostel_description`,`college_id`) VALUE('$hostel_description','$college_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function get_user_list_to_notify() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['flag'])) {
            $college_id = $this->college_id;
            $sql_query = "SELECT b.`id`,b.`email`,b.`first_name`,b.`last_name`,b.`profile_picture`,"
                    . "c.`stream_id`,c.`stream_course_id`,c.`study_type`,c.`semester_or_year`,"
                    . " d.`title`, e.`title` FROM `college_users_intranet` as a INNER JOIN `users` as b ON a.`user_id` = b.`id`"
                    . " INNER JOIN `college_student_users_intranet` as c ON a.`id` = c.`college_users_intranet_id` "
                    . "INNER JOIN `stream` as d ON c.`stream_id`= d.`id` "
                    . "INNER JOIN `stream_courses` as e ON c.`stream_course_id` = e.`id` "
                    . "WHERE a.`college_id` = '$college_id' AND a.`intranet_user_type` = 'student'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $user_list = array();

            foreach ($query_result as $value) {
                $user_list[$this->encryption_decryption_object->encode($value->id)] = $value;
            }
            echo json_encode($user_list);
        }
    }

}

?>