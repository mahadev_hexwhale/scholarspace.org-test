<?php

Class Intranet_posts extends UserClass {

    public $user = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
        $this->load->model('usersearch');
        $this->load->helper('college-admin_helper');
        $this->encryption_decryption_object = new Encryption();
    }

    public function GetCollegeMembers_Method() {
        $college_id = $this->session->userdata('college_id');
        $logged_user_id = $this->user->id;

        $PostData = $this->input->post();
        $group = $this->input->post('group');
        $batchboard = $this->input->post('batchboard');
        $Item = $this->input->post('Item');

        if (!empty($PostData) && $college_id) {
            // get list of members
            $SelectCollegeUsers = "SELECT *,id as user_intranet_id FROM college_users_intranet "
                    . "WHERE college_id = '$college_id' AND user_id != '$logged_user_id'";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            // Get Details of members
            $List = array();
            $List = $this->usersearch->getAllUserDetails($SelectCollegeUsers_Result, $Item);
            echo json_encode($List);
        }
    }

    public function PostPoll_Method() {
        $college_id = $this->session->userdata('college_id');
        $post_user_id = $this->user->id;

        $question = $this->input->post('question');
        $answer_array = $this->input->post('answer_array');
        $addAllCollege = $this->input->post('addAllGroup');
        $images_array = $this->input->post('images_array');
        $group = $this->input->post('group');
        $current_AddedMembersList = $this->input->post('current_GroupMembersList');
        $course_id = $this->input->post('course_id') != "" ? $this->encryption_decryption_object->is_valid_input($this->input->post('course_id')) : "";
        $batchboard = $this->input->post('batchboard');

        $UserIntranetIDs = array();
        $CCUserIntranetIDs = array();

        // Check if id from $current_AddedMembersList is group or user
        if ($current_AddedMembersList != false) {
            foreach ($current_AddedMembersList as $key => $value) {
                $CCUserIntranetIDs[] = $value;
            }
        }

        // Discard all duplicate entries 
        $CCUserIntranetIDs = array_unique($CCUserIntranetIDs);
        // Get original ids
        foreach ($CCUserIntranetIDs as $key => $value) {
            $CCUserIntranetIDs[$key] = $this->encryption_decryption_object->is_valid_input($value);
            if ($CCUserIntranetIDs[$key] == false) {
                echo 0;
                die();
            }
        }

        if ($group != "" && $answer_array != false && !empty($answer_array) && $question != "") {

            //  Insert main post into table
            $InsertPost = "INSERT INTO college_fixed_group_posts "
                    . "(post_group_name,type,user_id,college_id) "
                    . "VALUES ('$group','poll','$post_user_id','$college_id')";
            $InsertPost_Result = $this->data_insert->data_query($InsertPost);

            if ($InsertPost_Result) {
                $PostInsertID = $this->db->insert_id();

                // if its a post from course then insert into college_course_posts
                if (isset($course_id) && $course_id != "") {
                    $data_array = array('post_id' => $PostInsertID, 'course_id' => $course_id);
                    $InsertIntoCourse_Result = $this->data_insert->add_fields($data_array, college_course_posts);
                }

                // if its a post from batchboard then insert into college_batchboard_posts
                if ($group == 'batchboard' && isset($batchboard) && $batchboard != "") {
                    $data_array = array('post_id' => $PostInsertID, 'college_id' => $college_id, 'batchboard' => $batchboard);
                    $InsertIntoBatch_Result = $this->data_insert->add_fields($data_array, 'college_batchboard_posts');
                }

                if (count($CCUserIntranetIDs)) {
                    foreach ($CCUserIntranetIDs as $key => $value) {
                        // Get all user_id
                        $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                        $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                        $user_id = $GetUserID_Result[0]->user_id;

                        // Insert notify users to table
                        $InsertIntoNotify = "INSERT INTO college_fixed_group_posts_notify_user_details "
                                . "(post_id,user_id,college_id) "
                                . "VALUES ('$PostInsertID','$user_id','$college_id')";
                        $InsertIntoNotify_Result = $this->data_insert->data_query($InsertIntoNotify);
                    }
                }

                if (!empty($images_array)) {
                    foreach ($images_array as $key => $value) {
                        $temp = explode("/", $value);
                        $file_name = $temp[3];
                        $temp = explode(".", $file_name);
                        $file_type = $temp[1];
                        $InsertFiles = "INSERT INTO college_fixed_group_posts_files "
                                . "(post_id,file_type,file_path) "
                                . "VALUES ('$PostInsertID','$file_type','$value')";
                        $InsertFiles_Result = $this->data_insert->data_query($InsertFiles);
                    }
                }

                $InsertIntoPoll = "INSERT INTO college_fixed_group_poll_post_details "
                        . "(post_id,college_id,question) "
                        . "VALUES ('$PostInsertID','$college_id','$question')";
                $InsertIntoPoll_Result = $this->data_insert->data_query($InsertIntoPoll);

                if ($InsertIntoPoll_Result) {
                    $PostPollInsertID = $this->db->insert_id();

                    // if its a post from course then insert into college_course_posts
                    if (isset($course_id) && $course_id != "") {
                        $InsertIntoCourse = "INSERT INTO college_course_posts (post_id,course_id) "
                                . "VALUES ('$PostInsertID','$course_id')";
                        $InsertIntoCourse_Result = $this->data_insert->data_query($InsertIntoCourse);
                    }

                    foreach ($answer_array as $key => $value) {
                        $InsertIntoPollAnswer = "INSERT INTO college_fixed_group_poll_post_answer_details "
                                . "(post_id,poll_id,college_id,answer) "
                                . "VALUES ('$PostInsertID','$PostPollInsertID','$college_id','$value')";
                        $InsertIntoPollAnswer_Result = $this->data_insert->data_query($InsertIntoPollAnswer);
                    }
                    if ($InsertIntoPollAnswer_Result) {

                        $GetPostDetails = "SELECT t1.*,t1.post_id AS post_id, "
                                . "t2.id AS poll_id, t2.question, t2.timestamp AS posted_time "
                                . "FROM college_fixed_group_posts t1 "
                                . "INNER JOIN college_fixed_group_poll_post_details t2 ON t1.post_id = t2.post_id "
                                . "WHERE t1.type = 'poll' AND t1.user_id = '$post_user_id' AND t1.post_id = '$PostInsertID'";
                        $GetPostDetails_Result = $this->data_fetch->data_query($GetPostDetails);
                        $poll_id = $GetPostDetails_Result[0]->poll_id;

                        $GetAllAnswers = "SELECT id AS answer_id, answer FROM college_fixed_group_poll_post_answer_details "
                                . "WHERE post_id = '$PostInsertID' AND poll_id = '$poll_id'";
                        $GetAllAnswers_Result = $this->data_fetch->data_query($GetAllAnswers);

                        $CCUsers = array();
                        foreach ($CCUserIntranetIDs as $key => $value) {

                            $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                            $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                            $ccUserID = $GetUserID_Result[0]->user_id;

                            $GetCCUserDetails = "SELECT t1.id as user_id, t1.first_name, t1.last_name "
                                    . "FROM users t1 WHERE t1.id = '$ccUserID'";
                            $GetCCUserDetails_Result = $this->data_fetch->data_query($GetCCUserDetails);

                            $CCUsers[] = $GetCCUserDetails_Result[0];
                        }
                        ?>
                        <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="top_user_img">
                                        <img src=<?php echo base_url() . "" . $this->user->profile_picture; ?>>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="top_user_details">
                                        <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($this->user->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($this->user->user_id); ?>"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a> </h5>
                                        <div class="user_popup res_top_name"></div>
                                        <h6>
                                            <span class="post_time" timestamp="">
                                                <?php
                                                $postDate = date('M j Y');
                                                echo (($postDate == date('M j Y')) ? "Today" : $postDate) . " at " . date('g:i A', strtotime($postDate));
                                                ?>
                                            </span>
                                        </h6>
                                        <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($poll_id); ?>">
                                            <p class="poll_question_p"> 
                                                <?php echo $question; ?>
                                            </p>
                                            <div class="poll_answer_div">
                                                <form class="poll_answer_form">
                                                    <?php foreach ($GetAllAnswers_Result as $key => $value) { ?>
                                                        <p class="poll_answer_p">
                                                            <label>
                                                                <input type="radio" name="answer" class="radio_answer" value="<?php echo $this->encryption_decryption_object->encode($value->answer_id); ?>"><?php echo $value->answer; ?>
                                                            </label>
                                                        </p>
                                                    <?php } ?>
                                                    <button type="submit" name="submit_vote" class="btn btn-primary submit_vote"> Vote </button>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                        $file_count = (!empty($images_array)) ? count($images_array) : 0;
                                        if ($file_count) {
                                            ?>
                                            <div class="post_file_div">
                                                <div class="row">
                                                    <?php
                                                    $count = 1;
                                                    $inner_count = 2;
                                                    foreach ($images_array as $file_value) {
                                                        $temp = explode("/", $file_value);
                                                        $file_name = $temp[3];
                                                        $temp = explode(".", $file_name);
                                                        $file_type = $temp[1];
                                                        if ($count == 1) {
                                                            if ($file_type == "jpg") {
                                                                ?>
                                                                <div class="col-md-12">
                                                                    <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                        <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                    </a>
                                                                </div>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <div class="col-md-4" style="padding-right: 19px;">
                                                                    <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                </div>
                                                                <?php
                                                            }
                                                            $count++;
                                                        } else {
                                                            if ($inner_count == 2) {
                                                                ?>
                                                                <!--<div class="col-md-12">-->
                                                                <?php
                                                            }
                                                            if ($file_type == "jpg") {
                                                                ?>
                                                                <div class="col-md-4" style="padding-right: 19px;">
                                                                    <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                        <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                    </a>
                                                                </div>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <div class="col-md-4" style="padding-right: 19px;">
                                                                    <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                                </div>
                                                                <?php
                                                            }
                                                            if ($file_count == $inner_count) {
                                                                ?>
                                                                <!--</div>-->
                                                                <?php
                                                            }
                                                            $inner_count++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        if (count($CCUsers)) {
                                            ?>
                                            <div class="notified_user_div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="notify_user_heading">cc:</div>
                                                        <div class="notify_user_body">
                                                            <?php
                                                            $array_count = count($CCUsers);
                                                            $running_count = 0;
                                                            foreach ($CCUsers as $key => $value) {
                                                                $running_count++;
                                                                ?>
                                                                <a user="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="">
                                                                    <?php echo $value->first_name . " " . $value->last_name; ?>
                                                                </a>
                                                                <?php
                                                                if ($running_count != $array_count) {
                                                                    echo ",";
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                            <a class="post_like" href="javascript:void(0);" like_count="0" like_action ="like" title="Like">Like.</a>
                                            <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                            <a href="javascript:void(0);" follow_action="follow" class="post_follow" title="Follow">Follow.</a>
                                            <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                            <div class="more_list">
                                                <ul>
                                                    <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="commenting_box">
                                            <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_div" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_body">
                                                            <i class="fa fa-thumbs-up"></i>
                                                            <span class="current_user_like"></span>
                                                            like this.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_reply"></div>
                                            <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                                <div class="announce_img">
                                                    <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                                </div>
                                                <div class="share_box2 comment_in1">
                                                    <a href="javascript:void(0);" >Write a reply...</a>
                                                </div>
                                                <div class="more_share">
                                                    <form class="reply_form">
                                                        <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                        <div class="text_poll2">  
                                                            <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>"/>
                                                            <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                        </div>
                                                        <div class="text_poll2 reply_post_text_poll2">
                                                            <div class="AddedCCMembersOfReply"></div>
                                                            <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                            <div class="notify_pop" style="display: none;left: 10px;">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="people_list">
                                                                            <ul class="Reply_ListOfMembers">

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo json_encode(0);
                }
            } else {
                echo json_encode(0);
            }
        } else {
            echo json_encode(0);
        }
    }

    public function GetPrevNextBadge_Method() {

        $PostData = $this->input->post();
        $ImageID = $this->encryption_decryption_object->is_valid_input($PostData['ImageID']);

        if ($PostData != "" && $ImageID) {
            $Trigger = $PostData['Trigger'];

            if ($Trigger == "prev") {
                $GetNewImage = "SELECT MAX(id) AS id FROM `badges` WHERE id IN(SELECT id FROM badges WHERE id<'$ImageID')";
            } else {
                $GetNewImage = "SELECT MIN(id) AS id FROM `badges` WHERE id IN(SELECT id FROM badges WHERE id>'$ImageID')";
            }
            $GetNewImage_Result = $this->data_fetch->data_query($GetNewImage);
            $newID = $GetNewImage_Result[0]->id;
            $GetImage = "SELECT id,image FROM `badges` WHERE id = '$newID'";
            $GetImage_Result = $this->data_fetch->data_query($GetImage);

            $sendBack = array();
            if (count($GetImage_Result)) {
                $ImageID = $this->encryption_decryption_object->encode($GetImage_Result[0]->id);
                $sendBack[] = $ImageID;
                $sendBack[] = $GetImage_Result[0]->image;
                echo json_encode($sendBack);
            }
        }
    }

    public function GetPraiseMembers_Method() {
        $college_id = $this->session->userdata('college_id');
        $current_AddedMembersList = $this->input->post('praise_AddedMembersList');
        $group = $this->input->post('group');

        if ($current_AddedMembersList != false) {
            foreach ($current_AddedMembersList as $key => $value) {
                $NewVal = $this->encryption_decryption_object->is_valid_input($value);
                if ($NewVal == false) {
                    echo 0;
                    die();
                }
                $current_AddedMembersList[$key] = $NewVal;
            }
            $user_ids = implode(", ", $current_AddedMembersList);
        }

        $List = array();
        if ($college_id != "" && $group != "") {

            $SelectCollegeUsers = "SELECT t1.*, "
                    . "t2.intranet_user_type "
                    . "FROM college_group_users AS t1 "
                    . "INNER JOIN college_users_intranet AS t2 ON t1.user_intranet_id = t2.id ";
            if ($current_AddedMembersList != false)
                $SelectCollegeUsers .= "WHERE t2.id NOT IN ($user_ids) AND t1.college_id = '$college_id'";
            else
                $SelectCollegeUsers .= "WHERE t1.college_id = '$college_id'";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            foreach ($SelectCollegeUsers_Result as $key => $value1) {
                $intranet_user_type = $value1->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value1->user_intranet_id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }

                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value1->user_intranet_id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value3) {
                            $List[$value3->college_users_intranet_id] = $value3;
                        }

                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value1->user_intranet_id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value4) {
//                            $List[$value4->college_users_intranet_id] = $value4;
                        }
                        break;
                }
            }
        }

        foreach ($List as $key => $value) {
            $List[$key] = $this->encryption_decryption_object->EncryptThis($value);
        }
        echo json_encode($List);
    }

    public function FilterPraiseMembers_Method() {
        $PostData = $this->input->post();
        $college_id = $this->session->userdata('college_id');

        $Item = $PostData['Item'];
        if (!empty($PostData)) {
            $SelectCollegeUsers = "SELECT t1.* "
                    . "FROM `college_users_intranet` t1 "
                    . "INNER JOIN users t2 ON t1.user_id = t2.id "
                    . "WHERE `college_id` = '$college_id' "
                    . "ORDER BY t2.first_name ASC ";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            $List = array();

            foreach ($SelectCollegeUsers_Result as $key => $value) {
                $intranet_user_type = $value->intranet_user_type;

                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfStudent .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfAlumni .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfTeacher .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfTeacher .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value3) {
//                            $List[$value3->college_users_intranet_id] = $value3;
                        }
                        break;
                }
            }
        }
        foreach ($List as $key => $value) {
            $List[$key] = $this->encryption_decryption_object->EncryptThis($value);
        }
        echo json_encode($List);
    }

    public function PostPraise_Method() {
        $college_id = $this->session->userdata('college_id');
        $post_user_id = $this->user->id;

        // Get all post items
        $PostPraiseContent = $this->input->post('PostPraiseContent');
        $praise_AddedMembersList = $this->input->post('praise_AddedMembersList');
        $current_AddedMembersList = $this->input->post('current_GroupMembersList');
        $addAllCollege = $this->input->post('addAllCollege');
        $images_array = $this->input->post('images_array');
        $ImageID = $this->encryption_decryption_object->is_valid_input($this->input->post('ImageID'));
        $group = $this->input->post('group');
        $course_id = $this->input->post('course_id') != "" ? $this->encryption_decryption_object->is_valid_input($this->input->post('course_id')) : "";
        $batchboard = $this->input->post('batchboard');

        // Check if ids are correct
        foreach ($praise_AddedMembersList as $key => $value) {
            $temp = $this->encryption_decryption_object->is_valid_input($value);
            if ($temp == false) {
                echo 0;
                die();
            }
        }

        if ($PostPraiseContent != "" && $praise_AddedMembersList != false && $ImageID && $group != "") {
            $UserIntranetIDs = array();
            $CCUserIntranetIDs = array();

            // Check if id from $current_AddedMembersList is group or user
            if ($current_AddedMembersList != false) {
                foreach ($current_AddedMembersList as $key => $value) {
                    $CCUserIntranetIDs[] = $value; //Gets all user_intranet IDs and add to $UserIntranetIDs
                }
            }

            // Insert all user intranet ids to array $UserIntranetIDs
            // Discard all duplicate entries 
            $CCUserIntranetIDs = array_unique($CCUserIntranetIDs);
            foreach ($CCUserIntranetIDs as $key => $value) {
                $CCUserIntranetIDs[$key] = $this->encryption_decryption_object->is_valid_input($value);
                if ($CCUserIntranetIDs[$key] == false) {
                    echo 0;
                    die();
                }
            }

            // Get user ids for members to be praised and add to array
            $praise_Members = array();
            foreach ($praise_AddedMembersList as $key => $value) {
                $temp = $this->encryption_decryption_object->is_valid_input($value);
                $GetUserID = "SELECT user_id FROM college_users_intranet WHERE id = '$temp' AND college_id = '$college_id'";
                $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                $user_id = $GetUserID_Result[0]->user_id;
                $praise_Members[] = $user_id;
            }

            //  Insert main post into table
            $InsertPost = "INSERT INTO college_fixed_group_posts "
                    . "(type,post_group_name,user_id,college_id) "
                    . "VALUES ('praise','$group','$post_user_id','$college_id')";
            $InsertPost_Result = $this->data_insert->data_query($InsertPost);

            if ($InsertPost_Result) {
                $PostInsertID = $this->db->insert_id();

                // if its a post from course then insert into college_course_posts
                if (isset($course_id) && $course_id != "") {
                    $data_array = array('post_id' => $PostInsertID, 'course_id' => $course_id);
                    $InsertIntoCourse_Result = $this->data_insert->add_fields($data_array, college_course_posts);
                }

                // if its a post from batchboard then insert into college_batchboard_posts
                if ($group == 'batchboard' && isset($batchboard) && $batchboard != "") {
                    $data_array = array('post_id' => $PostInsertID, 'college_id' => $college_id, 'batchboard' => $batchboard);
                    $InsertIntoBatch_Result = $this->data_insert->add_fields($data_array, 'college_batchboard_posts');
                }

                // if its a post from course then insert into college_course_posts
                if (isset($course_id) && $course_id != "") {
                    $InsertIntoCourse = "INSERT INTO college_course_posts (post_id,course_id) "
                            . "VALUES ('$PostInsertID','$course_id')";
                    $InsertIntoCourse_Result = $this->data_insert->data_query($InsertIntoCourse);
                }

                // Insert into seperate table of praise
                $InsertPraise = "INSERT INTO college_fixed_group_praise_post_details "
                        . "(post_id,college_id,praise_content,badge_id) "
                        . "VALUES('$PostInsertID','$college_id','$PostPraiseContent','$ImageID')";
                $InsertPraise_Result = $this->data_insert->data_query($InsertPraise);
                $praise_id = $this->db->insert_id();

                // Insert files db
                if (!empty($images_array)) {
                    foreach ($images_array as $key => $value) {
                        $temp = explode("/", $value);
                        $file_name = $temp[3];
                        $temp = explode(".", $file_name);
                        $file_type = $temp[1];
                        $InsertFiles = "INSERT INTO college_fixed_group_posts_files "
                                . "(post_id,file_type,file_path) "
                                . "VALUES ('$PostInsertID','$file_type','$value')";
                        $InsertFiles_Result = $this->data_insert->data_query($InsertFiles);
                    }
                }

                // Insert all members who are getting praised
                foreach ($praise_Members as $value) {
                    $InsertPraiseMembers = "INSERT INTO college_fixed_group_praise_post_member_details "
                            . "(post_id,praise_id,college_id,member_user_id) "
                            . "VALUES('$PostInsertID','$praise_id','$college_id','$value')";
                    $InsertPraiseMembers_Result = $this->data_insert->data_query($InsertPraiseMembers);
                }

                if (count($CCUserIntranetIDs)) {
                    foreach ($CCUserIntranetIDs as $key => $value) {
                        // Get all user_id
                        $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                        $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                        $user_id = $GetUserID_Result[0]->user_id;

                        // Insert notify users to table
                        $InsertIntoNotify = "INSERT INTO college_fixed_group_posts_notify_user_details "
                                . "(post_id,user_id,college_id) "
                                . "VALUES ('$PostInsertID','$user_id','$college_id')";
                        $InsertIntoNotify_Result = $this->data_insert->data_query($InsertIntoNotify);
                    }
                }

                // Get image of badge
                $GetBadgeImage = "SELECT image FROM badges WHERE id = '$ImageID'";
                $GetBadgeImage_Result = $this->data_fetch->data_query($GetBadgeImage);
                $badge_image_source = $GetBadgeImage_Result[0]->image;

                $CCUsers = array();
                if (count($CCUserIntranetIDs)) {
                    foreach ($CCUserIntranetIDs as $key => $value) {
                        $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                        $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                        $ccUserID = $GetUserID_Result[0]->user_id;

                        $GetCCUserDetails = "SELECT t1.id as user_id, t1.first_name, t1.last_name "
                                . "FROM users t1 WHERE t1.id = '$ccUserID'";
                        $GetCCUserDetails_Result = $this->data_fetch->data_query($GetCCUserDetails);

                        $CCUsers[] = $GetCCUserDetails_Result[0];
                    }
                }
                ?>
                <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img">
                                <img src="<?php echo base_url() . "" . $this->user->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($this->user->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($this->user->user_id); ?>"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a> </h5>
                                <div class="user_popup res_top_name"></div>
                                <h6>
                                    <span class="post_time" timestamp="">
                                        <?php
                                        $postDate = date('M j Y');
                                        echo (($postDate == date('M j Y')) ? "Today" : $postDate) . " at " . date('g:i A', strtotime($postDate));
                                        ?>
                                    </span>
                                </h6>
                                <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($praise_id); ?>">
                                    <div class="row praised_div">
                                        <div class="col-md-2 praise_div_image">
                                            <img src="<?php echo base_url() . "" . $badge_image_source; ?>" alt="">
                                        </div>
                                        <div class="col-md-10 praise_div_details">
                                            <p class="row praise_member_p">
                                                <span class="praised_text">Praised</span>
                                                <?php
                                                $array_count = count($praise_Members);
                                                $running_count = 0;
                                                foreach ($praise_Members as $value) {
                                                    $running_count++;
                                                    echo $this->ion_auth->user($value)->row()->first_name . " " . $this->ion_auth->user($value)->row()->last_name;
                                                    if ($running_count != $array_count) {
                                                        echo ", ";
                                                    }
                                                }
                                                ?>
                                            </p><br>
                                            <p class="row praise_content_p">"
                                                <?php
                                                if (str_word_count($PostPraiseContent) >= 110) {
                                                    ?>
                                                    <span class="truncated_body">
                                                        <?php
                                                        echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($PostPraiseContent), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                        ?>
                                                    </span>
                                                    <span class="complete_body">
                                                        <?php
                                                        echo html_entity_decode($PostPraiseContent) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                        ?>
                                                    </span>
                                                    <?php
                                                } else {
                                                    echo html_entity_decode($PostPraiseContent);
                                                }
                                                ?>
                                                "
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $file_count = (!empty($images_array)) ? count($images_array) : 0;
                                if ($file_count) {
                                    ?>
                                    <div class="post_file_div">
                                        <div class="row">
                                            <?php
                                            $count = 1;
                                            $inner_count = 2;
                                            foreach ($images_array as $file_value) {
                                                $temp = explode("/", $file_value);
                                                $file_name = $temp[3];
                                                $temp = explode(".", $file_name);
                                                $file_type = $temp[1];
                                                if ($count == 1) {
                                                    if ($file_type == "jpg") {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value; ?>"/>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="col-md-4" style="padding-right: 19px;">
                                                            <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                            <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                        </div>
                                                        <?php
                                                    }
                                                    $count++;
                                                } else {
                                                    if ($inner_count == 2) {
                                                        ?>
                                                        <!--<div class="col-md-12">-->
                                                        <?php
                                                    }
                                                    if ($file_type == "jpg") {
                                                        ?>
                                                        <div class="col-md-4" style="padding-right: 19px;">
                                                            <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value; ?>"/>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="col-md-4" style="padding-right: 19px;">
                                                            <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                            <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                        </div>
                                                        <?php
                                                    }
                                                    if ($file_count == $inner_count) {
                                                        ?>
                                                        <!--</div>-->
                                                        <?php
                                                    }
                                                    $inner_count++;
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (count($CCUsers)) {
                                    ?>
                                    <div class="notified_user_div">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="notify_user_heading">cc:</div>
                                                <div class="notify_user_body">
                                                    <?php
                                                    $array_count = count($CCUsers);
                                                    $running_count = 0;
                                                    foreach ($CCUsers as $key => $value) {
                                                        $running_count++;
                                                        ?>
                                                        <a user="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="">
                                                            <?php echo $value->first_name . " " . $value->last_name; ?>
                                                        </a>
                                                        <?php
                                                        if ($running_count != $array_count) {
                                                            echo ",";
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                    <a class="post_like" href="javascript:void(0);" like_count="0" like_action ="like" title="Like">Like.</a>
                                    <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                    <a href="javascript:void(0);" follow_action="follow" class="post_follow" title="Follow">Follow.</a>
                                    <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                    <div class="more_list">
                                        <ul>
                                            <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="commenting_box">
                                    <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_div" style="display: none">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_body">
                                                    <i class="fa fa-thumbs-up"></i>
                                                    <span class="current_user_like"></span>
                                                    like this.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="all_reply"></div>
                                    <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                        <div class="announce_img">
                                            <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                        </div>
                                        <div class="share_box2 comment_in1">
                                            <a href="javascript:void(0);" >Write a reply...</a>
                                        </div>
                                        <div class="more_share">
                                            <form class="reply_form">
                                                <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                <div class="text_poll2">  
                                                    <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>"/>
                                                    <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                </div>
                                                <div class="text_poll2 reply_post_text_poll2">
                                                    <div class="AddedCCMembersOfReply"></div>
                                                    <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                    <div class="notify_pop" style="display: none;left: 10px;">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="people_list">
                                                                    <ul class="Reply_ListOfMembers">

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function PostAnnounce_Method() {
        $college_id = $this->session->userdata('college_id');
        $post_user_id = $this->user->id;

        // Get all post items
        $announce_heading = $this->input->post('announce_heading');
        $announce_content = nl2br(htmlentities($this->input->post('announce_content')));
        $addAllCollege = $this->input->post('addAllCollege');
        $images_array = $this->input->post('images_array');
        $current_AddedMembersList = $this->input->post('current_GroupMembersList');
        $group = $this->input->post('group');
        $course_id = $this->input->post('course_id') != "" ? $this->encryption_decryption_object->is_valid_input($this->input->post('course_id')) : "";
        $batchboard = $this->input->post('batchboard');

        if ($announce_heading != "" && $announce_content != "" && $group != "") {

            $UserIntranetIDs = array();
            $CCUserIntranetIDs = array();

            // Check if id from $current_AddedMembersList is group of user
            if ($current_AddedMembersList != false) {
                foreach ($current_AddedMembersList as $key => $value) {
                    $CCUserIntranetIDs[] = $value; //Gets all user_intranet IDs and add to $UserIntranetIDs
                }
            }

            // Insert all user intranet ids to array $UserIntranetIDs
            // Discard all duplicate entries 
            $CCUserIntranetIDs = array_unique($CCUserIntranetIDs);
            // Get original ids
            foreach ($CCUserIntranetIDs as $key => $value) {
                $CCUserIntranetIDs[$key] = $this->encryption_decryption_object->is_valid_input($value);
                if ($CCUserIntranetIDs[$key] == false) {
                    echo 0;
                    die();
                }
            }

            //  Insert main post into table
            $InsertPost = "INSERT INTO college_fixed_group_posts "
                    . "(type,post_group_name,user_id,college_id) "
                    . "VALUES ('announce','$group','$post_user_id','$college_id')";
            $InsertPost_Result = $this->data_insert->data_query($InsertPost);

            if ($InsertPost_Result) {
                $PostInsertID = $this->db->insert_id();

                // if its a post from course then insert into college_course_posts
                if (isset($course_id) && $course_id != "") {
                    $data_array = array('post_id' => $PostInsertID, 'course_id' => $course_id);
                    $InsertIntoCourse_Result = $this->data_insert->add_fields($data_array, college_course_posts);
                }

                // if its a post from batchboard then insert into college_batchboard_posts
                if ($group == 'batchboard' && isset($batchboard) && $batchboard != "") {
                    $data_array = array('post_id' => $PostInsertID, 'college_id' => $college_id, 'batchboard' => $batchboard);
                    $InsertIntoBatch_Result = $this->data_insert->add_fields($data_array, 'college_batchboard_posts');
                }

                foreach ($CCUserIntranetIDs as $key => $value) {
                    // Get all user_id
                    $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                    $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                    $user_id = $GetUserID_Result[0]->user_id;

                    // Insert notify users to table
                    $InsertIntoNotify = "INSERT INTO college_fixed_group_posts_notify_user_details "
                            . "(post_id,user_id,college_id) "
                            . "VALUES ('$PostInsertID','$user_id','$college_id')";
                    $InsertIntoNotify_Result = $this->data_insert->data_query($InsertIntoNotify);
                }

                // Insert files db
                if (!empty($images_array)) {
                    foreach ($images_array as $key => $value) {
                        $temp = explode("/", $value);
                        $file_name = $temp[3];
                        $temp = explode(".", $file_name);
                        $file_type = $temp[1];
                        $InsertFiles = "INSERT INTO college_fixed_group_posts_files "
                                . "(post_id,file_type,file_path) "
                                . "VALUES ('$PostInsertID','$file_type','$value')";
                        $InsertFiles_Result = $this->data_insert->data_query($InsertFiles);
                    }
                }

                $InsertAnnounce = "INSERT INTO college_fixed_group_announce_post_details "
                        . "(post_id,college_id,heading,content) "
                        . "VALUES('$PostInsertID','$college_id','$announce_heading','$announce_content')";
                $InsertAnnounce_Result = $this->data_insert->data_query($InsertAnnounce);

                if ($InsertAnnounce_Result) {
                    $announce_id = $this->db->insert_id();

                    $CCUsers = array();
                    foreach ($CCUserIntranetIDs as $key => $value) {
                        $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                        $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                        $ccUserID = $GetUserID_Result[0]->user_id;

                        $GetCCUserDetails = "SELECT t1.id as user_id, t1.first_name, t1.last_name "
                                . "FROM users t1 WHERE t1.id = '$ccUserID'";
                        $GetCCUserDetails_Result = $this->data_fetch->data_query($GetCCUserDetails);

                        $CCUsers[] = $GetCCUserDetails_Result[0];
                    }
                    ?>
                    <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="top_user_img">
                                    <img src="<?php echo base_url() . $this->user->profile_picture; ?>">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="top_user_details">
                                    <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($this->user->id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($this->user->user_id); ?>"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a> </h5>
                                    <div class="user_popup res_top_name"></div>
                                    <h6>
                                        <span class="post_time" timestamp="">
                                            <?php
                                            $postDate = date('M j Y');
                                            echo (($postDate == date('M j Y')) ? "Today" : $postDate) . " at " . date('g:i A', strtotime($postDate));
                                            ?>
                                        </span>
                                    </h6>
                                    <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($announce_id); ?>">
                                        <div class="post_announce_div">
                                            <div class="row">
                                                <div class="col-md-12 announce_head_div">
                                                    <i class="fa fa-bullhorn"></i>
                                                    <?php echo $announce_heading; ?>
                                                </div>
                                                <div class="col-md-12 announce_content_div">
                                                    <?php
                                                    if (str_word_count($announce_content) >= 110) {
                                                        ?>
                                                        <span class="truncated_body">
                                                            <?php
                                                            echo str_replace('<br />', '', substr(html_entity_decode($announce_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                            ?>
                                                        </span>
                                                        <span class="complete_body">
                                                            <?php
                                                            echo str_replace('<br />', '', html_entity_decode($announce_content)) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                            ?>
                                                        </span>
                                                        <?php
                                                    } else {
                                                        echo str_replace('<br />', '', html_entity_decode($announce_content));
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $file_count = (!empty($images_array)) ? count($images_array) : 0;
                                    if ($file_count) {
                                        ?>
                                        <div class="post_file_div">
                                            <div class="row">
                                                <?php
                                                $count = 1;
                                                $inner_count = 2;
                                                foreach ($images_array as $file_value) {
                                                    $temp = explode("/", $file_value);
                                                    $file_name = $temp[3];
                                                    $temp = explode(".", $file_name);
                                                    $file_type = $temp[1];
                                                    if ($count == 1) {
                                                        if ($file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        $count++;
                                                    } else {
                                                        if ($inner_count == 2) {
                                                            ?>
                                                            <!--<div class="col-md-12">-->
                                                            <?php
                                                        }
                                                        if ($file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                    <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($file_count == $inner_count) {
                                                            ?>
                                                            <!--</div>-->
                                                            <?php
                                                        }
                                                        $inner_count++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    if (count($CCUsers)) {
                                        ?>
                                        <div class="notified_user_div">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="notify_user_heading">cc:</div>
                                                    <div class="notify_user_body">
                                                        <?php
                                                        $array_count = count($CCUsers);
                                                        $running_count = 0;
                                                        foreach ($CCUsers as $key => $value) {
                                                            $running_count++;
                                                            ?>
                                                            <a user="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="">
                                                                <?php echo $value->first_name . " " . $value->last_name; ?>
                                                            </a>
                                                            <?php
                                                            if ($running_count != $array_count) {
                                                                echo ",";
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                        <a class="post_like" href="javascript:void(0);" like_count="0" like_action ="like" title="Like">Like.</a>
                                        <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                        <a href="javascript:void(0);" follow_action="follow" class="post_follow" title="Follow">Follow.</a>
                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                        <div class="more_list">
                                            <ul>
                                                <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="commenting_box">
                                        <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_div" style="display: none">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_body">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <span class="current_user_like"></span>
                                                        like this.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="all_reply"></div>
                                        <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                            <div class="announce_img">
                                                <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                            </div>
                                            <div class="share_box2 comment_in1">
                                                <a href="javascript:void(0);" >Write a reply...</a>
                                            </div>
                                            <div class="more_share">
                                                <form class="reply_form">
                                                    <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                    <div class="text_poll2">  
                                                        <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>"/>
                                                        <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                    </div>
                                                    <div class="text_poll2 reply_post_text_poll2">
                                                        <div class="AddedCCMembersOfReply"></div>
                                                        <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                        <div class="notify_pop" style="display: none;left: 10px;">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="people_list">
                                                                        <ul class="Reply_ListOfMembers">

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //upload the image
    public function image_upload_form() {
        //attachment file
        if ($_FILES['upload_post_file']['name']) {
            $target = "assets_front/image/group_post_files/";
            $target.=date("dmYhsi") . "-" . basename($_FILES['upload_post_file']['name']);

            if (move_uploaded_file($_FILES['upload_post_file']['tmp_name'], $target)) {
                echo $target;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //remove uploaded file
    public function remove_file_upload() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['file_path'])) {
            $file_path = $posted_data['file_path'];
            echo unlink($file_path);
        }
    }

    //FUNCTION IS TO SAVE THE UPDATE
    public function PostUpdate_Method() {
        $college_id = $this->session->userdata('college_id');
        $post_user_id = $this->user->id;
        $update_content = nl2br(htmlentities($this->input->post('update_content')));
        $addAllCollege = $this->input->post('addAllGroup');
        $images_array = $this->input->post('images_array');
        $current_AddedMembersList = $this->input->post('current_GroupMembersList');
        $group = $this->input->post('group');
        $course_id = $this->input->post('course_id') != "" ? $this->encryption_decryption_object->is_valid_input($this->input->post('course_id')) : "";
        $batchboard = $this->input->post('batchboard');

        $UserIntranetIDs = array();
        $CCUserIntranetIDs = array();

        // Check if id from $current_AddedMembersList is group or user
        if ($current_AddedMembersList != false) {
            foreach ($current_AddedMembersList as $key => $value) {
                $CCUserIntranetIDs[] = $value; //Gets all user_intranet IDs and add to $UserIntranetIDs
            }
        }

        $CCUserIntranetIDs = array_unique($CCUserIntranetIDs);
        foreach ($CCUserIntranetIDs as $key => $value) {
            $CCUserIntranetIDs[$key] = $this->encryption_decryption_object->is_valid_input($value);
            if ($CCUserIntranetIDs[$key] == false) {
                echo 0;
                die();
            }
        }

        if ($college_id) {
            // Insert main post into table
            $data_array = array('post_group_name' => $group, 'type' => 'update', 'user_id' => $post_user_id, 'college_id' => $college_id);
            $InsertPost_Result = $this->data_insert->add_fields($data_array, 'college_fixed_group_posts');

            if ($InsertPost_Result) {
                $PostInsertID = $InsertPost_Result;

                // if its a post from course then insert into college_course_posts
                if (isset($course_id) && $course_id != "") {
                    $data_array = array('post_id' => $PostInsertID, 'course_id' => $course_id);
                    $InsertIntoCourse_Result = $this->data_insert->add_fields($data_array, college_course_posts);
                }

                // if its a post from batchboard then insert into college_batchboard_posts
                if ($group == 'batchboard' && isset($batchboard) && $batchboard != "") {
                    $data_array = array('post_id' => $PostInsertID, 'college_id' => $college_id, 'batchboard' => $batchboard);
                    $InsertIntoBatch_Result = $this->data_insert->add_fields($data_array, 'college_batchboard_posts');
                }

                foreach ($CCUserIntranetIDs as $key => $value) {
                    // Get all user_id
                    $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                    $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                    $user_id = $GetUserID_Result[0]->user_id;

                    // Insert notify users to table
                    $data_array = array('college_id' => $college_id, 'post_id' => $PostInsertID, 'user_id' => $user_id);
                    $InsertIntoNotify_Result = $this->data_insert->add_fields($data_array, 'college_fixed_group_posts_notify_user_details');
                }

                if (!empty($images_array)) {
                    foreach ($images_array as $key => $value) {
                        $temp = explode("/", $value);
                        $file_name = $temp[3];
                        $temp = explode(".", $file_name);
                        $file_type = $temp[1];

                        // Insert the files into the table
                        $data_array = array('post_id' => $PostInsertID, 'file_type' => $file_type, 'file_path' => $value);
                        $InsertFiles_Result = $this->data_insert->add_fields($data_array, 'college_fixed_group_posts_files');
                    }
                }

                // Insert update details into table
                $data_array = array('post_id' => $PostInsertID, 'college_id' => $college_id, 'update_content' => $update_content);
                $InsertIntoUpdate_Result = $this->data_insert->add_fields($data_array, 'college_fixed_group_update_post_details');
                $update_id = $InsertIntoUpdate_Result;

                if ($InsertIntoUpdate_Result) {
                    $CCUsers = array();
                    foreach ($CCUserIntranetIDs as $key => $value) {
                        $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$value'";
                        $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                        $ccUserID = $GetUserID_Result[0]->user_id;

                        $GetCCUserDetails = "SELECT t1.id as user_id, t1.first_name, t1.last_name "
                                . "FROM users t1 WHERE t1.id = '$ccUserID'";
                        $GetCCUserDetails_Result = $this->data_fetch->data_query($GetCCUserDetails);

                        $CCUsers[] = $GetCCUserDetails_Result[0];
                    }
                    ?>
                    <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="top_user_img">
                                    <img src="<?php echo base_url() . $this->user->profile_picture; ?>">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="top_user_details">
                                    <h5> <a href="#" id="top_name"> <?php echo $this->user->first_name . " " . $this->user->last_name; ?> </a> </h5>
                                    <div class="user_popup" id="res_top_name">
                                        <span class="point_top"></span> 
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="intra_user_img" style="padding: 4px 9px;">
                                                    <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="pop_user_info">
                                                    <h5><?php echo $this->user->first_name . " " . $this->user->last_name; ?></h5>
                                                    <h6>Web developer</h6>
                                                    <h6>Groups: Web development design</h6>
                                                    <h6>Email : <a href="mailto:<?php echo $this->user->email; ?>"><?php echo $this->user->email; ?></a></h6>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="pop_send">
                                                    <a href="#">Send Message</a>
                                                    <a href="#"> <span class="tick"></span> Following</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h6>
                                        <span class="post_time" timestamp="">
                                            <?php
                                            $posted_date = date('M j Y');
                                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A');
                                            ?>
                                        </span>
                                    </h6>
                                    <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($update_id); ?>">
                                        <p class="post_message_p">
                                            <?php
                                            if (str_word_count($update_content) >= 110) {
                                                ?>
                                                <span class="truncated_body">
                                                    <?php
                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($update_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                    ?>
                                                </span>
                                                <span class="complete_body">
                                                    <?php
                                                    echo html_entity_decode($update_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                    ?>
                                                </span>
                                                <?php
                                            } else {
                                                echo html_entity_decode($update_content);
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <?php
                                    $file_count = (!empty($images_array)) ? count($images_array) : 0;
                                    if ($file_count) {
                                        ?>
                                        <div class="post_file_div">
                                            <div class="row">
                                                <?php
                                                $count = 1;
                                                $inner_count = 2;
                                                foreach ($images_array as $file_value) {
                                                    $temp = explode("/", $file_value);
                                                    $file_name = $temp[3];
                                                    $temp = explode(".", $file_name);
                                                    $file_type = $temp[1];
                                                    if ($count == 1) {
                                                        if ($file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        $count++;
                                                    } else {
                                                        if ($inner_count == 2) {
                                                            ?>
                                                            <!--<div class="col-md-12">-->
                                                            <?php
                                                        }
                                                        if ($file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                                    <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($file_count == $inner_count) {
                                                            ?>
                                                            <!--</div>-->
                                                            <?php
                                                        }
                                                        $inner_count++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    if (count($CCUsers)) {
                                        ?>
                                        <div class="notified_user_div">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="notify_user_heading">cc:</div>
                                                    <div class="notify_user_body">
                                                        <?php
                                                        $array_count = count($CCUsers);
                                                        $running_count = 0;
                                                        foreach ($CCUsers as $key => $value) {
                                                            $running_count++;
                                                            ?>
                                                            <a user="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="">
                                                                <?php echo $value->first_name . " " . $value->last_name; ?>
                                                            </a>
                                                            <?php
                                                            if ($running_count != $array_count) {
                                                                echo ",";
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                    <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                        <a class="post_like" href="javascript:void(0);" like_count="0" like_action ="like" title="Like">Like.</a>
                                        <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                        <a href="javascript:void(0);" follow_action="follow" class="post_follow" title="Follow">Follow.</a>
                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                        <div class="more_list">
                                            <ul>
                                                <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="commenting_box">
                                        <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_div" style="display: none">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>" class="post_like_body">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <span class="current_user_like"></span>
                                                        like this.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="all_reply"></div>
                                        <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>">
                                            <div class="announce_img">
                                                <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                            </div>
                                            <div class="share_box2 comment_in1">
                                                <a href="javascript:void(0);" >Write a reply...</a>
                                            </div>
                                            <div class="more_share">
                                                <form class="reply_form">
                                                    <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                    <div class="text_poll2">  
                                                        <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($PostInsertID); ?>"/>
                                                        <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                    </div>
                                                    <div class="text_poll2 reply_post_text_poll2">
                                                        <div class="AddedCCMembersOfReply"></div>
                                                        <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                        <div class="notify_pop" style="display: none;left: 10px;">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="people_list">
                                                                        <ul class="Reply_ListOfMembers">

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //like the post
    public function like_post() {
        $posted_data = $this->input->post();
        $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
        $group = $this->input->post('group');

        if (!empty($posted_data) && !empty($posted_data['post_id']) && $post_id && !empty($posted_data['like_action']) && $group != "") {
            $like_action = $posted_data['like_action'];

            //other details
            $user_id = $this->user->user_id;
            $college_id = $this->session->userdata('college_id');

            //like database operation
            if ($like_action == 'like') {
                // if action is "like"
                $sql_query = "INSERT INTO `college_fixed_group_posts_like_details`"
                        . "(`post_id`,`college_id`,`user_id`,`post_group_name`) "
                        . "VALUES('$post_id','$college_id','$user_id','$group')";
                $query_result = $this->data_insert->data_query($sql_query);
            } else {
                // if action is "unlike"
                $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `post_group_name` = '$group' "
                        . "AND `user_id` = '$user_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //function to save the post reply
    public function save_post_reply() {
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');
        $posted_data = $this->input->post();

        $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
        $group = $this->input->post('group');
        $membersForReplyCC = $this->input->post('membersForReplyCC');
        $reply_message = $this->input->post('reply_message');

        if ($membersForReplyCC != false) {
            $membersForReplyCC = array_unique($membersForReplyCC);
            foreach ($membersForReplyCC as $key => $value) {
                $membersForReplyCC[$key] = $this->encryption_decryption_object->is_valid_input($value);
                if ($membersForReplyCC[$key] == false) {
                    echo 0;
                    die();
                } else {
                    $GetUserID = "SELECT user_id FROM college_users_intranet WHERE college_id = '$college_id' AND id = '$membersForReplyCC[$key]'";
                    $GetUserID_Result = $this->data_fetch->data_query($GetUserID);
                    $membersForReplyCC[$key] = $GetUserID_Result[0]->user_id;
                }
            }
        }

        if (!empty($posted_data) && !empty($posted_data['post_id']) && $post_id && $group != "" && $posted_data['reply_message'] != "") {
            $reply_message = mysql_real_escape_string($posted_data['reply_message']);

            $sql_query = "INSERT INTO `college_fixed_group_posts_reply` "
                    . "(`post_id`,`user_id`,`college_id`,`reply_message`) "
                    . "VALUE ('$post_id','$user_id','$college_id','$reply_message')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $post_reply_id = $this->db->insert_id();

                /* For post notified user saving */
                if (count($membersForReplyCC) && $membersForReplyCC != false) {
                    foreach ($membersForReplyCC as $user_id) {
                        // Insert notify users to table
                        $sql_query = "INSERT INTO `college_fixed_group_posts_reply_notify_user_details` "
                                . "(`post_reply_id`,`post_id`,`college_id`,`user_id`) "
                                . "VALUE('$post_reply_id','$post_id','$college_id','$user_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }
                ?>
                <div class="post_content_reply_div">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img pull-right">
                                <img class="reply_user_img" src="<?php echo base_url() . $this->user->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($this->user->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($this->user->user_id); ?>"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a> </h5>
                                <h6>
                                    <span class="post_time" timestamp="">
                                        <?php
                                        $posted_date = date('M j Y');
                                        echo (($posted_date == date('M j Y')) ? "Today" : date('h:i A')) . " at " . date('h:i A');
                                        ?>
                                    </span>
                                </h6>
                                <div>
                                    <p class="post_message_p">
                                        <?php
                                        if (str_word_count($reply_message) >= 80) {
                                            ?>
                                            <span class="truncated_body">
                                                <?php
                                                echo preg_replace('/\s+?(\S+)?$/', '', substr($reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                ?>
                                            </span>
                                            <span class="complete_body">
                                                <?php
                                                echo $reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                ?>
                                            </span>
                                            <?php
                                        } else {
                                            echo $reply_message;
                                        }
                                        ?>
                                    </p>
                                </div>
                                <?php
                                if (count($membersForReplyCC) && $membersForReplyCC != false) {
                                    ?>
                                    <div class="reply_post_notified_user_div">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="reply_post_notify_user_heading">cc:</div>
                                                <div class="reply_post_notify_user_body">
                                                    <?php
                                                    $array_count = count($membersForReplyCC);
                                                    $running_count = 0;
                                                    foreach ($membersForReplyCC as $user_id) {
                                                        $running_count++;
                                                        $user_details = $this->ion_auth->user($user_id)->row();
                                                        ?>
                                                        <a user="<?php echo $this->encryption_decryption_object->encode($user_id); ?>" href="">
                                                            <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                        </a>
                                                        <?php
                                                        if ($running_count != $array_count) {
                                                            echo ",";
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" post_reply_id ="<?php echo $this->encryption_decryption_object->encode($post_reply_id); ?>" class="post_action_div">
                                    <a class="post_reply_like" href="javascript:void(0);" like_count ="0" like_action="like" title="Like">Like.</a>
                                    <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                    <div class="more_list">
                                        <ul>
                                            <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="post_reply_like_div-<?php echo $this->encryption_decryption_object->encode($post_reply_id); ?>" style="display: none" class="post_reply_like_div">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div id="post_reply_like_body-<?php echo $this->encryption_decryption_object->encode($post_reply_id); ?>" class="post_like_body">
                                    <i class="fa fa-thumbs-up"></i>
                                    <span class="current_user_post_reply_like"></span>
                                    like this.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function VoteForPoll_Method() {
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');

        $posted_data = $this->input->post();
        $group = $this->input->post('group');
        $answer = $this->encryption_decryption_object->is_valid_input($this->input->post('answer'));
        $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
        $poll_id = $this->encryption_decryption_object->is_valid_input($this->input->post('poll_id'));

        if (!empty($posted_data) && $group != "" && $college_id != "" && $post_id && $poll_id) {
            $GetTheVote = "SELECT ans_id FROM college_fixed_group_poll_post_answer_vote_details "
                    . "WHERE poll_id = '$poll_id' AND college_id = '$college_id' AND voted_user_id = '$user_id'";
            $GetTheVote_Result = $this->data_fetch->data_query($GetTheVote);

            if (count($GetTheVote_Result)) {
                $UpdateTheVote = "UPDATE college_fixed_group_poll_post_answer_vote_details "
                        . "SET ans_id = '$answer' "
                        . "WHERE poll_id = '$poll_id' AND college_id = '$college_id' AND voted_user_id = '$user_id'";
                $UpdateTheVote_Result = $this->data_update->data_query($UpdateTheVote);
            } else {
                $UpdateTheVote = "INSERT INTO college_fixed_group_poll_post_answer_vote_details "
                        . "(ans_id,poll_id,post_id,college_id,voted_user_id) "
                        . "VALUES('$answer','$poll_id','$post_id','$college_id','$user_id')";
                $UpdateTheVote_Result = $this->data_insert->data_query($UpdateTheVote);
            }

            if ($UpdateTheVote_Result) {

                $GetAnswers = "SELECT id as ans_id,answer FROM college_fixed_group_poll_post_answer_details "
                        . "WHERE poll_id = '$poll_id' AND post_id = '$post_id' AND college_id = '$college_id'";
                $GetAnswers_Result = $this->data_fetch->data_query($GetAnswers);

                $AnswerArray = array();
                $total_vote_count = 0;
                foreach ($GetAnswers_Result as $key => $value) {
                    $AnswerArray[$value->ans_id] = array('ans_id' => $value->ans_id, 'answer' => $value->answer);
                    $GetVotes = "SELECT count(id) as vote_count,voted_user_id FROM college_fixed_group_poll_post_answer_vote_details "
                            . "WHERE poll_id = '$poll_id' AND ans_id = '$value->ans_id' AND college_id = '$college_id'";
                    $GetVotes_Result = $this->data_fetch->data_query($GetVotes);
                    $AnswerArray[$value->ans_id]['vote_count'] = $GetVotes_Result[0]->vote_count;
                    $total_vote_count += $GetVotes_Result[0]->vote_count;
                }

                if (count($AnswerArray)) {
                    foreach ($AnswerArray as $key => $value) {
                        if ($value['vote_count'] == 0) {
                            $answer_percentage = 0;
                        } else {
                            $answer_percentage = (100 * $value['vote_count']) / $total_vote_count;
                        }
                        ?>
                        <div class="ans_option">
                            <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value['ans_id']); ?>"><?php echo $value['answer']; ?></p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $answer_percentage; ?>%"></div>
                            </div>
                            <p class="poll_answer_p ans_percentage_p"><?php echo round($answer_percentage); ?>%</p>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="total_vote_div"><?php echo $total_vote_count; ?> total votes <a href="javascript:void(0);" class="change_vote_button"> · Change Vote</a> </div>
                    <?php
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function ChangeVote_Method() {
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');

        $posted_data = $this->input->post();
        $group = $this->input->post('group');
        $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
        $poll_id = $this->encryption_decryption_object->is_valid_input($this->input->post('poll_id'));

        if ($posted_data && $group != "" && $post_id && $poll_id) {
            $GetAnswers = "SELECT id as ans_id,answer FROM college_fixed_group_poll_post_answer_details "
                    . "WHERE poll_id = '$poll_id' AND post_id = '$post_id' AND college_id = '$college_id'";
            $GetAnswers_Result = $this->data_fetch->data_query($GetAnswers);

            $sql_query = "SELECT `id`,`ans_id` FROM `college_fixed_group_poll_post_answer_vote_details` "
                    . "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);
            $userVotedAnswerID = $query_result[0]->ans_id;

            if (count($GetAnswers_Result)) {
                ?>
                <form class="poll_answer_form">
                    <?php
                    foreach ($GetAnswers_Result as $key => $value) {
                        ?>
                        <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value->ans_id) ?>">
                            <label>
                                <input type="radio" name="answer" class="radio_answer" value="<?php echo $this->encryption_decryption_object->encode($value->ans_id); ?>" <?php if ($value->ans_id == $userVotedAnswerID) echo "checked"; ?> ><?php echo $value->answer; ?>
                            </label>
                        </p>
                    <?php }
                    ?>
                    <button type="submit" name="submit_vote" class="btn btn-primary submit_vote"> Vote </button>
                </form>
                <?php
            }else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //post reply like action function
    public function like_post_reply() {
        $posted_data = $this->input->post();
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');

        $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
        $post_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_reply_id'));

        if (!empty($posted_data) && !empty($posted_data['post_reply_id']) && !empty($posted_data['post_id']) && $post_reply_id && $post_id && $college_id != "") {
            $like_action = $posted_data['like_action'];

            //other details
            //like database operation
            if ($like_action == 'like') {
                $sql_query = "INSERT INTO "
                        . "`college_fixed_group_posts_reply_like_details`"
                        . "(`post_id`,`post_reply_id`,`user_id`,`college_id`) "
                        . "VALUE('$post_id','$post_reply_id','$user_id','$college_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            } else {
                $sql_query = "DELETE FROM "
                        . "`college_fixed_group_posts_reply_like_details` "
                        . "WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id' "
                        . "AND `user_id` = '$user_id' AND `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function delete_post() {
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');
        $posted_data = $this->input->post();

        $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
        $group = $this->input->post('group');

        if (!empty($posted_data) && !empty($posted_data['post_id']) && $post_id && $group != "") {

            $GetPostDetails = "SELECT type FROM college_fixed_group_posts "
                    . " WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND post_group_name = '$group'";
            $GetPostDetails_Result = $this->data_fetch->data_query($GetPostDetails);

            switch ($GetPostDetails_Result[0]->type) {
                case 'update':
                    $sql_query = "DELETE FROM college_fixed_group_update_post_details "
                            . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                    break;
                case 'poll':
                    $sql_query = "DELETE FROM college_fixed_group_poll_post_answer_vote_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    $sql_query = "DELETE FROM college_fixed_group_poll_post_answer_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    $sql_query = "DELETE FROM college_fixed_group_poll_post_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                    break;
                case 'praise':
                    $sql_query = "DELETE FROM college_fixed_group_praise_post_member_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    $sql_query = "DELETE FROM college_fixed_group_praise_post_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                    break;
                case 'announce':
                    $sql_query = "DELETE FROM college_fixed_group_announce_post_details "
                            . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                    break;
            }

            //delete query for post and post reply
            $sql_query = "DELETE FROM `college_fixed_group_posts` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            $sql_query = "DELETE FROM `college_fixed_group_posts_follow` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            $sql_query = "DELETE FROM `college_fixed_group_posts_notify_user_details` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete from the post details table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete from the post likes table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_notify_user_details` "
                    . "WHERE `post_id` = '$post_id' AND college_id = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                //select files
                $sql_query_files = "SELECT * FROM `college_fixed_group_posts_files` "
                        . "WHERE `post_id` = '$post_id'";
                $sql_query_files_result = $this->data_fetch->data_query($sql_query_files);

                if (count($sql_query_files_result)) {
                    foreach ($sql_query_files_result as $files_value) {
                        $file_path = $files_value->file_path;
                        unlink($file_path);
                    }
                    $sql_query = "DELETE FROM `college_fixed_group_posts_files` "
                            . "WHERE `post_id` = '$post_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                }
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //delete post reply message
    public function delete_post_reply() {
        $user_id = $this->user->user_id;
        $college_id = $this->session->userdata('college_id');
        $posted_data = $this->input->post();
        $group = $this->input->post('group');

        $post_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_reply_id'));

        if (!empty($posted_data) && $post_reply_id != "" && $post_reply_id && $group != "") {

            //delete query for post reply
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply` "
                    . "WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";

            $query_result = $this->data_delete->data_query($sql_query);

            //delete all the likes from post reply like table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` "
                    . "WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete all the likes from post reply like table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_notify_user_details` "
                    . "WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    // When user presses the follow button for a post
    public function FollowPost_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');

        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['post_id'] != "" && $post_data['follow_action'] != "") {

            $group = $this->input->post('group');
            $post_id = $this->encryption_decryption_object->is_valid_input($this->input->post('post_id'));
            $follow_action = $this->input->post('follow_action');

            if ($group != "" && $post_id) {

                if ($follow_action == 'follow') {
                    // insert row into table
                    $InsertFollow = "INSERT INTO `college_fixed_group_posts_follow`"
                            . "(`post_id`, `user_id`, `college_id`, `post_group_name`) "
                            . "VALUES ('$post_id','$user_id','$college_id','$group')";
                    $query_result = $this->data_insert->data_query($InsertFollow);
                } else if ($follow_action == 'unfollow') {
                    // delete row into table
                    $DeleteFollow = "DELETE FROM `college_fixed_group_posts_follow` "
                            . "WHERE post_id = '$post_id'";
                    $query_result = $this->data_delete->data_query($DeleteFollow);
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function FilterPosts_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $postdata = $this->input->post();
        $group = $this->input->post('group');

        if (!empty($postdata) && $postdata['filter'] != "" && $group != "") {
            $filter = $this->input->post('filter');
            $AllPostIDs = array();

            if ($group != "" && $filter != "") {
                switch ($filter) {
                    case 'allPosts':
                        //Get the post_id from all posts of this group
                        $GetAllPosts = "SELECT t1.timestamp, t1.post_id "
                                . "FROM college_fixed_group_posts t1 "
                                . "WHERE t1.post_group_name = '$group' AND college_id = '$college_id' "
                                . "ORDER BY t1.timestamp DESC";
                        $GetPosts_Result = $this->data_fetch->data_query($GetAllPosts);
                        if (count($GetPosts_Result)) {
                            foreach ($GetPosts_Result as $value) {
                                $AllPostIDs[] = $value->post_id;
                            }
                        }
                        break;
                    case 'topPosts':
                        //Get the post_id of posts having max likes
                        $GetTopPosts = "SELECT count(t1.post_id)as count, t1.post_id "
                                . "FROM college_fixed_group_posts_like_details t1 "
                                . "WHERE t1.post_group_name = '$group' AND college_id = '$college_id' "
                                . "GROUP BY post_id "
                                . "ORDER BY count DESC";
                        $GetPosts_Result = $this->data_fetch->data_query($GetTopPosts);
                        if (count($GetPosts_Result)) {
                            foreach ($GetPosts_Result as $value) {
                                $AllPostIDs[] = $value->post_id;
                            }
                        }
                    case 'followingPosts':
                        //Get the post_id which are getting followed by user
                        $GetFollowPosts = "SELECT t1.post_id "
                                . "FROM college_fixed_group_posts_follow t1 "
                                . "WHERE t1.post_group_name = '$group' AND college_id = '$college_id' AND user_id = '$user_id'";
                        $GetPosts_Result = $this->data_fetch->data_query($GetFollowPosts);
                        if (count($GetPosts_Result)) {
                            foreach ($GetPosts_Result as $value) {
                                $AllPostIDs[] = $value->post_id;
                            }
                        }

                        // Get all the users whom the user is following
                        $GetFollowPosts = "SELECT t1.following_user_id, t2.post_id "
                                . "FROM college_users_follow t1 "
                                . "INNER JOIN college_fixed_group_posts t2 ON t2.college_id = t1.college_id "
                                . "WHERE t1.college_id = '$college_id' AND t1.followed_by_user_id = '$user_id' "
                                . "AND t2.post_group_name = '$group' AND t2.user_id = t1.following_user_id "
                                . "GROUP BY post_id";
                        $GetPosts_Result = $this->data_fetch->data_query($GetFollowPosts);
                        if (count($GetPosts_Result)) {
                            foreach ($GetPosts_Result as $value) {
                                $AllPostIDs[] = $value->post_id;
                            }
                        }
                        break;
                }
                $AllPostIDs = array_unique($AllPostIDs);
                $this->GetAllPostsFromArray($group, $AllPostIDs);
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function GetAllPostsFromArray($group, $AllPostIDs) {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');

        if (count($AllPostIDs)) {
            foreach ($AllPostIDs as $post_id) {
                $GetAllPosts = "SELECT t1.*,t1.post_id, "
                        . "t2.first_name, t2.last_name, t2.profile_picture "
                        . "FROM college_fixed_group_posts t1 "
                        . "INNER JOIN users t2 ON t1.user_id = t2.id "
                        . "WHERE t1.post_group_name = '$group' AND t1.college_id = '$college_id' AND t1.post_id = '$post_id'"
                        . "group by t1.post_id "
                        . "ORDER BY t1.timestamp DESC ";
                $GetAllPosts_Result[$post_id] = $this->data_fetch->data_query($GetAllPosts);
            }

            $data['posts_file_list'] = array();
            $data['posts_reply_list'] = array();
            $data['posts_like_current_user'] = array();
            $data['posts_follow_current_user'] = array();
            $data['posts_like_all_user'] = array();
            $data['posts_reply_like_all_user'] = array();
            $data['posts_reply_like_current_user'] = array();
            $data['posts_notified_users_list'] = array();
            $data['posts_reply_notified_users_list'] = array();

            $data['posts_update_details'] = array();
            $data['posts_poll_details'] = array();
            $data['posts_poll_answer_details'] = array();
            $data['posts_poll_answer_vote_details'] = array();
            $data['posts_praise_details'] = array();
            $data['posts_praise_member_details'] = array();
            $data['posts_announce_details'] = array();

            // Run a loop and get all deatails of post and send it to view
            foreach ($GetAllPosts_Result as $value) {
                $value = $value[0];
                $post_id = $value->post_id;

                //post like for the current user
                $sql_query = "SELECT post_id as id FROM `college_fixed_group_posts` "
                        . "WHERE `post_id` = '$post_id' AND `user_id` = '$user_id' "
                        . "AND `college_id` = '$college_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_current_user'][$post_id] = 1;
                } else {
                    $data['posts_current_user'][$post_id] = 0;
                }

                switch ($value->type) {
                    case 'update':
                        $sql_query = "SELECT id AS update_id, update_content FROM college_fixed_group_update_post_details "
                                . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_update_details'][$post_id] = $query_result;
                        break;
                    case 'poll':
                        $sql_query = "SELECT id AS poll_id, question FROM college_fixed_group_poll_post_details "
                                . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_poll_details'][$post_id] = $query_result;

                        foreach ($data['posts_poll_details'][$post_id] as $poll_value) {
                            $poll_id = $poll_value->poll_id;
                            $sql_query = "SELECT id as ans_id, answer FROM college_fixed_group_poll_post_answer_details "
                                    . "WHERE post_id = '$post_id' AND poll_id = '$poll_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_poll_answer_details'][$post_id][$poll_id] = $query_result;

                            foreach ($data['posts_poll_answer_details'][$post_id][$poll_id] as $poll_ans_value) {
                                $poll_ans_id = $poll_ans_value->ans_id;
                                $sql_query = "SELECT count(id) as vote_count, voted_user_id FROM college_fixed_group_poll_post_answer_vote_details "
                                        . "WHERE post_id = '$post_id' AND poll_id = '$poll_id' AND ans_id = '$poll_ans_id'";
                                $query_result = $this->data_fetch->data_query($sql_query);
                                $data['posts_poll_answer_vote_details'][$post_id][$poll_id][$poll_ans_id] = $query_result;
                            }

                            $sql_query = "SELECT `id` FROM `college_fixed_group_poll_post_answer_vote_details` "
                                    . "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
                            $query_result = $this->data_fetch->data_query($sql_query);

                            if (count($query_result)) {
                                $data['posts_poll_answer_vote_current_user'][$post_id] = 1;
                            } else {
                                $data['posts_poll_answer_vote_current_user'][$post_id] = 0;
                            }
                        }
                        break;
                    case 'praise':
                        $sql_query = "SELECT t1.id AS praise_id,t1.praise_content,t1.badge_id, "
                                . "t2.image as badge_image "
                                . "FROM college_fixed_group_praise_post_details t1 "
                                . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                                . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_praise_details'][$post_id] = $query_result;

                        foreach ($data['posts_praise_details'][$post_id] as $praise_value) {
                            $praise_id = $praise_value->praise_id;

                            $sql_query = "SELECT member_user_id as praised_user_id FROM college_fixed_group_praise_post_member_details "
                                    . "WHERE post_id = '$post_id' AND praise_id = '$praise_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_praise_member_details'][$post_id] = $query_result;
                        }
                        break;
                    case 'announce':
                        $sql_query = "SELECT id AS announce_id, heading,content FROM college_fixed_group_announce_post_details "
                                . "WHERE post_id = '$post_id' AND college_id = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_announce_details'][$post_id] = $query_result;
                        break;
                }

                //post files list
                $sql_query = "SELECT `id`,`file_type`,`file_path` "
                        . "FROM `college_fixed_group_posts_files` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_file_list'][$post_id] = $query_result;

                //post reply msg
                $sql_query = "SELECT post_reply_id AS post_reply_id,`user_id`,`reply_message`,`reply_like`,`timestamp` "
                        . "FROM `college_fixed_group_posts_reply` "
                        . "WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_reply_list'][$post_id] = $query_result;

                //post reply like
                foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                    $post_reply_id = $inner_value->post_reply_id;

                    $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_like_details` "
                            . "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                    //post like for the current user
                    $sql_query = "SELECT `id` FROM `college_fixed_group_posts_reply_like_details` "
                            . "WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' "
                            . "AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                    $query_result = $this->data_fetch->data_query($sql_query);

                    if (count($query_result)) {
                        $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                    } else {
                        $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                    }

                    //post reply notified user list
                    $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_notify_user_details` "
                            . "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    if (count($query_result)) {
                        $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                    } else {
                        $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                    }
                }

                //post reply like
                $sql_query = "SELECT post_reply_id AS post_reply_id,user_id "
                        . "FROM `college_fixed_group_posts_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                foreach ($query_result as $value1) {
                    $data['posts_reply_like'][$post_id][$value1->post_reply_id] = $value1->user_id;
                }

                //post like for all the user
                $sql_query = "SELECT `id`,`user_id` FROM `college_fixed_group_posts_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_like_all_user'][$post_id] = $query_result;

                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_fixed_group_posts_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_like_current_user'][$post_id] = 1;
                } else {
                    $data['posts_like_current_user'][$post_id] = 0;
                }

                //post follow for the current user
                $sql_query_follow = "SELECT `id` FROM `college_fixed_group_posts_follow` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $sql_query_follow_result = $this->data_fetch->data_query($sql_query_follow);

                if (count($sql_query_follow_result)) {
                    $data['posts_follow_current_user'][$post_id] = 1;
                } else {
                    $data['posts_follow_current_user'][$post_id] = 0;
                }

                //Post notified user list
                $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_notify_user_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                if (count($query_result)) {
                    foreach ($query_result as $value2) {
                        $data['posts_notified_users_list'][$post_id][] = $value2->user_id;
                    }
                } else {
                    $data['posts_notified_users_list'][$post_id] = array();
                }
                ?>
                <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img">
                                <img src="<?php echo base_url() . $value->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>"><?php echo $value->first_name . " " . $value->last_name; ?></a> </h5>
                                <div class="user_popup res_top_name"></div>
                                <h6>
                                    <span class="post_time" timestamp="<?php echo strtotime($value->timestamp); ?>">
                                        <?php
                                        $posted_date = date('M j Y', strtotime($value->timestamp));
                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                        ?>
                                    </span>
                                </h6>
                                <?php
                                switch ($value->type) {
                                    case 'update':
                                        ?>
                                        <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_update_details'][$value->post_id][0]->update_id); ?>">
                                            <p class="post_message_p">
                                                <?php
                                                if (str_word_count($data['posts_update_details'][$value->post_id][0]->update_content) >= 110) {
                                                    ?>
                                                    <span class="truncated_body">
                                                        <?php
                                                        echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                        ?>
                                                    </span>
                                                    <span class="complete_body">
                                                        <?php
                                                        echo html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                        ?>
                                                    </span>
                                                    <?php
                                                } else {
                                                    echo html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content);
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <?php
                                        break;
                                    case 'poll':
                                        if ($data['posts_poll_answer_vote_current_user'][$value->post_id] == 1) {
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                <p class="poll_question_p">
                                                    <?php echo $data['posts_poll_details'][$value->post_id][0]->question; ?>
                                                </p>
                                                <div class="poll_answer_div">
                                                    <?php
                                                    $poll_id = $data['posts_poll_details'][$value->post_id][0]->poll_id;
                                                    $total_votes = 0;
                                                    foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                        $vote_count = $data['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                        $total_votes += $vote_count;
                                                    }
                                                    foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                        $vote_count = $data['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                        if ($vote_count == 0) {
                                                            $answer_percentage = 0;
                                                        } else {
                                                            $answer_percentage = (100 * $vote_count) / $total_votes;
                                                        }
                                                        ?>
                                                        <div class="ans_option">
                                                            <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $answer_percentage; ?>%"></div>
                                                            </div>
                                                            <p class="poll_answer_p ans_percentage_p"><?php echo round($answer_percentage); ?>%</p>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="total_vote_div"><?php echo $total_votes; ?> total votes <!--<a href="javascript:void(0);" class="change_vote_button"> · Change Vote</a></div>-->
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                    <p class="poll_question_p"> 
                                                        <?php echo $data['posts_poll_details'][$value->post_id][0]->question; ?>
                                                    </p>
                                                    <div class="poll_answer_div">
                                                        <form class="poll_answer_form">
                                                            <?php
                                                            $poll_id = $data['posts_poll_details'][$value->post_id][0]->poll_id;
                                                            foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                ?>
                                                                <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>">
                                                                    <label>
                                                                        <input type="radio" name="answer" class="radio_answer" value="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?>
                                                                    </label>
                                                                </p>
                                                                <?php
                                                            }
                                                            ?>
                                                            <button type="submit" name="submit_vote" class="btn btn-primary submit_vote"> Vote </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            break;
                                        case 'praise':
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_praise_details'][$value->post_id][0]->praise_id); ?>">
                                                <div class="row praised_div">
                                                    <div class="col-md-2 praise_div_image">
                                                        <img src="<?php echo base_url() . "" . $data['posts_praise_details'][$value->post_id][0]->badge_image; ?>" alt="">
                                                    </div>
                                                    <div class="col-md-10 praise_div_details">
                                                        <p class="row praise_member_p"> 
                                                            <span class="praised_text">Praised</span>
                                                            <?php
                                                            $array_count = count($data['posts_praise_member_details'][$value->post_id]);
                                                            $running_count = 0;
                                                            foreach ($data['posts_praise_member_details'][$value->post_id] as $member_value) {
                                                                $running_count++;
                                                                echo $this->ion_auth->user($member_value->praised_user_id)->row()->first_name . " " . $this->ion_auth->user($member_value->praised_user_id)->row()->last_name;
                                                                if ($running_count != $array_count) {
                                                                    echo ", ";
                                                                }
                                                            }
                                                            ?>
                                                        </p><br>
                                                        <p class="row praise_content_p">"
                                                            <?php
                                                            if (str_word_count($data['posts_praise_details'][$value->post_id][0]->praise_content) >= 110) {
                                                                ?>
                                                                <span class="truncated_body">
                                                                    <?php
                                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                    ?>
                                                                </span>
                                                                <span class="complete_body">
                                                                    <?php
                                                                    echo html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                    ?>
                                                                </span>
                                                                <?php
                                                            } else {
                                                                echo html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content);
                                                            }
                                                            ?>
                                                            "
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        case 'announce':
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_announce_details'][$value->post_id][0]->announce_id); ?>">
                                                <div class="post_announce_div">
                                                    <div class="row">
                                                        <div class="col-md-12 announce_head_div">
                                                            <i class="fa fa-bullhorn"></i>
                                                            <?php echo $data['posts_announce_details'][$value->post_id][0]->heading; ?>
                                                        </div>
                                                        <div class="col-md-12 announce_content_div">
                                                            <?php
                                                            if (str_word_count($data['posts_announce_details'][$value->post_id][0]->content) >= 110) {
                                                                ?>  
                                                                <span class="truncated_body">
                                                                    <?php
                                                                    echo str_replace('<br />', '', substr(html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                    ?>
                                                                </span>
                                                                <span class="complete_body">
                                                                    <?php
                                                                    echo html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse </a>";
                                                                    ?>
                                                                </span>
                                                                <?php
                                                            } else {
                                                                echo str_replace('<br />', '', html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content));
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                    }

                                    $file_count = count($data['posts_file_list'][$value->post_id]);
                                    if ($file_count) {
                                        ?>
                                        <div class="post_file_div">
                                            <div class="row">
                                                <?php
                                                $count = 1;
                                                $inner_count = 2;
                                                foreach ($data['posts_file_list'][$value->post_id] as $file_value) {
                                                    if ($count == 1) {
                                                        if ($file_value->file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        $count++;
                                                    } else {
                                                        if ($inner_count == 2) {
                                                            ?>
                                                            <?php
                                                        }
                                                        if ($file_value->file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($file_count == $inner_count) {
                                                            ?>
                                                            <?php
                                                        }
                                                        $inner_count++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    if (count($data['posts_notified_users_list'][$value->post_id])) {
                                        ?>
                                        <div class="notified_user_div">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="notify_user_heading">cc:</div>
                                                    <div class="notify_user_body">
                                                        <?php
                                                        $array_count = count($data['posts_notified_users_list'][$value->post_id]);
                                                        $running_count = 0;
                                                        foreach ($data['posts_notified_users_list'][$value->post_id] as $user_id) {
                                                            $running_count++;
                                                            $user_details = $this->ion_auth->user($user_id)->row();
                                                            ?>
                                                            <a user="<?php echo $this->encryption_decryption_object->encode($user_id); ?>" href="">
                                                                <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                            </a>
                                                            <?php
                                                            if ($running_count != $array_count) {
                                                                echo ",";
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php // if ($this->user->id == $group->group_admin_user_id) {      ?>
                                    <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                                        <a class="post_like" href="javascript:void(0);" like_count="<?php echo count($data['posts_like_all_user'][$value->post_id]); ?>" like_action ="<?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                        <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                        <a href="javascript:void(0);" follow_action="<?php echo ($data['posts_follow_current_user'][$value->post_id] == 1) ? "unfollow" : "follow"; ?>" class="post_follow" title="Follow"><?php echo ($data['posts_follow_current_user'][$value->post_id] == 1) ? "Unfollow." : "Follow."; ?></a>
                                        <?php if ($data['posts_current_user'][$value->post_id]) { ?>
                                            <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                            <div class="more_list">
                                                <ul>
                                                    <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete </h6> </a> </li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="commenting_box">
                                        <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_div" style="display: <?php echo (!empty($data['posts_like_all_user'][$value->post_id])) ? "block" : "none"; ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_body">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <?php
                                                        $array_count = count($data['posts_like_all_user'][$value->post_id]);
                                                        $running_count = 0;
                                                        if ($data['posts_like_current_user'][$value->post_id] == 1) {
                                                            echo "You";
                                                            if (count($data['posts_like_all_user'][$value->post_id]) > 1) {
                                                                echo ",";
                                                            }
                                                            foreach ($data['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                $running_count++;
                                                                $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                    ?>
                                                                    <span>
                                                                        <?php
                                                                        echo $user_details->first_name . " " . $user_details->last_name;
                                                                        if ($running_count != $array_count) {
                                                                            echo ", ";
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                    <?php
                                                                }
                                                            }
                                                        } else {
                                                            if (count($data['posts_like_all_user'][$value->post_id]) >= 1) {
                                                                foreach ($data['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                    $running_count++;
                                                                    $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                    if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                        ?>
                                                                        <span>
                                                                            <?php
                                                                            echo $user_details->first_name . " " . $user_details->last_name;
                                                                            if ($running_count != $array_count) {
                                                                                echo ", ";
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        like this.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="all_reply">
                                            <?php
                                            foreach ($data['posts_reply_list'][$value->post_id] as $post_reply_key => $post_reply_value) {
                                                $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                ?>
                                                <div class="post_content_reply_div">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="top_user_img pull-right">
                                                                <img class="reply_user_img" src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="top_user_details">
                                                                <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($user_details->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($user_details->user_id); ?>"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a> </h5>
                                                                <h6>
                                                                    <span class="post_time" timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                        <?php
                                                                        $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                        ?>
                                                                    </span>
                                                                </h6>
                                                                <div>
                                                                    <p class="post_message_p">
                                                                        <?php
                                                                        if (str_word_count($post_reply_value->reply_message) >= 80) {
                                                                            ?>
                                                                            <span class="truncated_body">
                                                                                <?php
                                                                                echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                                ?>
                                                                            </span>
                                                                            <span class="complete_body">
                                                                                <?php
                                                                                echo $post_reply_value->reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                                ?>
                                                                            </span>
                                                                            <?php
                                                                        } else {
                                                                            echo $post_reply_value->reply_message;
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                                <?php
                                                                if (count($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id])) {
                                                                    ?>
                                                                    <div class="reply_post_notified_user_div">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="reply_post_notify_user_heading">cc:</div>
                                                                                <div class="reply_post_notify_user_body">
                                                                                    <?php
                                                                                    $array_count = count($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id]);
                                                                                    $running_count = 0;
                                                                                    foreach ($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id] as $user_id) {
                                                                                        $running_count++;
                                                                                        $user_details = $this->ion_auth->user($user_id->user_id)->row();
                                                                                        ?>
                                                                                        <a user="<?php echo $this->encryption_decryption_object->encode($user_id->user_id); ?>" href="">
                                                                                            <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                        </a>
                                                                                        <?php
                                                                                        if ($running_count != $array_count) {
                                                                                            echo ",";
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <div post_id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" post_reply_id ="<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_action_div">
                                                                    <a class="post_reply_like" href="javascript:void(0);" like_count ="<?php echo count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]); ?>" like_action="<?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                    <?php if ($data['posts_current_user'][$value->post_id]) { ?>
                                                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                                        <div class="more_list">
                                                                            <ul>
                                                                                <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                                            </ul>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="post_reply_like_div-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" style="display: <?php echo (!empty($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>" class="post_reply_like_div">
                                                        <div class="row">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-10">
                                                                <div id="post_reply_like_body-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_like_body">
                                                                    <i class="fa fa-thumbs-up"></i>
                                                                    <span class="current_user_post_reply_like">
                                                                        <?php
                                                                        //current user like
                                                                        if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1 && count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                            ?>
                                                                            You and
                                                                            <?php
                                                                        } else if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                            ?>You
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                    like this.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                                            <div class="announce_img">
                                                <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                            </div>
                                            <div class="share_box2 comment_in1">
                                                <a href="javascript:void(0);" >Write a reply...</a>
                                            </div>
                                            <div class="more_share">
                                                <form class="reply_form">
                                                    <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                    <div class="text_poll2">
                                                        <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>"/>
                                                        <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                    </div>
                                                    <div class="text_poll2 reply_post_text_poll2">
                                                        <div class="AddedCCMembersOfReply"></div>
                                                        <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                        <div class="notify_pop" style="display: none;left: 10px;">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="people_list">
                                                                        <ul class="Reply_ListOfMembers">

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <?php // }          ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }

}
