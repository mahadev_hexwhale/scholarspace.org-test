<?php
/* class is to mainten the user request from the college admin */

Class Internship extends UserClass {

    public $encryption_decryption_object = null;
    private $college_details = null;
    public $crypt_object = null;
    public $college_id = null;
    public $user = null;

    function __construct() {
        parent::__construct();

        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->crypt_object = new Encryption();
        $this->encryption_decryption_object = new Encryption();
        $this->college_id = $this->session->userdata('college_id');
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails(); // Function is getting called by helper
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    function index() {
        $college_id = $this->college_id;
        $user_id = $this->user->id;
        $today_date = date("Y-m-d");

        /* get all the job posted */
        $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t1.`is_approve`,t2.`post_title`,t2.`post_message`,t2.`start_date`,t2.`end_date` FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` WHERE t1.`post_group_name` = 'internship' AND t1.`college_id` = '$college_id' AND t2.`end_date` > '$today_date' ORDER BY t1.`timestamp` DESC";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['internship_list'] = $query_result;

        $data['user_favorite_internship'] = array();

        /* get all the job posted */
        foreach ($data['internship_list'] as $key => $value) {
            $post_id = $value->post_id;

            $sql_query = "SELECT * FROM `college_fixed_group_posts_favorite_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['user_favorite_internship'][$post_id] = (count($query_result) && $query_result[0]->favorite_status) ? 1 : 0;
        }
        $data['college_details'] = $this->college_details;
        $data['user'] = $this->user;
        $this->load->view("front-end/internship", $data);
    }

    function is_college_admin($user_id) {
        $college_id = $this->college_id;

        $sql_query = "SELECT `id` FROM `college_admin` WHERE `college_id` = '$college_id' AND `user_id` = '$user_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        return (count($query_result)) ? 1 : 0; //if 1 it means loggedin user is admin
    }

    public function get_internship_content() {
        $college_id = $this->college_id;
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
        if ($internship_id) {
            $sql_query = "SELECT `post_title`,`post_message`,`start_date`,`end_date` FROM `college_fixed_group_posts_details` WHERE `post_id` = '$internship_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $internship_content_array = array("title" => "", "description" => "", "start_date" => "", "end_date" => "");

            if (count($query_result)) {
                $internship_content_array["title"] = $query_result[0]->post_title;
                $internship_content_array["description"] = $query_result[0]->post_message;
                $internship_content_array["start_date"] = $query_result[0]->start_date;
                $internship_content_array["end_date"] = $query_result[0]->end_date;
            }
            echo json_encode($internship_content_array);
        }
    }

    public function update_internship_form() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));

        if ($internship_id) {
            $internship_title = $this->input->post('internship_title');
            $internship_description = $this->input->post('internship_description');

            $start_date_temp = explode("/", $this->input->post('start_date'));
            $end_date_temp = explode("/", $this->input->post('end_date'));

            $start_date = $start_date_temp[2] . "-" . $start_date_temp[1] . "-" . $start_date_temp[0];
            $end_date = $end_date_temp[2] . "-" . $end_date_temp[1] . "-" . $end_date_temp[0];

            $sql_query = "UPDATE `college_fixed_group_posts_details` SET `post_title` = '$internship_title', `post_message` = '$internship_description', `start_date`='$start_date',`end_date`='$end_date' WHERE `post_id` = '$internship_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function archive_post() {
        $posted_data = $this->input->post();

        if ($posted_data['test_flag'] == 1) {
            $college_id = $this->college_id;

            $today_date = date("Y-m-d");

            $logged_in_user_id = $this->user->id;

            //check current user is admin or not
            $current_user_is_college_admin = $this->is_college_admin($logged_in_user_id); //if 1 it means loggedin user is admin

            /* get all the job posted */
            $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t1.`is_approve`,t2.`post_title`,t2.`post_message`,t2.`start_date`,t2.`end_date` FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` WHERE t1.`post_group_name` = 'internship' AND t1.`college_id` = '$college_id' AND t2.`end_date` < '$today_date' ORDER BY t1.`timestamp` DESC";
            $query_result = $this->data_fetch->data_query($sql_query);

            $result_array = array();

            foreach ($query_result as $key => $value) {
                //Posted user details
                $posted_user = $this->ion_auth->user($value->user_id)->row();

                $posted_user_is_admin = $this->is_college_admin($value->user_id);

                if ($value->user_id == $this->user->id || $value->is_approve == 1 || $current_user_is_college_admin || $posted_user_is_admin) {
                    $result_array[$key]['post_id'] = $this->encryption_decryption_object->encode($value->post_id);
                    $result_array[$key]['user_id'] = $value->user_id;
                    $result_array[$key]['user_first_name'] = $posted_user->first_name;
                    $result_array[$key]['user_last_name'] = $posted_user->last_name;
                    $result_array[$key]['current_login_user_id'] = $this->user->id;
                    $result_array[$key]['timestamp'] = $value->timestamp;
                    $result_array[$key]['is_approve'] = $value->is_approve;
                    $result_array[$key]['post_title'] = $value->post_title;
                    $result_array[$key]['post_message'] = $value->post_message;
                    $result_array[$key]['start_date'] = $value->start_date;
                    $result_array[$key]['end_date'] = $value->end_date;
                    $result_array[$key]['current_user_is_college_admin'] = $current_user_is_college_admin;
                }
            }
            echo json_encode($result_array);
        }
    }

    public function favorite_post() {
        $posted_data = $this->input->post();

        if ($posted_data['test_flag'] == 1) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $logged_in_user_id = $this->user->id;

            //check current user is admin or not
            $current_user_is_college_admin = $this->is_college_admin($logged_in_user_id); //if 1 it means loggedin user is admin

            $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t1.`is_approve`,t2.`post_title`,t2.`post_message`,t2.`start_date`,t2.`end_date` 
                        FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` 
                        WHERE t1.`post_group_name` = 'internship' AND t1.`post_id` IN(SELECT `post_id` FROM `college_fixed_group_posts_favorite_details` 
                        WHERE `college_id` = '$college_id' AND `user_id` = '$user_id' AND `favorite_status` = '1') AND t1.`college_id` = '$college_id' "
                    . "ORDER BY t1.`timestamp` DESC";
            $query_result = $this->data_fetch->data_query($sql_query);

            $result_array = array();

            foreach ($query_result as $key => $value) {
                //Posted user details
                $posted_user = $this->ion_auth->user($value->user_id)->row();

                $posted_user_is_admin = $this->is_college_admin($value->user_id);

                if ($value->user_id == $this->user->id || $value->is_approve == 1 || $current_user_is_college_admin || $posted_user_is_admin) {
                    $result_array[$key]['post_id'] = $this->encryption_decryption_object->encode($value->post_id);
                    $result_array[$key]['user_id'] = $value->user_id;
                    $result_array[$key]['user_first_name'] = $posted_user->first_name;
                    $result_array[$key]['user_last_name'] = $posted_user->last_name;
                    $result_array[$key]['current_login_user_id'] = $this->user->id;
                    $result_array[$key]['timestamp'] = $value->timestamp;
                    $result_array[$key]['is_approve'] = $value->is_approve;
                    $result_array[$key]['post_title'] = $value->post_title;
                    $result_array[$key]['post_message'] = $value->post_message;
                    $result_array[$key]['start_date'] = $value->start_date;
                    $result_array[$key]['end_date'] = $value->end_date;
                    $result_array[$key]['current_user_is_college_admin'] = $current_user_is_college_admin;
                }
            }
            echo json_encode($result_array);
        }
    }

    public function save_update_post() {
        $posted_data = $this->input->post();
        if (!empty($posted_data)) {
            if ($posted_data['internship_start_date'] != '') {
                $start_date_array = explode('/', $posted_data['internship_start_date']);
                $internship_start_date = $start_date_array[2] . "-" . $start_date_array[1] . "-" . $start_date_array[0];
            } else {
                $internship_start_date = "0000-00-00";
            }
            if ($posted_data['internship_end_date'] != '') {
                $end_date_array = explode('/', $posted_data['internship_end_date']);
                $internship_end_date = $end_date_array[2] . "-" . $end_date_array[1] . "-" . $end_date_array[0];
            } else {
                $internship_end_date = "0000-00-00";
            }

            $internship_title = mysql_real_escape_string($posted_data['internship_title']);
            $internship_description = mysql_real_escape_string($posted_data['internship_description']);

            $user = $this->user;
            $user_id = $user->user_id;

            //group name
            $post_group_name = "internship";

            $college_id = $this->college_id;

            //Insert into jobopening group table
            $sql_query = "INSERT INTO `college_fixed_group_posts`"
                    . "(`post_group_name`,`type`,`user_id`,`college_id`) "
                    . "VALUES('$post_group_name','update','$user_id','$college_id')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $post_id = $this->db->insert_id();

                /* for post message saving */
                $sql_query = "INSERT INTO `college_fixed_group_posts_details`(`start_date`,`end_date`,`post_title`,`post_message`,`post_id`) VALUE('$internship_start_date','$internship_end_date','$internship_title','$internship_description','$post_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                /* for post file upload saving */
                if (!empty($posted_data['uploaded_file'])) {
                    foreach ($posted_data['uploaded_file'] as $file_value) {
                        $sql_query = "INSERT INTO `college_fixed_group_posts_files` (`file_path`,`post_id`) VALUE('$file_value','$post_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }

                /* For post notified user saving */
                if (!empty($selected_user_list)) {
                    foreach ($selected_user_list as $user_id) {
                        $sql_query = "INSERT INTO `college_fixed_group_posts_notify_user_details` (`college_id`,`post_id`,`user_id`) VALUE('$college_id','$post_id','$user_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }
            }
            if ($query_result) {
                ?>
                <li class="internship_list_li">
                    <h6 style="margin-bottom: 4px;">
                        <span id="internship_title-<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="internship_title_span"><?php echo $internship_title; ?></span>
                        <button title="Delete internship" post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="btn btn-default btn-xs pull-right delete_post"><i class="fa fa-trash"></i></button>
                        <!--<button title="Edit internship"  post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="btn btn-default btn-xs pull-right edit_post"><i class="fa fa-edit"></i></button>-->
                        <p style="color: #8A8A8A;font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">posted by <a href="#"><?php echo $user->first_name . " " . $user->last_name; ?></a>
                            <?php
                            $posted_date = date('M j Y');
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A');
                            ?>
                        </p>
                    </h6>  
                    <!--<p id="internship_description-<?php echo $this->encryption_decryption_object->encode($post_id); ?>"><?php echo $internship_description; ?></p>-->
                    <div class="internship_action_btn_div" style="margin-top: 5px">
                        <div class="row">
                            <div class="col-lg-12">
                                <!--<button class="edit_job_opening btn btn-xs"><i class="fa fa-thumbs-up"></i></button>-->
                                <a href="<?php echo site_url("user/internship/internship_details") . "?internshipId=" . $this->encryption_decryption_object->encode($post_id); ?>"><button class="edit_internship btn btn-xs"><i class="fa fa-external-link-square"></i> View Details</button></a>
                                <span style="color: red; font-size: 13px;"> Waiting to approve</span>
                                <span class="pull-right" style="font-family: 'open_sansregular';font-size: 12px;color: #626262;line-height: 15px;">
                                    FROM : <a href="javascript:void(0)"><?php echo date("d-M-Y", strtotime($internship_start_date)); ?></a> to <a href="#"><?php echo date("d-M-Y", strtotime($internship_end_date)); ?></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function approve_internship_posted() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
        if ($internship_id != '' && $internship_id) {
            $college_id = $this->college_id;
            $sql_query = "UPDATE `college_fixed_group_posts` SET `is_approve` = '1' WHERE `post_id` = '$internship_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_update->data_query($sql_query);
            echo ($query_result) ? "1" : "0";
        }
    }

    public function decline_internship_posted() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
        if ($internship_id) {
            $college_id = $this->college_id;
            $sql_query = "DELETE FROM `college_fixed_group_posts` WHERE `post_id` = '$job_id' AND `college_id` = '$internship_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_details` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_files` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$internship_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_notify_user_details` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_id` = '$internship_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? "1" : "0";
        }
    }

    public function make_internship_favorite() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
        if ($internship_id) {
            $user_id = $this->user->id;
            $college_id = $this->college_id;
            $user_favorite_status = ($this->input->post("favorite_status")) ? 0 : 1;

            $sql_query = "SELECT * from `college_fixed_group_posts_favorite_details` WHERE `user_id`='$user_id' AND `college_id` = '$college_id' AND `post_id`='$internship_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $favorite_id = $query_result[0]->id;

                $sql_query = "UPDATE `college_fixed_group_posts_favorite_details` SET `favorite_status` = '$user_favorite_status' WHERE `id` = '$favorite_id'";
                $query_result = $this->data_update->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            } else {
                /* Add comment's like */
                $sql_query = "INSERT INTO `college_fixed_group_posts_favorite_details`(`favorite_status`,`user_id`,`college_id`,`post_id`) VALUES('1','$user_id','$college_id','$internship_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            }
        }
    }

    public function internship_details() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->get('internshipId'));
        if ($internship_id != '' && $internship_id) {
            /* Get internship details */
            $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t2.`post_title`,t2.`post_message`,t2.`start_date`, t2.`end_date` FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` WHERE t1.`post_id` = '$internship_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['internship_details'] = $query_result[0];

            /* get internship comments */
            $sql_query = "SELECT `post_reply_id`,`user_id`,`reply_message`,`timestamp`,`post_id` FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$internship_id' ORDER BY `timestamp` DESC";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['internship_reply_list'] = $query_result;

            $data['internship_comment_like_status'] = array();

            /* get job likes details */
            $sql_query = "SELECT `like_status`,`user_id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$internship_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['internship_like_status'] = $query_result;

            $data['internship_comment_reply'] = array();

            /* get internship details comments likes */
            foreach ($data['internship_reply_list'] as $value) {
                $comment_id = $value->post_reply_id;

                $sql_query = "SELECT `like_status`,`user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$internship_id' AND `post_reply_id` = '$comment_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['internship_comment_like_status'][$comment_id] = $query_result;

                /* get reply details */
                $sql_query = "SELECT * FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_id` = '$internship_id' AND `post_reply_id` = '$comment_id' ORDER BY `timestamp` DESC";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['internship_comment_reply'][$comment_id] = $query_result;
            }

            $data['college_details'] = $this->college_details;
            $data['internship_id'] = $this->input->get('internshipId');
            $data['user'] = $this->user;
            $this->load->view("front-end/internship_details", $data);
        }
    }

    public function get_internship_comment_details() {
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        if ($comment_id) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $sql_query = "SELECT * FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            $internship_comment_details = array("comment" => '');

            if (count($query_result)) {
                $internship_comment_details["comment"] = $query_result[0]->reply_message;
            }
            echo json_encode($internship_comment_details);
        } else {
            echo 0;
        }
    }

    public function get_internship_comment_reply_details() {
        $comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_reply_id'));

        if ($comment_reply_id) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $sql_query = "SELECT * FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_reply_of_reply_id` = '$comment_reply_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            $internship_comment_reply_details = array("comment_reply" => '');

            if (count($query_result)) {
                $internship_comment_reply_details["comment_reply"] = $query_result[0]->reply_message;
            }
            echo json_encode($internship_comment_reply_details);
        } else {
            echo 0;
        }
    }

    public function update_internship_comment_form() {
        $internship_comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_comment_id'));

        if ($internship_comment_id) {
            $internship_comment = $this->input->post('internship_comment');

            $sql_query = "UPDATE `college_fixed_group_posts_reply` SET `reply_message` = '$internship_comment' WHERE `post_reply_id` = '$internship_comment_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function update_internship_comment_reply_form() {
        $internship_comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_comment_reply_id'));

        if ($internship_comment_reply_id) {
            $internship_comment_reply = $this->input->post('internship_comment_reply');

            $sql_query = "UPDATE `college_fixed_group_posts_reply_of_reply` SET `reply_message` = '$internship_comment_reply' WHERE `post_reply_of_reply_id` = '$internship_comment_reply_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function save_reply_comment() {
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));

        if ($internship_id && $comment_id) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $comment_reply = mysql_real_escape_string($this->input->post('comment_reply'));

            $sql_query = "INSERT INTO `college_fixed_group_posts_reply_of_reply`(`post_id`,`post_reply_id`,`user_id`,`college_id`,`reply_message`) VALUES('$internship_id','$comment_id','$user_id','$college_id','$comment_reply')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $comment_reply_id = $this->db->insert_id();
                ?>
                <div class="comment_replies_box">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="font-family: 'open_sansregular';color: #525252;border-top: 1px solid #DEE3E4;">
                                <div class="job_description_details" style="padding: 0px">
                                    <div class="row">
                                        <div class="col-md-12" id="internship_comment_reply-<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>">
                                            <?php echo $comment_reply; ?>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom: 5px;">
                                        <div class="col-md-10">
                                            <span class="pull-right" style="font-size: 11px; color: #A2AAAD; display: block">reply by
                                                <a href="javascript:void(0)">
                                                    <?php
                                                    $posted_date = date('M j Y');
                                                    $posted_date = (($posted_date == date('M j Y')) ? ", today" : $posted_date) . " at " . date('h:i A');
                                                    echo $this->user->first_name . " " . $this->user->last_name;
                                                    ?>
                                                </a>
                                                <?php
                                                echo " at " . $posted_date;
                                                ?>
                                            </span>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="pull-right"><a href="javascript:void(0)" internship_id = "<?php echo $this->encryption_decryption_object->encode($internship_id); ?>" comment_id = "<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" comment_reply_id="<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>" class="comment_reply_delete_btn"><button class="btn btn-xs"><i class="fa fa-trash"></i> </button></a></span>
                                            <span class = "pull-right"><a href = "javascript:void(0)" internship_id = "<?php echo $this->encryption_decryption_object->encode($internship_id); ?>" comment_id = "<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" comment_reply_id="<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>" class = "comment_reply_edit_btn"><button class = "btn btn-xs"><i class = "fa fa-edit"></i> </button></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function comment_reply_delete() {
        $comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_reply_id'));
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));

        if ($comment_reply_id) {
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_reply_of_reply_id` = '$comment_reply_id' AND `post_id` = '$internship_id' AND `post_reply_id` = '$comment_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            echo ($query_result) ? "1" : "0";
        }
    }

    public function submit_comment() {
        $posted_data = $this->input->post();
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
        if (!empty($posted_data) && $internship_id && !empty($posted_data['comment'])) {
            $posted_date = date('M j Y');

            //user comment
            $commet = mysql_real_escape_string($posted_data['comment']);

            //user id details
            $user_id = $this->user->id;

            //college id
            $college_id = $this->college_id;

            //college_fixed_group_posts_reply
            $sql_query = "INSERT INTO `college_fixed_group_posts_reply`(`post_id`,`user_id`,`college_id`,`reply_message`) VALUE('$internship_id','$user_id','$college_id','$commet')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $comment_id = $this->db->insert_id();
                ?>
                <div class="comment_container">
                    <div class="comment_div">
                        <div class="profile_details" style="padding: 0px; margin-top: 5px;border-bottom: 1px solid #e8eced;">
                            <div style="font-family: 'open_sansregular';color: #525252;">
                                <h3 style="font-size: 21px;">
                                    <?php
                                    $posted_user = $this->ion_auth->user($user_id)->row();
                                    ?> 
                                    <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 14px;margin: 0px;line-height: 20px;"><a href="#"><?php echo $posted_user->first_name . " " . $posted_user->last_name; ?></a></span>
                                    <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">
                                        <?php
                                        $posted_date = date('M j Y');
                                        echo (($posted_date == date('M j Y')) ? " today" : $posted_date) . " at " . date('h:i A');
                                        ?>
                                    </span>
                                    <button comment_id="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs pull-right delete_post_comment"><i class="fa fa-trash"></i></button>
                                    <button comment_id="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs pull-right edit_post_comment"><i class="fa fa-edit"></i></button>
                                </h3>
                                <div id="internship_comment-<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="internship_description_details" style="margin-top: 10px;">
                                    <?php
                                    echo $commet;
                                    ?>
                                </div>
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-md-12">
                                        <button type="button" like_status ="0" internship_id ="<?php echo $this->encryption_decryption_object->encode($internship_id); ?>" comment_id ="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs comment_like_btn"><i class="fa fa-thumbs-up"></i> Like</button>
                                        <button type="button" class="btn btn-default btn-xs reply_comment_btn"><i class="fa fa-edit"></i> Reply </button>
                                        <span class="internship_description_details" style="color: #337ab7;display:none"><span class="comment_count_like_span">0</span> Like</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment_reply_box">
                            <form class="comment_reply_form">
                                <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                                    <div style="font-size: 21px;border: 1px solid #e8eced;font-size: 14px;padding: 10px;border: 1px solid #e8eced;font-family: 'open_sansregular';color: #626262;">
                                        <?php
                                        echo $this->user->first_name . " " . $this->user->last_name;
                                        ?>
                                        <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;"> replying to Comment</span>
                                        <span class="pull-right"><a href="javascript:void(0)" class="cancel_reply_to_comment"><i class="fa fa-times"></i> cancel</a></span>
                                    </div>
                                    <div>
                                        <textarea name="comment_reply" class="share_text comment_reply_textarea"></textarea>
                                        <input type="hidden" name="comment_id" value="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>"/>
                                        <input type="hidden" name="internship_id" value="<?php echo $this->encryption_decryption_object->encode($internship_id); ?>"/>
                                    </div>
                                    <div class="row commet_reply_submit_button_div">
                                        <div class="col-md-12">
                                            <button type="submit" style="margin-top: 5px" class="btn pull-right comment_reply_submit_btn btn-sm">Reply</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                        </div>
                    </div>
                    <div class="comments_reply_container" style="display: none">
                        <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="font-size: 15px;">Replies (<span class="comment_count_span">0</span>)</h4>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="comment_reply_list"></div>
                        </div>
                        <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function like_internship_commet() {
        $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));

        if ($internship_id != '' && $internship_id) {
            $user_id = $this->user->id;
            $college_id = $this->college_id;
            $user_like_status = ($this->input->post("user_like_status")) ? 0 : 1;

            $sql_query = "SELECT * from `college_fixed_group_posts_like_details` WHERE `user_id`='$user_id' AND `college_id` = '$college_id' AND `post_id`='$internship_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $like_id = $query_result[0]->id;

                $sql_query = "UPDATE `college_fixed_group_posts_like_details` SET `like_status` = '$user_like_status' WHERE `id` = '$like_id'";
                $query_result = $this->data_update->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            } else {
                /* Add comment's like */
                $sql_query = "INSERT INTO `college_fixed_group_posts_like_details`(`like_status`,`user_id`,`college_id`,`post_id`) VALUES('1','$user_id','$college_id','$internship_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            }
        }
    }

    public function comment_like() {
        $posted_data = $this->input->post();
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));

        if (!empty($posted_data) && $comment_id) {
            $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
            $internship_id = $this->encryption_decryption_object->is_valid_input($this->input->post('internship_id'));
            $user_id = $this->user->id;
            $college_id = $this->college_id;
            $user_like_status = ($this->input->post("user_like_status")) ? 0 : 1;

            $sql_query = "SELECT * from `college_fixed_group_posts_reply_like_details` WHERE `user_id`='$user_id' AND `college_id` = '$college_id' AND `post_id`='$internship_id' AND `post_reply_id` = '$comment_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $like_id = $query_result[0]->id;

                $sql_query = "UPDATE `college_fixed_group_posts_reply_like_details` SET `like_status` = '$user_like_status' WHERE `id` = '$like_id'";
                $query_result = $this->data_update->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            } else {
                /* Add comment's like */
                $sql_query = "INSERT INTO `college_fixed_group_posts_reply_like_details`(`like_status`,`post_reply_id`,`user_id`,`college_id`,`post_id`) VALUES('1','$comment_id','$user_id','$college_id','$internship_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            }
        }
    }

    public function delete_post() {
        $posted_data = $this->input->post();
        $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
        if ($post_id) {
            $sql_query = "DELETE FROM `college_fixed_group_posts` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_notify_user_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? "1" : "0";
        }
    }

    /* Delete job Comment */

    public function delete_internship_comment() {
        $posted_data = $this->input->post();
        $comment_id = $this->encryption_decryption_object->is_valid_input($posted_data['comment_id']);
        if ($comment_id) {
            $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            if ($query_result[0]->user_id == $this->user->id) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? 1 : 0;
        }
    }

    function get_user_list_to_notify() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['flag'])) {
            $college_id = $this->college_id;
            $sql_query = "SELECT b.`id`,b.`email`,b.`first_name`,b.`last_name`,b.`profile_picture`,"
                    . "c.`stream_id`,c.`stream_course_id`,c.`study_type`,c.`semester_or_year`,"
                    . " d.`title`, e.`title` FROM `college_users_intranet` as a INNER JOIN `users` as b ON a.`user_id` = b.`id`"
                    . " INNER JOIN `college_student_users_intranet` as c ON a.`id` = c.`college_users_intranet_id` "
                    . "INNER JOIN `stream` as d ON c.`stream_id`= d.`id` "
                    . "INNER JOIN `stream_courses` as e ON c.`stream_course_id` = e.`id` "
                    . "WHERE a.`college_id` = '$college_id' AND a.`intranet_user_type` = 'student'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $user_list = array();

            foreach ($query_result as $value) {
                $user_list[$this->encryption_decryption_object->encode($value->id)] = $value;
            }

            echo json_encode($user_list);
        }
    }

}
?>