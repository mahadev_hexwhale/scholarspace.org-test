<?php
/* class is to mainten the user request from the college admin */

Class Jobopening extends UserClass {

    public $encryption_decryption_object = null;
    public $college_details = null;
    public $college_id = null;
    public $user = null;

    function __construct() {
        parent::__construct();

        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->encryption_decryption_object = new Encryption();
        $this->college_id = $this->session->userdata('college_id');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails(); // Function is getting called by helper

        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    function index() {
        $college_id = $this->college_id;

        /* get all the job posted */
        $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t1.`is_approve`,t2.`post_title`,t2.`post_message` FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` WHERE t1.`post_group_name` = 'jobopening' AND t1.`college_id` = '$college_id' ORDER BY t1.`timestamp` DESC";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['jobopening_list'] = $query_result;
        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;
        $this->load->view("front-end/jobopening", $data);
    }

    public function save_update_post() {
        $posted_data = $this->input->post();
        if (!empty($posted_data)) {
            $job_title = mysql_real_escape_string($posted_data['job_title']);
            $job_description = mysql_real_escape_string($posted_data['job_description']);

            $user = $this->user;
            $user_id = $user->user_id;

            //group name
            $post_group_name = "jobopening";

            $college_id = $this->college_id;

            //Insert into jobopening group table
            $sql_query = "INSERT INTO `college_fixed_group_posts`"
                    . "(`post_group_name`,`type`,`user_id`,`college_id`) "
                    . "VALUES('$post_group_name','update','$user_id','$college_id')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $post_id = $this->db->insert_id();

                /* for post message saving */
                $sql_query = "INSERT INTO `college_fixed_group_posts_details`(`post_title`,`post_message`,`post_id`) VALUE('$job_title','$job_description','$post_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                /* for post file upload saving */
                if (!empty($posted_data['uploaded_file'])) {
                    foreach ($posted_data['uploaded_file'] as $file_value) {
                        $sql_query = "INSERT INTO `college_fixed_group_posts_files` (`file_path`,`post_id`) VALUE('$file_value','$post_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }

                /* For post notified user saving */
                if (!empty($selected_user_list)) {
                    foreach ($selected_user_list as $user_id) {
                        $sql_query = "INSERT INTO `college_fixed_group_posts_notify_user_details` (`college_id`,`post_id`,`user_id`) VALUE('$college_id','$post_id','$user_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }
            }

            if ($query_result) {
                $sql_query = "SELECT * FROM `college_admin` WHERE `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);

                $college_admin_users = array();
                foreach ($query_result as $college_user_value) {
                    $college_admin_users[] = $college_user_value->user_id;
                }
                ?>
                <li class="job_opening_list_li">
                    <h6 style="margin-bottom: 4px;">
                        <span id="job_title-<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="job_title_span"><?php echo $job_title; ?></span>
                        <button post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="btn btn-default btn-xs pull-right delete_post"><i class="fa fa-trash"></i></button>
                        <!--<button title="Edit Job"  post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="btn btn-default btn-xs pull-right edit_post"><i class="fa fa-edit"></i></button>-->
                        <p style="color: #8A8A8A;font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">posted by <a href="#"><?php echo $user->first_name . " " . $user->last_name; ?></a>
                            <?php
                            $posted_date = date('M j Y');
                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A');
                            ?>
                        </p>
                    </h6>  
                    <!--<p id="job_description-<?php echo $this->encryption_decryption_object->encode($post_id); ?>"><?php echo $job_description; ?></p>-->
                    <div class="jobopening_action_btn_div" style="margin-top: 5px">
                        <div class="row">
                            <div class="col-lg-8">
                                <!--<button class="edit_job_opening btn btn-xs"><i class="fa fa-thumbs-up"></i></button>-->
                                <a href="<?php echo site_url("user/jobopening/job_details") . "?jobId=" . $this->encryption_decryption_object->encode($post_id); ?>"><button class="edit_job_opening btn btn-xs"><i class="fa fa-external-link-square"></i> View Details</button></a>
                            </div>
                            <div class="col-lg-4">
                                <?php
                                if (!in_array($user_id, $college_admin_users)) {
                                    ?>
                                    <p class="pull-right approve_notification_p"><h6 style="color: red; font-size: 13px;"> Waiting to approve</h6></p>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function get_job_comment_details() {
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        if ($comment_id) {
            $college_id = $this->college_id;
            $sql_query = "SELECT `reply_message` FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $job_comment_array = array("comment" => "");
            if (count($query_result)) {
                $job_comment_array["comment"] = $query_result[0]->reply_message;
            }
            echo json_encode($job_comment_array);
        }
    }

    public function update_job_comment_form() {
        $job_comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_comment_id'));

        if ($job_comment_id) {
            $job_comment = $this->input->post('job_comment');

            $sql_query = "UPDATE `college_fixed_group_posts_reply` SET `reply_message` = '$job_comment' WHERE `post_reply_id` = '$job_comment_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function get_job_comment_reply_details() {
        $comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_reply_id'));

        if ($comment_reply_id) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $sql_query = "SELECT * FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_reply_of_reply_id` = '$comment_reply_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            $job_comment_reply_details = array("comment_reply" => '');

            if (count($query_result)) {
                $job_comment_reply_details["comment_reply"] = $query_result[0]->reply_message;
            }
            echo json_encode($job_comment_reply_details);
        } else {
            echo 0;
        }
    }

    public function update_job_comment_reply_form() {
        $job_comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_comment_reply_id'));

        if ($job_comment_reply_id) {
            $job_comment_reply = $this->input->post('job_comment_reply');

            $sql_query = "UPDATE `college_fixed_group_posts_reply_of_reply` SET `reply_message` = '$job_comment_reply' WHERE `post_reply_of_reply_id` = '$job_comment_reply_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function get_job_content() {
        $college_id = $this->college_id;
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
        if ($job_id) {
            $sql_query = "SELECT `post_title`,`post_message` FROM `college_fixed_group_posts_details` WHERE `post_id` = '$job_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $job_content_array = array("title" => "", "description" => "");

            if (count($query_result)) {
                $job_content_array["title"] = $query_result[0]->post_title;
                $job_content_array["description"] = $query_result[0]->post_message;
            }
            echo json_encode($job_content_array);
        }
    }

    public function update_job_form() {
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
        if ($job_id) {
            $job_title = $this->input->post('job_title');
            $job_description = $this->input->post('job_description');

            $sql_query = "UPDATE `college_fixed_group_posts_details` SET `post_title` = '$job_title', `post_message` = '$job_description' WHERE `post_id` = '$job_id'";
            $query_result = $this->data_update->data_query($sql_query);

            echo ($query_result) ? "1" : "0";
        }
    }

    public function submit_comment() {
        $posted_data = $this->input->post();
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
        if (!empty($posted_data) && $job_id && !empty($posted_data['comment'])) {
            $posted_date = date('M j Y');

//user comment
            $commet = mysql_real_escape_string($posted_data['comment']);

//user id details
            $user_id = $this->user->id;

//college id
            $college_id = $this->college_id;

//college_fixed_group_posts_reply
            $sql_query = "INSERT INTO `college_fixed_group_posts_reply`(`post_id`,`user_id`,`college_id`,`reply_message`) VALUE('$job_id','$user_id','$college_id','$commet')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $comment_id = $this->db->insert_id();
                ?>
                <div class="comment_container">
                    <div class="comment_div">
                        <div class="profile_details" style="padding: 0px; margin-top: 5px;border-bottom: 1px solid #e8eced;">
                            <div style="font-family: 'open_sansregular';color: #525252;">
                                <h3 style="font-size: 21px;">
                                    <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 14px;margin: 0px;line-height: 20px;"><a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a></span>
                                    <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;">
                                        <?php
                                        $posted_date = date('M j Y');
                                        echo (($posted_date == date('M j Y')) ? " today" : $posted_date) . " at " . date('h:i A');
                                        ?>
                                    </span>
                                    <button comment_id="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs pull-right delete_post_comment"><i class="fa fa-trash"></i></button>
                                    <button comment_id="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs pull-right edit_post_comment"><i class="fa fa-edit"></i></button>
                                </h3>
                                <div class="job_description_details" id="job_comment-<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" style="margin-top: 10px;">
                                    <?php
                                    echo $commet;
                                    ?>
                                </div>
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-md-12">
                                        <button type="button" like_status ="0" job_id ="<?php echo $this->encryption_decryption_object->encode($job_id); ?>" comment_id="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" class="btn btn-default btn-xs comment_like_btn">
                                            <i class="fa fa-thumbs-up"></i> Like
                                        </button>
                                        <button type="button" class="btn btn-default btn-xs reply_comment_btn"><i class="fa fa-edit"></i> Reply </button>
                                        <span class="job_description_details" style="color: #337ab7;display:none"><span class="comment_count_like_span">0</span> Like</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment_reply_box">
                            <form class="comment_reply_form">
                                <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                                    <div style="font-size: 21px;border: 1px solid #e8eced;font-size: 14px;padding: 10px;border: 1px solid #e8eced;font-family: 'open_sansregular';color: #626262;">
                                        <?php
                                        echo $this->user->first_name . " " . $this->user->last_name;
                                        ?>
                                        <span style="color: #8A8A8A; font-family: 'open_sansregular';font-size: 11px;margin: 0px;line-height: 20px;"> replying to Comment</span>
                                        <span class="pull-right"><a href="javascript:void(0)" class="cancel_reply_to_comment"><i class="fa fa-times"></i> cancel</a></span>
                                    </div>
                                    <div>
                                        <textarea name="comment_reply" class="share_text comment_reply_textarea"></textarea>
                                        <input type="hidden" name="comment_id" value="<?php echo $this->encryption_decryption_object->encode($comment_id); ?>"/>
                                        <input type="hidden" name="job_id" value="<?php echo $this->encryption_decryption_object->encode($job_id); ?>"/>
                                    </div>
                                    <div class="row commet_reply_submit_button_div">
                                        <div class="col-md-12">
                                            <button type="submit" style="margin-top: 5px" class="btn pull-right comment_reply_submit_btn btn-sm">Reply</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                        </div>
                    </div>
                    <div class="comments_reply_container" style="display: none">
                        <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;background-color: #ECECEC;font-family: 'open_sansregular';font-size: 13px;color: #626262;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="font-size: 15px;">Replies (<span class="comment_count_span">0</span>)</h4>
                                </div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="comment_reply_list"></div>
                        </div>
                        <div class="profile_details" style="padding: 5px; border-bottom: 1px solid #e8eced;"></div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function approve_job_posted() {
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
        if ($job_id) {
            $college_id = $this->college_id;
            $sql_query = "UPDATE `college_fixed_group_posts` SET `is_approve` = '1' WHERE `post_id` = '$job_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_update->data_query($sql_query);
            echo ($query_result) ? "1" : "0";
        }
    }

    public function decline_job_posted() {
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
        if ($job_id) {
            $college_id = $this->college_id;
            $sql_query = "DELETE FROM `college_fixed_group_posts` WHERE `post_id` = '$job_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_details` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_files` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$job_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_notify_user_details` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_id` = '$job_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? "1" : "0";
        }
    }

    public function like_job_commet() {
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));

        if ($job_id != '' && $job_id) {
            $user_id = $this->user->id;
            $college_id = $this->college_id;
            $user_like_status = ($this->input->post("user_like_status")) ? 0 : 1;

            $sql_query = "SELECT * from `college_fixed_group_posts_like_details` WHERE `user_id`='$user_id' AND `college_id` = '$college_id' AND `post_id`='$job_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $like_id = $query_result[0]->id;

                $sql_query = "UPDATE `college_fixed_group_posts_like_details` SET `like_status` = '$user_like_status' WHERE `id` = '$like_id'";
                $query_result = $this->data_update->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            } else {
                /* Add comment's like */
                $sql_query = "INSERT INTO `college_fixed_group_posts_like_details`(`like_status`,`user_id`,`college_id`,`post_id`) VALUES('1','$user_id','$college_id','$job_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            }
        }
    }

    public function job_details() {
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->get('jobId'));
        if ($job_id != '' && $job_id) {

            /* Get job details */
            $sql_query = "SELECT t1.`post_id`,t1.`user_id`,t1.`post_like`,t1.`timestamp`,t2.`post_title`,t2.`post_message` FROM `college_fixed_group_posts` as t1 INNER JOIN `college_fixed_group_posts_details` as t2 ON t1.`post_id` = t2.`post_id` WHERE t1.`post_id` = '$job_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['job_details'] = $query_result[0];

            /* get job comments */
            $sql_query = "SELECT `post_reply_id`,`user_id`,`reply_message`,`timestamp`,`post_id` FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$job_id' ORDER BY `timestamp` DESC";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['job_reply_list'] = $query_result;

            /* get job likes details */
            $sql_query = "SELECT `like_status`,`user_id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$job_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['job_like_status'] = $query_result;

            /* get job details comments likes */
            $data['job_comment_like_status'] = array();

            $data['job_comment_reply'] = array();

            foreach ($data['job_reply_list'] as $value) {
                $comment_id = $value->post_reply_id;

                /* get likes details */
                $sql_query = "SELECT `like_status`,`user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$job_id' AND `post_reply_id` = '$comment_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['job_comment_like_status'][$comment_id] = $query_result;

                /* get reply details */
                $sql_query = "SELECT * FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_id` = '$job_id' AND `post_reply_id` = '$comment_id' ORDER BY `timestamp` DESC";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['job_comment_reply'][$comment_id] = $query_result;
            }

            $data['job_id'] = $this->input->get('jobId');
            $data['user'] = $this->user;
            $data['college_details'] = $this->college_details;
            $this->load->view("front-end/job_details", $data);
        }
    }

    public function comment_like() {
        $posted_data = $this->input->post();
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));

        if (!empty($posted_data) && $comment_id) {
            $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
            $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));
            $user_id = $this->user->id;
            $college_id = $this->college_id;
            $user_like_status = ($this->input->post("user_like_status")) ? 0 : 1;

            $sql_query = "SELECT * from `college_fixed_group_posts_reply_like_details` WHERE `user_id`='$user_id' AND `college_id` = '$college_id' AND `post_id`='$job_id' AND `post_reply_id` = '$comment_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $like_id = $query_result[0]->id;

                $sql_query = "UPDATE `college_fixed_group_posts_reply_like_details` SET `like_status` = '$user_like_status' WHERE `id` = '$like_id'";
                $query_result = $this->data_update->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            } else {
                /* Add comment's like */
                $sql_query = "INSERT INTO `college_fixed_group_posts_reply_like_details`(`like_status`,`post_reply_id`,`user_id`,`college_id`,`post_id`) VALUES('1','$comment_id','$user_id','$college_id','$job_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                echo ($query_result) ? "1" : "0";
            }
        }
    }

    public function save_reply_comment() {
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));

        if ($job_id && $comment_id) {
            $college_id = $this->college_id;
            $user_id = $this->user->id;
            $comment_reply = mysql_real_escape_string($this->input->post('comment_reply'));

            $sql_query = "INSERT INTO `college_fixed_group_posts_reply_of_reply`(`post_id`,`post_reply_id`,`user_id`,`college_id`,`reply_message`) VALUES('$job_id','$comment_id','$user_id','$college_id','$comment_reply')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $comment_reply_id = $this->db->insert_id();
                ?>
                <div class="comment_replies_box">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="font-family: 'open_sansregular';color: #525252;border-top: 1px solid #DEE3E4;">
                                <div class="job_description_details" style="padding: 0px">
                                    <div class="row">
                                        <div class="col-md-12" id="job_comment_reply-<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>">
                                            <?php echo $comment_reply; ?>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom: 5px;">
                                        <div class="col-md-10">
                                            <span class="pull-right" style="font-size: 11px; color: #A2AAAD; display: block">reply by
                                                <a href="javascript:void(0)">
                                                    <?php
                                                    $posted_date = date('M j Y');
                                                    $posted_date = (($posted_date == date('M j Y')) ? ", today" : $posted_date) . " at " . date('h:i A');
                                                    echo $this->user->first_name . " " . $this->user->last_name;
                                                    ?>
                                                </a>
                                                <?php
                                                echo " at " . $posted_date;
                                                ?>
                                            </span>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="pull-right"><a href="javascript:void(0)" job_id = "<?php echo $this->encryption_decryption_object->encode($job_id); ?>" comment_id = "<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" comment_reply_id="<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>" class="comment_reply_delete_btn"><button class="btn btn-xs"><i class="fa fa-trash"></i> </button></a></span>
                                            <span class = "pull-right">
                                                <a href = "javascript:void(0)" job_id = "<?php echo $this->encryption_decryption_object->encode($job_id); ?>" comment_id = "<?php echo $this->encryption_decryption_object->encode($comment_id); ?>" comment_reply_id="<?php echo $this->encryption_decryption_object->encode($comment_reply_id); ?>" class = "comment_reply_edit_btn">
                                                    <button class = "btn btn-xs"><i class = "fa fa-edit"></i> </button>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function comment_reply_delete() {
        $comment_reply_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_reply_id'));
        $comment_id = $this->encryption_decryption_object->is_valid_input($this->input->post('comment_id'));
        $job_id = $this->encryption_decryption_object->is_valid_input($this->input->post('job_id'));

        if ($comment_reply_id) {
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_of_reply` WHERE `post_reply_of_reply_id` = '$comment_reply_id' AND `post_id` = '$job_id' AND `post_reply_id` = '$comment_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            echo ($query_result) ? "1" : "0";
        }
    }

    public function submit_add_new_jobopening() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['job_title']) && !empty($posted_data['job_description'])) {
            $job_title = mysql_real_escape_string($posted_data['job_title']);
            $job_description = mysql_real_escape_string($posted_data['job_description']);
            $college_id = $this->college_id;
            $timestamp = date('Y-m-d h:i:s');

            /* INSERT INTO jobopening table */
            $sql_query = "INSERT INTO `college_jobopenings_list`(`college_id`,`job_title`,`job_description`,`job_added_timestamp`) VALUES('$college_id','$job_title','$job_description','$timestamp')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                echo $this->encryption_decryption_object->encode($this->db->insert_id());
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function delete_post() {
        $posted_data = $this->input->post();
        $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
        if ($post_id) {
            $sql_query = "DELETE FROM `college_fixed_group_posts` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_notify_user_details` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? "1" : "0";
        }
    }

    /* Delete job Comment */

    public function delete_job_comment() {
        $posted_data = $this->input->post();
        $comment_id = $this->encryption_decryption_object->is_valid_input($posted_data['comment_id']);
        if ($comment_id) {
            $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            if ($query_result[0]->user_id == $this->user->id) {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$comment_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            echo ($query_result) ? 1 : 0;
        }
    }

    /* Submit edit jobopening details form */

    public function submit_edit_jobopening() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['job_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['job_id']) && !empty($posted_data['job_title']) && !empty($posted_data['job_description'])) {
            $job_id = $this->encryption_decryption_object->is_valid_input($posted_data['job_id']);
            $job_title = mysql_real_escape_string($posted_data['job_title']);
            $job_description = mysql_real_escape_string($posted_data['job_description']);

            /* update the job opening details */
            $sql_query = "UPDATE `college_jobopenings_list` SET `job_title` = '$job_title', `job_description` = '$job_description' WHERE `job_id` = '$job_id'";
            $query_result = $this->data_update->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function delete_jobopening() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['jobopening_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['jobopening_id'])) {
            $jobopening_id = $this->encryption_decryption_object->is_valid_input($posted_data['jobopening_id']);
            $sql_query = "DELETE FROM `college_jobopenings_list` WHERE `job_id` = '$jobopening_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    function get_user_list_to_notify() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['flag'])) {
            $college_id = $this->college_id;
            $sql_query = "SELECT b.`id`,b.`email`,b.`first_name`,b.`last_name`,b.`profile_picture`,"
                    . "c.`stream_id`,c.`stream_course_id`,c.`study_type`,c.`semester_or_year`,"
                    . " d.`title`, e.`title` FROM `college_users_intranet` as a INNER JOIN `users` as b ON a.`user_id` = b.`id`"
                    . " INNER JOIN `college_student_users_intranet` as c ON a.`id` = c.`college_users_intranet_id` "
                    . "INNER JOIN `stream` as d ON c.`stream_id`= d.`id` "
                    . "INNER JOIN `stream_courses` as e ON c.`stream_course_id` = e.`id` "
                    . "WHERE a.`college_id` = '$college_id' AND a.`intranet_user_type` = 'student'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $user_list = array();

            foreach ($query_result as $value) {
                $user_list[$this->encryption_decryption_object->encode($value->id)] = $value;
            }

            echo json_encode($user_list);
        }
    }

}
