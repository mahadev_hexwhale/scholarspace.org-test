<?php
/* class is to mainten the user request from the college admin */

Class Groups extends UserClass {

    public $user = null;
    public $college_details = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->encryption_decryption_object = new Encryption();
    }

    /*     * ***************** Supporting Methods ****************** */

    // To decrypt the values from array and send
    public function EncryptThis($value) {
        foreach ($value as $k => $v) {
            switch ($k) {
                case 'id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'user_intranet_id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'college_users_intranet_id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'user_id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'stream_id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
                case 'stream_course_id':
                    $newVal = $this->encryption_decryption_object->encode($v);
                    $value->$k = $newVal;
                    break;
            }
        }
        return $value;
    }

    public function GetOriginalIDs($Array) {
        // Get original ids
        foreach ($Array as $key => $value) {
            $NewVal = $this->encryption_decryption_object->is_valid_input($value);
            if ($NewVal) {
                $Array[$key] = $NewVal;
            } else {
                return 0;
            }
        }
        return $Array;
    }

    public function GetMemberList_Method($GroupID) {
        $MemberList = array();
        $college_id = $this->session->userdata('college_id');

        $GetMemberList = "SELECT t1.*, t2.intranet_user_type "
                . "FROM college_group_users AS t1 "
                . "INNER JOIN college_users_intranet AS t2 ON t1.user_intranet_id = t2.id "
                . "WHERE t1.college_id = '$college_id' AND t1.group_id = '$GroupID'";
        $GetMemberList_Result = $this->data_fetch->data_query($GetMemberList);

        foreach ($GetMemberList_Result as $key => $value) {
            switch ($value->intranet_user_type) {
                case 'student':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.study_type,t1.semester_or_year, "
                            . "t2.title AS stream_name, "
                            . "t3.title AS stream_course_name, "
                            . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM college_student_users_intranet AS t1 "
                            . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                            . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $MemberList[$value1->user_intranet_id] = $value1;
                    }
                    break;
                case 'alumni':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.year_of_passing, "
                            . "t2.title AS stream_name, "
                            . "t3.title AS stream_course_name, "
                            . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM college_alumni_users_intranet AS t1 "
                            . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                            . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $MemberList[$value1->user_intranet_id] = $value1;
                    }
                    break;
                case 'teacher':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id, "
                            . "t4.id AS user_id, t4.first_name, t4.last_name, t4.email, t4.profile_picture,"
                            . "t5.intranet_user_type "
                            . "FROM college_teacher_users_intranet AS t1 "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $MemberList[$value1->user_intranet_id] = $value1;
                    }
                    break;
            }
        }

        foreach ($MemberList as $key => $value) {
            $MemberList[$key] = $this->EncryptThis($value);
        }
        return $MemberList;
    }

    public function GetMemberRequests_Method($GroupID) {
        $RequestList = array();
        $college_id = $this->session->userdata('college_id');

        $SelectRequests = "SELECT * FROM college_user_group_request "
                . "WHERE group_id = '$GroupID' AND college_id = '$college_id'";
        $SelectRequests_Result = $this->data_fetch->data_query($SelectRequests);

        foreach ($SelectRequests_Result as $key => $value) {
            switch ($value->intranet_user_type) {
                case 'student':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.study_type,t1.semester_or_year, "
                            . "t2.title AS stream_name, "
                            . "t3.title AS stream_course_name, "
                            . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM college_student_users_intranet AS t1 "
                            . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                            . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $RequestList[$value1->user_intranet_id] = $value1;
                    }
                    break;
                case 'alumni':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.year_of_passing, "
                            . "t2.title AS stream_name, "
                            . "t3.title AS stream_course_name, "
                            . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM college_alumni_users_intranet AS t1 "
                            . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                            . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $RequestList[$value1->user_intranet_id] = $value1;
                    }
                    break;
                case 'teacher':
                    $MemberListDetails = "SELECT "
                            . "t1.college_users_intranet_id AS user_intranet_id, "
                            . "t4.id AS user_id, t4.first_name, t4.last_name, t4.email "
                            . "t5.intranet_user_type "
                            . "FROM college_teacher_users_intranet AS t1 "
                            . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                    $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                    foreach ($MemberList_Result as $value1) {
                        $RequestList[$value1->user_intanet_id] = $value1;
                    }
                    break;
            }
        }
        foreach ($RequestList as $key => $value) {
            $RequestList[$key] = $this->EncryptThis($value);
        }
        return $RequestList;
    }

    /*     * ******************** Main Methods ****************** */

    // function will load the request list page into the college admin profile
    public function index() {
        $get = $this->input->get();
        $college_id = $this->session->userdata('college_id');
        $data['college_details'] = $this->college_details;
        $GroupID = $this->encryption_decryption_object->is_valid_input($get['grp_id']);
        if ($GroupID) {

            $GetGroupDetails = "SELECT * FROM college_groups "
                    . "WHERE id = '$GroupID' AND college_id = '$college_id'";
            $GetGroupDetails_Result = $this->data_fetch->data_query($GetGroupDetails);

            $GetGroupFileDetails = "SELECT t1.id,t1.file_type,t1.file_path,"
                    . "t2.user_id,t2.timestamp,"
                    . "t3.first_name,t3.last_name "
                    . "FROM college_unfixed_group_posts_files t1 "
                    . "INNER JOIN college_unfixed_group_posts t2 ON t1.post_id = t2.id "
                    . "INNER JOIN users t3 ON t2.user_id = t3.id "
                    . "WHERE t1.group_id = '$GroupID'";
            $GetGroupFileDetails_Result = $this->data_fetch->data_query($GetGroupFileDetails);

            //Get all badge images
            $data['badges'] = $this->data_fetch->getBadges();

            $data['all_posts'] = $this->get_unfixed_group_post($GroupID);
            $data['member'] = $this->GetMemberList_Method($GroupID);
            $data['group'] = $GetGroupDetails_Result[0];
            $data['files'] = $GetGroupFileDetails_Result;
            $data['GroupID'] = "$GroupID";
            $data['user'] = $this->user;

            if (count($GetGroupDetails_Result)) {
                // Check if user is admin of group, if not
                if ($GetGroupDetails_Result[0]->group_admin_user_id != $this->user->id) {
                    // Check if the group is public or private
                    if ($GetGroupDetails_Result[0]->visible_to == "Public") {
                        // If the group is public group, display group to the user
                        $this->load->view("front-end/groups", $data);
                    } else {
                        // If the group is not public
                        // Check if the user is member of group or no
                        $GroupID_encrypt = $this->encryption_decryption_object->encode($GroupID);
                        $IsMemberOfGroup = CheckIfMemberOfGroup_Method($GroupID_encrypt);
                        if ($IsMemberOfGroup) {
                            // If the user is member of group, display the group content
                            $this->load->view("front-end/groups", $data);
                        } else {
                            // If the user is not a member of group, display 404 page
                            show_404();
                        }
                    }
                } else {
                    $data['requests'] = $this->GetMemberRequests_Method($GroupID);
                    $this->load->view("front-end/groups", $data);
                }
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function view() {
        $data['user'] = $this->user;
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;
        $logged_user_id = $this->user->id;

        //Get all unfixed groups in which the user is member
        $GetGroups = "SELECT t1.* FROM college_groups t1 "
                . "INNER JOIN college_group_users t2 ON t2.group_id = t1.id "
                . "WHERE t1.college_id = '$college_id' AND t2.user_id = '$logged_user_id' ";
        $GetGroups_Result = $this->data_fetch->data_query($GetGroups);

        $data['groups_list'] = $GetGroups_Result;

        $this->load->view('front-end/groups_view', $data);
    }

    public function GetCollegeMembers_Method() {
        $PostData = $this->input->post();
        $college_id = $this->session->userdata('college_id');
        $logged_user_id = $this->user->id;

        $SelectCollegeUsers = "SELECT t1.* "
                . "FROM `college_users_intranet` t1 "
                . "INNER JOIN users t2 ON t1.user_id = t2.id "
                . "WHERE `college_id` = '$college_id' AND t1.user_id != '$logged_user_id' "
                . "ORDER BY t2.first_name ASC ";
//                . "LIMIT 0, 5";
        $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

        $List = array();
        $final_list = array();
        //count array list
        $number_to_word = array(1 => "First", 2 => "Second", 3 => "Third", 4 => "Forth", 5 => "Fifth", 6 => "Sixth", 7 => "Seventh", 8 => "Eighth", 8 => "Ninth", 8 => "Tenth", 8 => "Eleventh", 8 => "Twelth");

        foreach ($SelectCollegeUsers_Result as $key => $value) {
            $intranet_user_type = $value->intranet_user_type;
            switch ($intranet_user_type) {
                case 'student':
                    $SelectDetailsOfStudent = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_student_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                    foreach ($SelectDetailsOfStudent_Result as $value1) {
                        $List[$value1->college_users_intranet_id] = $value1;
                    }
                    break;
                case 'alumni':
                    $SelectDetailsOfAlumni = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_alumni_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                    foreach ($SelectDetailsOfAlumni_Result as $value2) {
                        $List[$value2->college_users_intranet_id] = $value2;
                    }
                    break;
                case 'teacher':
                    $SelectDetailsOfTeacher = "SELECT t1.*, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                            . "t5.intranet_user_type "
                            . "FROM `college_teacher_users_intranet` AS t1 "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);

                    foreach ($SelectDetailsOfTeacher_Result as $value3) {
                        $List[$value3->college_users_intranet_id] = $value3;
                    }
                    break;
            }
        }
        $final_list['student_list'] = $List;

        /* Student List Batch wise */
        $sql_query = "SELECT t2.`title` as `stream_name`, t3.`title` as `stream_course_name`, "
                . "t1.`study_type`, t1.`college_users_intranet_id`, t1.`semester_or_year`, t5.* "
                . "FROM `college_student_users_intranet` as t1 "
                . "INNER JOIN `stream` as t2 ON t1.`stream_id` = t2.`id` "
                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                . "INNER JOIN `college_users_intranet` as t4 ON t1.`college_users_intranet_id` = t4.`id` "
                . "RIGHT JOIN `users` as t5 ON t4.`user_id` = t5.`id` "
                . "WHERE t4.`college_id` = '$college_id' AND t4.`intranet_user_type` = 'student'";
        $query_result = $this->data_fetch->data_query($sql_query);

        foreach ($query_result as $value) {
            $final_list['student_batch_list']["<h5>" . $value->stream_name . " (" . $number_to_word[$value->semester_or_year] . " " . ucfirst($value->study_type) . ")</h5><h6>" . $value->stream_course_name . "</h6>"][] = $this->encryption_decryption_object->encode($value->college_users_intranet_id);
        }
        echo json_encode($final_list);
    }

    public function FilterCollegeMembers_Method() {
        $PostData = $this->input->post();
        $logged_user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');

        $Item = $PostData['Item'];
        if (!empty($PostData)) {
            $SelectCollegeUsers = "SELECT t1.* "
                    . "FROM `college_users_intranet` t1 "
                    . "INNER JOIN users t2 ON t1.user_id = t2.id "
                    . "WHERE `college_id` = '$college_id' AND t1.user_id != '$logged_user_id' "
                    . "ORDER BY t2.first_name ASC ";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            $List = array();
            $final_list = array();
            $number_to_word = array(1 => "First", 2 => "Second", 3 => "Third", 4 => "Forth", 5 => "Fifth", 6 => "Sixth", 7 => "Seventh", 8 => "Eighth", 8 => "Ninth", 8 => "Tenth", 8 => "Eleventh", 8 => "Twelth");

            foreach ($SelectCollegeUsers_Result as $key => $value) {
                $intranet_user_type = $value->intranet_user_type;

                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfStudent .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfAlumni .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfTeacher .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfTeacher .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value3) {
                            $List[$value3->college_users_intranet_id] = $value3;
                        }
                        break;
                }
            }
            $final_list['student_list'] = $List;

            /* Student List Batch wise */
            $sql_query = "SELECT t2.`title` as `stream_name`, t3.`title` as `stream_course_name`, "
                    . "t1.`study_type`, t1.`college_users_intranet_id`, t1.`semester_or_year`, t5.* "
                    . "FROM `college_student_users_intranet` as t1 "
                    . "INNER JOIN `stream` as t2 ON t1.`stream_id` = t2.`id` "
                    . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                    . "INNER JOIN `college_users_intranet` as t4 ON t1.`college_users_intranet_id` = t4.`id` "
                    . "RIGHT JOIN `users` as t5 ON t4.`user_id` = t5.`id` "
                    . "WHERE t4.`college_id` = '$college_id' AND t4.`intranet_user_type` = 'student' AND ";
            if ($Item != "") {
                $sql_query .= "(t2.`title` LIKE '$Item%' OR t3.`title` LIKE '$Item%' ) ";
            }
            $query_result = $this->data_fetch->data_query($sql_query);

            foreach ($query_result as $value) {
                $final_list['student_batch_list']["<h5>" . $value->stream_name . " (" . $number_to_word[$value->semester_or_year] . " " . ucfirst($value->study_type) . ")</h5><h6>" . $value->stream_course_name . "</h6>"][] = $this->encryption_decryption_object->encode($value->college_users_intranet_id);
            }
            echo json_encode($final_list);
        }
    }

    public function CreatNewGroup_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $PostData = $this->input->post();

        //Get post data
        $GroupName = $PostData['GroupName'];
        $VisibleTo = $PostData['VisibleTo'];
        $GroupAccess = $PostData['GroupAccess'];

        if ($this->input->post('AddedMembersList') != "") {
            $AddedMembersList = $this->input->post('AddedMembersList');
        }
        if ($this->input->post('group_members_group') != "{}") {
            $group_members_group = json_decode($this->input->post('group_members_group'));
            foreach ($group_members_group as $users_list) {
                $users_list = explode(',', $users_list);
                foreach ($users_list as $value) {
                    $value = trim($value);
                    $group_user_id = $this->encryption_decryption_object->is_valid_input($value);
                    if ($group_user_id) {
                        $group_member_list_array[] = $group_user_id;
                    } else {
                        echo 0;
                        die();
                    }
                }
            }
            $group_member_list_array = array_unique($group_member_list_array);
        }
        $created_on = date('Y-m-d H:i:s');

        if (!empty($PostData)) {
            //Insert into main groups table
            $group_icon_image = "assets_front/image/profile_pic.png";
            $InsertGroup = "INSERT INTO "
                    . "college_groups(name, visible_to, group_access, college_id, group_admin_user_id, group_icon_image,created_on) "
                    . "VALUES('$GroupName', '$VisibleTo', '$GroupAccess', '$college_id', '$user_id','$group_icon_image','$created_on')";
            $InsertGroup_Result = $this->data_insert->data_query($InsertGroup);

            //get newly added group's id
            $CollegeGroupId = $this->db->insert_id();

            $SelectIntranetId = "SELECT id FROM college_users_intranet "
                    . "WHERE college_id = '$college_id' AND user_id = '$user_id'";
            $SelectIntranetId_Result = $this->data_fetch->data_query($SelectIntranetId);
            $member_intranet_id = $SelectIntranetId_Result[0]->id;

            $InsertAdmin = "INSERT INTO college_group_users(group_id, college_id, user_id, user_intranet_id) "
                    . "VALUES('$CollegeGroupId', '$college_id', '$user_id', '$member_intranet_id')";
            $InsertAdmin_Result = $this->data_insert->data_query($InsertAdmin);

            //insert all members into this group_users_intranet table
            if ($this->input->post('AddedMembersList') != "") {
                foreach ($AddedMembersList as $key => $value) {
                    //get user_id of member
                    $SelectIntranetId = "SELECT user_id "
                            . "FROM college_users_intranet "
                            . "WHERE college_id = '$college_id' AND id = '$value'";
                    $SelectIntranetId_Result = $this->data_fetch->data_query($SelectIntranetId);
                    $member_user_id = $SelectIntranetId_Result[0]->user_id;

                    //insert into table
                    $InsertGroupUsers = "INSERT INTO "
                            . "college_group_users(group_id, college_id, user_id, user_intranet_id) "
                            . "VALUES('$CollegeGroupId', '$college_id', '$member_user_id', '$value')";
                    $InsertGroupUsers_Result = $this->data_insert->data_query($InsertGroupUsers);
                }
            }
            if ($this->input->post('group_members_group') != "") {
                if (!empty($group_member_list_array)) {
                    foreach ($group_member_list_array as $value) {
                        //get user_id of member
                        $SelectIntranetId = "SELECT user_id "
                                . "FROM college_users_intranet "
                                . "WHERE college_id = '$college_id' AND id = '$value'";
                        $SelectIntranetId_Result = $this->data_fetch->data_query($SelectIntranetId);
                        $member_user_id = $SelectIntranetId_Result[0]->user_id;

                        //insert into table
                        $InsertGroupUsers = "INSERT INTO "
                                . "college_group_users(group_id, college_id, user_id, user_intranet_id) "
                                . "VALUES('$CollegeGroupId', '$college_id', '$member_user_id', '$value')";
                        $InsertGroupUsers_Result = $this->data_insert->data_query($InsertGroupUsers);
                    }
                }
            }
        }
        if ($InsertGroupUsers_Result) {
            echo $this->encryption_decryption_object->encode($CollegeGroupId);
        } else {
            echo 0;
        }
    }

    public function CheckGroupName_Method() {

        $GroupName = $this->input->post('GroupName');
        $college_id = $this->session->userdata('college_id');

        $SelectGroups = "SELECT name FROM college_groups WHERE college_id = '$college_id' AND name = '$GroupName'";
        $SelectGroups_Result = $this->data_fetch->data_query($SelectGroups);

        if (count($SelectGroups_Result)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function GetRemainingMembers_Method() {

        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $AddedMembersList = $this->input->post('AddedMembersList_more');

        foreach ($AddedMembersList as $key => $value) {
            $NewVal = $this->encryption_decryption_object->is_valid_input($value);
            $AddedMembersList[$key] = $NewVal;
        }

        if (!empty($AddedMembersList) && $GroupID)
            $user_ids = implode(", ", $AddedMembersList);

        $college_id = $this->session->userdata('college_id');
        $List = array();
        $final_list = array();
        $number_to_word = array(1 => "First", 2 => "Second", 3 => "Third", 4 => "Forth", 5 => "Fifth", 6 => "Sixth", 7 => "Seventh", 8 => "Eighth", 8 => "Ninth", 8 => "Tenth", 8 => "Eleventh", 8 => "Twelth");

        if ($GroupID != "" && $GroupID) {

            $SelectIntranetMembers = "SELECT * "
                    . "FROM college_users_intranet ";
            if (!empty($AddedMembersList))
                $SelectIntranetMembers .= "WHERE id NOT IN ($user_ids) AND college_id = '$college_id'";
            else
                $SelectIntranetMembers .= "WHERE college_id = '$college_id'";
            $SelectIntranetMembers_Result = $this->data_fetch->data_query($SelectIntranetMembers);

            foreach ($SelectIntranetMembers_Result as $key => $value1) {
                $intranet_user_type = $value1->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value1->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value1->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value3) {
                            $List[$value3->college_users_intranet_id] = $value3;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value1->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value1->id "
                                . "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value4) {
//                            $List[$value4->college_users_intranet_id] = $value4;
                        }
                        break;
                }
            }
        }
        foreach ($List as $key => $value) {
            $List[$key] = $this->EncryptThis($value);
        }
        $final_list['student_list'] = $List;

        /* Student List Batch wise */
        $sql_query = "SELECT t2.`title` as `stream_name`, t3.`title` as `stream_course_name`, "
                . "t1.`study_type`, t1.`college_users_intranet_id`, t1.`semester_or_year`, t5.* "
                . "FROM `college_student_users_intranet` as t1 "
                . "INNER JOIN `stream` as t2 ON t1.`stream_id` = t2.`id` "
                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                . "INNER JOIN `college_users_intranet` as t4 ON t1.`college_users_intranet_id` = t4.`id` "
                . "RIGHT JOIN `users` as t5 ON t4.`user_id` = t5.`id` "
                . "WHERE t4.`college_id` = '$college_id' AND t4.`intranet_user_type` = 'student' ";
        if (!empty($AddedMembersList))
            $sql_query .= "AND t1.college_users_intranet_id NOT IN ($user_ids)";
        $query_result = $this->data_fetch->data_query($sql_query);

        foreach ($query_result as $value) {
            $final_list['student_batch_list']["<h5>" . $value->stream_name . " (" . $number_to_word[$value->semester_or_year] . " " . ucfirst($value->study_type) . ")</h5><h6>" . $value->stream_course_name . "</h6>"][] = $this->encryption_decryption_object->encode($value->college_users_intranet_id);
        }

        echo json_encode($final_list);
    }

    public function FilterAddMoreCollegeMembers_Method() {
        $college_id = $this->session->userdata('college_id');
        $PostData = $this->input->post();
        $AddedMembersList = $this->input->post('AddedMembersList_more');


        // Get original ids
        foreach ($AddedMembersList as $key => $value) {
            $NewVal = $this->encryption_decryption_object->is_valid_input($value);
            $AddedMembersList[$key] = $NewVal;
        }

        $user_ids = implode(", ", $AddedMembersList);
        $Item = $PostData['Item'];
        if (!empty($PostData)) {

            $SelectIntranetMembers = "SELECT * "
                    . "FROM college_users_intranet "
                    . "WHERE id NOT IN ($user_ids) AND college_id = '$college_id'";
            $SelectIntranetMembers_Result = $this->data_fetch->data_query($SelectIntranetMembers);

            $List = array();
            $final_list = array();
            $number_to_word = array(1 => "First", 2 => "Second", 3 => "Third", 4 => "Forth", 5 => "Fifth", 6 => "Sixth", 7 => "Seventh", 8 => "Eighth", 8 => "Ninth", 8 => "Tenth", 8 => "Eleventh", 8 => "Twelth");

            foreach ($SelectIntranetMembers_Result as $key => $value) {
                $intranet_user_type = $value->intranet_user_type;

                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfStudent = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfStudent .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfStudent .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                        foreach ($SelectDetailsOfStudent_Result as $value1) {
                            $List[$value1->college_users_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $SelectDetailsOfAlumni = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfAlumni .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfAlumni .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                        foreach ($SelectDetailsOfAlumni_Result as $value2) {
                            $List[$value2->college_users_intranet_id] = $value2;
                        }
                        break;
                    case 'teacher':
                        $SelectDetailsOfTeacher = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id AND ";
                        if ($Item != "") {
                            $SelectDetailsOfTeacher .= "(t4.first_name LIKE '$Item%' OR t4.last_name LIKE '$Item%' OR t4.email LIKE '$Item%') ";
                        }
                        $SelectDetailsOfTeacher .= "ORDER BY t4.first_name ASC";
                        $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                        foreach ($SelectDetailsOfTeacher_Result as $value3) {
//                            $List[$value3->college_users_intranet_id] = $value3;
                        }
                        break;
                }
            }

            foreach ($List as $key => $value) {
                $List[$key] = $this->EncryptThis($value);
            }
            $final_list['student_list'] = $List;

            /* Student List Batch wise */
            $sql_query = "SELECT t2.`title` as `stream_name`, t3.`title` as `stream_course_name`, "
                    . "t1.`study_type`, t1.`college_users_intranet_id`, t1.`semester_or_year`, t5.* "
                    . "FROM `college_student_users_intranet` as t1 "
                    . "INNER JOIN `stream` as t2 ON t1.`stream_id` = t2.`id` "
                    . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                    . "INNER JOIN `college_users_intranet` as t4 ON t1.`college_users_intranet_id` = t4.`id` "
                    . "RIGHT JOIN `users` as t5 ON t4.`user_id` = t5.`id` "
                    . "WHERE t4.`college_id` = '$college_id' AND t4.`intranet_user_type` = 'student' ";
            if (!empty($AddedMembersList))
                $sql_query .= "AND t1.college_users_intranet_id NOT IN ($user_ids)";
            if ($Item != "") {
                $sql_query .= "AND (t2.`title` LIKE '$Item%' OR t3.`title` LIKE '$Item%' ) ";
            }
            $query_result = $this->data_fetch->data_query($sql_query);

            foreach ($query_result as $value) {
                $final_list['student_batch_list']["<h5>" . $value->stream_name . " (" . $number_to_word[$value->semester_or_year] . " " . ucfirst($value->study_type) . ")</h5><h6>" . $value->stream_course_name . "</h6>"][] = $this->encryption_decryption_object->encode($value->college_users_intranet_id);
            }
            echo json_encode($final_list);
        }
    }

    public function addMoreGroupMembers_Method() {
        $college_id = $this->session->userdata('college_id');

        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));

        $AddedMembersList_more = $this->input->post('AddedMembersList_more');
        $AddedMembersList_more = $this->GetOriginalIDs($AddedMembersList_more);
        if ($AddedMembersList_more == 0) {
            echo 0;
            die();
        }

        if ($this->input->post('NewMembers') != false) {
            $NewMembers = $this->input->post('NewMembers');
            $NewMembers = $this->GetOriginalIDs($NewMembers);
            if ($NewMembers == 0) {
                echo 0;
                die();
            }
        }

        if ($this->input->post('AllNewMembers') != false) {
            $AllNewMembers = $this->input->post('AllNewMembers');
            $AllNewMembers = $this->GetOriginalIDs($AllNewMembers);
            if ($AllNewMembers == 0) {
                echo 0;
                die();
            }
        }

        if ($this->input->post('group_members_group') != "{}") {
            $group_members_group = json_decode($this->input->post('group_members_group'));
            foreach ($group_members_group as $users_list) {
                if ($users_list != "") {
                    $users_list = explode(',', $users_list);
                    foreach ($users_list as $value) {
                        $value = trim($value);
                        $group_user_id = $this->encryption_decryption_object->is_valid_input($value);
                        if ($group_user_id) {
                            $group_member_list_array[] = $group_user_id;
                        } else {
                            echo 0;
                            die();
                        }
                    }
                }
            }
            $group_member_list_array = array_unique($group_member_list_array);
        }

        $List = array();
        $status = 0;

        if ($GroupID != "") {
            $result = array();

            if ($this->input->post('group_members_group') != "{}") {
                foreach ($group_member_list_array as $key => $value) {
                    $result[] = $value;
                }
            }

            if ($this->input->post('NewMembers') != false) {
                foreach ($NewMembers as $key => $value) {
                    $result[] = $value;
                }
            }

            $result = array_unique($result);

            if (!empty($result) && $GroupID != "") {
                foreach ($result as $value) {
                    $GetUserId = "SELECT * "
                            . "FROM college_users_intranet "
                            . "WHERE id = '$value' AND college_id = '$college_id'";
                    $GetUserId_Result = $this->data_fetch->data_query($GetUserId);

                    $user_id = $GetUserId_Result[0]->user_id;
                    $intranet_user_type = $GetUserId_Result[0]->intranet_user_type;
                    $InsertQuery = "INSERT INTO college_group_users(group_id, college_id, user_id, user_intranet_id) "
                            . "VALUES('$GroupID', '$college_id', '$user_id', '$value')";
                    $InsertQuery_Result = $this->data_insert->data_query($InsertQuery);
                    if ($InsertQuery_Result) {

                        switch ($intranet_user_type) {
                            case 'student':
                                $SelectDetailsOfStudent = "SELECT t1.*, "
                                        . "t2.`title` AS `stream_name`, "
                                        . "t3.`title` AS `stream_course_name`, "
                                        . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                        . "t5.intranet_user_type "
                                        . "FROM `college_student_users_intranet` AS t1 "
                                        . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                        . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                        . "INNER JOIN `users` AS t4 ON t4.id = '$user_id' "
                                        . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                        . "WHERE t1.college_users_intranet_id = '$value' ";
                                ;
                                $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                                foreach ($SelectDetailsOfStudent_Result as $value1) {
                                    $List[$value1->college_users_intranet_id] = $value1;
                                }
                                break;
                            case 'alumni':
                                $SelectDetailsOfAlumni = "SELECT t1.*, "
                                        . "t2.`title` AS `stream_name`, "
                                        . "t3.`title` AS `stream_course_name`, "
                                        . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                        . "t5.intranet_user_type "
                                        . "FROM `college_alumni_users_intranet` AS t1 "
                                        . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                        . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                        . "INNER JOIN `users` AS t4 ON t4.id = '$user_id' "
                                        . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                        . "WHERE college_users_intranet_id = '$value' ";
                                $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                                foreach ($SelectDetailsOfAlumni_Result as $value2) {
                                    $List[$value2->college_users_intranet_id] = $value2;
                                }
                                break;
                            case 'teacher':
                                $SelectDetailsOfTeacher = "SELECT t1.*, "
                                        . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, t4.profile_picture, "
                                        . "t5.intranet_user_type "
                                        . "FROM `college_teacher_users_intranet` AS t1 "
                                        . "INNER JOIN `users` AS t4 ON t4.id = '$user_id' "
                                        . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                        . "WHERE college_users_intranet_id = '$value' ";
                                $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);
                                foreach ($SelectDetailsOfTeacher_Result as $value3) {
                                    $List[$value3->college_users_intranet_id] = $value3;
                                }
                                break;
                        }
                        $status = 1;
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
        if ($status == 1) {
            // Encrypt the ids 
            foreach ($List as $key => $value) {
                $List[$key] = $this->EncryptThis($value);
            }
            echo json_encode($List);
        }
    }

    public function DeleteGroupMember_Method() {
        $college_id = $this->session->userdata('college_id');

        $user_intranet_id = $this->encryption_decryption_object->is_valid_input($this->input->post('user_intranet_id'));
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));

        if ($user_intranet_id && $GroupID) {
            $DeleteUser = "DELETE FROM college_group_users "
                    . "WHERE college_id = '$college_id' AND group_id = '$GroupID' AND user_intranet_id = '$user_intranet_id'";
            $DeleteUser_Result = $this->data_delete->data_query($DeleteUser);
            if ($DeleteUser_Result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function settings() {
        $get = $this->input->get();
        $college_id = $this->session->userdata('college_id');
        $data['college_details'] = $this->college_details;
        $GroupID = $this->encryption_decryption_object->is_valid_input($get['grp_id']);

        $GetGroupDetails = "SELECT * FROM college_groups "
                . "WHERE id = '$GroupID' AND college_id = '$college_id'";
        $GetGroupDetails_Result = $this->data_fetch->data_query($GetGroupDetails);
        if (count($GetGroupDetails_Result)) {
            $data['group'] = $GetGroupDetails_Result[0];

            $MemberList = array();

            $GetMemberList = "SELECT t1.*, t2.intranet_user_type "
                    . "FROM college_group_users AS t1 "
                    . "INNER JOIN college_users_intranet AS t2 ON t1.user_intranet_id = t2.id "
                    . "WHERE t1.college_id = '$college_id' AND t1.group_id = '$GroupID'";
            $GetMemberList_Result = $this->data_fetch->data_query($GetMemberList);

            foreach ($GetMemberList_Result as $key => $value) {
                switch ($value->intranet_user_type) {
                    case 'student':
                        $MemberListDetails = "SELECT "
                                . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.study_type,t1.semester_or_year, "
                                . "t2.title AS stream_name, "
                                . "t3.title AS stream_course_name, "
                                . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM college_student_users_intranet AS t1 "
                                . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                                . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                                . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                        foreach ($MemberList_Result as $value1) {
                            $MemberList[$value1->user_intranet_id] = $value1;
                        }
                        break;
                    case 'alumni':
                        $MemberListDetails = "SELECT "
                                . "t1.college_users_intranet_id AS user_intranet_id,t1.stream_id,t1.stream_course_id,t1.year_of_passing, "
                                . "t2.title AS stream_name, "
                                . "t3.title AS stream_course_name, "
                                . "t4.id AS user_id,t4.first_name, t4.last_name, t4.email,t4.profile_picture, "
                                . "t5.intranet_user_type "
                                . "FROM college_alumni_users_intranet AS t1 "
                                . "INNER JOIN stream AS t2 ON t1.stream_id = t2.id "
                                . "INNER JOIN stream_courses AS t3 ON t1.stream_course_id = t3.id "
                                . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                        foreach ($MemberList_Result as $value1) {
                            $MemberList[$value1->user_intranet_id] = $value1;
                        }
                        break;
                    case 'teacher':
                        $MemberListDetails = "SELECT "
                                . "t1.college_users_intranet_id AS user_intranet_id, "
                                . "t4.id AS user_id, t4.first_name, t4.last_name, t4.email "
                                . "t5.intranet_user_type "
                                . "FROM college_teacher_users_intranet AS t1 "
                                . "INNER JOIN users AS t4 ON '$value->user_id' = t4.id "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = '$value->user_intranet_id'";
                        $MemberList_Result = $this->data_fetch->data_query($MemberListDetails);
                        foreach ($MemberList_Result as $value1) {
                            $MemberList[$value1->user_intanet_id] = $value1;
                        }
                        break;
                }
            }
            foreach ($MemberList as $key => $value) {
                $MemberList[$key] = $this->EncryptThis($value);
            }
            $data['member'] = $MemberList;
            $data['GroupID'] = "$GroupID";
            $data['user'] = $this->user;
            $data['requests'] = $this->GetMemberRequests_Method($GroupID);
            $this->load->view("front-end/group_settings", $data);
        } else {
            show_404();
        }
    }

    public function UploadGroupImage_Method() {
        if ($_FILES['group_image']['name']) {

            $files = glob('assets_front/image/temp/*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file))
                    unlink($file); // delete file
            }

            $target = "assets_front/image/temp/";
            $target.= basename($_FILES['group_image']['name']);
            if (move_uploaded_file($_FILES['group_image']['tmp_name'], $target)) {
                echo $target;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpdateGroupSettings_Method() {
        $PostData = $this->input->post();

        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $GroupName = $this->input->post('GroupName');
        $VisibleTo = $this->input->post('VisibleTo');
        $GroupAccess = $this->input->post('GroupAccess');
        $GroupIconImagePath = $this->input->post('GroupIconImage');

        if (!empty($PostData) && $GroupID) {

            if ($GroupIconImagePath != "") {
                $GroupIconImage = explode('/', $GroupIconImagePath);
                $destination = "assets_front/image/group_icon_image/" . date('Ymd') . "-" . $GroupIconImage[3];
                $source = $GroupIconImage;
                rename($GroupIconImagePath, $destination);

                $SelectGroup = "SELECT * FROM college_groups WHERE id = '$GroupID'";
                $SelectGroup_Result = $this->data_fetch->data_query($SelectGroup);
                $OldIcon = $SelectGroup_Result[0]->group_icon_image;
                unlink($OldIcon);
            }

            $UpdateGroup = "UPDATE `college_groups` "
                    . "SET `name`='$GroupName',`visible_to`='$VisibleTo',`group_access`='$GroupAccess' ";
            if ($GroupIconImagePath != "")
                $UpdateGroup .= ", `group_icon_image`='$destination' ";
            $UpdateGroup .= "WHERE id = '$GroupID'";

            $UpdateGroup_Result = $this->data_update->data_query($UpdateGroup);

            echo $UpdateGroup_Result;
        }
    }

    public function LeaveFromGroup_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));

        if ($GroupID != "" && $GroupID) {
            $SelectGroupMember = "SELECT * "
                    . "FROM college_group_users "
                    . "WHERE group_id = '$GroupID' AND college_id = '$college_id' AND user_id = '$user_id'";
            $SelectGroupMember_Result = $this->data_fetch->data_query($SelectGroupMember);

            if (count($SelectGroupMember_Result)) {
                $DeleteMember = "DELETE FROM college_group_users "
                        . "WHERE group_id = '$GroupID' AND college_id = '$college_id' AND user_id = '$user_id'";
                $DeleteMember_Result = $this->data_delete->data_query($DeleteMember);
                if ($DeleteMember_Result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function DeleteGroup_Method() {
        $college_id = $this->session->userdata('college_id');
        $user_id = $this->user->id;
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        if ($GroupID != "" && $GroupID) {
            $SelectGroup = "SELECT * "
                    . "FROM college_group_users "
                    . "WHERE group_id = '$GroupID' AND college_id = '$college_id'";
            $SelectGroup_Result = $this->data_fetch->data_query($SelectGroup);

            if (count($SelectGroup_Result)) {
                $DeleteGroupMembers = "DELETE FROM college_group_users "
                        . "WHERE group_id = '$GroupID' AND college_id = '$college_id'";
                $DeleteGroupMembers_Result = $this->data_delete->data_query($DeleteGroupMembers);

                if ($DeleteGroupMembers_Result) {
                    $DeleteGroup = "DELETE FROM college_groups "
                            . "WHERE id = '$GroupID' AND college_id = '$college_id' AND group_admin_user_id = '$user_id'";
                    $DeleteGroup_Result = $this->data_delete->data_query($DeleteGroup);
                    if ($DeleteGroup_Result) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function JoinGroupNow_Method() {
        $college_id = $this->session->userdata('college_id');

        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $user_id = $this->user->id;

        if ($GroupID != "") {
            $SelectGroup = "SELECT group_access FROM college_groups "
                    . "WHERE id = '$GroupID' AND college_id = '$college_id'";
            $SelectGroup_Result = $this->data_fetch->data_query($SelectGroup);

            if (count($SelectGroup_Result)) {
                if ($SelectGroup_Result[0]->group_access == "Public") {
                    $GetUserDetails = "SELECT id FROM college_users_intranet "
                            . "WHERE college_id = '$college_id' AND user_id = '$user_id'";
                    $GetUserDetails_Result = $this->data_fetch->data_query($GetUserDetails);
                    $user_intranet_id = $GetUserDetails_Result[0]->id;

                    $AddToGroup = "INSERT INTO college_group_users "
                            . "(group_id,college_id,user_id,user_intranet_id) "
                            . "VALUES ('$GroupID','$college_id','$user_id','$user_intranet_id')";
                    $AddToGroup_Result = $this->data_insert->data_query($AddToGroup);
                    if ($AddToGroup_Result) {
                        echo 'Added';
                    } else {
                        echo 0;
                    }
                } else {
                    $GetUserDetails = "SELECT id,intranet_user_type FROM college_users_intranet "
                            . "WHERE college_id = '$college_id' AND user_id = '$user_id'";
                    $GetUserDetails_Result = $this->data_fetch->data_query($GetUserDetails);
                    $user_intranet_id = $GetUserDetails_Result[0]->id;
                    $intranet_user_type = $GetUserDetails_Result[0]->intranet_user_type;

                    $AddToGroupRequest = "INSERT INTO college_user_group_request "
                            . "(group_id,college_id,user_id,user_intranet_id,intranet_user_type,read_status) "
                            . "VALUES ('$GroupID','$college_id','$user_id','$user_intranet_id','$intranet_user_type','0')";
                    $AddToGroupRequest_Result = $this->data_insert->data_query($AddToGroupRequest);
                    if ($AddToGroupRequest_Result) {
                        echo 'Requested';
                    } else {
                        echo 0;
                    }
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function ApproveGroupJoinRequest_Method() {
        $college_id = $this->session->userdata('college_id');
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $user_intranet_id = $this->encryption_decryption_object->is_valid_input($this->input->post('user_intranet_id'));

        if ($GroupID != "" && $GroupID && $user_intranet_id) {
            $college_groups = "SELECT id FROM college_groups "
                    . "WHERE id = '$GroupID'";
            $college_groups_Result = $this->data_fetch->data_query($college_groups);

            if (count($college_groups_Result)) {
                $GetUserDetails = "SELECT user_id FROM college_users_intranet "
                        . "WHERE college_id = '$college_id' AND id = '$user_intranet_id'";
                $GetUserDetails_Result = $this->data_fetch->data_query($GetUserDetails);
                $user_id = $GetUserDetails_Result[0]->user_id;

                // Delete row from request table
                $DeleteRequest = "DELETE FROM college_user_group_request "
                        . "WHERE group_id ='$GroupID' AND college_id ='$college_id' AND user_intranet_id = '$user_intranet_id'";
                $DeleteRequest_Result = $this->data_delete->data_query($DeleteRequest);

                // Add user into group users table
                $AddToGroup = "INSERT INTO college_group_users "
                        . "(group_id,college_id,user_id,user_intranet_id) "
                        . "VALUES ('$GroupID','$college_id','$user_id','$user_intranet_id')";
                $AddToGroup_Result = $this->data_insert->data_query($AddToGroup);
                if ($AddToGroup_Result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function DenyGroupJoinRequest_Method() {
        $college_id = $this->session->userdata('college_id');
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $user_intranet_id = $this->encryption_decryption_object->is_valid_input($this->input->post('user_intranet_id'));

        if ($GroupID != "" && $GroupID && $user_intranet_id) {
            $college_groups = "SELECT id FROM college_groups "
                    . "WHERE id = '$GroupID'";
            $college_groups_Result = $this->data_fetch->data_query($college_groups);

            if (count($college_groups_Result)) {
                // Delete row from request table
                $DeleteRequest = "DELETE FROM college_user_group_request "
                        . "WHERE group_id ='$GroupID' AND college_id ='$college_id' AND user_intranet_id = '$user_intranet_id'";
                $DeleteRequest_Result = $this->data_delete->data_query($DeleteRequest);
                if ($DeleteRequest_Result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function UpdateGroupInfo_Method() {
        $posted_data = $this->input->post();
        $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
        $info_content = htmlentities($this->input->post('info_content'));

        if (!empty($posted_data) && $GroupID && $info_content != "") {
            $update_college_group = "UPDATE college_groups "
                    . "SET group_info = '$info_content' "
                    . "WHERE id = '$GroupID'";
            $update_college_group_Result = $this->data_update->data_query($update_college_group);

            if ($update_college_group_Result) {
                $info_content = html_entity_decode($info_content);
                print_r($info_content);
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function GroupFilesDatatable_FetchMethod() {
        /*
         * Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $aColumns = array('college_name', 'city', 'state', 'zipcode', 'id', 'active_status');
        // DB table to use
        $sTable = 'college_unfixed_group_posts_files';

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);

        // Paging
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        // Ordering
        if (isset($iSortCol_0)) {
            for ($i = 0; $i < intval($iSortingCols); $i++) {
                $iSortCol = $this->input->get_post('iSortCol_' . $i, true);
                $bSortable = $this->input->get_post('bSortable_' . intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_' . $i, true);

                if ($bSortable == 'true') {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if (isset($sSearch) && !empty($sSearch)) {
            for ($i = 0; $i < count($aColumns); $i++) {
                $bSearchable = $this->input->get_post('bSearchable_' . $i, true);

                // Individual column filtering
                if (isset($bSearchable) && $bSearchable == 'true') {
                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                }
            }
        }
        // Select Data
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $aColumns)), false);
        $rResult = $this->db->get($sTable);


// Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;

        // Total data set length
        $iTotal = $this->db->count_all($sTable);

// Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach ($rResult->result_array() as $aRow) {
            $row = array();

            foreach ($aColumns as $col) {
                if ($col != 'id' && $col != 'active_status') {
                    $row[] = ($aRow[$col] == '') ? "-" : $aRow[$col];
                } else {
                    $temp_html = '<button type="button" college_state="' . $aRow['state'] . '" college_zipcode="' . $aRow['zipcode'] . '" college_city="' . $aRow['city'] . '" college_name="' . $aRow['college_name'] . '" college_id="' . $aRow['id'] . '" class="btn btn-success btn-xs college_edit_btn"><i class="fa fa-edit"></i> Edit</button> |';
                    $temp_html .= ' <button type="button" class="btn ';
                    $temp_html .= ($aRow['active_status'] == 1) ? "btn-danger " : "btn-success ";
                    $temp_html .= 'btn-xs active_status_college_btn" active_status=' . $aRow['active_status'] . ' college_id = ' . $aRow['id'] . '><i class="fa';
                    $temp_html .= ($aRow['active_status'] == 1) ? " fa-ban" : " fa-check";
                    $temp_html .= '"></i> ';
                    $temp_html .= ($aRow['active_status'] == 1) ? "Deactivate" : "Activate";
                    $temp_html .= '</button>';
                    $row[] = $temp_html;
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function SearchThisGroup_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $postdata = $this->input->post();

        if (!empty($postdata) && $postdata['SearchString'] != "" && $postdata['GroupID'] != "") {
            $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));

            $GetGroup = "SELECT * FROM college_groups WHERE id = '$GroupID' AND college_id = '$college_id'";
            $GetGroup_Result = $this->data_fetch->data_query($GetGroup);
            $group = $GetGroup_Result[0];

            // To check if the current user is member of this group
            $LoggedUserIsMember = 0;
            $GetGroupUsers = "SELECT user_id FROM college_group_users WHERE group_id = '$GroupID' AND college_id = '$college_id'";
            $GetGroupUsers_Result = $this->data_fetch->data_query($GetGroupUsers);
            foreach ($GetGroupUsers_Result as $value) {
                if ($value->user_id == $this->user->id)
                    $LoggedUserIsMember = 1;
            }

            if ($GroupID) {
                $SearchString = $this->input->post('SearchString');
                $AllPostIDs = array();

                //Get the post_id from updates
                $GetUpdates = "SELECT post_id "
                        . "FROM college_unfixed_group_update_post_details "
                        . "WHERE group_id = '$GroupID' AND update_content LIKE '$SearchString%'";
                $GetPosts_Result = $this->data_fetch->data_query($GetUpdates);
                foreach ($GetPosts_Result as $value) {
                    $AllPostIDs[] = $value->post_id;
                }

                //Get the post_id from polls
                $GetPolls = "SELECT t1.post_id "
                        . "FROM college_unfixed_group_poll_post_details t1 "
                        . "INNER JOIN college_unfixed_group_poll_post_answer_details t2 ON t2.post_id = t1.post_id "
                        . "WHERE t1.group_id = '$GroupID' AND "
                        . "(t1.question LIKE '$SearchString%' OR t2.answer LIKE '$SearchString%')";
                $GetPosts_Result = $this->data_fetch->data_query($GetPolls);
                foreach ($GetPosts_Result as $value) {
                    $AllPostIDs[] = $value->post_id;
                }

                //Get the post_id from praise
                $GetPraise = "SELECT t1.post_id,t2.member_user_id "
                        . "FROM college_unfixed_group_praise_post_details t1 "
                        . "INNER JOIN college_unfixed_group_praise_post_member_details t2 ON t1.post_id = t2.post_id "
                        . "INNER JOIN users t3 ON t2.member_user_id = t3.id "
                        . "WHERE t1.group_id = '$GroupID' AND "
                        . "(t1.praise_content LIKE '$SearchString%' "
                        . "OR t3.first_name LIKE '$SearchString%' "
                        . "OR t3.last_name LIKE '$SearchString%')";
                $GetPosts_Result = $this->data_fetch->data_query($GetPraise);
                foreach ($GetPosts_Result as $value) {
                    $AllPostIDs[] = $value->post_id;
                }

                //Get the post_id from announce
                $GetAnnounce = "SELECT t1.post_id "
                        . "FROM college_unfixed_group_announce_post_details t1 "
                        . "WHERE t1.group_id = '$GroupID' AND "
                        . "(t1.heading LIKE '$SearchString%' OR t1.content LIKE '$SearchString%')";
                $GetPosts_Result = $this->data_fetch->data_query($GetAnnounce);
                foreach ($GetPosts_Result as $value) {
                    $AllPostIDs[] = $value->post_id;
                }

                //Get the post_id from users
                $GetUsers = "SELECT t1.id as post_id "
                        . "FROM college_unfixed_group_posts t1 "
                        . "INNER JOIN users t2 ON t2.id = t1.user_id "
                        . "WHERE t1.group_id = '$GroupID' AND "
                        . "(t2.first_name LIKE '$SearchString%' OR t2.last_name LIKE '$SearchString%')";
                $GetPosts_Result = $this->data_fetch->data_query($GetUsers);
                foreach ($GetPosts_Result as $value) {
                    $AllPostIDs[] = $value->post_id;
                }

                // discard duplicate ids
                $AllPostIDs = array_unique($AllPostIDs);
                $GetAllPosts_Result = array();

                if (count($AllPostIDs)) {
                    $this->GetAllPostsFromArray($GroupID, $AllPostIDs, $group, $LoggedUserIsMember);
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function FilterPosts_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');
        $postdata = $this->input->post();

        if (!empty($postdata) && $postdata['filter'] != "" && $postdata['GroupID'] != "") {
            $GroupID = $this->encryption_decryption_object->is_valid_input($this->input->post('GroupID'));
            $filter = $this->input->post('filter');
            $AllPostIDs = array();

            if ($GroupID && $filter != "") {
                $GetGroup = "SELECT * FROM college_groups WHERE id = '$GroupID' AND college_id = '$college_id'";
                $GetGroup_Result = $this->data_fetch->data_query($GetGroup);
                $group = $GetGroup_Result[0];

                // To check if the current user is member of this group
                $LoggedUserIsMember = 0;
                $GetGroupUsers = "SELECT user_id FROM college_group_users WHERE group_id = '$GroupID' AND college_id = '$college_id'";
                $GetGroupUsers_Result = $this->data_fetch->data_query($GetGroupUsers);
                foreach ($GetGroupUsers_Result as $value) {
                    if ($value->user_id == $this->user->id)
                        $LoggedUserIsMember = 1;
                }

                switch ($filter) {
                    case 'allPosts':
                        //Get the post_id from all posts of this group
                        $GetAllPosts = "SELECT t1.timestamp, t1.id as post_id "
                                . "FROM college_unfixed_group_posts t1 "
                                . "WHERE t1.group_id = '$GroupID' AND college_id = '$college_id' "
                                . "ORDER BY t1.timestamp DESC";
                        $GetPosts_Result = $this->data_fetch->data_query($GetAllPosts);
                        foreach ($GetPosts_Result as $value) {
                            $AllPostIDs[] = $value->post_id;
                        }
                        break;
                    case 'topPosts':
                        //Get the post_id of posts having max likes
                        $GetTopPosts = "SELECT count(t1.post_id)as count, t1.post_id "
                                . "FROM college_unfixed_group_post_like_details t1 "
                                . "WHERE t1.group_id = '$GroupID' AND college_id = '$college_id' "
                                . "GROUP BY post_id "
                                . "ORDER BY count DESC";
                        $GetPosts_Result = $this->data_fetch->data_query($GetTopPosts);
                        foreach ($GetPosts_Result as $value) {
                            $AllPostIDs[] = $value->post_id;
                        }
                    case 'followingPosts':
                        //Get the post_id from which are getting followed by user
                        $GetFollowPosts = "SELECT t1.post_id "
                                . "FROM college_unfixed_group_posts_follow t1 "
                                . "WHERE t1.group_id = '$GroupID' AND college_id = '$college_id' AND user_id = '$user_id'";
                        $GetPosts_Result = $this->data_fetch->data_query($GetFollowPosts);
                        foreach ($GetPosts_Result as $value) {
//                            $AllPostIDs[] = $value->post_id;
                        }

                        // Get all the users whom the user is following
                        $GetFollowPosts = "SELECT t1.following_user_id, t2.id as post_id "
                                . "FROM college_users_follow t1 "
                                . "INNER JOIN college_unfixed_group_posts t2 ON t2.college_id = t1.college_id "
                                . "WHERE t1.college_id = '$college_id' AND t1.followed_by_user_id = '$user_id' "
                                . "AND t2.group_id = '$GroupID' AND t2.user_id = t1.following_user_id "
                                . "GROUP BY post_id";
                        $GetPosts_Result = $this->data_fetch->data_query($GetFollowPosts);

                        foreach ($GetPosts_Result as $value) {
                            $AllPostIDs[] = $value->post_id;
                        }
                        break;
                }
                $this->GetAllPostsFromArray($GroupID, $AllPostIDs, $group, $LoggedUserIsMember);
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function GetAllPostsFromArray($GroupID, $AllPostIDs, $group, $LoggedUserIsMember) {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');

        if (count($AllPostIDs)) {
            foreach ($AllPostIDs as $post_id) {
                $GetAllPosts = "SELECT t1.*,t1.id as post_id, "
                        . "t2.first_name, t2.last_name, t2.profile_picture "
                        . "FROM college_unfixed_group_posts t1 "
                        . "INNER JOIN users t2 ON t1.user_id = t2.id "
                        . "WHERE t1.group_id = '$GroupID' AND t1.college_id = '$college_id' AND t1.id = '$post_id'"
                        . "group by t1.id "
                        . "ORDER BY t1.timestamp DESC ";
                $GetAllPosts_Result[$post_id] = $this->data_fetch->data_query($GetAllPosts);
            }

            $data['posts_file_list'] = array();
            $data['posts_reply_list'] = array();
            $data['posts_like_current_user'] = array();
            $data['posts_follow_current_user'] = array();
            $data['posts_like_all_user'] = array();
            $data['posts_reply_like_all_user'] = array();
            $data['posts_reply_like_current_user'] = array();
            $data['posts_notified_users_list'] = array();
            $data['posts_reply_notified_users_list'] = array();

            $data['posts_update_details'] = array();
            $data['posts_poll_details'] = array();
            $data['posts_poll_answer_details'] = array();
            $data['posts_poll_answer_vote_details'] = array();
            $data['posts_praise_details'] = array();
            $data['posts_praise_member_details'] = array();
            $data['posts_announce_details'] = array();

            // Run a loop and get all deatails of post and send it to view
            foreach ($GetAllPosts_Result as $value) {
                $user_id = $this->user->id;
                $value = $value[0];
                $post_id = $value->post_id;
                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_unfixed_group_posts` "
                        . "WHERE `id` = '$post_id' AND `user_id` = '$user_id' "
                        . "AND `college_id` = '$college_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_current_user'][$post_id] = 1;
                } else {
                    $data['posts_current_user'][$post_id] = 0;
                }

                switch ($value->type) {
                    case 'update':
                        $sql_query = "SELECT id AS update_id, update_content FROM college_unfixed_group_update_post_details "
                                . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_update_details'][$post_id] = $query_result;
                        break;
                    case 'poll':
                        $sql_query = "SELECT id AS poll_id, question FROM college_unfixed_group_poll_post_details "
                                . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_poll_details'][$post_id] = $query_result;

                        foreach ($data['posts_poll_details'][$post_id] as $poll_value) {
                            $poll_id = $poll_value->poll_id;
                            $sql_query = "SELECT id as ans_id, answer FROM college_unfixed_group_poll_post_answer_details "
                                    . "WHERE post_id = '$post_id' AND poll_id = '$poll_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_poll_answer_details'][$post_id][$poll_id] = $query_result;

                            foreach ($data['posts_poll_answer_details'][$post_id][$poll_id] as $poll_ans_value) {
                                $poll_ans_id = $poll_ans_value->ans_id;
                                $sql_query = "SELECT count(id) as vote_count, voted_user_id FROM college_unfixed_group_poll_post_answer_vote_details "
                                        . "WHERE post_id = '$post_id' AND poll_id = '$poll_id' AND ans_id = '$poll_ans_id'";
                                $query_result = $this->data_fetch->data_query($sql_query);
                                $data['posts_poll_answer_vote_details'][$post_id][$poll_id][$poll_ans_id] = $query_result;
                            }

                            $sql_query = "SELECT `id` FROM `college_unfixed_group_poll_post_answer_vote_details` "
                                    . "WHERE `post_id` = '$post_id' AND poll_id = '$poll_id' AND `voted_user_id` = '$user_id' LIMIT 1";
                            $query_result = $this->data_fetch->data_query($sql_query);

                            if (count($query_result)) {
                                $data['posts_poll_answer_vote_current_user'][$post_id] = 1;
                            } else {
                                $data['posts_poll_answer_vote_current_user'][$post_id] = 0;
                            }
                        }
                        break;
                    case 'praise':
                        $sql_query = "SELECT t1.id AS praise_id,t1.praise_content,t1.badge_id, "
                                . "t2.image as badge_image "
                                . "FROM college_unfixed_group_praise_post_details t1 "
                                . "INNER JOIN badges t2 ON t2.id = t1.badge_id "
                                . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_praise_details'][$post_id] = $query_result;

                        foreach ($data['posts_praise_details'][$post_id] as $praise_value) {
                            $praise_id = $praise_value->praise_id;

                            $sql_query = "SELECT member_user_id as praised_user_id FROM college_unfixed_group_praise_post_member_details "
                                    . "WHERE post_id = '$post_id' AND praise_id = '$praise_id'";
                            $query_result = $this->data_fetch->data_query($sql_query);
                            $data['posts_praise_member_details'][$post_id] = $query_result;
                        }
                        break;
                    case 'announce':
                        $sql_query = "SELECT id AS announce_id, heading,content FROM college_unfixed_group_announce_post_details "
                                . "WHERE post_id = '$post_id' AND group_id = '$GroupID'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_announce_details'][$post_id] = $query_result;
                        break;
                }
                //post files list
                $sql_query = "SELECT `id`,`file_type`,`file_path` "
                        . "FROM `college_unfixed_group_posts_files` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_file_list'][$post_id] = $query_result;

                //post reply msg
                $sql_query = "SELECT id AS post_reply_id,`user_id`,`reply_content`,`like_count`,`timestamp` "
                        . "FROM `college_unfixed_group_post_reply` "
                        . "WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_reply_list'][$post_id] = $query_result;

                //post reply like
                foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                    $post_reply_id = $inner_value->post_reply_id;

                    $sql_query = "SELECT `user_id` FROM `college_unfixed_group_post_reply_like_details` "
                            . "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                    //post like for the current user
                    $sql_query = "SELECT `id` FROM `college_unfixed_group_post_reply_like_details` "
                            . "WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' "
                            . "AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                    $query_result = $this->data_fetch->data_query($sql_query);

                    if (count($query_result)) {
                        $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                    } else {
                        $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                    }

                    //post reply notified user list
                    $sql_query = "SELECT `user_id` FROM `college_unfixed_group_post_reply_notify_user_details` "
                            . "WHERE `post_id` = '$post_id' "
                            . "AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    if (count($query_result)) {
                        $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                    } else {
                        $data['posts_reply_notified_users_list'][$post_id][$post_reply_id] = $query_result;
                    }
                }

                //post reply like
                $sql_query = "SELECT id AS post_reply_id,`user_id` "
                        . "FROM `college_unfixed_group_post_reply_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                foreach ($query_result as $value1) {
                    $data['posts_reply_like'][$post_id][$value1->post_reply_id] = $value1->user_id;
                }

                //post like for all the user
                $sql_query = "SELECT `id`,`user_id` FROM `college_unfixed_group_post_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_like_all_user'][$post_id] = $query_result;

                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_unfixed_group_post_like_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_like_current_user'][$post_id] = 1;
                } else {
                    $data['posts_like_current_user'][$post_id] = 0;
                }

                //post follow for the current user
                $sql_query_follow = "SELECT `id` FROM `college_unfixed_group_posts_follow` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $sql_query_follow_result = $this->data_fetch->data_query($sql_query_follow);

                if (count($sql_query_follow_result)) {
                    $data['posts_follow_current_user'][$post_id] = 1;
                } else {
                    $data['posts_follow_current_user'][$post_id] = 0;
                }

                //Post notified user list
                $sql_query = "SELECT `user_id` FROM `college_unfixed_group_posts_notify_user_details` "
                        . "WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                if (count($query_result)) {
                    foreach ($query_result as $value2) {
                        $data['posts_notified_users_list'][$post_id][] = $value2->user_id;
                    }
                } else {
                    $data['posts_notified_users_list'][$post_id] = array();
                }
                ?>
                <div class="post_content_div" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img">
                                <img src="<?php echo base_url() . $value->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($value->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($value->user_id); ?>"><?php echo $value->first_name . " " . $value->last_name; ?></a> </h5>
                                <div class="user_popup res_top_name"></div>
                                <h6>
                                    <span class="post_time" timestamp="<?php echo strtotime($value->timestamp); ?>">
                                        <?php
                                        $posted_date = date('M j Y', strtotime($value->timestamp));
                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                        ?>
                                    </span>
                                </h6>
                                <?php
                                switch ($value->type) {
                                    case 'update':
                                        ?>
                                        <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_update_details'][$value->post_id][0]->update_id); ?>">
                                            <p class="post_message_p">
                                                <?php
                                                if (str_word_count($data['posts_update_details'][$value->post_id][0]->update_content) >= 110) {
                                                    ?>
                                                    <span class="truncated_body">
                                                        <?php
                                                        echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                        ?>
                                                    </span>
                                                    <span class="complete_body">
                                                        <?php
                                                        echo html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                        ?>
                                                    </span>
                                                    <?php
                                                } else {
                                                    echo html_entity_decode($data['posts_update_details'][$value->post_id][0]->update_content);
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <?php
                                        break;
                                    case 'poll':
                                        if ($data['posts_poll_answer_vote_current_user'][$value->post_id]) {
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                <p class="poll_question_p">
                                                    <?php echo $data['posts_poll_details'][$value->post_id][0]->question; ?>
                                                </p>
                                                <div class="poll_answer_div">
                                                    <?php
                                                    $poll_id = $data['posts_poll_details'][$value->post_id][0]->poll_id;
                                                    $total_votes = 0;
                                                    foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                        $vote_count = $data['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                        $total_votes += $vote_count;
                                                    }
                                                    foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                        $vote_count = $data['posts_poll_answer_vote_details'][$value->post_id][$poll_id][$value1->ans_id][0]->vote_count;
                                                        if ($vote_count == 0) {
                                                            $answer_percentage = 0;
                                                        } else {
                                                            $answer_percentage = (100 * $vote_count) / $total_votes;
                                                        }
                                                        ?>
                                                        <div class="ans_option">
                                                            <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $answer_percentage; ?>%"></div>
                                                            </div>
                                                            <p class="poll_answer_p ans_percentage_p"><?php echo round($answer_percentage); ?>%</p>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="total_vote_div"><?php echo $total_votes; ?> total votes <!--<a href="javascript:void(0);" class="change_vote_button"> · Change Vote</a></div>-->
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_poll_details'][$value->post_id][0]->poll_id); ?>">
                                                    <p class="poll_question_p"> 
                                                        <?php echo $data['posts_poll_details'][$value->post_id][0]->question; ?>
                                                    </p>
                                                    <div class="poll_answer_div">
                                                        <form class="poll_answer_form">
                                                            <?php
                                                            $poll_id = $data['posts_poll_details'][$value->post_id][0]->poll_id;
                                                            foreach ($data['posts_poll_answer_details'][$value->post_id][$poll_id] as $value1) {
                                                                ?>
                                                                <p class="poll_answer_p" id="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>">
                                                                    <label>
                                                                        <input type="radio" name="answer" class="radio_answer" value="<?php echo $this->encryption_decryption_object->encode($value1->ans_id); ?>"><?php echo $value1->answer; ?>
                                                                    </label>
                                                                </p>
                                                                <?php
                                                            }
                                                            if ($LoggedUserIsMember == 1 || $this->user->id == $group->group_admin_user_id) {
                                                                ?>
                                                                <button type="submit" name="submit_vote" class="btn btn-primary submit_vote"> Vote </button>
                                                            <?php } ?>
                                                        </form>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            break;
                                        case 'praise':
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_praise_details'][$value->post_id][0]->praise_id); ?>">
                                                <div class="row praised_div">
                                                    <div class="col-md-2 praise_div_image">
                                                        <img src="<?php echo base_url() . "" . $data['posts_praise_details'][$value->post_id][0]->badge_image; ?>" alt="">
                                                    </div>
                                                    <div class="col-md-10 praise_div_details">
                                                        <p class="row praise_member_p"> 
                                                            <span class="praised_text">Praised</span>
                                                            <?php
                                                            $array_count = count($data['posts_praise_member_details'][$value->post_id]);
                                                            $running_count = 0;
                                                            foreach ($data['posts_praise_member_details'][$value->post_id] as $member_value) {
                                                                $running_count++;
                                                                echo $this->ion_auth->user($member_value->praised_user_id)->row()->first_name . " " . $this->ion_auth->user($member_value->praised_user_id)->row()->last_name;
                                                                if ($running_count != $array_count) {
                                                                    echo ", ";
                                                                }
                                                            }
                                                            ?>
                                                        </p><br>
                                                        <p class="row praise_content_p">"
                                                            <?php
                                                            if (str_word_count($data['posts_praise_details'][$value->post_id][0]->praise_content) >= 110) {
                                                                ?>
                                                                <span class="truncated_body">
                                                                    <?php
                                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr(html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                    ?>
                                                                </span>
                                                                <span class="complete_body">
                                                                    <?php
                                                                    echo html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                                    ?>
                                                                </span>
                                                                <?php
                                                            } else {
                                                                echo html_entity_decode($data['posts_praise_details'][$value->post_id][0]->praise_content);
                                                            }
                                                            ?>
                                                            "
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        case 'announce':
                                            ?>
                                            <div class="post_type_div" id="<?php echo $this->encryption_decryption_object->encode($data['posts_announce_details'][$value->post_id][0]->announce_id); ?>">
                                                <div class="post_announce_div">
                                                    <div class="row">
                                                        <div class="col-md-12 announce_head_div">
                                                            <i class="fa fa-bullhorn"></i>
                                                            <?php echo $data['posts_announce_details'][$value->post_id][0]->heading; ?>
                                                        </div>
                                                        <div class="col-md-12 announce_content_div">
                                                            <?php
                                                            if (str_word_count($data['posts_announce_details'][$value->post_id][0]->content) >= 110) {
                                                                ?>  
                                                                <span class="truncated_body">
                                                                    <?php
                                                                    echo str_replace('<br />', '', substr(html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content), 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                                    ?>
                                                                </span>
                                                                <span class="complete_body">
                                                                    <?php
                                                                    echo html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content) . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse </a>";
                                                                    ?>
                                                                </span>
                                                                <?php
                                                            } else {
                                                                echo str_replace('<br />', '', html_entity_decode($data['posts_announce_details'][$value->post_id][0]->content));
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                    }

                                    $file_count = count($data['posts_file_list'][$value->post_id]);
                                    if ($file_count) {
                                        ?>
                                        <div class="post_file_div">
                                            <div class="row">
                                                <?php
                                                $count = 1;
                                                $inner_count = 2;
                                                foreach ($data['posts_file_list'][$value->post_id] as $file_value) {
                                                    if ($count == 1) {
                                                        if ($file_value->file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        $count++;
                                                    } else {
                                                        if ($inner_count == 2) {
                                                            ?>
                                                            <?php
                                                        }
                                                        if ($file_value->file_type == "jpg") {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="margin-bottom: 2px; width: 100%; object-fit: cover;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4" style="padding-right: 19px;">
                                                                <span class="file_exetension_span"><?php echo $file_value->file_type; ?></span>
                                                                <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . "assets_front/image/other_file.png"; ?>"/>
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($file_count == $inner_count) {
                                                            ?>
                                                            <?php
                                                        }
                                                        $inner_count++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    if (count($data['posts_notified_users_list'][$value->post_id])) {
                                        ?>
                                        <div class="notified_user_div">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="notify_user_heading">cc:</div>
                                                    <div class="notify_user_body">
                                                        <?php
                                                        $array_count = count($data['posts_notified_users_list'][$value->post_id]);
                                                        $running_count = 0;
                                                        foreach ($data['posts_notified_users_list'][$value->post_id] as $user_id) {
                                                            $running_count++;
                                                            $user_details = $this->ion_auth->user($user_id)->row();
                                                            ?>
                                                            <a user="<?php echo $this->encryption_decryption_object->encode($user_id); ?>" href="">
                                                                <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                            </a>
                                                            <?php
                                                            if ($running_count != $array_count) {
                                                                echo ",";
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php if ($LoggedUserIsMember == 1 || $this->user->id == $group->group_admin_user_id) { ?>
                                        <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                                            <a class="post_like" href="javascript:void(0);" like_count="<?php echo count($data['posts_like_all_user'][$value->post_id]); ?>" like_action ="<?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                            <a href="javascript:void(0);" class="reply_link" title="Reply">Reply.</a>
                                            <a href="javascript:void(0);" follow_action="<?php echo ($data['posts_follow_current_user'][$value->post_id] == 1) ? "unfollow" : "follow"; ?>" class="post_follow" title="Follow"><?php echo ($data['posts_follow_current_user'][$value->post_id] == 1) ? "Unfollow." : "Follow."; ?></a>
                                            <?php if ($data['posts_current_user'][$value->post_id]) { ?>
                                                <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                <div class="more_list">
                                                    <ul>
                                                        <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete </h6> </a> </li>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="commenting_box">
                                            <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_div" style="display: <?php echo (!empty($data['posts_like_all_user'][$value->post_id])) ? "block" : "none"; ?>">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_body">
                                                            <i class="fa fa-thumbs-up"></i>
                                                            <?php
                                                            $array_count = count($data['posts_like_all_user'][$value->post_id]);
                                                            $running_count = 0;
                                                            if ($data['posts_like_current_user'][$value->post_id] == 1) {
                                                                echo "You";
                                                                if (count($data['posts_like_all_user'][$value->post_id]) > 1) {
                                                                    echo ",";
                                                                }
                                                                foreach ($data['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                    $running_count++;
                                                                    $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                    if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                        ?>
                                                                        <span>
                                                                            <?php
                                                                            echo $user_details->first_name . " " . $user_details->last_name;
                                                                            if ($running_count != $array_count) {
                                                                                echo ", ";
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                }
                                                            } else {
                                                                if (count($data['posts_like_all_user'][$value->post_id]) >= 1) {
                                                                    foreach ($data['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                        $running_count++;
                                                                        $user_details = $this->ion_auth->user($inner_value->user_id)->row();
                                                                        if ($inner_value->user_id != $this->ion_auth->user()->row()->id) {
                                                                            ?>
                                                                            <span>
                                                                                <?php
                                                                                echo $user_details->first_name . " " . $user_details->last_name;
                                                                                if ($running_count != $array_count) {
                                                                                    echo ", ";
                                                                                }
                                                                                ?>
                                                                            </span>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                            like this.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_reply">
                                                <?php
                                                foreach ($data['posts_reply_list'][$value->post_id] as $post_reply_key => $post_reply_value) {
                                                    $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                    ?>
                                                    <div class="post_content_reply_div">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="top_user_img pull-right">
                                                                    <img class="reply_user_img" src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="top_user_details">
                                                                    <h5><a class="top_name" uid="<?php echo $this->encryption_decryption_object->encode($user_details->user_id); ?>" href="<?php echo site_url() . "user/user_profile/view/" . $this->encryption_decryption_object->encode($user_details->user_id); ?>"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a> </h5>
                                                                    <h6>
                                                                        <span class="post_time" timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                            <?php
                                                                            $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                            ?>
                                                                        </span>
                                                                    </h6>
                                                                    <div>
                                                                        <p class="post_message_p">
                                                                            <?php
                                                                            if (str_word_count($post_reply_value->reply_content) >= 80) {
                                                                                ?>
                                                                                <span class="truncated_body">
                                                                                    <?php
                                                                                    echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                                    ?>
                                                                                </span>
                                                                                <span class="complete_body">
                                                                                    <?php
                                                                                    echo $post_reply_value->reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                                    ?>
                                                                                </span>
                                                                                <?php
                                                                            } else {
                                                                                echo $post_reply_value->reply_content;
                                                                            }
                                                                            ?>
                                                                        </p>
                                                                    </div>
                                                                    <?php
                                                                    if (count($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id])) {
                                                                        ?>
                                                                        <div class="reply_post_notified_user_div">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="reply_post_notify_user_heading">cc:</div>
                                                                                    <div class="reply_post_notify_user_body">
                                                                                        <?php
                                                                                        $array_count = count($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id]);
                                                                                        $running_count = 0;
                                                                                        foreach ($data['posts_reply_notified_users_list'][$value->post_id][$post_reply_value->post_reply_id] as $user_id) {
                                                                                            $running_count++;
                                                                                            $user_details = $this->ion_auth->user($user_id->user_id)->row();
                                                                                            ?>
                                                                                            <a user="<?php echo $this->encryption_decryption_object->encode($user_id->user_id); ?>" href="">
                                                                                                <?php echo $user_details->first_name . " " . $user_details->last_name; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                            if ($running_count != $array_count) {
                                                                                                echo ",";
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <div post_id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" post_reply_id ="<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_action_div">
                                                                        <a class="post_reply_like" href="javascript:void(0);" like_count ="<?php echo count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]); ?>" like_action="<?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                        <a href="#" title="Reply">Reply.</a>
                                                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                                        <div class="more_list">
                                                                            <ul>
                                                                                <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="post_reply_like_div-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" style="display: <?php echo (!empty($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>" class="post_reply_like_div">
                                                            <div class="row">
                                                                <div class="col-md-2"></div>
                                                                <div class="col-md-10">
                                                                    <div id="post_reply_like_body-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_like_body">
                                                                        <i class="fa fa-thumbs-up"></i>
                                                                        <span class="current_user_post_reply_like">
                                                                            <?php
                                                                            //current user like
                                                                            if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1 && count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                                ?>
                                                                                You and
                                                                                <?php
                                                                            } else if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                                ?>You
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                        like this.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="reply_box" id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                                                <div class="announce_img">
                                                    <img src="<?php echo base_url() . $this->user->profile_picture; ?>" alt="">
                                                </div>
                                                <div class="share_box2 comment_in1">
                                                    <a href="javascript:void(0);" >Write a reply...</a>
                                                </div>
                                                <div class="more_share">
                                                    <form class="reply_form">
                                                        <a href="#"><?php echo $this->user->first_name . " " . $this->user->last_name; ?> is replying.</a>
                                                        <div class="text_poll2">
                                                            <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>"/>
                                                            <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                        </div>
                                                        <div class="text_poll2 reply_post_text_poll2">
                                                            <div class="AddedCCMembersOfReply"></div>
                                                            <input type="text" placeholder="Notify additional people.." class="poll_text addCCMembersToReply" name="note">
                                                            <div class="notify_pop" style="display: none;left: 10px;">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="people_list">
                                                                            <ul class="Reply_ListOfMembers">

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary reply_post_button" disabled=""> Post </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }

    public function GetListOfPeopleAndGroups_Method() {
        $user_id = $this->user->id;
        $college_id = $this->session->userdata('college_id');


        $post_data = $this->input->post();
        if (!empty($post_data) && $post_data['search_string']) {
            $search_string = $this->input->post('search_string');
            $SelectCollegeUsersAll = "SELECT * "
                    . "FROM `college_users_intranet` "
                    . "WHERE `college_id` = '$college_id'";
            $SelectCollegeUsersAll_Result = $this->data_fetch->data_query($SelectCollegeUsersAll);

            $CompleteList = array();

            foreach ($SelectCollegeUsersAll_Result as $value) {
                $intranet_user_type = $value->intranet_user_type;
                switch ($intranet_user_type) {
                    case 'student':
                        $SelectDetailsOfAll = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                                . "t5.intranet_user_type "
                                . "FROM `college_student_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE t1.college_users_intranet_id = $value->id "
                                . "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%')"
                                . "ORDER BY t1.timestamp DESC";
                        break;
                    case 'alumni':
                        $SelectDetailsOfAll = "SELECT t1.*, "
                                . "t2.`title` AS `stream_name`, "
                                . "t3.`title` AS `stream_course_name`, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                                . "t5.intranet_user_type "
                                . "FROM `college_alumni_users_intranet` AS t1 "
                                . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                                . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id "
                                . "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%')"
                                . "ORDER BY t1.timestamp DESC";
                        break;
                    case 'teacher':
                        $SelectDetailsOfAll = "SELECT t1.*, "
                                . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                                . "t5.intranet_user_type "
                                . "FROM `college_teacher_users_intranet` AS t1 "
                                . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                                . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                                . "WHERE college_users_intranet_id = $value->id "
                                . "AND (t4.first_name LIKE '$search_string%' OR t4.last_name LIKE '$search_string%')"
                                . "ORDER BY t1.timestamp DESC";
                        break;
                }
                $SelectDetailsOfAll_Result = $this->data_fetch->data_query($SelectDetailsOfAll);
                foreach ($SelectDetailsOfAll_Result as $value) {
                    $CompleteList[$value->user_id]['value'] = $this->encryption_decryption_object->encode($value->user_id);
                    $CompleteList[$value->user_id]['label'] = $value->fname . " " . $value->lname;
                }
            }

            $GetGroupNames = "SELECT id,name FROM college_groups WHERE college_id = '$college_id' "
                    . "AND visible_to = 'Public' AND (group_access IN ('Public','Private')) AND name LIKE '$search_string%'";
            $GetGroupNames_Result = $this->data_fetch->data_query($GetGroupNames);

            foreach ($GetGroupNames_Result as $value1) {
                $CompleteList[$value1->id]['value'] = "g>-<" . $this->encryption_decryption_object->encode($value1->id);
                $CompleteList[$value1->id]['label'] = $value1->name;
            }
            echo json_encode($CompleteList);
        } else {
            echo 0;
        }
    }

}
