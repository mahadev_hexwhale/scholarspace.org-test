<?php

/* class is to mainten the user request from the college admin */

Class Teacher extends UserClass {

    public $user = null;
    public $college_details = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');

        if ($this->user->user_type != "college-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();
        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library
        $this->load->helper(array('url', 'html', 'form'));  // load url,html,form helpers
        $this->encryption_decryption_object = new Encryption();
    }

    public function index() {
        $data['user'] = $this->user;
        $data['college_id'] = $this->college_id;
        $data['college_details'] = $this->college_details;

        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[100]|alpha');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]|alpha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|exact_length[10]|callback_numeric_wcomma');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front-end/teacher', $data);
        } else {
            $college_id = $this->college_id;
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $user_type = "front-end-user";

            $username = strtolower($first_name) . " " . strtolower($last_name);
            $email = strtolower($email);
            $password = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', 5)), 0, 8);
            $profile_picture = "assets_front/image/user_profile_pic/user_default_profile.jpg";

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'encoded_pwd' => base64_encode($password),
                'user_type' => $user_type,
                'user_type_id' => 3,
                'profile_picture' => $profile_picture
            );
            if ($user_id = $this->ion_auth->register($username, $password, $email, $additional_data)) {
                //insert data into college_users_intranet
                $sql_query = "INSERT INTO "
                        . "`college_users_intranet`(`college_id`,`user_id`,`intranet_user_type`) "
                        . "VALUES('$college_id','$user_id','teacher')";
                $query_result = $this->data_insert->data_query($sql_query);

                //inserted id use it as forgen key in college_student_users_intranet table
                $college_users_intranet_id = $this->db->insert_id();

                //insert into college_teacher_users_intranet table
                $sql_query = "INSERT INTO `college_teacher_users_intranet`(`college_users_intranet_id`) VALUES('$college_users_intranet_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $this->session->set_flashdata('FormSubmit', 'success');
                    redirect('user/teacher', 'refresh');
                } else {
                    $this->session->set_flashdata('FormSubmit', 'failure');
                    redirect('user/teacher', 'refresh');
                }
            } else {
                $this->session->set_flashdata('item_error', 'Request can not completed, Please try again later.');
                redirect(current_url());
            }
        }
    }

    public function GetTeacherEmail() {
        $college_id = $this->college_id;
        $query_variable = $this->input->post("email");
        $query = "SELECT t1.user_id, "
                . "t2.email "
                . "FROM college_users_intranet t1 "
                . "INNER JOIN users t2 ON t2.id = t1.user_id "
                . "WHERE t1.college_id = '$college_id' AND t1.intranet_user_type != 'teacher' "
                . "AND t2.email LIKE '$query_variable%' LIMIT 10";
        $teacher_details_array = $this->data_fetch->data_query($query);

        $teacher_details = array();
        foreach ($teacher_details_array as $key => $value) {
            $teacher_details[$value->user_id] = $value->email;
        }
        echo json_encode($teacher_details);
    }

    public function GetTeacherDetails() {
        $college_id = $this->college_id;

        $email = $this->input->post("email");

        $query = "SELECT t1.id,t1.first_name,t1.last_name,t1.phone "
                . "FROM users t1 "
                . "WHERE t1.email = '$email'";
        $teacher_details = $this->data_fetch->data_query($query);
        echo json_encode($teacher_details[0]);
    }

}
