<?php

Class Alumni extends UserClass {

    public $user = null;
    public $college_id = null;
    public $college_details = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->college_id = $this->session->userdata('college_id');
        $this->user = $this->ion_auth->user()->row();

        $this->group_name = "alumni";

        $this->load->model('usersearch');
        $this->load->model('posts_model');
        $this->load->model('data_insert');
        $this->load->model('data_update');

        $this->load->helper('college-admin_helper');
        $this->college_details = GetCollegeDetails();

        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $data['college_details'] = $this->college_details;
        $college_id = $this->college_id;
        $data['user'] = $this->user;
        $logged_user_id = $this->user->id;
        
        //Get all badge images
        $data['badges'] = $this->data_fetch->getBadges();
        
        $GetAllUsers = "SELECT * FROM college_users_intranet WHERE college_id = '$college_id' AND intranet_user_type = 'alumni'";
        $GetAllUsers_Result = $this->data_fetch->data_query($GetAllUsers);
        
        // Get all the posts from this batchboard
        $PostsArray = array();
        $GetPosts = "SELECT * FROM college_fixed_group_posts "
                . "WHERE college_id = '$college_id' AND post_group_name = 'alumni' "
                . "ORDER BY timestamp DESC";
        $GetPosts_Result = $this->data_fetch->data_query($GetPosts);
        $data['alumni_posts'] = $GetPosts_Result;
        foreach ($GetPosts_Result as $value) {
            $PostsArray[$value->post_id] = $value->post_id;
        }
        if (count($PostsArray)) {
            $PostsArray = $this->posts_model->get_all_posts("fixed", $PostsArray);
        }
        $data['all_posts'] = $PostsArray;

        $this->load->view('front-end/alumni_page', $data);
    }

    public function GetCollegeMembers_Method() {
        $college_id = $this->session->userdata('college_id');
        $logged_user_id = $this->user->id;
        
        $PostData = $this->input->post();
        $group = $this->input->post('group');
        $Item = $this->input->post('Item');
        
        if (!empty($PostData) && $college_id) {
            // get list of members
            $SelectCollegeUsers = "SELECT *,id as user_intranet_id FROM college_users_intranet "
                    . "WHERE college_id = '$college_id' AND user_id != '$logged_user_id' AND intranet_user_type = 'alumni'";
            $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

            // Get Details of members
            $List = array();
            $List = $this->usersearch->getAllUserDetails($SelectCollegeUsers_Result, $Item);
            echo json_encode($List);
        }
    }


}
