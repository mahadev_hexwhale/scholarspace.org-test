<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Android_login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('android_model');
    }

    function index() {
        
    }

    function user_login() {
        if (!empty($_POST) && $this->input->post('username') != '' && $this->input->post('password') != '') {
            $remember = 0;
            if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember)) {
                $user = $this->ion_auth->user()->row();
                $user_id = $user->id;
                $random_token = uniqid() . time();
                $encr_token = md5($random_token);
                
                $result = $this->android_model->insert_token("UPDATE users SET android_token='$encr_token' WHERE id='$user_id'");
                
                $college_result = $this->android_model->get_college($user_id);
                $college_name = array();
                $college_id = array();
                if($college_result){
                    foreach ($college_result as $cr){
                        array_push($college_name, $cr->college_name);
                        array_push($college_id, $cr->id);
                    }
                }
                
                $response['user_id'] = $user_id;
                $response['token'] = $random_token;
                $response['login'] = "success";
                
                $response["college_names"] = array();
                $response["college_ids"] = array();
                $response["college_names"] = $college_name;
                $response["college_ids"] = $college_id;
                
                print(json_encode($response));
            } else {
                $response['login'] = "unsuccessfull";
                print(json_encode($response));
            }
        } else {
            $response['login'] = "invalid";
            print(json_encode($response));
        }
    }

}
