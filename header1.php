<!DOCTYPE html>
<html lang="en">

<head>
	<title>Scholar Space | Home page</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

 <link type="text/css" rel="stylesheet" href="css/style.css"/>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

</head>

<body>

<div class="header">

<div class="top">

  <div class="row">
    
<div class="col-md-5">
  
<div class="logo">
  <a href="#"><img src="image/logo.png" alt=""></a>
</div>

</div>          

<div class="col-md-7">
  
<div class="top_links">
  
<ul>
  <li> <a href="#">Skill Maps</a> </li>
  <li> <a href="#">College Ranking</a> </li>
  <li> <a href="#">Admission Calendar</a> </li>
  <li id="no_border"> <a href="#">Discussion Forum</a> </li>
  <li id="log_box">
   <a href="#" data-toggle="modal" data-target="#myModal_login">Login</a> 
   <a href="register.php">Register</a> 

    </li>
</ul>


<div class="social_icons">
  <ul>
    <li> <a href="#"><span class="fb"></span></a> </li>
    <li> <a href="#"><span class="tw"></span></a> </li>
     <li> <a href="#"><span class="go"></span></a> </li>
     <li> <a href="#"><span class="li"></span></a> </li>
  </ul>

</div>



</div>

</div>



  </div>

</div>
<!-- end of top -->

<div class="middle">

<div class="main_menu">
 <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
         <!--    <a class="navbar-brand" href="#">Project name</a> -->
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">engineering</a></li>
              <li><a href="#">mba</a></li>
              <li><a href="#">medicine</a></li>
               <li><a href="#">humanities</a></li>
                 <li><a href="#">economics</a></li>
                 <li><a href="#">ias</a></li>
                 <li><a href="#">bank</a></li>
                 <li><a href="#">scholarship</a></li>
                 <li><a href="#">my college space</a></li>

    <!--           <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li> -->
            </ul>
            <ul class="nav navbar-nav navbar-right">
             <!--  <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li> -->
              <li>  
              <div class="header_search">
                 <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for Colleges,Universities " name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
            </div>
        </div>


       </form>

              </div>
              
               </li>
          <!--     <li><a href="../navbar-fixed-top/">Fixed top</a></li> -->
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

</div>


</div>

</div>
<!-- end of header -->


<div class="main_page_login">

<div class="modal fade" id="myModal_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true">X</span></button>
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-body">
   
    <div class="login_social">
     <div class="col-md-12">
      <div class="fb_login">
      <span class="fb_log"></span>
      <a href="#"> Login with Facebook </a>
      </div>
       </div>
     <div class="col-md-12">
      <div class="go_login">
      <span class="go_log"></span>
      <a href="#"> Login with Google </a>
      </div>
       </div>
        </div>

<div class="login_input">
 <div class="col-md-12"><h6> <span>Or</span> </h6></div>
   </div>


<div class="login_input">
 <div class="col-md-12"><label> Email </label></div>
  <div class="col-md-12"><input type="email" class="log_text" name="email"></div>
   </div>

<div class="login_input">
 <div class="col-md-12"><label> Password</label></div>
  <div class="col-md-12"><input type="password" class="log_text" name="password"></div>
  <div class="col-md-12"> <a href="#" id="forgot_link">Forgot Password?</a> </div>

<div class="login_input" id="second_login">
 <div class="col-md-12"><label> Enter Email </label></div>
  <div class="col-md-12"><input type="email" class="log_text" name="email"></div>
  <div class="col-md-12"><label> Reset Password</label></div>
  <div class="col-md-12"><input type="password" class="log_text" name="password"></div>
  
   </div>

   </div>

      </div>

      <div class="modal-footer">
     <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
     <div class="login_input" style="margin: 30px 0px 15px;">
       <div class="col-md-12">
         <button type="button" class="btn btn-primary">Log In</button>
          </div>
         </div>
        
      </div>
    </div>
  </div>
</div>
</div>
<!-- end of popup for login -->








