/**
 * Created by ameenot on 27/12/2015.
 */
//******************************** CHAT MENU SIDEBAR ******************************//
var chat_check_counter = 0;
var stop_scroll = 1;
var content = document.querySelector('body');
var controller_name = $('#controller_name').val();
//$('#controller_name').remove();

var lastMessageTimestamp;
function customScroll() {
    var chat_resetHeight = $('#messages_chat').height() - 10;
    if (stop_scroll == 1) {
        $('.conversation-body').animate({
            scrollTop: chat_resetHeight
        }, 1000);
    }
}

function inboxCustomScroll() {
    var chat_resetHeight = $('.inbox_details').height() + 15500;
    if (stop_scroll == 1) {
        $('.inbox_details').animate({
            scrollTop: chat_resetHeight
        }, 1000);
    }
}


//function  customInboxScroll() {
//    var div_height = $('.inbox_details').height();
//    if (stop_scroll == 1) {
//        $('.inbox_details').animate({
//            scrollTop: $('.inbox_details').get(0).schollHeight
//        }, 1000);
//    }
//
//}


function ajaxCallCheckNewMessages(timestampChat, chatwith) {
//alert(timestampChat);
//alert(chatwith);

    var sender_img = $('#receiver_profile_picture').val();
    var sender_name = $('#receiver_user_name').val();
    if (chat_check_counter == 1) {
        console.log('checking messages after' + timestampChat);
        var chat_img = "";
        var UsersChat = "";
        var inbox_users_chat = "";
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller_name + "/getLatestMessage",
            data: {
                ChatWith: chatwith,
                timeStamp: timestampChat
            },
            success: function (data) {
                console.log(data);
                var iChat = 0;
                $.each(data, function (key, value) {
                    console.log("user_id: " + value.user_id);
                    chatPos = "";
                    console.log("chatwith :" + chatwith);
                    if (value.user_id != chatwith) {
                        chatPos = "chat-right";
                    }
                    customScroll();
                    UsersChat += "<li class=\"img\">\
                                            <div class=\"chat-detail " + chatPos + "\">\
                                                <span class=\"chat-date\">" + value.timestamp + "</span>\
                                                <div class=\"conversation-img\">\
                                                    <img src=\"" + chat_img + "\" alt=\"avatar\"/>\
                                                </div>\
                                                <div class=\"chat-bubble\">\
                                                    <span>" + value.message + "</span>\
                                                </div>\
                                            </div>\
                                        </li>";
                    inbox_users_chat += '<div class="first_msg">' +
                            '<div class="intra_user_img" >' +
                            '<img src="' + sender_img + '"/>' +
                            '</div>' +
                            '<div class="intra_user_img" style="margin:0px 14px; float:left;" id="name" >' +
                            '<a href="#">' + sender_name + '</a>' +
                            '<p style="margin-top: 13px;margin-left: 25px;">' +
                            value.message +
                            '</p>' +
                            '</div>' +
                            '<h6>' + value.timestamp + '</h6>' +
                            '</div>' +
                            '</div>' +
                            '<div>';
                    lastMessageTimestamp = value.timestamp;
                });
                if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
                    lastMessageTimestamp = timestampChat;
                }
                if (UsersChat == "") {
                    console.log("nothing");
                }
                $('#messages_chat').append(UsersChat);
                $('.inbox_details').append(inbox_users_chat);
            }
        });
        setTimeout(function () {
            ajaxCallCheckNewMessages(lastMessageTimestamp, chatwith);
        }, 3000);
    }
}


//Function to check the group latest message.
function ajaxCallCheckNewGroupMessages(timestampChat, chatGroupId)
{
    var sender_img = $('#receiver_profile_picture').val();
    var sender_name = $('#receiver_user_name').val();
    if (chat_check_counter == 1) {
        console.log('checking group messages after the timestamp ' + timestampChat);
        var chat_img = "";
        var UsersChat = "";
        var inbox_users_chat = "";
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller_name + "/getLatestGroupMessage",
            data: {
                ChatWith: chatGroupId,
                timeStamp: timestampChat
            },
            success: function (data) {
                console.log(data);
                var iChat = 0;
                $.each(data, function (key, value) {
                    chatPos = "";
                    UsersChat += "<li class=\"img\">\
                                            <div class=\"chat-detail " + chatPos + "\">\
                                                <span class=\"chat-date\">" + value.timestamp + "</span>\
                                                <div class=\"conversation-img\">\
                                                 <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                                </div>\
                                                <div class=\"chat-bubble\">\
                                                    <span>" + value.message + "</span>\
                                                </div>\
                                            </div>\
                                        </li>";
                    lastMessageTimestamp = value.timestamp;
                });
                if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
                    lastMessageTimestamp = timestampChat;
                }
                if (UsersChat == "") {
                    console.log("nothing");
                }
                $('#messages_chat').append(UsersChat);
                //  $('.inbox_details').append(inbox_users_chat);
            }
        });
        setTimeout(function () {
            ajaxCallCheckNewGroupMessages(lastMessageTimestamp, chatGroupId);
        }, 3000);
    }


}



function quickviewSidebar() {

    $('.chat-back').on('click', function () {
        chat_check_counter = 0;
        $('#messages_chat').html('');
        $('.chat-conversation').removeClass('current');
        $('.chat-body').addClass('current');
    });
//for  one-to-one chat
    $('.chat-list').on('click', 'li', function () {
        quickviewHeight();
        chat_check_counter = 1;
        $('.dropdown-chat').addClass('hide');
        $('.getList').val('');
        var chat_name = $(this).find('.user-name').html();
        var chat_txt = $(this).find('.user-txt').html();
        var chat_status = $(this).find('.user-status').html();
        var chat_img = $(this).find('img').attr('src');
        var chatwith = $(this).attr('user_id');
        $('.user-name').attr("user_id", chatwith);
        var UsersChat = "";
        var chatPos = "";
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller_name + "/getChatUser",
            data: {
                ChatWith: chatwith
            },
            success: function (data) {
                console.log(data);
                if (data.length != "") {
                    var iChat = 0;
                    $.each(data, function (key, value) {
//                    console.log("user_id: " + value.user_id);
                        chatPos = "";
//                    console.log("chat with: " + chatwith);
                        if (value.user_id != chatwith) {
                            chatPos = "chat-right";
                        }
                        UsersChat += "<li class=\"img\">\
                                            <div class=\"chat-detail " + chatPos + "\">\
                                                <span class=\"chat-date\">" + value.timestamp + "</span>\
                                                <div class=\"conversation-img\">\
                                                    <img src=\"" + chat_img + "\" alt=\"avatar\"/>\
                                                </div>\
                                                <div class=\"chat-bubble\">\
                                                    <span>" + value.message + "</span>\
                                                </div>\
                                            </div>\
                                        </li>";
                        lastMessageTimestamp = value.timestamp;
                        //Code to check the chat status.
                        if (value.status == 0) {
                            UsersChat += "<button type='button' class='respons' name='accept' value='1' >accept</button>";
                            UsersChat += "<button type='button' class='respons' name='Reject' value='2' >Reject</button>";
                            UsersChat += "<button type='button' class='respons'  name='Ignore' value='3' >Ignore</button>";
                        }



                    });
                    $('#messages_chat').append(UsersChat);
                    if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
                        lastMessageTimestamp = -1;
                    }
                    customScroll();
                    setTimeout(function () {
                        ajaxCallCheckNewMessages(lastMessageTimestamp, chatwith);
                    }, 3000);
                }
                else
                {//If chat is not permitted.
                    $('#messages_chat').append("Yor Are blocked by the user");
                    $('.conversation-message input[type=text] ').hide();
                }


            }
        });
        $('.chat-conversation .user-name').addClass('chat-header');
        $('.chat-conversation .user-name .chat-header').html("");
        $('.chat-conversation .user-txt .chat-header').html("");
        $('.chat-conversation .user-name .chat-header').attr('group_id', "");
        $('.chat-conversation .user-name').html(chat_name);
        $('.chat-conversation .user-txt').html(chat_txt);
        $('.chat-conversation .user-status').html(chat_status);
        $('.chat-conversation .user-img img').attr("src", chat_img);
        $('.chat-conversation .conversation-body .conversation-img img').attr("src", chat_img);
        $('.chat-body').removeClass('current');
        $('.chat-conversation').addClass('current');
    });
//End of code to-one-to one chat.





//Code to group chat
//code for groupchat
    $('.chat-groups').on('click', 'li', function () {

        quickviewHeight();
        chat_check_counter = 1;
        $('.dropdown-chat').addClass('hide');
        $('.getList').val('');
        var group_chat_id = $(this).attr('id');
        var chat_group_name = $(this).html();
        //console.log(group_chat_id);
        //console.log(chat_group_name);
        var group_members_list = "";
        var totla_not_shown_members;
        var UsersChat = "";
        var chatPos = "";
        //Code to show the group member list in the top of chat box.
        if (group_members.length > 3)
        {
            var i;
            for (i = 0; i < 3; i++)
            {
                group_members_list += group_members[i] + ",";
            }
            group_members_list = group_members_list.substring(0, group_members_list.length - 1);
            group_members_list += "...";
            totla_not_shown_members = group_members.length - 3;
        }
        else
        {
            var i;
            for (i = 0; i < group_members.length; i++)
            {
                group_members_list += group_members[i] + ",";
            }
            group_members_list = group_members_list.substring(0, group_members_list.length - 1);
            if (group_members_list.length > 30)
            {
                group_members_list = group_members_list.substring(0, 29);
                group_members_list = group_members_list + "...";
            }
        }

        if (totla_not_shown_members)
        {
            group_members_list += totla_not_shown_members + " Others";
        }
//End

        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: site_url + "user/" + controller_name + "/getGroupChatUsers",
            data: {
                groupChatId: group_chat_id
            },
            success: function (data) {
                console.log(data);
                if (data.length != "") {
                    var iChat = 0;
                    $.each(data, function (key, value) {
//                        console.log("user_id: " + value.college_chat_groups_id);
                        chatPos = "";
//                        console.log("chat with: " + group_chat_id);
                        if (value.college_chat_groups_id != group_chat_id) {
                            chatPos = "chat-right";
                        }
                        UsersChat += "<li class=\"img\">\
                                            <div class=\"chat-detail " + chatPos + "\">\
                                                <span class=\"chat-date\">" + value.timestamp + "</span>\
                                                <div class=\"conversation-img\">\
                                                 <img src='" + base_url + "" + value.profile_pic + "' alt=''>\
                                                 </div>\
                                                <div class=\"chat-bubble\">\
                                                    <span>" + value.message + "</span>\
                                                </div>\
                                            </div>\
                                        </li>";
                        lastMessageTimestamp = value.timestamp;
                        //Code to check the chat status.
                        if (value.status == 0) {
                            UsersChat += "<button type='button' class='respons' name='accept' value='1' >accept</button>";
                            UsersChat += "<button type='button' class='respons' name='Reject' value='2' >Reject</button>";
                            UsersChat += "<button type='button' class='respons'  name='Ignore' value='3' >Ignore</button>";
                        }



                    });
                    $('#messages_chat').append(UsersChat);
                    if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
                        lastMessageTimestamp = -1;
                    }
                    customScroll();
                    setTimeout(function () {
                        ajaxCallCheckNewGroupMessages(lastMessageTimestamp, group_chat_id);
                    }, 3000);
                }
                else
                {//If chat is not permitted.
                    $('#messages_chat').append("Yor Are blocked by the user");
                    $('.conversation-message input[type=text] ').hide();
                }


            }
        });
        $('.chat-conversation .user-name').addClass('chat-header');
        $('.chat-conversation .user-name .chat-header').html("");
        $('.chat-conversation .user-txt .chat-header').html("");
        // $('.chat-conversation .user-status').html(chat_status);
        //$('.chat-conversation .user-img img').attr("src", chat_img);
        $('.chat-conversation .user-name .chat-header').attr('user_id', "");
        $('.chat-conversation .user-name').html(chat_group_name);
        $('.chat-conversation .user-txt').html("(" + group_members_list + ")");
        // $('.chat-conversation .user-status').html(chat_status);
        //$('.chat-conversation .user-img img').attr("src", chat_img);
        $('.chat-conversation .user-name').attr('group_id', group_chat_id);
        //$('.chat-conversation .conversation-body .conversation-img img').attr("src", chat_img);
        $('.chat-body').removeClass('current');
        $('.chat-conversation').addClass('current');
    });

    /* Get the list of users to chat search */
    var current_GroupMembersList = [];
    $('.getList').keypress(function (e) {
        var ListUsersChat = "";
        var SearchChat = $(this).val();
        if (SearchChat != "") {
            $.ajax({
                type: "POST",
                dataType: "JSON",
//                url: site_url + "user/" + controller_name + "/FilterCollegeMembers_Method",
                url: site_url + "user/dashboard/FilterCollegeMembers_Method",
                data: {
                    Item: SearchChat
                },
                success: function (data) {
                    $('.ListOfMembersChat').html("");
                    $.each(data, function (key, value) {
                        if (current_GroupMembersList.indexOf(value.college_users_intranet_id) == -1) {
                            ListUsersChat += "<li class=\"clearfix\" user_id='" + value.user_id + "'>\
                                        <div class='user-img'>\
                                            <img src='" + base_url + "" + value.profile_picture + "' alt=''>\
                                        </div>\
                                        <div class=\"getChatUser\">\
                                            <div class=\"user-name\">" + value.fname + " " + value.lname + "</div>\
                                            <div class=\"user-txt\">" + value.stream_name + "</div>\
                                        </div>\
                                    </li>";
                        }
                    });
                    $('.dropdown-chat').removeClass('hide').show();
                    $('.ListOfMembersChat').append(ListUsersChat);
                }
            });
        }
    });
    $('.recent_user_li').keypress(function (e) {

    });
    /* Remove current message when opening */
    $('.have-message').on('click', function () {
        $(this).removeClass('have-message');
        $(this).find('.badge-danger').fadeOut();
    });

    /* Send messages */
    $('body').on('keypress', '.send-message', function (eve) {

//    $('.send-message').keypress(function(e) {
//      
        var element = $(this);
        var lastMessageTimestamp = "";
//        var chatwith = $('#receiver_user_id').val().trim();
        var temp = 1;
        if (eve.keyCode == 13) {
            stop_scroll = 1;
            var chatinsermessage = $(this).val();
            var user_id = $('.chat-header').attr("user_id");
            var groupid = $('.chat-header').attr("group_id");
            //var name23 = $('.chat-header').html();

            if (!groupid) {
                //If group id is notdefined or empty
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    url: site_url + "user/" + controller_name + "/inserChat",
                    data: {
                        Chat_user_id: user_id,
                        chat_message: chatinsermessage
                    },
                    async: false,
                    success: function (data) {
                        console.log(data);
                        temp = data.trim();
                    }
                });
            }
            else
            {
                $.ajax({
                    type: "POST",
                    dataType: "text",
                    url: site_url + "user/" + controller_name + "/insertGroupChat",
                    data: {
                        group_id: groupid,
                        chat_message: chatinsermessage
                    },
                    async: false,
                    success: function (data) {
                        console.log(data);
                        temp = data.trim();
                    }
                });
            }


            var logged_user_image = $('.intra_user_img img').attr('src');

            if (temp.length != 0)
            {

                var chat_message = '<li class="img">' +
                        '<span>' +
                        '<div class="chat-detail chat-right">' +
                        '<img style="height: 36px; border-radius: 100%;" src="' + logged_user_image + '" data-retina-src="' + logged_user_image + '"/>' +
                        '<div class="chat-detail">' +
                        '<div class="chat-bubble">' +
                        $(this).val() +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</span>' +
                        '</li>';
                var sender_name = $('#sender_name').val();
                var sender_img = $('#sender_profile_picture').val();
                var currentdate = new Date();
                var datetime = currentdate.getFullYear() + "-"
                        + ("0" + currentdate.getMonth() + 1).slice(-2) + "-"
                        + ("0" + currentdate.getDate()).slice(-2) + "  "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
                lastMessageTimestamp = datetime;
                var chat_inbox_msg = '<div class="first_msg">' +
                        '<div class="intra_user_img" >' +
                        '<img src="' + sender_img + '"/>' +
                        '</div>' +
                        '<div class="intra_user_img" style="margin:0px 14px; float:left;" id="name" >' +
                        '<a href="#">' + sender_name + '</a>' +
                        '<p style="margin-top: 13px;margin-left: 25px;">' +
                        element.val() +
                        '</p>' +
                        '</div>' +
                        '<h6>' + datetime + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '<div>';
                $(chat_message).hide().appendTo(element.parent().parent().parent().find('.conversation-body ul')).fadeIn();
                $(chat_inbox_msg).hide().appendTo(element.parents('.courses_page_tab').find('.inbox_details')).fadeIn();
                element.val("");

//                quickviewHeight();
//                customScroll();
//                customInboxScroll();
//                 chat_check_counter = 0;
//               
//                setTimeout(function() {
//                  ajaxCallCheckNewMessages(lastMessageTimestamp , chatwith);
//                }, 3000);
            }
            else
            {
                alert("chat is not permitted");
            }
        }
    });

    content.addEventListener('click', function (ev) {
        chatSidebar = $("#quickview-sidebar");
        chatbutton = $("#quickview-toggle");
        var target = ev.target;
        if (!chatSidebar.is(ev.target) && chatSidebar.has(ev.target).length === 0) {

            if (chatbutton.is(ev.target)) {
                if ($('#quickview-sidebar').hasClass('open')) {
                    $('#quickview-sidebar').addClass('closing');
                    $('#quickview-sidebar').removeClass('open');
                    $('#quickview-sidebar').removeClass('closing');
                }
                else {
                    if ($('#quickview-sidebar').hasClass('closing')) {
                        $('#quickview-sidebar').removeClass('closing');
                    }
                    $('#quickview-sidebar').addClass('open');
                }
                $('#chat-notification').hide();
                setTimeout(function () {
                    $('.mm-panel .badge-danger').each(function () {
                        $(this).removeClass('hide').addClass('animated bounceIn');
                    });
                }, 1000);
            }
            else {
                if ($('#quickview-sidebar').hasClass('open')) {
                    $('#quickview-sidebar').addClass('closing');
                    $('#quickview-sidebar').removeClass('open')
                    $('#quickview-sidebar').removeClass('closing');
                }
            }
        }

    });
}
function quickviewHeight() {
    $('.chat-conversation').height('');
    chatConversationHeight = $('.chat-conversation').height();
    windowHeight = $(window).height();
    $('.get-height-chat').height($(window).height() - 215);
    $('.conversation-body').height($(window).height() - 160);
    if (chatConversationHeight < windowHeight) {
        $('.chat-conversation').height($(window).height() - 50);
    }
}

/****  Initiation of Main Functions  ****/
$(document).ready(function () {

    $(".conversation-body").scroll(function () {
        var pos = $('.conversation-body').scrollTop();
        if (pos < 500) {
            stop_scroll = 0;
        }
    });
    quickviewSidebar();
    quickviewHeight();
});
/****  On Resize Functions  ****/
$(window).resize(function () {
    quickviewHeight();
});
//Get the user response wheter accepted, ignore  and ignore.
$('body').on('click', '.respons', function () {
    var respons = $(this).val();
    var chatwith = $('.conversation-header .user-name').attr('user_id');
    alert(chatwith);
    $.ajax({
        type: "POST",
        url: site_url + "user/" + controller_name + "/changeChatResponse",
        data: {
            chatwith: chatwith,
            chatresponse: respons
        },
        success: function (data) {
//            console.log(data);
//            alert(data);
            if (data == "1") {
                $('.respons').remove();
            }
        }
    });
});
//make the all massege to read once user click on the message notification.
$('body').on('click', '#quickview-toggle', function () {
    // alert("sdfs");
    var is_message_read = $(this).val();
    // alert(is_message_read);
    console.log(is_message_read);
    $.ajax({
        type: "POST",
        url: site_url + "user/" + controller_name + "/changMessageStatus",
        success: function (data) {
//            console.log(data);
//            alert(data);
            if (data >= 1) {
                $('.badge-sucess').remove();
            }
        }

    });
});
//$('.respons').click(function() {
//
//    alert("hi");
//    var response = $(this).val();
//    alert(response);
//
//});
//
//
//
//Code to display the chat message after clicking the user name.
$('.inbox_details').on('click', 'li', function () {
    // $('.dropdown-chat').addClass('hide');
    // $('.getList').val('');

    var chat_name = $(this).find('.user-name').html();
    alert(chat_name);
    var chat_txt = $(this).find('.user-txt').html();
    var chat_status = $(this).find('.user-status').html();
    var chat_img = $(this).find('img').attr('src');
    var chatwith = $(this).attr('user_id');
    $('.user-name').attr("user_id", chatwith);
    var UsersChat = "";
    var chatPos = "";
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: site_url + "user/" + controller_name + "/getChatUser",
        data: {
            ChatWith: chatwith
        },
        success: function (data) {
            console.log(data);
            if (data.length != "") {
                var iChat = 0;
                $.each(data, function (key, value) {
//                    console.log("user_id: " + value.user_id);
                    chatPos = "";
//                    console.log("chat with: " + chatwith);
                    if (value.user_id != chatwith) {
                        chatPos = "chat-right";
                    }
                    UsersChat += "<li class=\"img\">\
                                            <div class=\"chat-detail " + chatPos + "\">\
                                                <span class=\"chat-date\">" + value.timestamp + "</span>\
                                                <div class=\"conversation-img\">\
                                                    <img src=\"" + chat_img + "\" alt=\"avatar\"/>\
                                                </div>\
                                                <div class=\"chat-bubble\">\
                                                    <span>" + value.message + "</span>\
                                                </div>\
                                            </div>\
                                        </li>";
                    lastMessageTimestamp = value.timestamp;
                    //Code to check the chat status.
                    if (value.status == 0) {
                        UsersChat += "<button type='button' class='respons' name='accept' value='1' >accept</button>";
                        UsersChat += "<button type='button' class='respons' name='Reject' value='2' >Reject</button>";
                        UsersChat += "<button type='button' class='respons'  name='Ignore' value='3' >Ignore</button>";
                    }



                });
                $('#messages_chat').append(UsersChat);
                if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
                    lastMessageTimestamp = -1;
                }
                customScroll();
                setTimeout(function () {
                    ajaxCallCheckNewMessages(lastMessageTimestamp, chatwith);
                }, 3000);
            }
            else
            {//If chat is not permitted.
                $('#messages_chat').append("Yor Are blocked by the user");
                $('.conversation-message input[type=text] ').hide();
            }


        }
    });
    $('.chat-conversation .user-name').html(chat_name);
    $('.chat-conversation .user-txt').html(chat_txt);
    $('.chat-conversation .user-status').html(chat_status);
    $('.chat-conversation .user-img img').attr("src", chat_img);
    $('.chat-conversation .conversation-body .conversation-img img').attr("src", chat_img);
    $('.chat-body').removeClass('current');
    $('.chat-conversation').addClass('current');
});
//Function inbox chat
function inboxChat() {
    chat_check_counter = 1;
    var chat_name = $('#receiver_user_name').val();
    var chat_img = $('#receiver_profile_picture').val();
    var chatwith = $('#receiver_user_id').val();
    var UsersChat = "";
    var user_chat = "";
    var chatPos = "";
    var lastMessageTimestamp = $('.first_msg:last-child h6').text();
    // alert(lastMessageTimestamp);

    if (typeof lastMessageTimestamp == 'undefined' || lastMessageTimestamp == "") {
        lastMessageTimestamp = -1;
    }

    inboxCustomScroll();

    setTimeout(function() {

    customScroll();
  
        ajaxCallCheckNewMessages(lastMessageTimestamp, chatwith);
    }, 3000);
}

