$("#login_form_pop").submit(function (e) {
    e.preventDefault();
    var identity = $("#identity").val();
    var password = $("#password").val();

    var check = 0;
    if (identity == '') {
        $(".username_error_alert").text("(Enter username)");
        check++;
    }
    if (password == '') {
        $(".password_error_alert").text("(Enter password)");
        check++;
    }
    if (check == 0) {
        $.ajax({
            type: "post",
            url: site_url + "login/user_login",
            data: {
                identity: identity,
                password: password
            },
            success: function (data) {
                if (data.trim() != '0') {
                    switch (data) {
                        case 'super-admin':
                            window.location.href = site_url + "super-admin/home/";
                            break;
                        case 'data-entry':
                            window.location.href = site_url + "data-entry/home/";
                            break;
                        case 'others':
                            $('#myModal_login').modal('hide');
                            location.reload();
                            break;
                        default:
                            $(".incorrect_error_msg").html(data);
                            $(".incorrect_error").css("display", "block");
                    }
                }
            }
        });
    }
});

/* remove the required error msg */
$("input, textarea, select").focus(function () {
    $(".error_alert").text("");
    $(".incorrect_error_msg").html("");
    $(".incorrect_error").css("display", "none");
});

$(".logout_user").click(function () {
    $.ajax({
        type: "post",
        url: site_url + "login/user_logout",
        data: {
            flag: "1"
        },
        success: function (data) {
            if (data == "1") {
                window.location.href = site_url;
            }
        }
    });
});